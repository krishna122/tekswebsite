<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Apps made for Bluetooth by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;margin-bottom: 0px;">Bluetooth Apps</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">It was a smart move by the Teks Team to create such apps which can be connected via Bluetooth (a wireless technology) with other bluetooth enabled gadgets.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
<section id="appstory" class="dataworks">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="color: #fff; font-size: 25px; text-align: center;">DataWorks</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Bluetooth Connectivity App</span>
				  	<br><br>	
				  	<p style="padding: 0px;">DataWorks taps into the power of Bluetooth technology to the fullest. With this powerful app, devices can be connected with other compatible gadgets, and data can be measured, calibrated, maintained and managed.</p>
				  <br><br>
				  <a href="dataworks.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dataworkslogo.jpg" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</div> 
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>