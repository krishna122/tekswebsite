<?php //include_once 'mysqlconnect.php'; ?>
<?php // $BASE_URL="http://staging.teks.co.in/tekscoin/"; $query="SELECT * FROM  metapages where pagename = 'index'"; $sqlObj=new MySqlconnect(); $sqlObj->fetch($query);
// if(mysql_num_rows($sqlObj->mysql_result) > 0)
// {
// $result = mysql_fetch_array($sqlObj->mysql_result);
// }
?>
<!DOCTYPE html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1">
<title><?php echo $result['metatitle']; ?></title>
<meta name=description content='<?php echo $result['metadescription']; ?>' />
<meta name=keywords content='<?php echo $result['metakey']; ?>' />
<?php include 'head.php';?>
<style>.letterpress{text-shadow:0 1px 1px #4d4d4d;color:#fff;font-size:28px;padding:10px;</style>
<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel=stylesheet type=text/css>
</head>
<body id=page-top data-spy=scroll data-target=.navbar-fixed-top ondragstart="return false" onselectstart="return false">
<?php include 'headerindex.php';?>
<header class="intro fullscreen-bg">
<video loop muted autoplay poster=img/teks-video-intro-bg.jpg class=fullscreen-bg__video>
<source src=video/teks-intro.mp4 type=video/mp4></source>
<source src=video/teks-intro.ogv type=video/ogv></source>
<source src=video/teks-intro.webm type=video/webm></source>
</video>
<div class=intro-body style=display:initial;padding-left:0;padding-right:0>
<div class=container style="padding-top:8%;">
<div class=row>
<div class=col-md-12 style="vertical-align: middle;">
<a href=./><img style=outline:0 src=img/logo.png alt="Teksmobile India"></a><br>
<h1 class=pageheader style=font-weight:700;text-align:center;>WE CREATE<br> AWARD WINNING MOBILE & WEB APPS</h1>
<p class=letterpress >We help to convert ideas into building next generation innovative products with close knit collaboration, cutting edge technology and a friendly smile over hot coffee!</p>
<a href=#cbp-so-scroller class="page-scroll col-md-12" style=text-align:center;margin-top:5px;outline:0>
<img src=img/scrollbutton.png>
</a>
</div>
</div>
</div>
</header>
<?php include "favourite.php";?>
<?php include "map.php";?>
<?php include "footer.php";?>
<?php include 'script.php';?>
</body>
</html>