<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of I Can Be Anything</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf" style="height: 50%;">
        <div class="icbf-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">
							I Can Be Anything</span><br><span style="color:#fff; font-weight: 200;  text-transform: uppercase;">(Suite Of 12 Apps)
						</span></h1>
                        <a href="https://itunes.apple.com/in/app/i-can-be-free-relax-remove/id327538172" target="_blank"><img alt="I can be anything" src="img/appstore.png"></a>
                        <a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img alt="I can be anything" src="img/play-store.png"></a>                    		
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <blockquote class="color text-center">
	            Don’t take so much of stress...it is affecting your health!
	         </blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	I have been studying human behavior for over 20 years now. If you asked me to identify the single largest cause for most mental disquiets, I would mention the daily stresses and past bad experiences. My app is all about bringing down the regular tension levels of users, by, well, as much as possible.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          How many times have we heard this from doctors? All of us are aware that remaining stress-free goes a long way in ensuring mental peace – but even so, our daily workplace tensions, family troubles, distractions, and other minor quabbles tend to bog us down. Wouldn’t it have been just great if there was a nice mobile app to get rid of such unwanted stresses? Well, Jacob Strachotta and his I Can Be Anything team thought so…and this application took practical shape.
		         </p>
		  	 </div>
		  	 
		  	 <div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/icbf.jpg" style="border: 5px solid #CA9862;"> 
				   <source src="video/appstories/icbf.mp4" type="video/mp4"> 
				</video>
		  	 </div>   
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/ic1.png" alt="i can be anything"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Collaboration Of Team I Can Be Anything and Teknowledge Software</h3>
		  	    
		         <p class="color">
		         	Herein lies an interesting tale. Jacob and his partner Ditte wanted to make sure that the app was not just another ‘gimmicky mood-reliever’. They had years of in-depth insight into the inner psyche of human beings, and wanted to make the most of it – to create an app that would actually help people feel uplifted. They took out time to search the web for a suitable mobile app development company, and finally zeroed in on our company, Teknowledge.
		         </p>
                 <p class="color">
                 	The Strachotta-s got in touch with our mobile app company in the third quarter of 2014, and we provided them with a detailed free app quote within 19 working hours. The project started after 4 more days.
                 </p>
                 
		   </div>
		   
		  </div>
		  
		  <div class="col-lg-12">
		  	  <blockquote class="color">
			  	  While working with Cimber/Sterling Airlines and Danish Government Administration (prior to starting on ICBA in 2009), I had become more or less an expert on tackling various forms of HR issues. Jacob here is, of course, a globally lauded authority on human behavior and related stuff. Together, we were determined to make a stress-reliever app that actually worked. Teknowledge Software seemed proficient enough to create the application – and boy, were they interested to come on board!
			  </blockquote>	
		  </div>
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Choice Of Mobile Platforms</h3>
		  	    <br>
		  	    <blockquote class="color">
		  	    	Both Jacob and Ditte were, initially, interested in making I Can Be Anything an iOS app only. Soon though, they took the smart decision of developing an Android version of their cool new app as well. I suppose it’s a good thing that my company had separate teams for Android and iPhone app development!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          The focus of Jacob and Ditte was to make their ground-breaking mood-elevation and relaxation app available to as many people as possible. That’s precisely why they opted for a cross-platform app development solution. The iOS and the Android platforms were, of course the first choices (they had briefly toyed with the idea of developing a Blackberry version as well, only to abandon it later).
		          <br>
		          A non-competing agreement was also signed on behalf of Teks, before the work proceeded.
		         </p>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/ic2.png" alt="i can be anything">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">12 Apps In A Single App Suite</h3>
		  		<br>
		         <blockquote class="color">
		  	    	Me and my team had previously worked on apps with built-in internal software, but I Can Be Anything was different from them all. In it, there were 12 apps – each devoted to relieve a particular form of tension or pressure. Dumping all the solutions on a single interface would not have worked out well, and although making apps within an app was a challenge, it was fun working on it.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Although the theoretical concept of I Can Be Anything was remarkably sound, its implementation was not as easy as initially deemed by Jacob and Ditte. They realized soon enough that the app had to have multiple applications (let’s call them sub-apps) within it. Each app within the main application would be focused on tackling with one particular aspect of daily stresses. After several consultations with representatives from our app-making firm, it was decided that there would be a total of 12 apps inside I Can Be Anything.
		         </p>
		         
		         <h3 class="color">In its final form, I Can Be Anything has the following apps:</h3>
		         
		         <ul class="color">
		         	<li>I Can Be Healthy</li>
		         	<li>I Can Be Free</li>
		         	<li>I Can Be Clean</li>
		         	<li>I Can Be A Sound Sleeper</li>
		         	<li>I Can Be Confident</li>
		         	<li>I Can Be Lucky In Love</li>
		         	<li>I Can Be In Control Of My Pregnancy</li>
		         	<li>I Can Be Great In Bed</li>
		         	<li>I Can Be A Happy Teenager</li>
		         	<li>I Can Be Motivated</li>
		         	<li>I Can Be A Model Student</li>
		         	<li>I Can Be Fearless</li>
		         </ul>
		         
		         <blockquote class="color">
		  	    	A lot of research went into our selection of the apps that would be present in the I Can Be Anything mobile project. There were some informed guesses and a fair amount of surveys done too – to zero in upon 12 most common uncertainties in the minds of people. Each of them had to be addressed separately.
		  	    </blockquote>
                 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">How Does The App Work?</h3>
		  	    
		  	    
		         <p class="color">
		         	The I Can Be Anything catalog of apps was conceptualized by Jacob and Ditte, and created by us, as a voice-based application. Audio lessons and soothing, calming sounds were included in each of the apps – which had the effect of driving away negative thoughts from the human mind. Apart from the nice relaxation music, users can also listen to the lessons in a male voice, with complete silence in the background. In all, there are as many as 172 sessions in the learning catalog of I Can Be Anything.
		         </p>
                 
                 <blockquote class="color">
		  	    	The thing that distressed me the most during my research was the sheer volume of people around the world so dependant on anti-depressant pills. Me, Ditte and the other members of the I Can Be Anything team decided to try out the effects of sound for the same purpose – but minus the adverse side-effects of strong medications. And that was where Magnus Bergentz came on board.
		  	    </blockquote>
                 
                 <p class="color">
                 	Magnus was given the responsibility of recording the lesson narrations (done by Jacob himself), and modify the sounds appropriately. Being an audio engineer of rich experience and considerable reputation, Magnus finds this job fun, challenging, and in a way, very, very, fulfilling.
                 </p>
                 
                 <blockquote class="color">
		  	    	When my team of app developers took up the project, I was mostly worried about one thing – the quality of the relaxation sounds that were to be included in the app. The sheer excellence of Magnus Bergentz amazed me. He made the actual task of creating this app that much easier – and once again underlined the importance of expert professionals behind any successful software.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/i can be anythingscreen1.png" alt="i can be anything"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/i can be anythingscreen2.png" alt="i can be anything"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/i can be anythingscreen3.png" alt="i can be anything"></center></div>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/i can be anythingscreen4.png" alt="i can be anything"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/i can be anythingscreen6.png" alt="i can be anything"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/i can be anythingscreen7.png" alt="i can be anything"></div>
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">In-App Purchases</h3>
					<p class="color">
						At first, including in-app purchase options did not seem really necessary for the I Can Be Anything app project. However, general user feedback and specified requirements indicated that Jacob and Ditte’s application could become all the more effective with some custom sound files – available for purchase directly from the app. Accordingly, common stuff like ‘What People Are Thinking”, “Quit Smoking”, “Afraid Of Ghosts”, “Insomnia Cure” and a few more files were included as in-app purchase options. The prices were tagged at very competitive levels.
					</p>
					<blockquote class="color">
						After listening to the relaxation sounds to tackle any specific form of worrying, a person might wish to get more up, close and personal with that issue. Via in-app purchases, we have made this a possibility. There is a host of things that people can buy from the app, depending on their interests.					
					</blockquote>		
				</div>
		  </div>
		</div>
	    
	    <div class="row">
		  <div class="col-lg-6"><br>
				<h3 class="color">Adulations For I Can Be Anything</h3><br>
				<blockquote class="color">
					At Teknowledge, we expect each of our apps to be successful. Even so, the extent of popularity that I Can Be Anything has managed to generate among users in the United States, Europe and Australia has been pleasantly surprising. Me and my developers are proud to be a part of the ICBF team.					
				</blockquote>
				<p class="color">
					Thanks to the personalized nature of the app, its intuitive designs and layouts, HQ sound effects, and other top-notch features – the I Can Be Anything catalog of apps has been a winner all the way. Its combined rating of 4.5 (out of 5) speaks volumes about how well it has been received by people all over.
				</p>
				<blockquote>
					I Can Be Free was selected by healthline.com as one of the best anxiety apps consecutively for 2013 and 2014. We knew that we were on to something good, but had we anticipated this phenomenal success? Not entirely, to be honest.
				</blockquote>
		  </div>
		  <div class="col-lg-6"><br>
		  	 	<img src="appstories/ic8.png" alt="i can be anything">
		  </div>
	   </div>
	   
	   <div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
						I Can Be Anything has also got the nod of the medical community, with the ‘I Can Be Fearless’ app having been recommended on HealthTap by doctors in the US. Online app review portals like Apppicker and Appcrawlr have given glowing feedback of this suite of mental stress-relieving apps too.
					</p>
					<p class="color">
						It is an interesting, and immensely liked, take on stress-alleviation in a soothing, healthy manner. The Teks team is more than happy to have been able to assist Jacob and Ditte in their vision to create a tension-free world of positive thoughts.
					</p>			
				</div>
		  </div>
		</div>
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>