<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Process on how we work</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="hww">
        <div class="hww-body">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h3 style="color:#fff; font-weight: 900;text-transform: uppercase;">From Idea Generation & Implementation To App Submission</h3>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><h4 style="color:#fff; font-weight: 700; ">We Help You At Every Stage</h4></center>
                    </div>
                </div>
       <br/>         
                <div class="row" style="margin-bottom: 0px;">
                    <div class="col-md-12">
                        <a href="#howwework" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
                
            </div>
        
        </div>

    </header>
	
<section id="howwework">
	<div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-heading">The Teks Way Of Working</h3>
              <br><br><br>  </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline" >
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px;padding: 20px;" ><span class="hit-the-floor">1. </span>Get In Touch</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Submit your request for a free app quote for app and website creation/ designing/ maintenance. We will get back to you within 24 working hours. At Teknowledge, we respect the confidentiality and proprietary rights of each project. Your idea is always safe with us!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px;padding: 20px;" ><span class="hit-the-floor">2. </span>Get The Details Sorted Out</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Representatives from our mobile app development team will contact you over telephone/email/Skype, to discuss the details of your service request. A custom 'Requirements Plan' will be made, and the work will proceed accordingly.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px;padding: 20px;" ><span class="hit-the-floor">3. </span>Wireframing</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Clients who have wireframes (low or high-fidelity) of their apps or websites can share them with us. Otherwise, we will sketch out the layouts and interfaces and share them with you for approval. The process would be repeated as we will move on to the mockup and prototyping stage.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px;padding: 20px;" ><span class="hit-the-floor">4. </span>UI/UX Designs</h3>
                                </div> 
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">We have a 50-strong team of international standard mobile app designers, graphic artists, animators and UI/UX experts. The layout plans, color schemes, navigation system, and other related features of every mobile application and responsive website are carefully planned out and implemented by them. Based on your feedback and suggestions, multiple modifications can be made to the UI plans.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/5.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px;padding: 20px;" ><span class="hit-the-floor">5. </span>Agile Development</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Qualified iOS/Android/PHP developers with years of experience will be in charge of your project. We follow a cutting-edge agile development strategy, to provide you with timely, pocket-friendly, and most importantly, more than satisfactory products. When we promise you a deadline...rest assured that your work will be completed before that!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/6.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px; padding: 20px;" ><span class="hit-the-floor">6. </span>At Teks, Quality Matters</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Uniformly high quality-standards are maintained at every stage of website/app development processes. Our developers perform real-time testing, while working on any project. We also have a separate team of mobile app testers, who check apps on simulators, actual devices, and in the cloud. We don't just make applications...we make GOOD APPLICATIONS!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/7.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading" >
                                
                                    <h3 style="font-weight: 100;font-size:30px;margin-bottom: 20px; padding: 20px;" ><span class="hit-the-floor">7. </span>Submission, App Launch & Post-launch Promotions</h3>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="font-weight: 100;">Our work does not end with completing a project. We handle the responsibility of submitting applications at the App Store/Play Store as well. Nearly all the apps in our portfolio have been approved within a matter of days, and at the first attempt. Once your app is displayed at the store, we draw up plans for promoting it through various offline/online channels.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                        <a href="startproject.php">
                            <div class="timeline-image">
                                <h4>GET  
                                    <br>IN
                                    <br>TOUCH
                                </h4>
                            </div>
                           </a>
                        </li>
                    </ul>                    
                </div>
            </div>
        </div>
</section>
<section>
<div class="container">
	<h3>Frequently Asked Questions</h3><br><br>
	
<div class="panel panel-success">
    <div class="panel-heading" style="background: #080808; color: #fff;">1. What about your intellectual property rights? Can you trust us with your ideas & concepts?</div>
    <div class="panel-body">Totally. We will be more than happy to sign non-disclosure and/or non-competing agreements (if you so wish). At Teknowledge, we take the issue of confidentiality and intellectual property rights very seriously. It is always YOUR idea and YOUR project that we work on. It's not for nothing that we have garnered the reputation of being among the most trusted mobile app companies across the globe.</div>
</div>

<div class="panel panel-success">
    <div class="panel-heading" style="background: #080808; color: #fff;">2. What are the app development costs?</div>
    <div class="panel-body">You can take your pick from the multiple budget options that we have on offer. At the time of providing you the free app quote, we will specify the budget estimate for your project. Our lean software development techniques ensure that cost figures never spiral out of control. You can check out the services that are associated with each package, before making your choice. Two things are for sure - our fees are very competitive, and we provide full value for money. Every time.</div>
</div>

<div class="panel panel-success">
    <div class="panel-heading" style="background: #080808; color: #fff;">3. What Makes Teknowledge Stand Out?</div>
    <div class="panel-body">More than 900 successful apps (created for different platforms), over 85% success rate, expertise in responsive website creation  & designing, offices in three countries (India, Australia and Sweden), unwavering user-orientation - there are a lot of things that make our company stand out from the competition. Let's just say, when you choose Teknowledge...you get MORE!</div>
</div>

<div class="panel panel-success">
    <div class="panel-heading" style="background: #080808; color: #fff;">4. Which Projects Do You Outsource?</div>
    <div class="panel-body">None. Irrespective of the scale and type of app development/website development projects, each and every one of them is handled by our own in-house developers, designers and testers. We stay in charge and remain responsible for every aspect of the tasks we take up. You hire our services - and it's only fair that we do the work for you.</div>
</div>

<div class="panel panel-success">
    <div class="panel-heading" style="background: #080808; color: #fff;">5. So, how can you delegate your project to us?</div>
    <div class="panel-body">That's simple enough. Visit the <a href="startproject.php" style="color: #ffa867;">'Start Your project'</a> page, fill up a short online form and submit it. Within 24 hours, one of our experts will get back to you - with a detailed quote for your website or iOS/Android app. Once you have gone through it carefully and agreed to our terms of service, work on your project will start immediately.</div>
</div>
</div>
</section>

<section style="background: #080808;">
<div class="container-fluid">
<div class="row">
<p align="center">Whatever the type of website or app you wish to create, we have just the right personnel and process in place for you. Contact us, let us work on your idea, and give your project the 'Teks' edge!</p>
</div>
</div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#work').addClass('active');
	});

</script>
</body>
</html>