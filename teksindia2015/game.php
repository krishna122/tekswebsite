<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Game Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">Game Apps</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Looking for the best Gaming apps to pass your time? The Teks Team has rounded up a unique range of gaming apps for kids to grandP’s with all mind boggling and engaging features which will give you an adrenaline rush.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
 
<section id="appstory" class="moovers">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Moovers</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Word Game</span>
				  	<br><br>
				  	<p style="padding: 0px;">Moover is a two-player iOS word-search application. Users have to swipe along word mazes to find certain words, before their opponents manage to do so. Additional coins are available through in-app purchases.</p>
				  <br><br>

				  <a href="moovers.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/m.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="appbattles">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">App Battles</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">The real-time multiplayer iOS fighting game that will send your pulses racing. Take your pick from the multiple available avatars, learn how to use the weapons and special powers available, and let’s get down to a fight to the finish!</p>
				  <br><br>

				  <a href="appbattles.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/appbattles.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="princeofasia">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Prince Of Asia</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">How about donning the role of a prince, and rescuing a charming princess? Get set for Prince Of Asia - the strategy-based, multi-level role-playing game that merges fantasy fun with thrilling adventure. An app our game development agency is very proud of!</p>
				  <br><br>

				  <a href="princeofasia.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/princeofasia.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="rst">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Ready Set Text</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Typing Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">Ready Set Text is a fun multiplayer mobile typing game for the iOS and Android platforms. Players earn coins by winning typing games, and can then use the coins to win attractive prizes. Both ‘Crowd Games’ and ‘Two-person games’ can be played in the Ready Set Text app.</p>
				  <br><br>

				  <a href="readysettext.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/rst.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>
