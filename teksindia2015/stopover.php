<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Stopover</title>

	<?php include 'head.php';?>
<link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="stopover" style="height: 34%;">
        <div class="stopover-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                        <br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Stopover</span></h1>
                        <a href="https://play.google.com/store/apps/details?id=com.stopover" target="_blank"><img alt="Stopover" src="img/play-store.png"></a>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            What do you do if your flight is delayed for 14 hours and you are stuck at the airport? Watch the television, listen to the PA system, maybe grab some snacks, read a novel, right? Well, if Amber Blumanis, an up-and-coming entrepreneur from Australia, had done any of these to while away the time during a frustratingly long wait at a Vietnam airport - Stopover would not have seen the light of day. Thankfully, she didn’t...and travelers across the world got a mobile application that doubles up as an informative virtual travel assistant.
	         </p>
	         
	          <blockquote class="color blockstopover">
		  	    	Being a frequent traveller has its own share of troubles - unforeseen flight delays being a major one among them. I also realized that many, many other people regularly faced the type of problem that I was facing at the Vietnamese airport. That was the point when the idea of making a travelers’ app started taking root.
		  	 </blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	<h3 class="color">Finding The Right Mobile Applications Developer in Australia</h3>
		  	
		  	<div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/stopover.jpg" style="border: 5px solid #F3A423;">
				   <source src="video/appstories/stopover.mp4" type="video/mp4"> 
				</video>
		  	 </div>  
		  	 
		  	 <div class="col-lg-6">
		  		
		  	    <p class="color">
		          A Bachelor of Advertising and Marketing, Media and Communication, and Media and Cultural Studies (2009-2012), Amber had brief stints as an advertising and marketing professional at organizations like Let’s Launch and Labor Options. However, while conceptualizing the idea for the mobile travel assistant came easy to her, Amber was not a coder. Neither was she willing to learn coding...and then start making her app. Instead, she started searching for companies that provided iOS and Android app development services on the internet, and soon enough, we were in touch with her.
		         </p>
		         
		         <blockquote class="color blockstopover">
		  	    	I had no pretensions about knowing about programming techniques. All that I wanted is to make the best possible mobile app for travelers out there. A few hours of research, and I chanced upon the website of Teknowledge Software. I had a chat with Hussain, their CEO, and we took it from there.
		  	    </blockquote>

		  	 </div>
		  	 
		  	 
		  	  
		  	 
		  </div>
		  
		  <div class="col-lg-12">
			  <p class="color">
			     We sent the online free quote for the Stopover app within 3 hours of her contacting our app company. The project itself took only 19 days to be completed.
			   </p>
			         
			  <blockquote class="color blockstopover">
			  	 Being an avid traveler myself, I could instantly relate to the nature of problems that Amber was planning to solve through her app. Of course it was an interesting project, and I was confident that my iPhone app developers had the necessary acumen to handle it competently. However, the mutual like for traveling served as a personal motivation.
			  </blockquote>
		  </div>
	</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">What Is The Stopover App All About?</h3>
		  	    <p></p>
		         <blockquote class="color blockstopover">
		  	    	Instead of yawning repeatedly and casting glances at the watch time after time, people waiting at any airport should be able to use the time to better use. I had conceptualized Stopover as a mobile tool for people whose airport waiting times coincide, and they could meet up.
		  	    </blockquote>
		  	    
		  	    <p class="color">
                 	Making new acquaintances and spending time with them between flights is the main thing Stopover helps users with. The airport meetings, business-oriented or just fun, drives away the boredom of being in the midst of a (theoretically!) endless wait. That’s the biggest ‘tick’ in favor of the app. 
                 </p>
                 
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/stopover1.png" alt="stopover" width="50%"></center>
		  	</div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/stopover2.png" alt="stopover" style="width:70%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7"><br>
		  		<h3 class="color">How Does Stopover Connect People?</h3>
		  	    
		  	    <p class="color">
		          With the help of Amber’s inputs, and after several rounds of brainstorming, we decided to let travelers connect with each other on the basis of a wide range of things that they might mutually adore - from the same brand of coffee or soda, to love for the same travel destinations. Amber is perpetually eager to learn new languages (she holds a Certificate A in French) - and this trait is also reflected in Stopover, which allows users to meet up with the locals at any place and learn the language of that place. For business travelers, the app comes across as handy to meet peers from other countries and expand their networks.
		         </p>
		         
		  	    <blockquote class="color blockstopover">
		  	    	When we were at the early stages of this app development project, I asked Amber how she was planning to connect people via the app. Her inputs were amazingly simple and effective - make people meet on the basis of their mutual interests. It’s this simplicity that has made Stopover such a raging success at present.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Stopover was initially created for the iOS platform only (iPhone, iPad, iPod Touch). Buoyed by its success (more on that later), an Android version of this mobile travelers’ app was developed and released in October 2014. It has also been very well-received.
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">For The Romantically Inclined Too</h3>
		  		<br>
		         <blockquote class="color blockstopover">
		  	    	Only one thing can be more exciting than a blind date - an airport blind date! On a whim, I had mentioned that Airport Dating should be one of the features of my app. Never did I realize that it would, in fact, turn out to be probably the biggest USP of Stopover!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          The one thing that Amber did worry a bit about was the security factor. After all, meeting strangers at an unknown location can turn out to be dangerous. We made sure that all appointments set up via the Stopover application were held at the airport itself. What’s more, only registered, verified users of the app could connect with other people in the airport at the same time.
		         </p>
		         
		         <blockquote class="color blockstopover">
		  	    	My team of developers gave the feel of a safe social networking environment to Amber’s unique mobile travel app. People could use it to have a gala time...even go on a quick little date, instead of getting painfully bored at the airport. And they could do all this without ever compromising their personal security.
		  	    </blockquote>  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">The Challenge Ahead</h3>
		  	    
		  	    
		         <p class="color">
		         	Glowing reviews and awards aside, Stopover is still on a growth curve. To become as successful as Amber had envisioned it to be, the user-base of the app has to increase steadily. Only when there is a significantly large worldwide ‘Stopover community’, can it of real help to travelers.
		         </p>
                 
                 <blockquote class="color blockstopover">
		  	    	I cannot overstate my happiness at the positive reviews pouring in for Stopover. However, Hussain and I are both fully aware that it still has a long way to go - before becoming a truly popular airport app. For the moment, it’s all about spreading the word about it...and I guess the launch of the Android version would also enhance the app’s reach.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/stop1.png" alt="stopover"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/stop2.png" alt="stopover"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/stop3.png" alt="stopover"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/stop4.png" alt="stopover"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/stop5.png" alt="stopover"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/stop6.png" alt="stopover"></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
			<h5 class="color">Stopover: Amber’s One-Way Ticket To Becoming A Globally Acknowledged Mobile Entrepreneur</h5>
			  <br>
				<div class="col-lg-6">
					<blockquote class="color blockstopover">
		  	    		During the development and testing phases for Stopover, I had a hunch that this might turn out to be one of the biggest entries in my company’s portfolio. Amber’s innovative way of thinking, the novelty and simplicity of the concept, the intuitive UI and designs we made - everything blended so well...it was like parts of a puzzle fitting together with a ‘click’.
		  	        </blockquote>
		  	        
					<p class="color">
						The Stopover application bagged the prestigious ‘Rising Star’ award at Talent Unleashed 2014, fending off competition from more than 600 other entries. At the event, Blumanis’ app received rave reviews from leading international entrepreneurs. It was just reward for the young Aussie’s vision, and her determination to pull it through.
					</p>
					
					<blockquote class="color blockstopover">
		  	    		I am no techie...but the adulations from software experts, including stalwarts like Mr. Branson and Mr. Wozniak, really made me happy. As the winner in the ‘Rising Star’ category, I was invited for week-long workshops at the Branson Center of Entrepreneurship, South Africa. For someone as much in love with traveling as myself, this was simply the icing on the cake!
		  	        </blockquote>
					
		  	  </div>
		  	  <div class="col-lg-6">
		  	  		<center><img src="appstories/stopover3.png" alt="stopover" style="width:70%;"></center>
		  	  </div>		
		  </div>
		  
		  <div class="col-lg-12">
		  		<blockquote class="color blockstopover">
		  	    	<strong>I could actually use this app, and I love the drive that motivated this app.</strong>
		  	    	<br>-- Steve Wozniak (Apple co-founder), at the Talent Unleashed event.
		  	  </blockquote>
		  </div>
		</div>

	
     </div>
	
</section>


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>