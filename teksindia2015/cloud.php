<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Apps made for Cloud by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;margin-bottom: 0px;">Cloud Apps</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Cloud helps you store, manage and process data using a network of remote servers hosted on the Internet. This technology allows for much more efficient computing by centralizing data storage, processing and bandwidth.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
<section id="appstory" class="">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
			 <h1>UPDATING VERY SOON</h1>
		  </div>
		</div>
	</div> 
</div> 
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>