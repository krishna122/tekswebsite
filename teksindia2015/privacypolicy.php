<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Privacy Policy</title>

	<?php include 'head.php';?>

</head>


<body data-spy="scroll" >

<?php include 'header.php';?>
	

<!-- Intro Header -->
    <header class="privacy" style="height: 34%;">
        <div class="privacy-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Privacy-Policy</span></h1>
                       </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
    </header>	
<section>
	<div class="project-container">
	  <div class="row">
           <p>At Teknowledge, we take all due precautions against probable unauthorized access of personal, confidential information. The data you provide remains stored in our secure database, and can be accessed only by the authorized personnel of our mobile app development company. It’s our responsibility to keep your information safe!</p>

     <h5>The Information You Need To Give At The Time Of Requesting Free App Quotes</h5>

       <ul style="margin-left:20px;">  
         <li>Your Full Name</li>
         <li>Your Email id (if you have a business email, kindly use that)</li>
         <li>A brief outline of your app idea.</li>
      </ul>
<p>In case you have a low/high-fidelity wireframe of the app ready, you can share it with us.</p> 

<p>Kindly note that the data is NEVER distributed to third-party agents for any form of commercial purpose. The team of app developers working on your project has access to the information. Check out our overall <a href="terms.php">Terms & Conditions</a> here as well.</p>


<h5>Protection Of Individual App Ideas/Concepts</h5>

<p>We have a strong stand in relation with the intellectual property rights of mobile apps. If it is so required, non-disclosure/non-competing agreements are signed by our representatives. Neither is any billing/payment-related information disclosed to unauthorized third-parties.</p> 

<h5>Unique, Original Apps</h5>

<p>The in-house app development procedures at our company are in agreement with the latest legal norms and regulations related to international mobile technology. We make sure that each of our applications is unique, original, customized, and deliver value to clients in particular, and final users in general. Any form of plagiarism done by other entities is strictly punishable.</p>


<h5>E-Newsletters & Regular App Notifications</h5>

<p>By providing your email address, you automatically subscribe to our weekly app newsletter. You, of course, have the option to ‘unsubscribe’ from the same. Keep an eye out on our official <a href="teks.co.in/blog">blog</a> section, for the latest highlights, talking points, and all things new from the domain of mobile technology.</p>

<p>Following a uniform, all-encompassing, client-oriented code of ethics has been a key reason behind our sustained success over the years. We make great apps...and ensure that you do not have to worry about anything!</p>
     

    </div>
 </div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>