<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Us</title>
    <meta name='description' content='' />
    <meta name='keywords' content='' />

	<?php include 'head.php';?>

	<style type="text/css">

		h4{
			font-size: 18px;
		}
	</style>
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" ondragstart="return false" onselectstart="return false">

	<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="about">
        <div class="about-body">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">OUR STORY</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Teknowledge was there when there was no iPhone. Or an Android device. It all started when our CEO, Mr. Hussain Fakhruddin (with a motley team), started out making small Java applications. That was back in 2006, and over the last decade, we have grown into one of the best mobile app companies in the world. At least, our clients say so!</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#services" class="page-scroll" style="text-align: center;outline:0;">
                        <img src="img/scrollbutton.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                    <div class="col-md-12">

                    </div>
                    <div class="col-md-12">

                    </div>
        </div>

    </header>


<!-- Services Section -->
     <section id="services">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 text-center">
                     <h2 class="section-heading">Services</h2>
                     <h4 class="section-subheading text-muted">Our Key Areas Of Expertise</h4>
                 </div>
             </div>
             <div class="row text-center">
                 <div class="col-md-3">
                     <span class="fa-stack fa-4x">
                         <i class="fa fa-circle fa-stack-2x text-primary"></i>
                         <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                     </span>
                     <h4 class="service-heading">Mobile App Development</h4>
                     <p class="text-muted" style="padding:0px;">We are into making custom mobile applications for the iOS, Android, Blackberry and Windows Phone platforms. Agile development methods are followed at our company.</p>
                 </div>
                 <div class="col-md-3">
                     <span class="fa-stack fa-4x">
                         <i class="fa fa-circle fa-stack-2x text-primary"></i>
                         <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                     </span>
                     <h4 class="service-heading">Website Designing & Development</h4>
                     <p class="text-muted" style="padding:0px;">From domain registration and website designing, to handling web development and projects - we give you end-to-end service solutions. Mobile usability of websites is emphasized on.</p>
                 </div>
                 <div class="col-md-3">
                     <span class="fa-stack fa-4x">
                         <i class="fa fa-circle fa-stack-2x text-primary"></i>
                         <i class="fa fa-cog fa-stack-1x fa-inverse"></i>
                     </span>
                     <h4 class="service-heading">Api Development</h4>
                     <p class="text-muted" style="padding:0px;">At Teks, we specialize in creating custom, scalable and high-performance RESTful API development. From API designing and development, To API Testing, Monetization and Consultancy, we offer 360-degree API support for you.</p>
                 </div>
                 <div class="col-md-3">
                     <span class="fa-stack fa-4x">
                         <i class="fa fa-circle fa-stack-2x text-primary"></i>
                         <i class="fa fa-paint-brush fa-stack-1x fa-inverse"></i>
                     </span>
                     <h4 class="service-heading">UI/UX Designing</h4>
                     <p class="text-muted" style="padding:0px;">We are a creative lot at Teks. For mobile apps and websites, trust us to come up with innovative and user-friendly interfaces, layouts and other graphic elements.</p>
                 </div>
             </div>
         </div>
     </section>

    <!-- Team Section -->
    <section id="team" style="background: #5d5d5d;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Those Who Drive Teks</h2>
                    <h4 class="section-subheading text-team">Meet The Team Behind The Success Of Teks</h4>
                </div>
            </div>
            <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
		    <!-- use jssor.slider.debug.js instead for debug -->
		    <script>
		        jQuery(document).ready(function ($) {

		            var jssor_1_SlideoTransitions = [
		              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
		              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
		              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
		              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
		              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
		              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
		              [{b:10000,d:2000,x:-379,e:{x:7}}],
		              [{b:10000,d:2000,x:-379,e:{x:7}}],
		              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
		            ];

		            var jssor_1_options = {
		              $AutoPlay: true,
		              $SlideDuration: 800,
		              $SlideEasing: $Jease$.$OutQuint,
		              $CaptionSliderOptions: {
		                $Class: $JssorCaptionSlideo$,
		                $Transitions: jssor_1_SlideoTransitions
		              },
		              $ArrowNavigatorOptions: {
		                $Class: $JssorArrowNavigator$
		              },
		              $BulletNavigatorOptions: {
		                $Class: $JssorBulletNavigator$
		              }
		            };

		            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		            //responsive code begin
		            //you can remove responsive code if you don't want the slider scales while window resizing
		            function ScaleSlider() {
		                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
		                if (refSize) {
		                    refSize = Math.min(refSize, 1920);
		                    jssor_1_slider.$ScaleWidth(refSize);
		                }
		                else {
		                    window.setTimeout(ScaleSlider, 30);
		                }
		            }
		            ScaleSlider();
		            $(window).bind("load", ScaleSlider);
		            $(window).bind("resize", ScaleSlider);
		            $(window).bind("orientationchange", ScaleSlider);
		            //responsive code end
		        });
		    </script>

		    <style>

		        /* jssor slider bullet navigator skin 05 css */
		        /*
		        .jssorb05 div           (normal)
		        .jssorb05 div:hover     (normal mouseover)
		        .jssorb05 .av           (active)
		        .jssorb05 .av:hover     (active mouseover)
		        .jssorb05 .dn           (mousedown)
		        */
		        .jssorb05 {
		            position: absolute;
		        }
		        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
		            position: absolute;
		            /* size of bullet elment */
		            width: 16px;
		            height: 16px;
		            background: url('img/b05.png') no-repeat;
		            overflow: hidden;
		            cursor: pointer;
		        }
		        .jssorb05 div { background-position: -7px -7px; }
		        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
		        .jssorb05 .av { background-position: -67px -7px; }
		        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

		        /* jssor slider arrow navigator skin 22 css */
		        /*
		        .jssora22l                  (normal)
		        .jssora22r                  (normal)
		        .jssora22l:hover            (normal mouseover)
		        .jssora22r:hover            (normal mouseover)
		        .jssora22l.jssora22ldn      (mousedown)
		        .jssora22r.jssora22rdn      (mousedown)
		        */
		        .jssora22l, .jssora22r {
		            display: block;
		            position: absolute;
		            /* size of arrow element */
		            width: 40px;
		            height: 58px;
		            cursor: pointer;
		            background: url('img/a22.png') center center no-repeat;
		            overflow: hidden;
		        }
		        .jssora22l { background-position: -10px -31px; }
		        .jssora22r { background-position: -70px -31px; }
		        .jssora22l:hover { background-position: -130px -31px; }
		        .jssora22r:hover { background-position: -190px -31px; }
		        .jssora22l.jssora22ldn { background-position: -250px -31px; }
		        .jssora22r.jssora22rdn { background-position: -310px -31px; }
		    </style>


		    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
		        <!-- Loading Screen -->
		        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
		            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
		            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
		        </div>
		        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
		            <div data-p="225.00" style="display: none;">
		                <img data-u="image" src="img/team/1.png" />
		            </div>
		            <div data-p="225.00" style="display: none;">
		                <img data-u="image" src="img/team/2.png" />
		            </div>
		            <div data-p="225.00" data-po="80% 55%" style="display: none;">
		                <img data-u="image" src="img/team/3.png" />
		            </div>

		        </div>
		        <!-- Bullet Navigator -->
		        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
		            <!-- bullet navigator item prototype -->
		            <div data-u="prototype" style="width:16px;height:16px;"></div>
		        </div>
		        <!-- Arrow Navigator -->
		        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
		        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
		    </div>

		    <!-- #endregion Jssor Slider End -->
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="large text-team">Teknowledge has always been, and will forever be, a mobile app company that accords first-priority to its clients' wishes. Their support has helped us grow, and we look forward to more challenges, more success, more appreciation... and more scopes to serve clients across the world.</p>
                </div>
            </div>
        </div>
    </section>

	<!-- About Section -->
    <section id="about">
        <div class="container" style="padding-bottom:20px;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#FD610C;">The Teks Timeline</h2>
                    <h4 class="section-subheading text-muted">Our Journey... At A Glance</h4>
              <br>  </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline" >
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2006</h4>
                                    <h4 class="subheading">The First Steps</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Java apps were all that we started out with. We had a small group of coders and developers, and our main asset was the passion and dedication for coding, making something new. Not only our operations... even our logo was a bit different at the time!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2007</h4>
                                    <h4 class="subheading">Our Debut App</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">2007 saw the release of several Teks apps, and the first of them was Qualicell - a J2ME app written in...Notepad (yepp, you read that right!). Over the next couple of years, as more projects started to come in, separate teams for iOS and Android app development were formed.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2011</h4>
                                    <h4 class="subheading">Teks Reaches Out</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">The year when we started providing 24x7 services to clients. The number of time zones we served rose to 18. We also began round-the-clock web-based support and app consultations in 2011. Most of our apps got positives reviews and ratings.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2013</h4>
                                    <h4 class="subheading">When The Awards Started Pouring In</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Fast forward half a decade, and by 2013, we had blossomed into full-fledged cross-platform mobile app company. The total app count crossed 500 during this year. The highest point, however, was 'Story Time For Kids' - a storytelling app for children - winning the Adobe Design Award.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-5.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2014</h4>
                                    <h4 class="subheading">Teks Debuts Down Under</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">In this year, each of our apps got featured at the App Store - a truly proud record for us. Also, the Australian chapter of Teknowledge - Teks Mobile Australia - started operations this year, with Ms. Amber Blumanis as the COO. Incidentally, Amber was also the maker of 'Stopover', another prize-winning app.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/a-6.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2015</h4>
                                    <h4 class="subheading">More Awards. Another International Branch</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">January witnessed the start of 'Teks Sweden'. Along with the other platforms, we started developing apps for Apple Watch, and created a sub-team of WatchKit app developers. 'Story Time For Kids' won an award again - this time at GMASA 2015.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                        <a href="startproject.php">
                            <div class="timeline-image">
                                <h4>Let's Create
                                    <br>Apps
                                    <br>Together
                                </h4>
                            </div>
                           </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#about').addClass('active');
	});

</script>

</body>

</html>
</body>
</html>
