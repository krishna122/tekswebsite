<div class="favouriteapps cbp-so-scroller" id="cbp-so-scroller">

<div data-favouriteapp="7 " class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav7">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/bender.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/benderNew1_x264_002.mp4"></source>
									<source type="video/webm" src="video/vidWebM/benderNew1_VP8_001.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav7.png" alt="Bender" class="favouriteapp__logo__img"><br>
					<a class="" href="bender.php">
					<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a><br>
				</div><!--
			 --><!-- .favouriteapp__phone
			 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul style="text-align: left;font-weight: 100;margin-left: 1px;">
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> A first-of-its-kind mobile venue locator app, that also serves as a social events finder.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Top public venues in different cities can be viewed on Bender, along with details.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Being a part of the Bender community helps readers get details of the hottest social events.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> This app brings you up, close and personal with new places!</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>





<div data-favouriteapp="1" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav1">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="1" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="1" class="phone__reveal__wrap">
								<video poster="video/onebrand.jpg" id="video--1" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/onebrandNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/onebrandNew_VP8.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav1.png" alt="OneBrand" class="favouriteapp__logo__img"><br>
					<a class="" href="onebrand.php">
					<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a><br>
					
					<a data-toggle="modal" data-target="#onebrand"><img width="290" height="99" src="img/btn_beta.png" alt="Get beta version"></a>

				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Shopping fun, right from the comforts of your home.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > A vast, regularly updated, stock of products - arranged in groups or placed individually. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > One Brands offers secure payment options too.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Optimized for iPhone 6 and iPhone 6 Plus.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > The perfect shopping companion!</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>






<div data-favouriteapp="2" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav2">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="2" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="2" class="phone__reveal__wrap">
								<video poster="video/babysitter.jpg" id="video--2" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/babysitterNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/babysitterNew1_VP8_001.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav2.png" alt="Babysitter" class="favouriteapp__logo__img"><br>
					<a class="" href="babysitter.php">
					<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a><br>
					
                   <a data-toggle="modal" data-target="#babysitter"><img width="290" height="99" src="img/btn_beta.png" alt="Get beta version"></a>
				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> For finding professional, reliable, expert, local babysitters.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Parents can directly send appointment requests to the babysitters they shortlist. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> In the 'Babysitter View' of the app, babysitters can view appointment requests. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Finding the best caregiver for kids is only a matter of a few taps!</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>





<div data-favouriteapp="3" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav3">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="3" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="3" class="phone__reveal__wrap">
								<video poster="video/timesnaps.jpg" id="video--3" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/timesnapsNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/timesnapsNew_VP8.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav3.png" alt="Timesnaps" class="favouriteapp__logo__img"><br>
					<a class="" href="timesnaps.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					<br>
<!-- 					<a href="" target="_blank"><img width="290" height="99" src="img/btn_beta.png" alt="Available in Playstore"></a> -->
				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Created to preserve the magic of moments.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Users can take images of plants, outdoor scenery, babies and other loved ones. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Pictures can be viewed as slideshows. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Timesnaps comes with seamless social sharing options.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>





<div data-favouriteapp="4" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav4">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="4" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="4" class="phone__reveal__wrap">
								<video poster="video/stopover.jpg" id="video--4" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/stopoverNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/stopoverNew_VP8.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav4.png" alt="Stopover" class="favouriteapp__logo__img"><br>
					<a  class="" href="stopover.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					<br>
					<a href="https://play.google.com/store/apps/details?id=com.stopover" target="_blank"><img width="290" height="99" src="img/play-store.png" alt="Available in playstore"></a>
				</div><!--
			 --><!-- .favouriteapp__phone
			 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> iOS and Android versions available.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Strike up appointments with like-minded people - directly from the airport.  </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Chance for some fun airport dating.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Stopover won big at the 2014 Talent Unleashed Awards.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>

<div data-favouriteapp="5" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav5">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/freebird.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/freebirdNew1_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/freebirdNew1_VP8_001.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav5.png" alt="Freebird" class="favouriteapp__logo__img"><br>
					<a class="" href="freebird.php">
					<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					
				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Show off your creative side, by uploading your poems, snaps, songs and tunes.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Find and follow fellow artists, poets, composers, and other creative professionals. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Our app developers have included a real-time chatting option in the app.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> This one is absolute magic for indie artists.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>

<div data-favouriteapp="6" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav6">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/inslideout.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/inslideoutNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/inslideoutNew_VP8.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav6.png" alt="InSlideOut" class="favouriteapp__logo__img"><br>
					<a class="" href="inslideout.php">
							<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
				</div><!--
			 --><!-- .favouriteapp__phone
			 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Developed particularly for those who wish to keep track of events.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Real-time information feed on as many as 5 different sports.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Users have the option of following the events of top celebs and personalities.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Event tickets can also be purchased via this user-friendly app.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>



<div data-favouriteapp="8" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav8">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/speedykey.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/speedykeysNew_x264.mp4"></source>
									<source type="video/webm" src="video/vidWebM/speedykeysNew_VP8.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav8.png" alt="SpeedyKey" class="favouriteapp__logo__img"><br>
					<a class="" href="speedykey.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					<br>
					<a href="https://itunes.apple.com/dk/app/speedykey-keyboard/id957834205?mt=8" target="_blank"><img width="290" height="99" src="img/appstore.png" alt="Available in appstore"></a>
				</div><!--
			 --><!-- .favouriteapp__phone
			  --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Pre-written custom replies, and other convenient keyboard shortcuts</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Supports fast, error-free typing in three different languages. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Special provisions for displaying the additional characters of foreign languages.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Four additional themes can be downloaded via in-app purchase.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> With SpeedyKey, respond quickly to messages and emails!</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>


<div data-favouriteapp="9" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav9">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/icba.png" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/ICBF_x264_002.mp4">
									<source type="video/webm" src="video/vidWebM/ICBF_VP8.webm">
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav9.png" alt="ICBA" class="favouriteapp__logo__img"><br>
					<a class="" href="icba.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					
					<br>
					<a href="https://itunes.apple.com/dk/app/speedykey-keyboard/id957834205?mt=8" target="_blank"><img width="290" height="99" src="img/appstore.png" alt="Available in appstore"></a>
					<br>
					<a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img width="290" height="99" src="img/play-store.png" alt="Available in Playstore"></a>
				</div><!--
			 --><!-- .favouriteapp__phone
			  --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> The user-friendly mobile stress-reliever.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Comprises of a series of 12 separate apps. </li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Comes with high-quality audio lesson narrations.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Four additional themes can be downloaded via in-app purchase.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Available for iOS &amp; Android platforms.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> One of the featured apps of our mobile application development company.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>


</div>

<style>
	ul{
		list-style-image: url("img/bullet.png") !important;
	}

</style>

<!-- Beta form modal Onebrand-->

<div class="modal fade" id="onebrand" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" style="outline:0;font-size:16px"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h5 class="modal-title color" id="lineModalLabel" style="color:#555">Check out the FREE beta version of Onebrand</h5>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Enter email" id="email" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="fn" class="form-control" placeholder="Enter First Name" id="userfn" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="ln" class="form-control" placeholder="Enter Last Name" id="userln" style="color:#555">
              </div>
              <button type="button" class="btn btn-default" id="submitonebrand" style="background: #212B34;color:#fff;">Submit</button>
            </form>

		</div>
	</div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
		$(document).ready(function () {
			
          $("#submitonebrand").click(function() {
        	  var email = $("#email").val();
              var userfn = $("#userfn").val();
              var userln = $("#userln").val();

//               alert(userfn);
              
  			$.post('https://staging.teks.co.in/testflightbetainvite/phpinvititionscriptrun.php', {'email':email, 'appID':'1111154921', 'userfn':userfn, 'userln':userln}, function(data){
  				alert('Please Check you email for beta app link');
  			  });
              
             });
          
          });
        
		     
    </script>
    
    
    
    <!-- Beta form modal Babysitter -->

<div class="modal fade" id="babysitter" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" style="outline:0;font-size:16px"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h5 class="modal-title color" id="lineModalLabel" style="color:#555">Check out the FREE beta version of Babysitter</h5>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Enter email" id="email" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="fn" class="form-control" placeholder="Enter First Name" id="userfn" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="ln" class="form-control" placeholder="Enter Last Name" id="userln" style="color:#555">
              </div>
              <button type="button" class="btn btn-default" id="submitbabysitter" style="background: #212B34;color:#fff;">Submit</button>
            </form>

		</div>
	</div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
		$(document).ready(function () {
			
          $("#submitbabysitter").click(function() {
        	  var email = $("#email").val();
              var userfn = $("#userfn").val();
              var userln = $("#userln").val();
				console.log('hello');
//               alert(userfn);
              
  			$.post('https://staging.teks.co.in/testflightbetainvite/phpinvititionscriptrun.php', {'email':email, 'appID':'986773705', 'userfn':userfn, 'userln':userln}, function(data){
  				alert(data);
  				alert('Please Check you email for beta app link');
  			  });
              
             });
          
          });	     
</script>
