<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Start Your Dream Project</title>
    

	<?php include 'head.php';?>

</head>


<body id="page-TOP" data-spy="scroll" data-target=".navbar-fixed-top" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<section class="project">
		<div class="project-body" >
			<div class="project-container"><br>
                         <div>
                         <?php
                         	if(isset($_POST['submit']))
                         	{
                         		
				// multiple recipients
				$to  = 'krishna.kumar@teks.co.in'; // note the comma
									
									
				// subject
				$subject = 'Quote Request';
									
				// message
				$message = '
					<html>
									<head>
									  <title>Quote Request</title>
									</head>
									<body>
									  <p><strong>Name: </strong> '.$_POST['uname'].'</p>
									  <p><strong>Email: </strong>'.$_POST['uemail'].'</p>
									  <p><strong>Description: </strong>'.$_POST['describe'].'</p>
									  
									</body>
									</html>
				';
									
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
									
				// Additional headers
				
				$headers .= 'From: '.$_POST['uname'].' <'.$_POST['uemail'].'>' . "\r\n";
									
				// Mail it
				$retval = mail($to, $subject, $message, $headers);
				if( $retval == true )
				{
				?>
										
			        <div class="alert alert-success" role="alert">
				<?php echo "Thank You! We have received your request and will get back to you shortly. Meanwhile, take a tour of our website, check out some of our best apps (link here - to app stories page), or view our app-awesome team on Facebook (link here - to Facebook page)."; ?>
				</div>
				<?php 	
				}
				else
				{?>
									
				<div class="alert alert-danger" role="alert">
				<?php echo "Message could not be sent"; ?>
				</div>
				<?php 		
				}
                         	
                         	}
                               ?>
                         
                         
                            <h3>Get A FREE App Quote Within 24 Hours</h3>
                        <br> 
                  </div>
                  
                  
				<form role="form" style="text-align: left; color: #fff;" padding:10px; method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
					
				  <div class="border-box">
					  	<div class="clearfix"></div>
					  	<div class="row">
						  <div class="col-md-2"></div>
						  	<div class="col-md-8">
						  		<div class="form-group">
							      <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 1 of 7</label>
							     </div>
							  	<div class="form-group">
							      <label for="name" style="font-size: 25px; font-weight: 200;">What Do You Require ?</label>
							       <div class="checkbox">
									  <label><input type="checkbox" value="optradio">&nbsp;iOS app</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" value="optradio">&nbsp;Android app</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" value="optradio">&nbsp;Web Development</label>
									</div>
							     </div>
						  	  </div>
						  <div class="col-md-2"></div>
					   </div>
					   
					    <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue 
					    <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box"> 
					   <div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 2 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">What Will Be The Main Focus Of Your Project ?</label>
						     </div> 
						      <div class="form-group">
								<label> 
									I want &nbsp;<input type="radio" value="optradio">&nbsp; a mobile app / 
									&nbsp;<input type="radio" value="optradio">&nbsp; website
									that will <input type="text" name="" class="inputunderline" >
								</label>
							   </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  	 <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box">	
					  	<div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 3 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">List 5 features that your project MUST have :</label>
						      
						      <div class="form-group">
						      	<ul style="list-style-type: decimal;">
						      		<li>
						      		  <label><input type="text" name="" class="inputunderlinefeatures" ></label> 	
						      		</li>
						      		<li>
						      		  <label><input type="text" name="" class="inputunderlinefeatures" ></label> 	
						      		</li>
						      		<li>
						      		  <label><input type="text" name="" class="inputunderlinefeatures" ></label> 	
						      		</li>
						      		<li>
						      		  <label><input type="text" name="" class="inputunderlinefeatures" ></label> 	
						      		</li>
						      		<li>
						      		  <label><input type="text" name="" class="inputunderlinefeatures" ></label> 	
						      		</li>
						      	</ul>
								
							   </div>
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  	 <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box">	
					  	<div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 4 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">How many screens do you want the app to have ?</label>
						      
						      <div class="form-group">
						      		<div class="radio">
									  <label><input type="radio" value="optradio">&nbsp;< 5&nbsp; screens</label>	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;5 - 10&nbsp; screens</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;11 - 15&nbsp; screens</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;16 - 20&nbsp; screens</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;21 - 30&nbsp; screens</label>	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;> 30&nbsp; screens</label>	
						      		</div>
							   </div>
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  	 <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box">
					  	<div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 5 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">What will be the scale of your project ?</label>
						      
						      <div class="form-group">
						      		<div class="radio">
									  <label><input type="radio" value="optradio">&nbsp;Very Small</label>	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;Small</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;Mid-level</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;Large</label> 	
						      		</div>
						      		<div class="radio">
						      		  <label><input type="radio" value="optradio">&nbsp;Very Large</label>	
						      		</div>
							   </div>
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  	 <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box">	
					  	<div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 6 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">Do you want to talk with a Teks representative now ?</label>
						    </div>
						      <div class="form-group">
						      	<label> 
									<input type="radio" value="optradio"> &nbsp;Yes (Please provide your Skype ID) <input type="text" name="" class="inputunderline" > <br>
									<input type="radio" value="optradio"> &nbsp;No, I will chat later.
								</label>
							   </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  	 <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					  </div>
					  
					  <div class="border-box">	
					  	<div class="clearfix"></div>
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
					  		<div class="form-group">
							   <label for="name" style="font-size: 25px; font-weight: 200;text-decoration: underline;">Question 7 of 7</label>
							 </div>
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;">May We Know You ?</label>
						    </div>
						      <div class="form-group">
						      	<label>
						      		Name<br> <br><input type="text" name="" class="inputunderlinefeatures"> <br>
								</label>
								<label>
						      		Email<br> <br><input type="text" name="" class="inputunderlinefeatures" > <br>
								</label>
							   </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  	
					  <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Save & Continue <i class="fa fa-long-arrow-right" style="float:right;margin-top: -29px;"></i></button>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
					 </div>
					  
                      <div class="clearfix"></div>
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;font-weight: 400;" name="submit">Finish & Submit</button>
					    <p style="color: #CFCFC4; font-size: 15px; text-align: center;">By submitting this form, you agree to our <a href="terms.php" style="text-decoration: underline; cursor: pointer; color: #CFCFC4;">Terms Of Service</a> </p>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
					    
				    </div>
				</div>
				</form>
				
			</div>
		</div>
	</section>
	
<?php include "map.php";?>
	
<?php include 'footer.php';?>

<?php include 'script.php';?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">

setTimeout(function() {
    $('#alert-warning').fadeOut('slow');
    $('.alert-danger').fadeOut('slow');
    $('.alert-success').fadeOut('slow');
    $('#error-alert').fadeOut('slow');
}, 3000);
                         </script>
                         
                         <script type="text/javascript">
$(document).ready(function(){
	$('#start').addClass('active');
});

</script>
</body>
</html>