<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://staging.teks.co.in/tekscoin/";
  $query = "SELECT * FROM  metapages where pagename = 'applyandroid'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $result['metatitle']; ?></title>
    <meta name='description' content='<?php echo $result['metadescription']; ?>' />
    <meta name='keywords' content='<?php echo $result['metakey']; ?>' />

	<?php include 'head.php';?>

</head>


<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="career">
        <div class="career-body">
            <div class="project-container" style="margin-top: 8%; margin-bottom: 2%;">
            	<div class="row">
                    <div class="col-md-12">
                        <p style="text-align: center;"><img alt="" src="img/android.png" ></p>
                    </div>
                 </div>
                <br>
                   <div class="row" style="margin-bottom: 0px;">
                    <div class="col-md-12">
                        <h1 style="color:#fff;">Apply For Android Developer</h1>
                        <p style="color:#fff;">There are four (4) vacancies in our Android app development team. Candidates with B.Tech.(IT)/BCA/M.Tech. degrees are eligible for applying for this post. Kindly note that applicants are required to have in-depth knowledge of Google API integration and the Android SDK.</p>
                    </div>
                </div>
              </div>
        </div>
    </header>
	
<section >
	<div class="container">
	  <div class="row">
           <h4 style="font-weight:200;">Required Skills & Experience</h4>
          <div style="career-skills"><ul>
	  <li>Knowledge of universal app programming language (for tablets & smartphones).
<li>Experience in working with Soap, JSON & XML.</li>
<li>Expertise in Core JAVA.</li>
<li>Ability to use UI customization methods and SQLite.</li>
<li>Ability to create/develop/customize user interfaces from scratch.</li>
<li>Android app testing knowledge (including device compatibility tests & run version tests).</li>
<li>Experience in working with multiple coding structures, including CamelCase.</li>
<li>Knowledge of working with ICS and JellyBean.</li>
<li>Expertise in core optimization, memory handling, and other components of app testing.</li>
          <ul>
          </div>

 <br /> <div class="dropcv"><p style="text-align:centre;">To apply for this post drop in your resume to <a style="color:#ff7018" href="mailto:hr@teks.co.in?subject= Android app developer">hr@teks.co.in</a></p></div><br/>
           
    </div>
 </div>
</section>

<section id="career">
	<div class="container">
	  <div class="row">
	  <h1>Join The Teks Team</h1>
	  <p style=" text-align: center; color:#9c9c9c;font-size: 30px; margin-top: -50px;">Varied Opportunities. Multiple Openings. Ample Scope To Do What You Love.</p><BR></BR>
	  <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                    <img src="img/c1.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Ui/Ux Designer</h4>
                    </div>
                </div>
                <div class="space"></div>
           
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
         
                <div class="icon">
                    <img src="img/c2.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Android App Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
          
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c3.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Project Coordinator</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c4.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">PHP Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
    </div>
 </div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-6"> 
				<div class="panel panel-default job1" style=" border: none; cursor: pointer;" onclick="job1()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Graphics</span><br>
			    			<span style="font-weight: 200;color:#fff;">UI/UX Designer</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px; font-weight: 300;">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job2" style=" border: none; cursor: pointer;" onclick="job2()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Android</span><br>
			    			<span style="font-weight: 200;color:#fff;">App Developer</span>
		    			</h3> <br> 
	    			<center>
	    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
	    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job3" style=" border: none; cursor: pointer;" onclick="job3()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Operation</span><br>
			    			<span style="font-weight: 200;color:#fff;">Project Coordinator</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job4" style=" border: none; cursor: pointer;" onclick="job4()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Coding (next line)</span><br>
			    			<span style="font-weight: 200;color:#fff;">Php Developer</span>
		    			</h3> <br> 
			    			<center>
			    			<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
			    			</center> 
	    			</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
function job1() {
    window.location.assign("applydesigner.php")
}

function job2() {
    window.location.assign("applyandroid.php")
}

function job3() {
    window.location.assign("applypm.php")
}

function job4() {
    window.location.assign("applyphp.php")
}
</script>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>