<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Lifestyle Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">Lifestyle Apps</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Your smartphones and tablets are already decorated with innumerable genres of applications. But its time to personalize your device according to your own tastes and preferences. Lifestyle Apps are just meant to make your life better and easier. Try’em out right away !</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
<section id="appstory" class="lifestyle">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Lifestyle Travel</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Travel App</span>
				  	<br><br>
				  	<p style="padding: 0px;">The perfect mobile tour planner, information provider and real-time helper - all rolled into one. Conceptualized by Leonie Spencer - a long-time travel fan herself - this is an app that every travel-lover would love.</p>
				  <br><br>

				  <a href="lifestyle.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/lifestyle.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>    
    
<section id="appstory" class="timesnaps">
	<div class="container">
		<div  style="width:100%;"><br/><br/>
			<div class="row">
		  		<div class="col-lg-12">
				  	<div class="col-lg-6">
				  		<span style="color: #fff; font-size: 25px; text-align: center;">Timesnaps</span><br>
				  		<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Photo/Video</span>
				  		<br><br>
				  		<p style="padding: 0px;">For those who wish to capture, preserve and treasure the magic of fleeting moments, Timesnaps is a must-have application.  The app allows people to take images of practically anything at regular intervals, create slideshows with them, and revel at the changes that time can make!</p>
				  		<br><br>
				  		<a href="timesnaps.php" ><img src="img/view-project.png"></a><br><br>
				  	</div>

					<div class="col-lg-6 storiesimg">

				  	<img src="appimages/timesnaps.png" align="center">

				   </div>
		  	 </div>
		 </div>
	   </div>
	</div>
</section>


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>