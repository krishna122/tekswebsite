<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>CLIENTS</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">
    
    <style>
		p {
		  font-size:1em
		}
    </style>
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
		<!-- Intro Header -->
<header class="clients" style="height: 80%;">
        <div class="clients-body">
            <div class="container" >
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Our Apps Make Our Clients Happy </span></h1>
	                           <p style="font-size: 25px;">And They Have Great Things To Say About Us Too</p>
                       </div>
                 </div>
            </div>
        </div>
</header>



<section class="offwhite-background">
	
	<div class="container">
		<div class="row">
		  <div class="col-lg-42">
		  		<h2 class="color text-center" style="line-height: 0px !important;">Our Top Projects</h2>
		  	    <p class="color text-center" style="line-height: 0px !important;">
		          Check out some key projects in our 900+ app portfolio
		         </p>
		  </div>
		</div>
		<br>
		
	    <div class="row">
	  	<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/1_cloud.png">
                </div>
				<a>1Cloud2</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/baby_sitter.png">
                </div>
				<a>babysitter</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/currently.png">
                </div>
				<a>Currently</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/dimes-loops.png">
                </div>
				<a>Looops</a>
        </div>
        </center>
	</div>
		<br>
    <div class="row">    
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/fuel_up.png">
                </div>
				<a>FuelUp</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/icbf.png">
                </div>
				<a>I Can Be Anything</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/inslid_out.png">
                </div>
				<a>Inslide Out</a>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/kids_tiles.png">
                </div>
				<a>Kids Tiles</a>
        </div>
        </center>
	</div>
		<br>
	<div class="row">	
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/prince.png">
                </div>
				<a>Prince Of Asia</a>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/property_match.png">
                </div>
				<a>Property Match</a>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/smart_trips.png">
                </div>
				<a>Smarttrips</a>
        </div>
        </center>
        
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/speedy_key.png">
                </div>
				<a>SpeedyKey</a>
        </div>
        </center>
		
    </div>
		
		<br>
		
		<div class="row">
	  	
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/times_snap.png">
                </div>
				<a>Timesnap</a>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/veeie.png">
                </div>
				<a>Veeaie Keyboard</a>
        </div>
        </center>
	
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/vuwsa.png">
                </div>
				<a>VUWSA</a>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/aways.png">
                </div>
				<a>awayys</a>
        </div>
        </center>
		
        
    </div>
		
		
     </div>
</section>


<div class="carousel-reviews broun-block">
    <div class="container">
		<br>
		<div class="row">
			<h3 class="text-center">Great Apps Make For Great Reviews!</h3>
			<p class="text-center" style="line-height:0px;">Here's What Our Happy Clients Have To Say:</p>
		</div>
		<br>
        <div class="row">
        
		
		                <div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Inslide Out </a>
						        <p>Technical expertise meets creativity meets commitment to excellence at Teksmobile. They have this amazing teamwork going, to make sure applications are created as per the precise preference of clients like myself. The skill factor is extremely high here, and they have a keen eye for innovation too. Highly recommended!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/02/aaron.png" style="width:90px; height:81px;"/>
								<a>Aaron Rosenzweig</a>
								<i>Founder, Managing Partner</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Drift Keyboard</a>
						        <p>One of the first things that struck me about Teks was their portfolio. After all, not many companies can claim to have worked on more than 900 app projects. The developers and designers work with a high level of efficiency, intensity and they have a nice imaginative streak going as well. They more than matched up to my expectations...much appreciated!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/hazlett.png" style="width:90px; height:81px;"/>
								<a>Paul Hazlett</a>
								<i>Lead Software Engineer, Owner</i>
							</div>
						</div>
						
						<div class=" col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Veeaie Keyboard</a>
						        <p>Teksmobile is one of those rare companies that actually put in extra hard yards to add real value to their projects. They are always in touch with clients, carefully chalking out the next round of activities during a project. Not only do they make world-class mobile applications...but they are also available on a 24x7 basis. Great stuff.</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/04/v8tejmur.png" style="width:90px; height:81px;"/>
								<a>Tejmur Sattarov</a>
								<i>Senior Art Director & Concept Developer</i>
							</div>
						</div>
		</div>
		
		
		<div class="row">
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >I Can Be Anything</a>
						        <p>Easily one of the best mobile app companies in Denmark. The guys at Team Teks have excellent technological and quality standards. We were just a tad apprehensive at first, while looking for an app agency...and we’re so glad to have found Hussain’s company. Forget being mere app developers - these guys are real app architects!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/02/unnamed-24.png" style="width:90px; height:81px;"/>
								<a>Jacob Strachotta</a>
								<i>Co-founder</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a>Dataworks</a>
						        <p>An Android app with Bluetooth technology as its main driver - the project we hired Teksmobile for was not the easiest out there. Credit to them...they took time out to understand the essence of the app idea, and then proceeded to use cutting-edge technologies for development. Dataworks was always meant to be an innovative app - and Teks helped us make it just that.</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/willLawler.png" style="width:90px; height:81px;"/>
								<a>Will Lawler</a>
								<i>Instrument Works Pty Ltd.</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Smarttrips</a>
						        <p>Hussain’s team is an absolute delight to work with. Right from the time of providing the free app quote, to the successive stages of designing, development and testing - they were so sincere about everything...and they kept explaining every single thing to me. I also found them more than eager to resolve all my queries, and discuss how the project was to be taken forward. An app company you can really, really trust on!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/lionei.png" style="width:90px; height:81px;"/>
								<a>Leonie Spencer</a>
								<i>Managing Director</i>
							</div>
						</div>
						
		</div>
		
		<div class="row">
						
						
		</div>		
					
						
        </div>
    </div>
</div>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#clients').addClass('active');
	});

</script>

</body>
</html>