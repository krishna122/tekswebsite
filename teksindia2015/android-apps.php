<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Android Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">Android Apps.</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">The app market has become quite ubiquitous with the Android operating system already. Keeping that in mind the Teks Team has spent relentless hours in curating the most needful of Android Apps to meet the best possible number of purposes.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
</header>
		
           <section id="appstory" class="stopover">
	<div class="container">
                <div  style="width:100%;"><br/><br/>
		<div class="row">  
		  <div class="col-lg-12">
				  <div class="col-lg-6">
				  
				  	<span style="color: #fff; font-size: 25px; text-align: center;">Stop Over</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Social Networking</span>
				  	<br><br>	
				  	<p style="padding: 0px;">Conceptualized by Ms. Amber Blumanis, Stopover is a breakthrough app for travelers. It helps to remove the 'bore-factor' of long waits at airports. With the app, users can connect with people with common interests, right from the airport. Stopover, unsurprisingly, has won multiple awards.</p>
				  <br><br>
				  	<a href="stopover.php" ><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  
				  <img src="appimages/stopover.png" align="center">
				  
				</div>
		  </div>
		 </div>
                 </div></div>
</section>
		  


<section id="appstory" class="icbf">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="color: #fff; font-size: 25px; text-align: center;">I Can Be Anything</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Motivational App</span>
				  	<br><br>	
				  	<p style="padding: 0px;">A multi-featured stress-reliever app, with as many as 12 different suites. Conceptualized by Jacob and Ditte Strachotta and optimized for the iOS and Android platforms, the app has several free sessions and easy in-app purchase options.</p>
				  <br><br>
				  <a href="icba.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/icbflogo.png" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</div> 
</section>

<section id="appstory" class="dataworks">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="color: #fff; font-size: 25px; text-align: center;">DataWorks</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Bluetooth Connectivity App</span>
				  	<br><br>	
				  	<p style="padding: 0px;">DataWorks taps into the power of Bluetooth technology to the fullest. With this powerful app, devices can be connected with other compatible gadgets, and data can be measured, calibrated, maintained and managed.</p>
				  <br><br>
				  <a href="dataworks.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dataworkslogo.jpg" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</div> 
</section>


<section id="appstory" class="LMIOpt-InChecker">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="color: #fff; font-size: 25px; text-align: center;">LMI Opt-In Checker</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">RFID app</span>
				  	<br><br>	
				  	<p style="padding: 0px;">A cutting-edge RFID app created for the Android platform. Ideal for keeping track of prizes won by users at any time. The app boasts of a new technology, and is further bolstered by its smooth usability features</p>
				  <br><br>
				  <a href="LMIOpt-InChecker.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/LMIOpt-InCheckerlogo.png" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</div> 
</section>





<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>