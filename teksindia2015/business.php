<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Buisness Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">Buisness Apps</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Equip your smartphone with the right digital tools to become more efficient. Start developing your professional skills through our Business Apps, staying connected, organizing projects, having perfect security and all your preferences.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <section id="appstory" class="fuelup">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12"><br><br>
					  <div class="col-lg-6">
	
					  	<span style="color: #fff; font-size: 25px; text-align: center;">Fuelup</span><br>
					  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Fuel-buying app</span>
					  	<br><br>
					  	<p style="padding: 0px;">Fuel Up is a mobile fuel-buying app, optimized for the iOS 8 and iOS 9 platforms. Via this GPS-powered app, users can find fellow drivers in their locality, and put in fuel purchase requests to them. The user-friendly driver's app makes car rides free of fuel-related worries!</p>
					  <br><br>
					  <a href="fuelup.php"><img src="img/view-project.png"></a><br><br>
					  </div>
	
					<div class="col-lg-6 storiesimg">
					  <img src="appstories/Fuelup.png" align="center">
					</div>
	
			  </div>
			</div>
		</div>
	</div>
	</section>
	
<section id="appstory" class="flypal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Flypal CRS</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Aviation app</span>
				  	<br><br>
				  	<p style="padding: 0px;">FlyPal CRS is an iOS/Android app geared to make aviation management smarter and more efficient than ever before. The app facilitates prompt and real-time communication between administrators, operators and crew members, and provides detailed reports and document access too. A winning app for flight managers!</p>
				  <br><br>

				  <a href="flypal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/flypal.png" align="center">
				</div>

		    </div>
		  </div>
	   </div>
   </div>
</section>

<section id="appstory" class="deal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">365deal</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Business app</span>
				  	<br><br>
				  	<p style="padding: 0px;">365 Deal is an exclusive iOS ad-sharing app for sellers. Two different types of ads can be posted on this user-friendly application. All ads are information-rich, and the contact details of sellers are provided with each advertisement.</p>
				  <br><br>

				  <a href="365deal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/365.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

	<section id="appstory" class="onebrand">
	<div class="container">

	<!--  <h1 style="margin-top: 0px;">iPHONE APPS</h1><br> -->
               <div  style="width:100%;"><br/><br/>
	  	<div class="row">
	  		<div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">One Brand</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Mobile Commerce</span>
				 	<br><br>
				  	<p style="padding: 0px;">Probably the best mobile shopping app in our portfolio. From browsing through product categories to purchasing items and making payments - One Brands has all the requisite m-commerce features. The graphics and design layout make it a really user-friendly app too!</p>
				  	<br><br>
				  	<a  href="onebrand.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/onebrand.png" align="center">
				</div>
		  </div>
		</div>
                </div></div>
          </section>

<section id="appstory" class="babysitter">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		 <div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Baby Sitter</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">App For Parents/Babysitters</span>
				  	<br><br>
				  	<p style="padding: 0px;">Hiring an expert professional babysitter becomes easier than ever before with this revolutionary iOS application. Parents can fix appointments with babysitters, specify the hourly rates they are willing to pay, and hire the best caregiver for their children - all with the Baby Sitter app.</p>
				  <br><br>
				  <a href="babysitter.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/babysitter.png" align="center">

				</div>
		  </div>
		</div></div></div>
</section>


<section id="appstory" class="LMIOpt-InChecker">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">LMI Opt-In Checker</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">RFID app</span>
				  	<br><br>
				  	<p style="padding: 0px;">A cutting-edge RFID app created for the Android platform. Ideal for keeping track of prizes won by users at any time. The app boasts of a new technology, and is further bolstered by its smooth usability features</p>
				  <br><br>

				  <a href="LMIOpt-InChecker.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/LMIOpt-InCheckerlogo.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>