<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Moovers</title>

	<?php include 'head.php';?>
    <link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="moovers" style="height: 34%;">
        <div class="moovers-body">
            <div class="container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                        	<br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Moover</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Some types of mobile gaming apps just never get old. Just think about the popular endless running or jelly-blasting games in the Apple App Store - and you’ll get the picture. Word games are yet another highly popular type of mobile game, and we got the chance to create an excellent one a few weeks back. The game is called Moover, and its powerful, exciting features are likely to keep players hooked for long times at a stretch!
	         </p>

	         <blockquote class="color blockmoovers">
		  	     On a personal level, I am a big fan of mobile word games. I keep downloading and checking out all the new word games that arrive at the app stores. When the opportunity of creating such a game app came knocking, me and my colleagues grabbed it with both hands.
		  	 </blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  		<h3 class="color">Smooth Sign In & Tutorials</h3>
		  	    
		  	    <p class="color">
		          Moover has both email signin/signup options as well as the provision of logging in with Facebook credentials (i.e., social login). Once a user is logged in, (s)he is automatically redirected to the ‘Home Page’ of the app. There are detailed tutorial screens with brief instructions on how to play the game. A glimpse of the ‘Score Board’ page is also available.
		         </p>
		         
		  	    <blockquote class="color blockmoovers">
		  	    	Something I totally hate is downloading an app, and having trouble to find out how it works. On my iPhone multiplayer game, users would face no such problems. Simply follow the tutorials...and you should be good to start playing and winning!
		  	    </blockquote>
		  	    
		         <p class="color">
		            There is a separate ‘Practice Mode’ option for new users, where they can hone their skills before fighting it out with actual opponents. The gameplay is the same in the ‘Practice Mode’, although game coins cannot be used to redeem points here. It’s like having a couple of warm-up sessions before a big match.
		         </p>
		  	 </div>
		  	 
		  	 <div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/mbg.jpg" style="border: 5px solid #DD3A0A;"> 
				   <source src="video/appstories/moovers.mp4" type="video/mp4"> 
				</video>
		  	 </div>   
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/m8.png" alt="moovers" ></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">What Is The Game All About?</h3>
		  	    
		         <p class="color">
		         	There is a word maze, there are words, there is a countdown timer - and a user has to find all the given words in the maze before his/her opponent does...that, in a nutshell, is the Moover mobile word finder game app for you. Players can swipe horizontally (left and right), vertically (up and down) and diagonally to locate the words. Games can be played with FB buddies, in-game friends, or with ‘Random Opponents’.
		         </p>
		         <blockquote class="color blockmoovers">
		  	    	There are many word games that are too clever for their own good. I liked the clarity of vision of the client for the Moover app - he was determined to make a word game that was, in essence, simple...so that anyone could play it...whenever and wherever they liked.
		  	    </blockquote>
		   </div>
		  
		  </div>
		  
		  <div class="col-lg-12">
		  	   <p class="color">
                 	Before sending out invites to ‘Game Friends’, a user can view his/her username, accumulated points and levels. The faded ‘Invited’ button changes to an active ‘Play’ - as soon as the invitation is accepted. Facebook friends, on the other hand, have to be added to the personal friendlist first, and then invited for a game.
                 </p>
                 <blockquote class="color blockmoovers">
		  	    	Once a user gets a popup message that someone has invited him or her for a game, he or she can either accept the challenge or simply resign. Apart from the list of Friends, there is a Global list of Moover players - who can be invited by an individual user.
		  	    </blockquote>
                 <p class="color">
                 	A game between two players on this unique iOS word-hunt game app comprises of 3 rounds. The player who accumulates more coins at the end of the three rounds is deemed to be winner. Coins and points are awarded to him/her.
                 </p>	
		  </div>
		  
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Home Page & Profile Screen</h3>
		  	    <br>
		  	    <blockquote class="color blockmoovers">
		  	    	All the stats of a player - right from the live games and completed games, to all the pending game invitations - are present on the home screen of Moover. I wanted players to get all the information of their game statuses at one place - and that’s exactly how the app developers at Teks designed the app.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		            The profile page of the app is high on information as well. There are places for the user to upload his/her photo, name and date of birth. The level and total points earned are displayed here. There is also a graphical representation of the game statistics for every player. It’s easy to keep track of how you are performing on Moover over time.
		         </p>
		         
		         <h3 class="color">In-Game Helps</h3>
		  		
		  	    <p class="color">
		          	There are many different types of ‘Help’-s that a player can avail, while trying to search down words on the Moover maze. The first letter of the word can be revealed, the available time limit can be expanded, and, if the user is prepared to spend more coins, the entire word can be made to blink on the maze for 3 seconds. There is also the option to finding out the meaning of the word, and then searching it.
		         </p>
		         
		         <blockquote class="color blockmoovers">
		  	    	The patience levels of app users are low, and if a person gets stuck at any point in a word game, he is more than likely to abandon it altogether. On Moover, we have provided all types of assistance to players for finding the words they are after. All they have to do is spend coins wisely.
		  	    </blockquote>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/moovers1.png" alt="moovers">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">And What If The Coins Run Out?</h3>
		  	    
		         <p class="color">
		         	This is where the importance of the in-app purchase options in Moover come into the picture. Following the advice of the client, we made different packages of coins available for purchase to players at any time. By paying $35, a user can upgrade to the ad-free version of the app as well.
		         </p>
                 
                 <blockquote class="color blockmoovers">
		  	    	Determining the exact nature of in-app purchases for the Moover was slightly tricky. After several rounds of discussion with Hussain and his team, we decided to make slots of 250 coins, 350 coins, 550 coins and 750 coins, available for $10, $40, $80 and $120 respectively.
		  	    </blockquote>
                 
                 <p class="color">
                 	There are additional slots of 20, 40 and 60 coins - for buying the different types of ‘Help’-s.
                 </p>
                 
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/moovers3.jpg" alt="moovers"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/moovers4.jpg" alt="moovers"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/moovers5.jpg" alt="moovers"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/moovers6.jpg" alt="moovers"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/moovers7.jpg" alt="moovers"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/moovers8.jpg" alt="moovers"></div>
		  
		  </div>
		</div>
		<p></p>
		
		<div class="row">
		  <div class="col-lg-12">
		  
		  		<div class="col-lg-5">
		  	  		<center><img src="appstories/moovers2.png" alt="moovers" ></center>
		  	  </div>
		  	
		  	  <div class="col-lg-7">
		  	  
		  	  	<h3 class="color">Chat Feature & Multi-Language Support</h3>
		  	    
		         <p class="color">
		         	While a game is on, a player can interact with his/her opponent directly - with the help of the smooth in-app chatting feature of Moover. The ‘Chat’ button is present on the top-right corner of the ‘Score Board’ page of the iPhone word search app. As already mentioned above, each game is made up of 3 rounds, and a player can ‘Resign’ at the end of any round, or tap ‘Play’ to move to the next round. If neither is tapped, the opponent will get to see a ‘Waiting For…’ message.
		         </p>
                 
                 <blockquote class="color blockmoovers">
		  	    	Simple and engaging mobile word games like Moover should ideally be available to as large a user-base as possible. We had a chat with the client about this, and were delighted to find that he was willing to release the app in multiple languages. Moover is available in 4 other languages apart from English - Italian, French, German and Spanish.
		  	    </blockquote>
		  	  	<h3 class="color">Want A Level Up? You Have To Win Big!</h3>
		         <p class="color">
		         	To move up up by three levels, a player has to defeat his/her opponent by a whopping 200 points or more. A win margin of 120 points is good enough for a two-level jump, while if a user beats an opponent by anything between 60-119 points, (s)he moves up one level.
		         </p>
                 
                 <blockquote class="color blockmoovers">
		  	    	On Moover, the actual margin of winning and losing matters. The difference in points between the winner and loser determines how the former will be moved to the next levels. The bigger your win margins, the more levels you can clear in one go.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		  	    	Moover is armed with powerful social sharing features (including Twitter and WhatsApp). To resolve probable doubts and queries of first-time players, a dedicated FAQ page is present as well. The option to activate/deactivate notifications and vibrations add further customization to this cool new iPhone word search application.
		  	    </p>
		  	  </div>
		  	  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					
					<p class="color">
						Moover will soon be submitted at the Apple App Store, and we expect it to get featured at the store by the end of this quarter. It is a very interesting variant of the regular word search genre of mobile games, and it has the potential to become yet another #win for us!
					</p>
							
				</div>
		  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>