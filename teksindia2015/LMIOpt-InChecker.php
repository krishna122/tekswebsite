<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of LMI Opt-In Checker</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="LMIOpt-InChecker" style="height: 50%;">
        <div class="LMIOpt-InChecker-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">LMI Opt-In Checker</span></h1>
                    </div>
                 </div>
            </div>
        </div>
</header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>The best mobile apps are not necessarily the ones with the most features and a gamut of functionalities. At times, even a single-screen application can be remarkable - due to its advanced backend technology and general user-friendliness. The LMI Opt-In Checker app is a classic example of such applications. It is indeed a great addition to our 900-odd app portfolio.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/LMIOpt-InCheckerlogo.png" alt="LMI Opt-In Checker">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#aa2131;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
It is not the most complex app that me and my team of developers have worked on...far from it, in fact. But what LMI Opt-In Checker brought to the table was a new technology - something with which we had not full-fledgedly worked till date. All of us were excited about this.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>The Background</h4>
            <div class="col-lg-6">
               <img src="appstories/lmi.png" alt="LMI Opt-In Checker">
             </div>
             <div class="col-lg-6">
               <P>Built for the Android platform, LMI Opt-In Checker works with the help of cutting-edge RFID (Radio Frequency Distribution) technology. It is one of the very first RFID apps we started working on - and although there were a few initial hitches, we managed to complete it successfully. Our in-house Android app developers started out about a week after we had sent along the free app quote to the client.</p>
             </div>
         	 	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#aa2131;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
LMI was never meant to do a lot of things. It was, however, important that it should do well what it was supposed to do. Since RFID is a fairly new concept, we were at first sceptical of whether a third-party mobile app company would be able to create the app properly. Well done to the guys at Teknowledge - the certainly did not disappoint us!
                                <br><br>
                                <center>
				<br> 
				 
				<span style="font-size: 25px;">Concept Owner, LMI Opt-In Checker App</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>What Does LMI Opt-In Checker Do?</h4>
         	<P>‘Smart’ and ‘straightforward’ would be the two adjectives that suit this RFID technology based app the best. After downloading LMI Opt-In Checker from Google Play Store and installing it on their devices, all that users have to do is activate the app and bring the paired wristbands in front of it. The app would, in a contactless manner, read data off the wristbands, and display a screen - with details of all the prizes that the users have won. For checking prize information at a glance, this is just the right mobile application.
         	<br><br>
         	The profile information of individual users (i.e., the ‘core data’) is stored in a separate application. Based on that data, the prizes are determined - and accordingly, the display page of LMI Opt-In Checker gets generated. The prizes can also be categorized in two different types.
         	</p> 	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#aa2131;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
When an app does not match up to the expectations of a client, I consider that to be a personal failure. Fortunately, over the last 7-8 years, there have been precious few of such instances - and I made sure that LMI Opt-In Checker turned out to be an excellent app as well. It was a challenge to muster and implement a new technology...but we managed to do it with relative ease.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Real-time Mobile App Testing</h4>
         	<P>This was vital. The last thing we wanted while working with a high-end technology like RFID was a coding bug going unnoticed. The app prototype was tested at different stages, and the wireframes were shared with the owner of the application. We received several interesting suggestions for the app as well, many of which were implemented. By the time the app was complete, we could say with confidence that we had done something new.
         	<br><br>
         	LMI Opt-In Checker is in the final stage of testing, and will soon be released at the Google Play Store. It is a one-of-its-kind RFID mobile application - and it is the best possible proof that single-screen apps can be remarkably good too!
         	</p> 	
	   </div>
	</div>
     </div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>