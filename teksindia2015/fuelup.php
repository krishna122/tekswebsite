<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Story Of Fuelup</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="fuelup" style="height: 34%;">
        <div class="fuelup-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">fuelup</span></h1>
	                         <center>
		                         <a href="https://itunes.apple.com/us/app/fuelup-your-mobile-gas-station!!/id1104355539?ls=1&mt=8">
		                         	<img alt="Availablet on the Appstore" src="img/appstore.png">
		                         </a>
	                         </center> 
                       </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Ever felt the frustration when your car runs out of petrol during a ride, and there’s not a single fuel station nearby? Well, most vehicle-owners have - and the Fuel Up application would certainly help them heave a sigh of relief. With this innovative and fast-working iPhone fuel search app, refilling the vehicle tanks is never a tough ask!
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">What Is The App All About?</h3>
		  	    <br>
		  	    <blockquote class="color blockFuelup">
		  	    	The idea behind Fuel Up is simple. Random guy is driving, finds that he has run out of petrol, looks for other drivers in his locality, and sends out a fuel request to them. Someone responds to the request, delivers the fuel, gets paid...and it’s done. No gimmicks, only prompt service!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          After the app quote had been sent to the client, we organized three consultation sessions to grasp the concept of the Fuel Up app properly. Our in-house team of iPhone app developers, on the basis of given instructions, made the app compatible with iOS 8 and iOS 9 handsets (that is, iPhone 6 and iPhone 6S).
		         </p>
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-6">
		  		<center>
			  		<video autoplay loop muted  poster="appstories/Fuelupbg.jpg" style="border: 5px solid #82001d;">  
					   <source src="video/appstories/fuelup.mp4" type="video/mp4">  
					</video>
				</center>
		  	</div>
		  	
		  	<div class="col-lg-6">
		  		
		  		<h3 class="color">Getting On The Fuel Up Network</h3>
		  	    
		         <p class="color">
		         	A person stranded in the middle of (apparently) nowhere with an out-of-fuel car is already frustrated, and (s)he needs help quickly. We wholeheartedly agreed with the concept developer’s view that the registration/sign in feature needed to be as uncomplicated as possible. A couple of discussions later, we opted to add a social login option (via Facebook) in addition to the regular email registration/sign in feature.
		         </p>
                 
                 <blockquote class="color blockFuelup">
		  	    	The last thing that any driver wants after having run out of gas with not a fuel pump station in sight is having to fill up lengthy app registration forms on his/her mobile screen. The quick login feature of Fuel Up ensures that the concerned driver can start seeking help within a few minutes after launching the app.
		  	    </blockquote>
		   </div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">How Does Fuel Up Work?</h3>
		  	    
		  	    <p class="color">
		          As soon as a user fires up the iPhone fuel buying app on his/her handset, she can view all the other drivers currently present in the vicinity (the app works with the phone GPS to bring this information to the user). A fuel request has to be then sent out via the app. All the drivers in the locality who are also using Fuel Up will be notified of this request - and one of them will respond. 
		         </p>
		  	    <blockquote class="color blockFuelup">
		  	    	There is nothing elaborate or long-drawn about the Fuel Up application. Provided that the GPS of the user’s phone is working fine, he can see all fellow-drivers nearby, request for a refill from them, and proceed on their journey. I’d say it’s a lot easier than having to search for a petrol station and lugging the car over there.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          As soon as one driver responds to a fuel request, that person HAS to deliver the fuel to the concerned app-user. The request appears disabled for all the other drivers who had got the notification. 
		         </p>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<center><img src="appstories/fuelups4.png" alt="fuelup" width="60%"></center>
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">How Long Is The Wait?</h3>
		  		<br>
		         <blockquote class="color blockFuelup">
		  	    	A great thing about Fuel Up is that, it does not force drivers to pay up arbitrary, exorbitant amounts for the fuel that they desperately need. Depending on how quickly they want the petrol to be delivered...and obviously, the quantity required, the total chargeable amount is calculated. Buyers have a strong say in deciding what they have to pay.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          The more promptly a driver needs the fuel, the higher is the amount (s)he will have to fork out. For instance, someone who specifies on the app that the petrol has to be delivered within 1 hour has to pay more than, say, another person who needs the fuel within 3 hours. Quicker service translates to higher price!
		         </p>
                 
		  </div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
		  		<center><img src="appstories/fuelups6.png" alt="fuelup" width="67%"></center>
		    </div>
			  <div class="col-lg-6">
			  	
			  		<h3 class="color">Paying For The Fuel</h3>
			  	    
			  	    
			         <p class="color">
			         	The payment procedure on this iPhone fuel buying app is completely secure and user-friendly. Users have to provide their credit card information at the time of setting up their profile on the application. Once the fuel is delivered to a driver, the chargeable amount is debited from the latter’s credit card.
			         </p>
	                 
	                 <blockquote class="color blockFuelup">
			  	    	It is only natural that a person might feel apprehensive about paying money for fuel in advance. I mean, what if the fuel does not reach him/her at all? In Fuel Up, nothing has to be paid before delivery. An amount gets frozen from the buyer’s credit card when a fellow driver responds to his request...and the payment is made only after the fuel is delivered. One less thing for the petrol-buyers to worry about!
			  	    </blockquote>
	                 
	                 <p class="color">
	                 	If a person does not know the exact amount of petrol (s)he requires (i.e., ‘I need 2/5/10 litres of petrol’), that’s not a problem either. The user can simply give instruction to fill the tank of the car - and the fuel bill will be calculated on the basis of the quantity of fuel delivered. This, in turn, allows people to use Fuel Up to buy petrol BEFORE their cars run out of gas completely. Getting the tanks filled is easier than ever before with this app.
	                 </p>
			  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/fuelups1.jpg" alt="fuelup"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/fuelups3.jpg" alt="fuelup"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/fuelups2.jpg" alt="fuelup"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/fuelups5.jpg" alt="fuelup"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/fuelups11.jpg" alt="fuelup"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/fuelups10.jpg" alt="fuelup"></div>
		  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-6">
		  <br>
		  		<h3 class="color">Using Fuel Up As ‘Staff’</h3>
		  		<p class="color">A person can use Fuel Up as a general ‘User’ (for buying fuel) or a ‘Staff’ (for selling petrol). Till now, we had been discussing the app from the perspective of the ‘User’. Before summing up, let’s turn our attentions for a bit on how fuel-sellers can use the application:</p>
		  		
		         <blockquote class="color blockFuelup">
		  	    	To log in as a ‘staff’, a user needs to take permissions from the Administrator and the City Manager. Once these are obtained, the staff...who are drivers themselves, can start reaching out to people who need fuel and deliver the petrol.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          With the help of their device GPS, fuel-suppliers can view all the latest, unserved fuel demand requests in the locality. They can then pick and choose the fuel requests they would like to serve, and then receive the payments.
		         </p>
                 
		  </div>
		  
		  <div class="col-lg-6">
		  	 	<center><img src="appstories/fuelups7.png" alt="fuelup" width="60%"></center>
		  	 </div>
		  	 
		</div>
		
		<div class="row">
		<div class="col-lg-6">
		  	 	<center><img src="appstories/fuelups8.png" alt="fuelup" width="60%"></center>
		  	 </div>
		  <div class="col-lg-6">
		  	
		  		<h3 class="color">Enhanced Convenience For Every User</h3>
		  	    
		  	    
		         <p class="color">
		         	From top-notch GPS accuracy for locating fellow drivers, to the general screen appearances - everything has a seamless feel on this iPhone fuel-finder app. Using it is an absolute breeze.
		         </p>
                 
                 <blockquote class="color blockFuelup">
		  	    	At my mobile app company, we take pride in making apps that people can use without any problems. While working on the Fuel Up project, we were very careful about maintaining a high quality standard in the screen layouts, navigation and other visual elements. Transforming the smart app idea into a user-friendly app was the goal - and I daresay we did a good job.
		  	    </blockquote>
                 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">From The Backend</h3>
		  	    
		         <p class="color">
		         	How does the backend support/resources of FuelUp help users? Let’s take a look:
		         </p>
                 
                 <ul class="color">
                 	<li>
		         		<strong>Nearest Staff</strong> - Fuel-buyers can view the locational information of all the Staff (fuel-suppliers) in the locality.
		         	</li>
		         	<li>
		         		<strong>Updated fuel prices</strong> - From the admin panel, fuel prices are updated real-time on the app. The actual payable price varies with the type of fuel and the desired promptness of delivery.
		         	</li>
		         	<li>
		         		<strong>Payment gateway</strong> - FuelUp has a built-in, completely secure payment flow mechanism. 
		         	</li>
		         	<li>
		         		<strong>Past/Present fuel orders</strong> - Clients/Buyers can easily check out all the previous fuel orders placed by them via this iOS application. Current order details are also updated & viewable.
		         	</li>
		         </ul>
                 
		  </div>
		  
		  <div class="col-lg-12">
		  	<img alt="fuelup" src="appstories/fuelupadmin.jpg">
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
						Fuel Up is currently being checked by our mobile app testing department, and it will debut at the iTunes Store soon. It addresses a very common concern of drivers - the sudden, unforeseen need for fuel - and with top-class features, the app should soar popularity levels easily.
					</p>	
				</div>
		  </div>
		</div>
	
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>