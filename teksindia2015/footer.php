<!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="text-team" style="text-transform: uppercase;">Copyright &copy; Teksmobile <?php echo date("Y") ?></span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/teknowledge_s" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/TeknowledgeMobileStudio" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/company/teknowledge-llc" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks" style="text-transform: uppercase;">
                        <li><a href="privacypolicy.php" class="text-team">Privacy Policy</a>
                        </li>
                        <li><a href="terms.php" class="text-team">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
    <style>
		
    ul.list-inline {
    margin-top: 0;
    margin-bottom: 30px;
}

       footer {
    padding: 10px 0;
    text-align: center;
    font-size: 13px;
}

footer span.copyright {
    text-transform: uppercase;
    text-transform: none;
    font-family: "Source Sans Pro",sans-serif;
    line-height: 28px;
    
}

footer ul.quicklinks {
    margin-bottom: 0;
    text-transform: uppercase;
    text-transform: none;
    font-family: "Source Sans Pro",sans-serif;
    line-height: 28px;
    font-size: 13px;
}

ul.social-buttons {
    margin-bottom: 0;
}

ul.social-buttons li a {
    display: block;
    width: 28px;
    height: 28px;
    border-radius: 100%;
    font-size: 14px;
    line-height: 28px;
    outline: 0;
    color: #fff;
    -webkit-transition: all .3s;
    -moz-transition: all .3s;
    transition: all .3s;
}

.facebook{
	background: #43609C;
}
		
.twitter{
	background: #55ACEE;
}
		
.linkedin{
	background: #1D87BE;
}
		
		


ul.social-buttons li a:hover,
ul.social-buttons li a:focus,
ul.social-buttons li a:active {
  border: 2px solid transparent;
}

	</style>
	
	
	
	