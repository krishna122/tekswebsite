<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all iPhone Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="appstories" style="padding: 8% 0;">
        <div class="appstories-body" style="margin-top: 5%;">
            <div class="project-container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">iPhone Apps.</h1>
                    </div>
                 </div>
                   <div class="row">
                    <div class="col-md-12">
                        <center><p style="color:#fff; font-weight: 100; ">Storing up your new iPhone? Now cut to the chase with the most interesting, essential and must - have iOS apps. Make your phone the ultimate productive device with the wide range of our applications.</p></center>
                    </div>
                </div>
<br/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="#appstory" class="page-scroll" style="text-align: center;">
                        	<span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </header>
	
	<section id="appstory" class="fuelup">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12"><br><br>
					  <div class="col-lg-6">
	
					  	<span style="color: #fff; font-size: 25px; text-align: center;">Fuelup</span><br>
					  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Fuel-buying app</span>
					  	<br><br>
					  	<p style="padding: 0px;">Fuel Up is a mobile fuel-buying app, optimized for the iOS 8 and iOS 9 platforms. Via this GPS-powered app, users can find fellow drivers in their locality, and put in fuel purchase requests to them. The user-friendly driver's app makes car rides free of fuel-related worries!</p>
					  <br><br>
					  <a href="fuelup.php"><img src="img/view-project.png"></a><br><br>
					  </div>
	
					<div class="col-lg-6 storiesimg">
					  <img src="appstories/Fuelup.png" align="center">
					</div>
	
			  </div>
			</div>
		</div>
	</div>
	</section>
	
	<section id="appstory" class="dimes">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Dimes</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Social app</span>
				  	<br><br>
				  	<p style="padding: 0px;">Dimes is a custom iPhone app that puts a fresh new fun spin on campus life. There are many exciting polls in the app, and users can rank his/her friends on them. Dimes comes with in-app chat functionality too.</p>
				  <br><br>
				  <a href="dimes.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dimes.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>
    
    <section id="appstory" class="flypal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Flypal CRS</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Aviation app</span>
				  	<br><br>
				  	<p style="padding: 0px;">FlyPal CRS is an iOS/Android app geared to make aviation management smarter and more efficient than ever before. The app facilitates prompt and real-time communication between administrators, operators and crew members, and provides detailed reports and document access too. A winning app for flight managers!</p>
				  <br><br>

				  <a href="flypal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/flypal.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="deal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">365deal</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Business app</span>
				  	<br><br>
				  	<p style="padding: 0px;">365 Deal is an exclusive iOS ad-sharing app for sellers. Two different types of ads can be posted on this user-friendly application. All ads are information-rich, and the contact details of sellers are provided with each advertisement.</p>
				  <br><br>

				  <a href="365deal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/365.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="dbt">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">DBT</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Lifestyle app</span>
				  	<br><br>
				  	<p style="padding: 0px;">DBT is a comprehensive, multi-layered iPhone self-help application. Right from creating lists of skills and crisis/problems and updating them on a daily basis, to tracking key health parameters - the app lets users perform a wide range of tasks.</p>
				  <br><br>

				  <a href="dbt.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dbt.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="moovers">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Moovers</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Word Game</span>
				  	<br><br>
				  	<p style="padding: 0px;">Moover is a two-player iOS word-search application. Users have to swipe along word mazes to find certain words, before their opponents manage to do so. Additional coins are available through in-app purchases.</p>
				  <br><br>

				  <a href="moovers.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/m.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="currently">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Currently</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Audio clip sharing app</span>
				  	<br><br>
				  	<p style="padding: 0px;">Currently is an innovatively conceptualized audio-clip sharing app for iOS and Android devices. On the app, users can record sound clips and share them in their ‘Stream’ or in the ‘Ocean’.</p>
				  <br><br>
				  <a href="currently.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/currently.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

	<section id="appstory" class="onebrand">
	<div class="container">

	<!--  <h1 style="margin-top: 0px;">iPHONE APPS</h1><br> -->
               <div  style="width:100%;"><br/><br/>
	  	<div class="row">
	  		<div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">One Brand</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Mobile Commerce</span>
				 	<br><br>
				  	<p style="padding: 0px;">Probably the best mobile shopping app in our portfolio. From browsing through product categories to purchasing items and making payments - One Brands has all the requisite m-commerce features. The graphics and design layout make it a really user-friendly app too!</p>
				  	<br><br>
				  	<a  href="onebrand.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/onebrand.png" align="center">
				</div>
		  </div>
		</div>
                </div></div>
          </section>


<section id="appstory" class="timesnaps">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		<div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Timesnaps</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Photo/Video</span>
				  	<br><br>
				  	<p style="padding: 0px;">For those who wish to capture, preserve and treasure the magic of fleeting moments, Timesnaps is a must-have application.  The app allows people to take images of practically anything at regular intervals, create slideshows with them, and revel at the changes that time can make!</p>
				  <br><br>
				  	<a href="timesnaps.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/timesnaps.png" align="center">

				</div>
		  </div>
		 </div></div>
</section>

<section id="appstory" class="babysitter">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		 <div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Baby Sitter</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">App For Parents/Babysitters</span>
				  	<br><br>
				  	<p style="padding: 0px;">Hiring an expert professional babysitter becomes easier than ever before with this revolutionary iOS application. Parents can fix appointments with babysitters, specify the hourly rates they are willing to pay, and hire the best caregiver for their children - all with the Baby Sitter app.</p>
				  <br><br>
<!-- <a class="btn btn-default viewproject" href="babysitter.php" data-toggle="modal" data-target="#myModal">View Project</a> -->
				  <a href="babysitter.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/babysitter.png" align="center">

				</div>
		  </div>
		</div></div></div>
</section>


<section id="appstory" class="speedykey">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">SpeedyKey</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Virtual Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;">A third-party iOS keyboard app par excellence. Available in three different languages and four different themes (via in-app purchase), SpeedyKey makes typing on iPhones/iPads quicker, easier and more accurate. Its USP? The host of custom 'Speedy Replies'!</p>
				  <br><br>

				  <a href="speedykey.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/speedykey.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="veeaie">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Veeaie Keyboard</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;">The key USP of Veeaie Keyboard is its host of pre-written custom replies, for emails and messages. With this third-party iOS keyboard app, staying in touch and responding to messages becomes quicker than ever.</p>
				  <br><br>
				  <a href="veeaie.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/veeaie.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="bender">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Bender</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="bender.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/bender.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="freebird">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Freebird</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="freebird.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/freebird.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="inslideout">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">InslideOut</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Social Networking App</span>
				  	<br><br>
				  	<p style="padding: 0px;">Live performances, sports events, concerts - all these and more arrive right at your fingertips, when you have the Inslide Out application. This is one events app-meets-mobile ticketing app you will love to have.</p>
				  <br><br>

				  <a href="inslideout.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/inslideout.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>


<section id="appstory" class="lifestyle">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Lifestyle Travel</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Travel App</span>
				  	<br><br>
				  	<p style="padding: 0px;">The perfect mobile tour planner, information provider and real-time helper - all rolled into one. Conceptualized by Leonie Spencer - a long-time travel fan herself - this is an app that every travel-lover would love.</p>
				  <br><br>

				  <a href="lifestyle.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/lifestyle.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="icbf">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">I Can Be Anything</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Motivational App</span>
				  	<br><br>
				  	<p style="padding: 0px;">A multi-featured stress-reliever app, with as many as 12 different suites. Conceptualized by Jacob and Ditte Strachotta and optimized for the iOS and Android platforms, the app has several free sessions and easy in-app purchase options.</p>
				  <br><br>

				  <a href="icba.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/icbflogo.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="appbattles">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">App Battles</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">The real-time multiplayer iOS fighting game that will send your pulses racing. Take your pick from the multiple available avatars, learn how to use the weapons and special powers available, and let’s get down to a fight to the finish!</p>
				  <br><br>

				  <a href="appbattles.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/appbattles.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="princeofasia">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Prince Of Asia</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">How about donning the role of a prince, and rescuing a charming princess? Get set for Prince Of Asia - the strategy-based, multi-level role-playing game that merges fantasy fun with thrilling adventure. An app our game development agency is very proud of!</p>
				  <br><br>

				  <a href="princeofasia.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/princeofasia.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<section id="appstory" class="rst">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="color: #fff; font-size: 25px; text-align: center;">Ready Set Text</span><br>
				  	<span style="color: #f8f8f8; font-size: 16px; text-transform: uppercase;">Typing Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;">Ready Set Text is a fun multiplayer mobile typing game for the iOS and Android platforms. Players earn coins by winning typing games, and can then use the coins to win attractive prizes. Both ‘Crowd Games’ and ‘Two-person games’ can be played in the Ready Set Text app.</p>
				  <br><br>

				  <a href="readysettext.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/rst.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>