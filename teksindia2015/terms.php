<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Privacy Policy</title>

	<?php include 'head.php';?>

</head>


<body data-spy="scroll">

<?php include 'header.php';?>
	
<!-- Intro Header -->
    <header class="privacy" style="height: 34%;">
        <div class="privacy-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Terms & Conditions</span></h1>
                       </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
	<div class="project-container">
	  <div class="row">
           <p>Prior to availing the services of Teknowledge Mobile Studio, requesting for free app quotes, or contacting our developers for any purpose, kindly go through the ‘Teks Terms Of Service’, as listed below:</p>
           <ul style="margin-left:20px;">  
           <li>All terms, clauses and conditions mentioned herein are equally binding on us (Teknowledge Software) and you (the client).</li>
<li>All media content (images and videos) and text content present on this website are copyrighted. Unauthorized access, download, reproduction, sale and other commercial usage of the same is prohibited and punishable in a court of law.</li>
<li>We are not responsible for any false/misleading/erroneous information about our company published on the internet. Neither are the advertisements on our website endorsed by our team of developers.</li>
<li>In case of any conflicts/confusion between information present on this website and on other portals, the former would prevail.</li>
<li>Clients/App Users are required to have valid accounts in Apple iTunes, Google Play Store, Blackberry App World and the Windows Store.</li>
<li>Teknowledge focuses on creating ORIGINAL iOS/Android apps. Deliberately using the names, features and/or core functionality of any of the applications in our portfolio is a punishable offence.</li>
<li>Kindly note that the prices stated in our free app quotes do not include additional carrier charges. Users are advised to find out about their device settings before installing any of our applications. We cannot be held responsible for additional charges (if any).</li>
<li>If your handset is jailbroken (iOS) or rooted (Android), the performance of some of our applications can be adversely affected. Bear in mind that our developers create apps in accordance to the standard iPhone/Android settings. Performance glitches due to defects in/customization of devices cannot be traced back to us.</li>
<li>Teknowledge has an age-barrier while negotiating with clients. You might be required to reveal your age, before we provide a free app quote to you. In case you wish to get a mobile app for kids developed, kindly state your age in your first correspondence.</li>
<li>You are requested to go through the Privacy <a href="privacypolicy.php">Policy</a> of our company before hiring our services. In order to get quotes on apps, you will need to provide certain basic personal information.</li>
<li>We take full responsibility for any of our mobile apps that turn out to be defective (in any way). Kindly note though that if an app’s performance is affected due to misuse/wrong use, that is your sole responsibility.</li>
<li>You DO NOT have the permission to sell our apps to other third-party business entities (unless otherwise stated). Apps by Teknowledge are meant for personal, non-profit-seeking use.</li>
<li>We encourage our clients to kindly provide their feedback on our services, along with their valuable suggestions and recommendations. The same would help to improve the services of our Android/iPhone app company further.</li>
<li>If you have justifiable causes to believe that your mobile applications are being used by unauthorized third-party entities and/or your intellectual property rights have been violated, kindly lodge a THEFT OF SERVICE complaint at our company.</li>
<li>We do not entertain communication between clients and individual mobile app developers at our company. If you have any query, request or any other requirement, kindly call 91-33-40649087 or get in touch via email (info@teks.co.in). A Teks representative will get back to you within 24 working hours.</li>

           </ul>

         </div>
 </div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>