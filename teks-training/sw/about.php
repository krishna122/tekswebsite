<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | About Us</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider-aboutus.php' ?>
  
  
  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="big-title"><span style="font-size: 40px; text-transform: uppercase;">HISTORIEN OM TEKS</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Teks har ett team hängivna utbildningsamordnare inom mobil och webbutveckling, inriktade på att hjälpa dig att bli framgångsrik inom detta område. Vi har anordnat klasser och erbjudit utbildningar sedan 2006, när iPhone ännu inte var framtagen. Vårt fokus på den tiden var Java applikationsutveckling. Idag har vi över 60 kurser med  iOS och Android - utveckling, 15 webbutbildningar (inklusive HTML och CSS -kurser) och över 120 instruktörer. Det har varit en spännande resa och som har format karriärer och skapat många nya och kompetenta utvecklare.</p>
              
              <!-- <div class="margin-top"></div> -->
              

            </div>

            <div class="col-md-6">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-03.JPG">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
  
  
  <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div style="text-transform: uppercase;">
            <h1 class="big-title text-center"><span style="text-transform: uppercase;">VAD ÄR DET SOM GÖR ATT VÅR UTBILDNING STICKER UT? </span></h1>
          </div>
          <!-- End Big Heading -->
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/corporate/1.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>VÄLJ DIN KURS </h2><br>
              <p>Vi presenterar skräddarsydda kurser om hur man gör iOS-appar , Android-appar och professionella / personliga hemsidor. Du kan välja mellan ett stort utbud av kurser som vi har att erbjuda .</p>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/corporate/2.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>LÄR DIG FRÅN EXPERTERNA</h2><br>
              <p>Vid TEKS institut , får varje elev interagera med de bästa webb och app-utvecklare i Indien . I vårt team ingår även entreprenörer från Australien och Sverige .</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/corporate/3.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>KARRIÄRORIENTERADE </h2><br>
              <p>Våra iOS / Android / web utbildningsmoduler är utformade för att hjälpa dig att bli en professionell utvecklare. Flera av våra seniora mentorer är idag placerade hos företag runt om i världen. </p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
  
   <div class="section courses-white">

      <div class="container" >
        
        <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-02.jpg">
                </div>
              
            </div>
            
        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>GENOMFÖR DIN UTBILDNING HOS TEKS </span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Vårt institut erbjuder en gedigen utbildning inom applikationsutveckling och webbsidor. Vid Teks, får du arbeta under ledning av några av de bästa IT-guiderna i Indien. Du får lära dig att skapa projekt på de senaste enheterna (från iPhone 6S till Mac OS X 10.11 system), och lära sig att använda de senaste ramarna och verktygen för att skapa program. Vi håller oss alltid uppdaterade med den senaste tekniken för att säkerställa bästa möjliga utbildning för varje elev. Det är inte utan anledning vi betraktas som ett prestigefullt utbildningsinstitut, både i  Indien och utomlands.</p>

            </div>

      </div>
      <!-- .container -->
  </div>
  
    <!-- Start Services Section -->
    <div class="section service" style="background: #fecc02;">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div style="text-transform: uppercase;">
            <h1 class="big-title text-center"><span style=" text-transform: uppercase;">DIN UTBILDNING, DITT LIV, DITT VAL</span></h1>
            <p>VID TEKS UTBILDNINGSCENTER KAN DU VÄLJA BLAND ETT STORT UTBUD AV UTBILDNINGAR OCH KURSER. VI SKRÄDDARSYR DIN UTBILDNING MED HÄNSYN TILL DINA FÖRKUNSKAPER OCH VILKEN TID DU HAR ATT AVSÄTTA TILL DITT LÄRANDE. HÄR FINNS NÅGOT FÖR ALLA.</p>
          </div>
          <!-- End Big Heading -->
          <br>
          <!-- Start Service Icon 1 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/parttime.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>HELTIDSSTUDIER (8 - 12 VECKOR) </h2><br>
              <p>Vi erbjuder heltidskurser för nybörjare. Du får då lära dig hur man gör en app från scratch. Vi erbjuder även separata utbildningsmoduler för företag. </p>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/fulltime.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>DELTIDSKURSER  (2 - 6 VECKOR) </h2><br>
              <p>Dessa kurser liknar intensivkurser på webben och är idealiska för yrkesverksamma som vill ta sitt lärande gällande exempelvis applikationsutveckling ett steg längre. </p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/seminar.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>SEMINARIER </h2><br>
              <p>Branschseminarier och paneldiskussioner är vanliga inslag på alla kurser som erbjuds av vårt institut. Att ta del av dessa seminarier hjälper eleverna att direkt interagera och lära av mentorerna. </p>
            </div>
          </div>
          <!-- End Service Icon 3 -->
          
          <!-- Start Service Icon 4 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/workshop.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>WORKSHOPS </h2><br>
              <p>Teks organiserar workshops i olika städer med jämna mellanrum för att ge dig chansen att arbeta på "dummy"projekt och testa kod, för att bli en mer kvalificerad utvecklare.</p>
            </div>
          </div>
          <!-- End Service Icon 4 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    
  
  <div class="section courses">

      <div class="container" >
        <h1 class="big-title" style="text-align: center;"><span>OKEJ, NU ÄR DET NOG OM OSS!</span></h1>
        
        <div class="call-action call-action-boxed call-action-style1 clearfix">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:8px;"><a href="request-more-info.php" class="btn-system btn-large" id="color">SÖK TILL EN KURS!</a></div>
            <h2 class="primary">LÅT OSS BÖRJA MED ATT KIKA PÅ WEBB OCH APPKURSER SOM ÄR LÄMPLIGA FÖR DIG!</h2>
          </div>
          
      </div>
      <!-- .container -->
  </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#about').addClass('active');
	});

</script>

</body>

</html>