<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Corporate Training</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider-corporatetraining.php' ?>
  
  
  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>VI FORMAR KARRIÄREN, UNDERHÅLLER DINA KUNSKAPER OCH  TILLHANDAHÅLLER FÖRETAGSUTBILDNINGAR</span></h1>
              
             
              <!-- Some Text -->
              <p>De utbildningsmoduler inom mobil/ webbutvecklings som erbjuds av Teks är alla inriktade på att hjälpa dig att skapa ett varumärke som expertutvecklare i den professionella världen. Våra instruktörer  är några av de vassaste i branchen. De hjälper dig att bekanta dig med den senaste branschstandardern, kodningsmetoder och utvecklingstekniker. Särskild uppmärksamhet läggs på att göra mobila applikationer för de senaste plattformarna (iOS 9, Android 6.0 Marshmallow ), samt att skapa webbplatser med olika verktyg (Drupal, Wordpress, Joomla, Magento). Vi har även särskilda sessioner gällande felsökning och klientfrågehantering.</p>
              

            </div>

            <div class="col-md-6">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/corporate/4.jpg">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
  
  
  <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1><strong>TEKS</strong> FÖRETAGSUTBILDNING</h1>
          </div>
          <!-- End Big Heading --><br>
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/corporate/1.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>IOS UTVECKLING</h2><br>
              <p>Avancerade kurser inom applikationsutveckling och utbildning för iOS 9 plattform med hjälp av ramverket Xcode 7 </p>
			  <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151; background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/corporate/2.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>ANDROID - UTVECKLING</h2><br>
              <p>Anmäl Dig/Er till en utvecklingskurs för Android och lär dig att göra en effektiva och engagerande Android applikationer.</p>
              <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151;background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/corporate/3.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>WEBBUTVECKLING </h2><br>
              <p>Delta på lektioner, seminarier och demo-sessioner med fokus på utveckling och design av företagswebbplatser. Inriktning CSS och HTML - utbildning </p>
              <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151;background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
  
   <div class="section courses-white">

      <div class="container" >
        
        <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/corporate/5.jpg">
                </div>
              
            </div>
            
        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>HUR KAN TEKS UTBILDNING GÖRA SKILLNAD? </span></h1>
              
              
              <!-- Some Text -->
              <p>Webb/appkurser kurser samt handledning som anordnas av Teks kompletterar din utvecklingskompetens på bästa möjliga sätt. Beroende på vad som passar just Dig och dina krav på lärande kan du välja från våra heltids (8 -12 veckor) och deltids (2 - 6 veckor) kurser. Förutom våra appkurser för nybörjare, erbjuder vi företagsutbildningar som omfattar avancerade ämnen och tekniker för både iOS och Android - utveckling. Du får också lära dig appkodning av några av de bästa utvecklarna på marknaden. Teks är inte bara ett lärande institut - Vi är din inlärningspartner!</p>

            </div>

      </div>
      <!-- .container -->
  </div>
  
  <div class="section" style="background: #fecc02;">

      <div class="container" >
        
        <h4 class="classic-title"><span>MÖT MENTORERNA</span></h4>
        
        <p id="color">På Teknowledge Software får du lära av internationellt hyllade app-utvecklare , programvara testare , programmerare , webbutvecklare , UI / UX experter och andra toppnivå IT-proffs . Vi träffas en del av personalen som ansvarar för företagets utbildningar .</p>
        
        <div class="row">
           
           
           
            <!-- Start Memebr 1 -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/team/ved.JPG" style="border-radius:50%; width:80%;">
                  <div class="member-name">Ved Prakash <span>iOS Trainer</span></div>
                </div>
                <!-- Memebr Words -->
                <div class="member-info">
                  <p id="color">Mobilappsentreprenör, med nästan ett decennium av branscherfarenhet. Han började med Javautveckling, och är nu en erfaren och uppskattad iOS/Android utvecklare .</p>
                </div>
              </div>
            </div>
            <!-- End Memebr 1 -->

            <!-- Start Memebr 2 -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/team/bharavi.jpg" style="border-radius:50%; width:80%;">
                  <div class="member-name">Bhairavi Chavan <span>Android Trainer</span></div>
                </div>
                <!-- Memebr Words -->
                <div class="member-info">
                  <p id="color">Skaparen av prisbelönta Story Time for Kids (iOS och Android). Genomför regelbundet utbildningar inom applikationsutveckling vid olika institut i Asien.</p>
                </div>
              </div>
            </div>
            <!-- End Memebr 2 -->

			
          </div>

      </div>
      <!-- .container -->
  </div>
  
  <div class="section courses">

      <div class="container" >
        <h1 class="big-title" style="text-align: center;"><span style=" text-align: center;">BOOSTA DIN KARRIÄR HOS TEKS</span></h1>
        
        <div class="call-action call-action-boxed call-action-style1 clearfix">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:8px;"><a href="request-more-info.php" class="btn-system btn-large" id="color">Välj en kurs</a></div>
            <h2 class="primary">Anmäl dig på en kurs, och låt oss hjälpa dig att bli en förstklassig professionell mobil / webbutvecklare</h2>
          </div>
          
      </div>
      <!-- .container -->
  </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#corporate').addClass('active');
	});

</script>

</body>

</html>