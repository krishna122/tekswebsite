<!-- Start Services Section -->
    <div class="section service courses" id="android" style="display: non;">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="text-align: center;text-transform: uppercase;">ANDROID APPUTVECKLINGSUTBILDNING HOS TEKS</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/18.png" />
            </div>
            <div class="service-content">
              <h4>JOBBA MED ANDROID STUDIO</h4>
              <p>Lär dig att göra egna Android-appar med verktyg och API: er av den senaste versionen av Android Studio. Experter inom området  finns till hands för att guida dig. </p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/19.png" />
            </div>
            <div class="service-content">
              <h4>JAVA PRPROGRAMMERING</h4>
              <p>Vi erbjuder Java programmeringskurser för Android applikationsutveckling. Du lär dig koda med Java och använda Android SDK för att göra program och appar. </p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/20.png" />
            </div>
            <div class="service-content">
              <h4>ANDROIDUTBILDNING FÖR ALLA</h4>
              <p>"Teks erbjuder Android kurser för nybörjare och erfarna. Vi lär dig att göra Android-appar med den senaste tekniken."</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    
    
    
    
    
    
<!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center">ANDROIDUTBILDNING HOS TEKS</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">Vi på Teks erbjuder Branchorienterade kurser med praktisk utbildning inom Androidutveckling. </p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/h1.png" />
            </div>
            <div class="service-content">
              <h4>VÅRA UTVECKLARE FUNGERAR SOM GUIDER</h4>
              <p>Bli utbildad och guidad i varje skede av några av de bästa Androidutvecklarna i Indien. Arbeta på dummy projekt och förbättra dina programmeringskunskaper.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/v1.png" />
            </div>
            <div class="service-content">
              <h4>MER ÄN BARA KLASSRUMSUNDERVISNING</h4>
              <p>Våra seminarier, paneldiskussioner och praktiska workshops är betydande för att hjälpa dig att få en relevant erfarenhet av att utveckla Android-appar.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/bh1.png" />
            </div>
            <div class="service-content">
              <h4>FLEXIBLA KURSER</h4>
              <p>För din bekvämlighet har vi hel och deltidskurser och moduler. Läs igenom kursplanen och anmäl dig till den kurs som passar dig bäst.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->