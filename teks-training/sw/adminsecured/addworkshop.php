<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php include 'header.php'; ?>
      
      <?php include 'sidebar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 style="text-align: center;">
           ADD WORKSHOPS HERE
          </h1>
          <BR><BR>
        </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">ADD WORKSHOPS</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label>COURSE NAME</label>
                      <input type="text" class="form-control" placeholder="Enter email">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
              
              
              <!-- VIEW ADDED LOCATION -->
              
              <section class="content-header">
		          <h1 style="text-align: center;">
		           VIEW WORKSHOPS HERE
		          </h1>
		          <BR><BR>
		        </section>
        
        
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">VIEW ADDED WORKSHOPS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>COURSE NAME</th>
                      
                      <th style="width: 40px">EDIT</th>
                      <th style="width: 40px">DELETE</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>Kolkata</td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Pune</td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Stockholm</td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Sydney</td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
                </div><!-- /.box-body -->
              </div>
              
              
            </div>
            
          </div>   <!-- /.row -->
        </section>
        
        
      </div><!-- /.content-wrapper -->
      
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php include 'script.php'; ?>
  </body>
</html>