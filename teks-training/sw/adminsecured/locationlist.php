<?php
include ('userclass.php');
$userObj = new User();
$returnVal = $userObj -> getAllLocations();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php
	include 'header.php';
 ?>
      
      <?php
	include 'sidebar.php';
 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
              		<button class="btn btn-block btn-flat pull-right" style="width:auto; background:#fd702b;color:#fff;" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 data-toggle="modal" data-target="#addLocationModel"><i class="fa fa-plus-circle"></i> ADD LOCATION</button>
                </div>
          <BR><BR>
        </section>
        
<!-- Modal  for add new location -->
<div id="addLocationModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ADD LOCATION</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>LOCATION NAME </label>
           <input type="text" name="addlocationname" id="addlocationname" class="form-control" placeholder="ENTER LOCATION NAME" required="required">
        </div>
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="return addLocation();">ADD</button>
      </div>
    </div>
  </div>
</div>


<!------------------- Function to update location name  ------------------------------------------------------------------------>
<div id="editLocationModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="updatelocationheader">UPDATE LOCATION</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>LOCATION NAME </label>
           <input type="text" name="updatedlocationname" id="updatedlocationname" class="form-control" placeholder="ENTER LOCATION NAME" required="required">
           <input type="hidden" name="updatedlocationid" id="updatedlocationid" >
        </div>
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="return updateLocation();">UPDATE</button>
      </div>
    </div>
  </div>
</div>
       
       
       
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW LOCATIONS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	
    			<?php if($returnVal != 0) { ?>
                		
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10%">LOCATION ID</th>
                      <th>LOCATION NAME</th>
                      <th style="width: 5%">EDIT</th>
                      <th style="width: 5%">DELETE</th>
                    </tr>
                    
                    <?php while($data = mysql_fetch_assoc($returnVal)) { ?>
                    <tr id="<?php echo $data['locationid']; ?>">
                      <td><?php echo $data['locationid']; ?>.</td>
                      <td><?php echo $data['locationname']; ?></td>
                      <td>
                       <i class="fa fa-pencil fa-2x btn getLocationName" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 data-toggle="modal" data-target="#editLocationModel"  data-locationname="<?php echo $data['locationname']; ?>" data-locationid="<?php echo $data['locationid']; ?>"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x btn"  onclick='deleteLocation(<?php echo $data['locationid'] ?>)'></i>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                
                <?php  } else {
					echo '<center><h3>No data found! </h3></center>';
					}
				?>
                </div><!-- /.box-body -->
              </div>
            </div>
            
          </div>   <!-- /.row -->
        </section>
        
        
      </div><!-- /.content-wrapper -->
      
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php
	include 'script.php';
 ?>
    
    <script>
		$("button").click(function() {
			$("p").show("slow");
		});

		//Show pop up (model) with location name and hidden location id to update 
		$(document).on("click", ".getLocationName", function() {
			var locationname = $(this).attr('data-locationname');
			var locationid = $(this).attr('data-locationid');
			$('#updatedlocationname').val(locationname);
			$('#updatedlocationid').val(locationid);
		});

		//function for delete location
		function deleteLocation(locationid) {
			var confirmDel = confirm('Are you sure you want to delete this location?');
			if (confirmDel) {
				$.ajax({
					url : 'ajax.php',
					data : {
						locationid : locationid,
						type : 'deletelocation'
					},
					type : 'post',
					success : function(output) {
						alert('Location is deleted successfully!');
						location.reload();
					}
				});
			}
		}
	
		//function to update location detail
		function updateLocation(){
			var locationid = $('#updatedlocationid').val();
			var locationname = $('#updatedlocationname').val();
			$.ajax({
				url : 'ajax.php',
				data : {
					locationname : locationname,
					type : 'updatelocation',
					locationid : locationid
				},
				type : 'post',
				success : function(output) {
					if (output == 0) {
						alert('Location is already exist!');
						return false;
					} else {
						alert('Location is updated successfully!');
						location.reload();
					}
				}
			});
		}
		
		//function for add location
		function addLocation() {
			var locationname = $("#addlocationname").val();
			$.ajax({
				url : 'ajax.php',
				data : {
					locationname : locationname,
					type : 'addlocation'
				},
				type : 'post',
				success : function(output) {
					if (output == 0) {
						alert('Location is already exist!');
						return false;
					} else {
						alert('Location is added successfully!');
						location.reload();
					}
				}
			});
		}


          $("#locationhover").attr('class', 'treeview active');
          $("#locationhover").attr('class', 'active');
          $("#locationhover").attr('style', 'display:block');
	</script>

  </body>
</html>