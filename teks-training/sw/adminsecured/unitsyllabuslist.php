<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

// if (!$_SESSION("employee_id")) {
	// header("Location:index.php");
	// exit ;
// }

include ('userclass.php');
$userObj = new User();

$returnVal = $userObj -> getAllSyllabus();
$allUnits = $userObj -> getAllUnit();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php
	include 'header.php';
 ?>
      
      <?php
	include 'sidebar.php';
 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
              		<button class="btn btn-block btn-flat pull-right" style="width:auto; background:#fd702b;color:#fff;" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addSyllabusModel"><i class="fa fa-plus-circle"></i> ADD SYLLABUS</button>
                </div>
          <BR><BR>
        </section>
        
<!-- Modal  for add new unit -->
<div id="addSyllabusModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ADD SYLLABUS</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>HEADING (EN)</label>
           <input type="text" name="addheading_en" id="addheading_en" class="form-control" placeholder="ENTER HEADING (EN)" required="required">
        </div>
        <div class="form-group">
           <label>DESCRIPTION (EN)</label>
           <textarea id="adddescription_en" name="adddescription_en" class="form-control"></textarea>
        </div>
        
        
        <div class="form-group">
           <label>HEADING (SW)</label>
           <input type="text" name="addheading_sw" id="addheading_sw" class="form-control" placeholder="ENTER UNIT NAME (SW)" required="required">
        </div>
        <div class="form-group">
           <label>DESCRIPTION (SW)</label>
            <textarea id="adddescription_sw" name="adddescription_sw" class="form-control"></textarea>
        </div>
        
        <div class="form-group">
           <label>HEADING (DE)</label>
           <input type="text" name="addheading_de" id="addheading_de" class="form-control" placeholder="ENTER UNIT NAME (DE)" required="required">
        </div>
        <div class="form-group">
           <label>DESCRIPTION (DE)</label>
         <textarea id="adddescription_de" name="adddescription_de" class="form-control"></textarea>
        </div>
        
        <div class="form-group">
           <label>SELECT UNIT</label>
          <select class="form-control" id="addunitid" name="addunitid">
          	<?php while($unit = mysql_fetch_assoc($allUnits)) { ?>
          		<option value="<?php echo $unit['unitid']; ?>"><?php echo $unit['title_en']; ?></option>
          		<?php } ?>
          </select>
        </div>
		
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="addSyllabus();">ADD</button>
      </div>
    </div>
  </div>
</div>


<!------------------- Function to update course name  ------------------------------------------------------------------------>
<div id="editUnitModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >UPDATE COURSE</h4>
      </div>
      <div class="modal-body">
      	    <input type="hidden" name = 'editunitid' id="editunitid" >
      	    <input type="hidden" name = 'courseID' id="courseID" >
      	    
      	    <div class="form-group">
           <label>UNIT NAME (EN)</label>
           <input type="text" name="edittitle_en" id="edittitle_en" class="form-control" placeholder="ENTER UNIT NAME (EN)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (SW)</label>
           <input type="text" name="edittitle_sw" id="edittitle_sw" class="form-control" placeholder="ENTER UNIT NAME (SW)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (DE)</label>
           <input type="text" name="edittitle_de" id="edittitle_de" class="form-control" placeholder="ENTER UNIT NAME (DE)" required="required">
        </div>
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="return updateUnit();">UPDATE</button>
      </div>
    </div>
  </div>
</div>
         
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW UNIT SYLLABUS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	
    			<?php if($returnVal != 0) { ?>
                		
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10%">SYLLABUS ID</th>
                      <th>UNIT(EN)</th>
                      <th>HEADING (EN)</th>
                      <th>HEADING (SW)</th>
                      <th>HEADING (DE)</th>
                      <th>UPDATE ON</th>
                      <th style="width: 5%">EDIT</th>
                      <th style="width: 5%">DELETE</th>
                    </tr>
                    
                    <?php while($data = mysql_fetch_assoc($returnVal)) { ?>
                    <tr id="<?php echo $data['syllabusid']; ?>">
                      <td><?php echo $data['syllabusid']; ?>.</td>
                      <td><?php echo $data['title_en']; ?></td>
                      <td><?php echo $data['heading_en']; ?></td>
                      <td><?php echo $data['heading_sw']; ?></td>
                      <td><?php echo $data['heading_de']; ?></td>
                   
                      <td><?php echo $data['updatedon']; ?></td>
                      <td>
                       <i class="fa fa-pencil fa-2x btn getUnitName" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 	data-toggle="modal" data-target="#editUnitModel_old"  data-unitid="<?php echo $data['unitid']; ?>"
              		 	data-title_en="<?php echo $data['title_en']; ?>" data-title_sw="<?php echo $data['title_sw']; ?>" data-title_de="<?php echo $data['title_de']; ?>"
              			data-coursenameid="<?php echo $data['coursenameid']; ?>"  ></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x btn"  onclick='deleteSyllabus(<?php echo $data['syllabusid'] ?>)'></i>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                
                <?php  } else {
					echo '<center><h3>No data found! </h3></center>';
					}
				?>
                </div><!-- /.box-body -->
              </div>
            </div>
            
          </div>   <!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
 
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php
	include 'script.php';
 ?>
    
    <script>
		$("button").click(function() {
			$("p").show("slow");
		});

		//Show pop up (model) with location name and hidden location id to update
		$(document).on("click", ".getUnitName", function() {
			$('#editunitid').val($(this).attr('data-unitid'));
			$('#edittitle_en').val($(this).attr('data-title_en'));
			$('#edittitle_sw').val($(this).attr('data-title_sw'));
			$('#edittitle_de').val($(this).attr('data-title_de'));
		});

		//function for delete location
		function deleteSyllabus(syllabusid) {
			var confirmDel = confirm('Are you sure you want to delete this unit syllabus?');
			if (confirmDel) {
				$.ajax({
					url : 'ajax.php',
					data : {
						syllabusid : syllabusid,
						type : 'deletesyllabus'
					},
					type : 'post',
					success : function(output) {
						alert('Unit syllabus is deleted successfully!');
						location.reload();
					}
				});
			}
		}

		//function to update location detail
		function updateUnit() {
			var unitid = $('#editunitid').val();
			var title_en = $('#edittitle_en').val();
			var title_sw = $('#edittitle_sw').val();
			var title_de = $('#edittitle_de').val();
			$.ajax({
				url : 'ajax.php',
				data : {
					title_en : title_en,
					title_sw : title_sw,
					title_de : title_de,
					type : 'updateunit',
					unitid : unitid
				},
				type : 'post',
				success : function(output) {
					alert('Unit is updated successfully!');
					location.reload();
				}
			});
		}

		//function for add location
		function addSyllabus() {
			var heading_en = $("#addheading_en").val();
			var heading_sw = $("#addheading_sw").val();
			var heading_de = $("#addheading_de").val();
			var description_en = $("#adddescription_en").val();
			var description_sw = $("#adddescription_sw").val();
			var description_de = $("#adddescription_de").val();
			var unitid = $("#addunitid").val();
			$.ajax({
				url : 'ajax.php',
				data : {
					heading_en : heading_en,
					heading_sw : heading_sw,
					heading_de : heading_de,
					description_en : description_en,
					description_sw : description_sw,
					description_de : description_de,
					unitid : unitid ,
					type : 'addsyllabus'
				},
				type : 'post',
				success : function(output) {
					alert('Syllabus is added successfully!');
					location.reload();
				}
			});
		}
               

          $("#syllabushover").attr('class', 'treeview active');
          $("#syllabushover").attr('class', 'active');
          $("#syllabushover").attr('style', 'display:block');
	</script>

  </body>
</html>