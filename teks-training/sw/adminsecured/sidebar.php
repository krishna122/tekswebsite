<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar"><br><br>
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="favicon.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Admin</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
       
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>
            
             <li class="treeview" id="coursehover">
              <a href="courselist.php">
                <i class="fa fa-pencil-square-o"></i> <span>Courses</span>
              </a>
            </li>
            
             <li class="treeview" id="unithover">
              <a href="unitlist.php">
                <i class="fa fa-pencil-square-o"></i> <span>Unit</span>
              </a>
            </li>
            
            <li class="treeview" id="syllabushover">
              <a href="unitsyllabuslist.php">
                <i class="fa fa-shirtsinbulk"></i> <span>Unit Syllabus</span>
              </a>
            </li>
            
            <li class="treeview" id="locationhover">
              <a href="locationlist.php">
                <i class="fa fa-pencil-square-o"></i> <span>Location</span>
              </a>
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>