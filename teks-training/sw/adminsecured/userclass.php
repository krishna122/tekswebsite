<?php

error_reporting('E_ERROR|E_WARNING');
session_start();

include ('db.php');

//define class here
class User {

	//function to admin login
	function adminLogin($email, $password) {
		$password = mysql_real_escape_string($password);
		$query = "SELECT  *  FROM  `admin`  WHERE  (email='$email') AND (password= '$password') ";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);

		if ($row == 0) {
			return 0;
		} else {
			$data = mysql_fetch_assoc($result);
			$returnArr = array();
			$returnArr['adminid'] = $data['adminid'];
			$returnArr['email'] = $data['email'];
			return $returnArr;
		}
	}

	//function to fetch all location
	function getAllLocations() {
		$query = "SELECT  *  FROM  `location`  order by locationid ASC";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		if ($row == 0) {
			return 0;
		} else {
			return $result;
		}
	}

	// function to check location is exist or not
	function isLocationExist($id, $locationname) {
		if ($id != 0) {
			$query = "SELECT  *  FROM  `location`  WHERE  (locationname='$locationname') AND  (locationid != '$id')";
		} else {
			$query = "SELECT  *  FROM  `location`  WHERE  (locationname='$locationname')";
		}
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		return $row;
	}

	// add location
	function updateLocation($id, $locationname) {
		if ($id == 0) {
			$query = "INSERT INTO `location` (locationname) VALUES ('$locationname')";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_insert_id();
		} else {
			//update locationname
			$query = "UPDATE `location` SET locationname = '$locationname' where locationid = '$id'";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_affected_rows();
		}
	}

	//function to delete location
	function deleteLocation($locatioid) {
		$query = "delete from  `location` where locationid = '$locatioid' ";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_affected_rows();
	}

	//function to deelte course by vo
	function deleteCourse($coursenameid) {
		$query = "delete from  `coursename` where coursenameid = '$coursenameid' ";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_affected_rows();
	}

	// function to fetch all course
	function getAllCourse() {
		$query = "SELECT  *  FROM  `coursename`  order by coursenameid ASC";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		if ($row == 0) {
			return 0;
		} else {
			return $result;
		}
	}

	//function to add or update course
	function updateCourse($id, $name_en, $name_sw, $name_de) {
		$date = date('Y-m-d H:i:s');
		if ($id == 0) {
			$query = "INSERT INTO `coursename` (name_en,name_sw,name_de,updatedon) VALUES ('$name_en','$name_sw','$name_de','$date')";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_insert_id();
		} else {
			$query = "UPDATE coursename SET name_en = '$name_en',name_sw = '$name_sw' , name_de = '$name_de' , updatedon = '$date' where coursenameid = '$id'";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_affected_rows();
		}
	}

	//function to get all units
	function getAllUnit() {
		$query = "SELECT  unit.* ,coursename.name_en,coursename.name_sw,coursename.name_de  FROM  `unit`   
						INNER JOIN coursename ON  coursename.coursenameid = unit.coursenameid  order by unitid  ASC";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		if ($row == 0) {
			return 0;
		} else {
			return $result;
		}
	}

	//function to add unit for a syablus
	function addUnit($coursenameid, $title_en, $title_sw, $title_de, $isfulltime, $isparttime) {
		$date = date('Y-m-d H:i:s');
		$lastfulltimeunitnumber = $this -> getLastUnitNumber('fulltime', $coursenameid);
		$lastparttimeunitnumber = $this -> getLastUnitNumber('parttime', $coursenameid);
		if ($isfulltime == '1') {
			$fullunitno = $lastfulltimeunitnumber + 1;
		} else {
			$fullunitno = 0;
		}
		if ($isparttime == '1') {
			$partunitno = $lastparttimeunitnumber + 1;
		} else {
			$partunitno = 0;
		}
		$query = "INSERT INTO `unit` (coursenameid,partunitno,fullunitno,title_en,title_sw,title_de,isfulltime,isparttime,updateon) 
						VALUES ('$coursenameid','$partunitno','$fullunitno','$title_en','$title_sw','$title_de','$isfulltime','$isparttime','$date') ";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_insert_id();
	}

	//fetch last unit number
	function getLastUnitNumber($type, $coursenameid) {
		if ($type == 'fulltime') {
			$query = "select fullunitno from unit where isfulltime = '1' and coursenameid = '$coursenameid' order by fullunitno desc limit 1 ";
		} else {
			$query = "select partunitno from unit where isparttime = '1' and coursenameid = '$coursenameid' order by partunitno desc limit 1 ";
		}
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		if ($row == 0) {
			return 0;
		} else {
			$data = mysql_fetch_assoc($result);
			if ($type == 'fulltime') {
				return $data['fullunitno'];
			} else {
				return $data['partunitno'];
			}
		}
	}

	//function to delete unit
	function deleteUnit($unitid) {
		$unitDetail = $this -> unitDetailByID($unitid);
		$coursenameid = $unitDetail['coursenameid'];
		//check full time course and update other unit number
		if ($unitDetail['isfulltime'] == '1') {
			$fullunitno = $unitDetail['fullunitno'];
			//select all units which have unit number more than this unit (get all 4,5 ,6 if it has unit no 3)
			$sql_fullTimes = "select * from unit where isfulltime = '1' and coursenameid ='$coursenameid' and fullunitno > '$fullunitno'   ";
			$result_fulltime = mysql_query($sql_fullTimes) or die(mysql_error());
			while ($data_fulltime = mysql_fetch_assoc($result_fulltime)) {
				$uID = $data_fulltime['unitid'];
				$uFN = $data_fulltime['fullunitno'];
				$sql_update_full_time = "update unit set fullunitno = $uFN -1 where unitid = $uID  ";
				mysql_query($sql_update_full_time) or die(mysql_error());
			}
		}
		//update part time unit number
		if ($unitDetail['isparttime'] == '1') {
			$partunitno = $unitDetail['partunitno'];
			//select all units which have unit number more than this unit (get all 4,5 ,6 if it has unit no 3)
			$sql_parttime = "select * from unit where isparttime = '1' and coursenameid ='$coursenameid' and partunitno > '$partunitno'   ";
			$result_parttime = mysql_query($sql_parttime) or die(mysql_error());
			while ($data_parttime = mysql_fetch_assoc($result_parttime)) {
				$uID = $data_parttime['unitid'];
				$uPN = $data_parttime['partunitno'];
				$sql_update_part_time = "update unit set partunitno = $uPN -1 where unitid = $uID  ";
				mysql_query($sql_update_part_time) or die(mysql_error());
			}
		}
		//finally delete unit
		$query = "delete from unit where unitid = '$unitid'";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_affected_rows();
	}

	//function to delete unit
	function unitDetailByID($unitid) {
		$query = "SELECT  unit.* ,coursename.name_en,coursename.name_sw,coursename.name_de  FROM  `unit`   
						INNER JOIN coursename ON  coursename.coursenameid = unit.coursenameid  where unit.unitid = '$unitid'";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_fetch_assoc($result);
		return $row;
	}

	//update unit detail
	function updateUnitDetail($unitid, $title_en, $title_sw, $title_de) {
		$time = date('Y-m-d H:i:s');
		$query = "UPDATE unit SET title_en = '$title_en',title_sw = '$title_sw' , title_de = '$title_de' , updateon = '$time' where unitid = '$unitid'";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_affected_rows();
	}

	//get all syllabus unit wise for a course
	function getAllSyllabus() {
		$query = "SELECT  unitsyllabus.* ,unit.title_en,unit.title_sw,unit.title_de  FROM  `unitsyllabus`   
						INNER JOIN unit ON  unit.unitid = unitsyllabus.unitid  order by syllabusid  ASC";
		$result = mysql_query($query) or die(mysql_error());
		$row = mysql_num_rows($result);
		if ($row == 0) {
			return 0;
		} else {
			return $result;
		}
	}

	//function to add new syllabus
	function updateSyllabus($syllabusid, $unitid, $heading_en, $heading_sw, $heading_de, $description_en, $description_sw, $description_de) {
		$date = date('Y-m-d H:i:s');
		if ($syllabusid == 0) {
			//add new syllabus
			$query = "INSERT INTO `unitsyllabus` (unitid,heading_en,heading_sw,heading_de,description_en,description_sw,description_de,updatedon)
							 VALUES ('$unitid','$heading_en','$heading_sw','$heading_de','$description_en','$description_sw','$description_de','$date')";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_insert_id();
		} else {
			//update syllabus
			$query = "UPDATE unitsyllabus SET unitid = '$unitid',heading_en = '$heading_en' , heading_sw = '$heading_sw' , heading_de = '$heading_de',
								description_en = '$description_en',description_sw = '$description_sw' , description_de = '$description_de' , updatedon = '$date' where syllabusid = '$syllabusid'";
			$result = mysql_query($query) or die(mysql_error());
			return mysql_affected_rows();
		}
	}

	//function to delete unit syllabus
	function deleteUnitSyllabus($syllabusid) {
		$query = "delete from unitsyllabus where  syllabusid = '$syllabusid' ";
		$result = mysql_query($query) or die(mysql_error());
		return mysql_affected_rows();
	}

	//End of class here
}
?>