<!-- Start Header Section -->
    <div class="hidden-header"></div>
    <header class="clearfix">

      <!-- Start Top Bar -->
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
              <!-- Start Contact Info -->
              <ul class="contact-details">
                <li><a href="#"><i class="fa fa-envelope-o"></i> info@teksmobile.se</a>
                </li>
                <!-- <li><a href="#"><i class="fa fa-phone"></i> +12 345 678 000</a>
                </li> -->
              </ul>
              <!-- End Contact Info -->
            </div>
            <!-- .col-md-6 -->
            <div class="col-md-5">
              <!-- Start Social Links -->
              <ul class="social-list">
              	<li>
					<div class="btn-group" role="group">
				    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #484646;border: none;">
				      <a href="#" class="lang-sm lang-lbl" lang="sv"></a>
				      <!-- <span class="caret" style="color: grey;font-size: 15px;"></span> -->
				    </button>
				    <!-- <ul class="dropdown-menu" style="background-color: #484646;border: none;">
				      <li style="padding-left:7px;"><a href="#" class="lang-sm lang-lbl" lang="en" style="color: #fff;"></a></li><br>
				      <li style="padding-left:7px;"><a href="#" class="lang-sm lang-lbl" lang="sv" style="color: #fff;"></a></li><br>
				      <li style="padding-left:7px;"><a href="#" class="lang-sm lang-lbl" lang="da" style="color: #fff;"></a></li>
				    </ul> -->
				  </div>
                </li>
                <li>
                  <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a class="twitter itl-tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <!-- <li>
                  <a class="google itl-tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a>
                </li> -->
                <li>
                  <a class="linkdin itl-tooltip" data-placement="bottom" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                </li>
              </ul>
              <!-- End Social Links -->
            </div>
            <!-- .col-md-6 -->
          </div>
          <!-- .row -->
        </div>
        <!-- .container -->
      </div>
      <!-- .top-bar -->
      <!-- End Top Bar -->


      <!-- Start  Logo & Naviagtion  -->
      <div class="navbar navbar-default navbar-top">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="index.php" >
              <img alt="" src="images/logo.png" style="margin-bottom: 20px;">
            </a>
          </div>
          <div class="navbar-collapse collapse">
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right" style="text-transform: uppercase;">
              <li id="index">
                <a href="index.php">Kurser</a>
              </li>
              <li id="corporate">
                <a href="corporatetraining.php">Företagsutbildning</a>
              </li>
              <li id="about">
                <a href="about.php">Om oss</a>
              </li>
              <li id="contact">
              	<a href="contactus.php">Kontakta oss</a>
              </li>
              <li id="press">
                <a href="pressrelease.php">Press release</a>
              </li>
              <li>
                <a href="http://teks.co.in/site/blog/" target="_blank">Blogg</a>
              </li>
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          	  <li>
                <a class="active" href="index.php">Kurser</a>
              </li>
              <li>
                <a href="corporatetraining.php">Företagsutbildning</a>
              </li>
              <li>
                <a href="about.php">Om oss</a>
              </li>
              <li>
              	<a href="contactus.php">Kontakta oss</a>
              </li>
              <li>
                <a href="pressrelease.php">Press release</a>
              </li>
              <li>
                <a href="http://teks.co.in/site/blog/" target="_blank">Blogg</a>
              </li>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header Logo & Naviagtion -->

    </header>
    <!-- End Header Section -->