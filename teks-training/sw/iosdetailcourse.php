<!-- Start Services Section -->
    <div class="section service courses" id="ios" style="display: non;">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="text-align: center; text-transform: uppercase;">BLI EN PROFFESIONELL IPHONE/IPAD - UTVECKLARE</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/10.png" />
            </div>
            <div class="service-content">
              <h3>XCODE- UTBILDNING</h3><br>
              <p>Få praktisk erfarenhet av att arbeta med den senaste iteration av Xcode verktyget .</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/11.png" />
            </div>
            <div class="service-content">
              <h3>LÄR DIG IOS PROGRAMMERING</h3><br>
              <p>Få goda kunskaper om kodning för iPhone / iPad-appar med Objective- C och Swift 2 som programmeringsspråk .</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/12.png" />
            </div>
            <div class="service-content">
              <h3>JOBBA MED APPLE</h3><br>
              <p>Från Xcode och AppCode, du får tips och verktyg för olika metoder för iOS 8 & iOS 9 applikationsutveckling - bli utbildade på allt. Lär dig de viktigaste funktionerna som krävs för en lyckad apputveckling. </p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    




<!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style=" text-transform: uppercase;">APPUTVECKLINGSKURSER</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">Teks utbildningscenter uppmuntrar interaktivitet i alla sina iOS utveckling kurser och moduler . Vi har speciella apputvecklings kurser för nybörjare och proffs.</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/h1.png" />
            </div>
            <div class="service-content">
              <h4>ERFARNA MENTORER</h4>
              <p>Lär dig att skapa en iPhone app med en senior apputvecklare som fungerar som din guide. De finns där som stöd när du behöver hjälp. </p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/v1.png" />
            </div>
            <div class="service-content">
              <h4>FALLSTUDIER</h4>
              <p>Arbeta med "dummy" iOS utvecklingsprojekt och få kläm på att arbeta med de senaste versionerna av plattformen, liksom verktyg, ramverk och API: er.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/bh1.png" />
            </div>
            <div class="service-content">
              <h4>GRUPPTRÄNING</h4>
              <p>Dela och få kunskap från dina kamrater och kolleger. Arbeta med grupparbeten och jobba med utvecklingsfrågor och buggar tillsammans.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->