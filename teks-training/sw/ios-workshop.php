<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | iOS Workshop</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
    <div class="section courses-white">

      <div class="container" >

        	<div class="col-md-12">
        		
             <div class="col-md-6">

	              <!-- Classic Heading -->
	              <h1 class="big-title">iOS-Con 2016: Training & Development</h1>
	              
	              <div class="margin-top"></div>
	              
	              <!-- Some Text -->
	              <p style="text-decoration: underline;"><strong>At A Glance</strong></p>
	              <p>5-Day Workshop On Various Aspects Of iOS App Development</p>
	              
	             <div class="margin-top"></div>
	             
	             <h3>About The Workshop</h3>
	             
	             <br>
	             
	             <p>iOS-Con 2016 will be held on 09th may to 13th may of 2016. It will be a five-day affair, with four separate sessions on each day. The morning sessions will start from 09:00 and will continue till 11:00. Following a half-hour recess, the 90-minute second sessions will start on all the days. The luncheon break (60 minutes) follows, with another 2-hour session coming up after that. </p>
	             
	             <br>
	             
	             <p>On each of the workshop days, the proceedings will round up with a one-hour Q&A/Review session. The session will be ideal for participating delegates and aspiring iOS app developers to directly interact with the speakers and clarify all their doubts and queries.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>What’s In It For Learners?</h3>
	             
	             <br>
	             
	             <p>The scope for learning about the nitty-gritty of making iPhone apps is immense at iOS-Con 2016. Both newbies as well as experienced app developers can join in, and learn from the very best in the industry.</p>
	             
	             <br>
	             
	             <h3>Some Highlights Of iOS-Con 2016</h3>
	             
	             <br />
	             
	             <ul style="list-style: circle; padding-left: 15px;">
	             	<li>
	             	  <strong style="font-size: 18px;">Day 1:</strong> Introduction to iOS apps and XCode (09:00 - 11:00).	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 2:</strong> JSON and XML parsing (14:00 - 16:00)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 3:</strong> Using TableView, ScrollView and CollectionView (09:00 - 11:00).	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 3:</strong> Animation & UI Dynamics (14:00 - 16:00)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 4:</strong> Using CocoaPods and Internal/External Frameworks (09:00 - 11:00)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 5:</strong> App Debugging (11:30 - 13:00).
	             	</li>
	             </ul>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Special Session On iOS Agile Development</h3>
	             
	             <br>
	             
	             <p>During the afternoon session on the final day of the workshop, an interactive discussion will be held on Agile Development on the iOS platform. Interesting tips and ideas will be shared with attendees on a wide array of topics - right from Notifications and Code Reuse, to using Shortcuts and following other best practices.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Attendance At iOS-Con 2016</h3>
	             
	             <br>
	             
	              <p>The 5-day workshop will be attended by a large contingent of leading app developers, programmers and testers. Several leading corporate houses are also expected to be present, along with many individual participants. Be there...it’s your ticket to knowing all that there is to know about iOS app development!</p>
	             
            </div>
            
            <div class="col-md-6">
              
              <div class="item text-center">
                	<img alt="" src="images/course/13.jpg">
                </div>
              
             
             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#ececec;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">09th may to 13th may of 2016</h3>
	               
	                
	                <p><img src="images/course/line.png" /></p>
	                
	                <h1><i class="fa fa-map-marker fa-2x"></i> GA Singapore @ The Working Capitol</h1>
	                
	                <br />
	                
	                <p>Annexe, The Working Capitol, 1A Keong Saik Rd - Turn left after Lollapalooza, and enter via the white door on the side lane Singapore</p>
	                
	              </div>
	              
	              <div class="plan-list">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;padding: 40px;"><span style="float: left;"><strong>Regular Ticket</strong></span> <span style="float: right;">$35 SGD</span></li>
	                  <li style="border-bottom: 0px;"></li>
	                  <a href="contactus.php" class="btn-system btn-large" style="color: #515151;">Attend</a>
	                  <li></li>
	                </ul>
	              </div>
	            </div>
          
        	</div>

            </div>
            
          </div>
          
          

      	</div>
      <!-- .container -->
	</div>
	
   

<?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>