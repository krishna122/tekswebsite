<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Detail of Courses</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div class="section" style="background: #FE6F5E;">

      <div class="container" >

        <div class="col-md-12">
				
				<h4 style="text-align: center;color: #fff;">10 Weeks | <?php echo $_REQUEST['coursetime']; ?></h4>
				
				<div class="margin-top"></div>
				
              <!-- Classic Heading -->
              <h1 class="big-title" style="font-size: 50px; text-align: center; color: #fff; line-height: 50px; text-transform: uppercase;"><?php echo $_REQUEST['detailcourse']; ?></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              
              <?php if ( $_REQUEST['detailcourse']=='iOS App utveckling' ) { ?>
              <p style="color:#fff;text-align: center; text-transform: uppercase;">Holistic Training Courses To Help You Learn How To Make Custom iPhone Apps</p>
              
              <?php } if ( $_REQUEST['detailcourse']=='Android App utveckling' ) { ?>
              <p style="color:#fff;text-align: center; text-transform: uppercase;">Lära sig att arbeta med AndroidA utvecklingsverktyg och göra Android-appar</p>
              
              <?php } if ( $_REQUEST['detailcourse']=='Web App utveckling' ) { ?>
              <p style="color:#fff;text-align: center; text-transform: uppercase;">Modulkurser för webdesigntjänster, utveckling och underhåll - INKLUSIVE sessioner på CSS och HTML.</p>
              <?php } ?>
              
              <div class="margin-top"></div>
              
              <div class="" style="text-align: center;">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="contactus.php" style="color: #515151;">Be om en kursplan</a>
                  <a class="animated4 slider btn btn-default btn-min-block" href="contactus.php">Gå en kurs </a>
                </div>
              
            </div>

      	</div>
      	<!-- .container -->
	</div>

<!-- --------------------------------- iOS ---------------------------------------------------------------------- -->
 
 
 
		
	<?php ($_REQUEST['detailcourse']=='iOS App utveckling')?include 'iosdetailcourse.php' :'';?>
    
<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->

<!-- --------------------------------- Android ---------------------------------------------------------------------- -->
		
	<?php ($_REQUEST['detailcourse']=='Android App utveckling')?include 'androiddetailcourse.php' :''; ?>
    
<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->

<!-- --------------------------------- Web ---------------------------------------------------------------------- -->
		
	<?php ($_REQUEST['detailcourse']=='Web utveckling')?include 'webdetailcourse.php' :''; ?>
    
<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->

    
    
    <!-- ---------------------------------SYLLABUS ---------------------------------------------------------------------- -->
    
    <div class="section" style="background: #ffdf00;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="Big-title text-center" style="text-transform: uppercase;">KURSPLANEN I KORTHET</h1>
              
              
              <div class="margin-top"></div>
              
              
              
              
              
              
              <?php if ( $_REQUEST['detailcourse']=='iOS App utveckling' ) { ?>
              	
              	<!-- ----------------------------- iOS --------------------------------------------------------------------------- -->
              	
              <div class="panel-group" id="accordion">

              <!-- Start Toggle 1 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-up control-icon"></i>
						INTRODUCTION TO iOS APPS
						</a>
				  </h4>					
                </div>
                <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Introduction iOS Apps</li>
                  		<li>Introduction to XCode,</li>
                  		<li>Creating different applications, App Settings</li>
                  		<li>Introduction to Application Life Cycle.</li>
                  		<li>Classes & Naming conventions</li>
                  		<li>Variables & Data Types</li>
                  		<li>Functions declarations</li>
                  		<li>Interface builder</li>
                  		<li>Storyboarding</li>
                  		<li>Properties</li>
                  	</ul>
                  	
                  </div>
                </div>
              </div>
              <!-- End Toggle 1 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						UI CONTROLS AND FILE OPERATIONS
						</a>
				  </h4>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Introduction to Different UI Controls</li>
                  		<li>Delegates, User Delegates</li>
                  		<li>Completion Handlers, Custom Completion Handler</li>
                  		<li>Enums, User defaults</li>
                  		<li>App folder structure and usage</li>
                  		<li>File Operations</li>
                  		<li>JSON, XML Parsing</li>
                  		<li>Arrays, Dictionaries & Data.</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 2 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						VIEWS AND CONTROLLERS
					  </a>
				  </h4>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Using Scrollview</li>
                  		<li>Using Table View</li>
                  		<li>Using Collection View</li>
                  		<li>Navigation Controllers</li>
                  		<li>Tabbar Controllers</li>
                  		<li>Split Controllers</li>
                  		<li>Animation & UI Dynamics</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 3 -->
              
              <!-- Start Toggle 4 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						GPS, PARSE AND iCLOUD
					  </a>
				  </h4>
                </div>
                <div id="collapse-5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Using CocoaPods</li>
                  		<li>Using Internal/External Frameworks</li>
                  		<li>Networking, APIs</li>
                  		<li>GPS, Map</li>
                  		<li>Core Data & Sqlite</li>
                  		<li>Parse</li>
                  		<li>iCloud</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 4 -->
              
               <!-- Start Toggle 4 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						AUTOLAYOUTS, DESIGNS AND DEVELOPING
					  </a>
				  </h4>
                </div>
                <div id="collapse-4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Using AutoLayout</li>
                  		<li>UI Design patterns</li>
                  		<li>Debugging</li>
                  		<li>Using Tools</li>
                  		<li>Using Assets</li>
                  		<li>Setting Launch Images and App Icons</li>
                  		<li>Running App into Device</li>
                  		<li>OTA Deployment</li>
                  		<li>Agile Development iOS Tricks (Reusing code, Notifications, Shortcuts, Best Practice)</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 4 -->

            </div>
            
            
            <?php } if ( $_REQUEST['detailcourse']=='Android App utveckling' ) { ?>
            	
            	
            	<!-- -------------------------------------- Android ------------------------------------------------------------- -->
            	
            	
            	
            	
            	<div class="panel-group" id="accordion">

              <!-- Start Toggle 1 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-up control-icon"></i>
						INTRODUCTION TO ANDROID
						</a>
				  </h4>					
                </div>
                <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>What is Android?</li>
                  		<li>Setup Development Environment</li>
                  		<li>Application Structure</li>
                  		 <ul>
                  		 	<li>(i) AndroidManifest.xml</li>
                  		 	<li>(ii) uses-permission</li>
                  		 	<li>(iii) Resources, Assets</li>
                  		 	<li>(iv) Layouts and drawables</li>
                  		 	<li>(v) Activities and lifecycle</li>
                  		 </ul>
                  		<li>My First Android Application</li>
                  		<li>Running Application</li>
                  		<li>Logcat usage,DDMS</li>
                  	</ul>
                  	
                  </div>
                </div>
              </div>
              <!-- End Toggle 1 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						LAYOUTS, DESIGNS AND DEVELOPING
						</a>
				  </h4>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Simple UI -Layouts & Layout properties</li>
                  		<li>Resolution and density</li>
                  		<li>independence (px,dip,dp,sip,sp)</li>
                  		<li>Basic UI Design-form design - (textviews, edittexts,buttons etc)</li>
                  		<li>Second application - Event Driven Programming</li>
                  		<li>Develop an app for demonstration communication between Intents Toast, Menu, Dialog,Imageviews, webviews etc</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 2 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						INTERFACE AND STORAGE
					  </a>
				  </h4>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Listviews,Gridviews,Adapters</li>
                  		<li>examples</li>
                  		<li>Tab Activities</li>
                  		<li>Database Sqliite, Some examples</li>
                  		<li>shared Preferences, internal storage ,</li>
                  		<li>externale storage SD cards</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 3 -->
              
              <!-- Start Toggle 4 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Services & Background Operations
					  </a>
				  </h4>
                </div>
                <div id="collapse-4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Broadcast Receivers</li>
                  		<li>Alarms</li>
                  		<li>Services</li>
                  		<li>Threads,Handlers and runnables</li>
                  		<li>Asynctask</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 4 -->
              
               <!-- Start Toggle 4 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						HANDLING, TESTING AND INSTALLING
					  </a>
				  </h4>
                </div>
                <div id="collapse-5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Fragments</li>
                  		<li>Exception handling</li>
                  		<li>Location Based Services and Google Maps</li>
                  		<li>testing debugging</li>
                  		<li>installing android apk</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 4 -->

            </div>
            	
            	
            	
            
            	
            	<?php } if ( $_REQUEST['detailcourse']=='Web utveckling' ) { ?>
            		
            		
            		<!-- ------------------------------------------------- Web ------------------------------------------------------------------ -->
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		<?php } ?>
            	
            	
            	
            	
            	
            	
            	
            
            
              <div class="margin-top"></div>
              
             
            
            <div class="margin-top"></div><br><br>
            
           <?php if ( $_REQUEST['detailcourse']=='iOS App utveckling' ) { ?>
            	
            <!-- ---------------------------------iOS ---------------------------------------------------------------------- -->
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="contactus.php" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Be om kursplan</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">Se detaljerad kursplan för iOS</h2>
            </div>
            
            <!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->
            
           <?php  } 
          
             if ( $_REQUEST['detailcourse']=='Android App utveckling' ) {
          	
            ?>
            
            <!-- ---------------------------------Android ---------------------------------------------------------------------- -->
            
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="contactus.php" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> HÄMTA KURSPLAN</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">SE DETALJERAD KURSPLAN FÖR ANDROIDUTBILDNING</h2>
            </div>
          
            <!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->
            
            
            <?php  } 
          
             if ( $_REQUEST['detailcourse']=='Web Devlopment' ) {
          	
            ?>
          
            <!-- ---------------------------------Web ---------------------------------------------------------------------- -->
            
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="contactus.php" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Get Syllabus</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">View Detailed Web Development Training Syllabus</h2>
            </div>
            
            <!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->
             
             <?php  } ?>
          
        </div>
	
	</div>
      <!-- .container -->
</div>
 
 
 <?php if ( $_REQUEST['detailcourse']=='iOS App utveckling' ) { ?>
 
 <!-- ---------------------------------iOS ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	
        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>De IOS applikationsutvecklings kurser som lanseras av Teks är omfattande. De utbildningsmoduler här är grundliga - vi börjar med grunderna i appkodning och wireframing, till avancerade Xcode sessioner, seminarier och workshops. Dessutom uppdateras kurserna regelbundet och är alltid branschorienterade. Om du vill bli en expert inom iPhone-utveckling, anmäl dig till en Teks kurs! Det är ett bra sätt att börja på. </p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->

<?php  } 
          
if ( $_REQUEST['detailcourse']=='Android App utveckling' ) {
          	
?>

<!-- ---------------------------------Android ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	
        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>Det är ett privilegium att vara en del av Teks team i form av instruktör av apputveckling på Teks institut. Från min yrkeserfarenhet, kan jag garantera att dessa moduler är verkligen väl utformade. Det finns stora möjligheter för kunskapsutbyte, både med elever och mentorer. </p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->



<?php  } 
          
if ( $_REQUEST['detailcourse']=='Web utveckling' ) {
          	
?>
            
            
<!-- ---------------------------------Web ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	<div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/team/4.png">
                </div>
              
            </div>

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>I feel that there is a genuine absence of quality web development training institutes in India. In that regard, Teks is doing an excellent job - grooming promising young talents into smart, well-informed web developers and site designers. Attention to detail is one of the high points of all the training courses offered by this institute. If you are passionate about making a career in web development, I strongly suggest joining Teks Learning Center.</p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->

<?php } ?>




<div class="section" style="background: #ffdf00;">

      <div class="container" >
      	
      	<div class="col-md-12">
            
            <div class="margin-top"></div>
            
            <!-- Classic Heading -->
              <h1 class="big-title" style=" text-align: center; text-transform: uppercase;">KURSSCHEMA OCH PROGRAM </h1>
             
              
              <div class="margin-top"></div>
              
              
             <?php if ( $_REQUEST['detailcourse']=='iOS App utveckling' ) { ?> 
<!-- ---------------------------------iOS ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	För mer information om kommande kurser, vänligen <a href="contactus.php" style="text-decoration: underline;">prenumerera</a> på vårt nyhetsbrev. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->
              <?php  } 
          
				if ( $_REQUEST['detailcourse']=='Android App utveckling' ) {
          	
			  ?>

<!-- ---------------------------------Android ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	Datum för nästa serie av Androida utvecklingskurser kommer att meddelas på hemsidan. Du kan även <a href="contactus.php" style="text-decoration: underline;">anmäla dig här</a> för att få vårt nyhetsbrev med information om viktiga datum. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->

              <?php  } 
          
              if ( $_REQUEST['detailcourse']=='Web utveckling' ) {
          	
              ?>
<!-- ---------------------------------Web ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	The next batch of web development tutorial classes will be starting soon. <a href="contactus.php" style="text-decoration: underline;">Subscribe</a> here to get notified about important dates. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->
              <?php } ?>

        </div>

      </div>
      <!-- .container -->
</div>


<!-- ---------------------------------FAQ ---------------------------------------------------------------------- -->

<!-- ---------------------------------iOS ---------------------------------------------------------------------- -->

<?php //($_REQUEST['detailcourse']=='iOS App utveckling')?include 'iosfaqdetailcourse.php' :''; ?>


<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->


<!-- ---------------------------------ANDROID ---------------------------------------------------------------------- -->

<?php //($_REQUEST['detailcourse']=='Android App utveckling')?include 'androidfaqdetailcourse.php' :''; ?>


<!-- ---------------------------------END OF ANDROID ---------------------------------------------------------------------- -->


<!-- ---------------------------------WEB ---------------------------------------------------------------------- -->


<?php //($_REQUEST['detailcourse']=='Web utveckling')?include 'webfaqdetailcourse.php' :''; ?>

<!-- ---------------------------------END OF WEB ---------------------------------------------------------------------- -->

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>