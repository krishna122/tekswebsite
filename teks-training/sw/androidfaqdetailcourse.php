<div class="section courses" style="display: non;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><span style="font-size: 40px; text-transform: uppercase;">Wish To Know More?</span></h1>
              
              <!-- Some Text -->
              <p>Isn’t it a good idea to resolve all doubts before joining a professional coding and app development course? We at Teks believe this, and are only too happy to resolve your queries. Here are a few often-asked questions:</p>
              
              <div class="margin-top"></div>
              
          <div class="panel-group" id="accordion" >

                  <!-- Start Accordion 1 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Do I really need to join a formal Android tutorial course?
						</a>
					  </h4>						
                    </div>
                    <div id="collapse-7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Well, actually you do. General coding expertise is completely different from the systematic manner in which coding for mobile apps needs to be done. We make you masters in the latter.
						</p>
                    </div>
                  </div>
                  <!-- End Accordion 1 -->

                  <!-- Start Accordion 2 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							Who will be my guides? Will the course be worth it?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-8" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Totally. You will have leading Android app developers, testers, trainers and app quality analysts as your mentors. All that you have to do is tap their knowledge pool - you’ll be richer for the experience!
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 2 -->

                  <!-- Start Accordion 3 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							Okay, so I will get trained on Android development. What’s the long-term gain?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-9" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Getting a certification from a reputed institution like Teks will bolster your career prospects manifold. When companies hire mobile app developers, they look for candidates with updated practical knowledge on how to make mobile apps. That’s precisely what you get from us.
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 3 -->

                </div>
        </div>
</div>
      </div>
      <!-- .container -->
</div>