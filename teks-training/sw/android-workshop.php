<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Android Workshop</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
    <div class="section courses-white">

      <div class="container" >

        	<div class="col-md-12">
        		
             <div class="col-md-7 ">

	              <!-- Classic Heading -->
	              <h1 class="big-title">Deal With Droid 2016: The Android Workshop</h1>
	              
	              <div class="margin-top"></div>
	              
	              <!-- Some Text -->
	              <p style="text-decoration: underline;"><strong>At A Glance</strong></p>
	              <p>5-Day Workshop, Covering All Important Topics Related To Android app development.</p>
	              
	             <div class="margin-top"></div>
	             
	             <h3>About The Workshop</h3>
	             
	             <br>
	             
	             <p>This year, the ‘Deal With Droid’ workshop will be organized at ___________ , from 09th may to 13th may of 2016. The schedule on each day is divided in 4 sessions. The first session (09:00 - 11:00) and the third session (14:00 - 16:00) will be of two hours each. In between, there will be a 90-minute session (11:30 - 13:00). The day’s proceeding will round off with an hour-long final session (16:30 - 17:30).</p>
	             
	             <br>
	             
	             <p>There will be a special Q&A session during the last session on each day of Deal With Droid 2016. Speakers will be taking questions from the audience and sharing their knowledge on the queried topics.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>What’s In It For Learners?</h3>
	             
	             <br>
	             
	             <p>Deal With Droid 2016 promises to be an absolute goldmine of information for aspiring Android app developers. All pertinent points related to making mobile apps for this platform will be touched upon, and several top-level coders and app entrepreneurs will be in attendance to guide learners.</p>
	             
	             <br>
	             
	             <h3>Key Highlights Of Deal With Droid 2016</h3>
	             
	             <br />
	             
	             <ul style="list-style: circle; padding-left: 15px;">
	             	<li>
	             	 <strong style="font-size: 18px;">Day 1:</strong> Android introduction (09:00 - 11:00).	
	             	</li>
	             	
	             	<li>
	             	 <strong style="font-size: 18px;">Day 2:</strong> Using Logcat and DDMS (16:30 - 17:30)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 3:</strong> Basic UI Design/Form Design (09:00 - 11:00).	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 3:</strong> Discussion on Adapter, Gridview and Listview examples (11:30 - 13:00)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 4:</strong> Database Sqlite (14:00 - 16:00)	
	             	</li>
	             	
	             	<li>
	             	  <strong style="font-size: 18px;">Day 5:</strong> Android app testing and debugging (16:30 - 17:30).
	             	</li>
	             </ul>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Live Demonstrations</h3>
	             
	             <br>
	             
	             <p>On Day 1 of Deal With Droid 2016, a demo running app will be created during the ‘My First Android Application’ session (14:00 - 16:00). On Day 2, there will be another live demonstration - to showcase how event-driven programming should be done.</p>
	             <br>
	             <p>A special session on Asynctask has been scheduled during the final session of Day 4. Speakers will shed light on Exception Handling (second session) and location-based services (third session) on the last day of the workshop.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Attendance At Deal With Droid 2016</h3>
	             
	             <br>
	             
	              <p>Anyone interested in making apps for the Android platform can register him/herself for this 5-day workshop. There will be several industry leaders and professional app developers at the event. For corporate attendees, the networking opportunities will also be ample. Join Deal With Droid 2016...this is one business workshop you simply cannot miss!</p>
	             
            </div>
            
            <div class="col-md-5">
              
              <div class="item text-center">
                	<img alt="" src="images/course/13.jpg">
                </div>
              
             
             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#ececec;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">09th may to 13th may of 2016</h3>
	                
	                
	                <p><img src="images/course/line.png" /></p>
	                
	                <h1><i class="fa fa-map-marker fa-2x"></i> GA Singapore @ The Working Capitol</h1>
	                
	                <br />
	                
	                <p>Annexe, The Working Capitol, 1A Keong Saik Rd - Turn left after Lollapalooza, and enter via the white door on the side lane Singapore</p>
	                
	              </div>
	              
	              <div class="plan-list">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;padding: 40px;"><span style="float: left;"><strong>Regular Ticket</strong></span> <span style="float: right;">$35 SGD</span></li>
	                  <li style="border-bottom: 0px;"></li>
	                  <a href="contactus.php" class="btn-system btn-large" style="color: #515151;">Attend</a>
	                  <li></li>
	                </ul>
	              </div>
	            </div>
          
        	</div>

            </div>
            
          </div>
          
          

      	</div>
      <!-- .container -->
	</div>
	
   

<?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>