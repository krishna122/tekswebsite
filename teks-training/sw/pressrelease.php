<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

  <!-- Basic -->
  <title>Teks Training | Press Release</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <div id="content">
      <div class="container">
        <div class="page-content">
          <div class="error-page" style="padding: 0px 0;">
            <h3 style="font-size: 35px;line-height: 50px;">Teks Mobile Sverige</h3>
            
            <h4 style="line-height: 40px;">Arrangerar</h4>
            
            <br>
			<button class="btn-default btn-large border-btn" style="cursor:default; outline: 0;border-radius: 5px;background: transparent;border:1px solid #ff7018;"><h2 style="font-size: 50px;line-height: 60px;font-weight: 400">9-13 Maj 2016</h2></button><br><br>
			
			<h2 style="font-size: 60px;line-height: 70px;">Bootcamp</h2>
			
			<h2 style="font-size: 40px;line-height: 70px;">Apputveckling</h2>
			
				<h5 style="font-size: 20px;line-height: 37px;">iOS Workshop- 20 platser</h5>

				<h5 style="font-size: 20px;line-height: 37px;">Android Workshop- 20 platser</h5>
				
				<h5 style="font-size: 20px;line-height: 37px;">Kistamässan -Stockholm</h5>
				
				<h5 style="font-size: 20px;line-height: 37px;">Anmälan senast 25 april</h5>
				
				<br>
				<p>För att delta krävs grundläggande programmeringskunskap. Ta med egen dator. Vid iOS workshop krävs en Mac dator med senaste Xcode installerat. ( Finns att ladda ner gratis på App Store) Utbildningen hålls på engelska. För prisuppgifter, kontakta oss. 
					<a href= "mailto:info@teksmobile.se" style="color:#FF7018;">info@teksmobile.se</a></p>
          </div>
        </div>
      </div>
    </div>
  


  </div>
  <!-- End Full Body Container -->

<?php include 'footer.php' ?>
 <?php include 'bottom.php' ?>
 
  <script type="text/javascript">
	$(document).ready(function(){
		$('#press').addClass('active');
	});

</script>

</body>

</html>