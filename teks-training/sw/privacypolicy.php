<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Privacy Policy</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
    
    <div id="content">
      <div class="container">
        <div class="row blog-post-page">
          <div class="col-md-12 blog-box">

            <!-- Start Single Post Area -->
            <div class="blog-post gallery-post">

              <!-- Start Single Post Content -->
              <div class="post-content">
                <h1 style="text-align: center;">Privacy Policy</h1><br>
                
                <p>Maintaining the authenticity of personal, confidential data is of paramount importance. At Teks Learning, we abide by a pre-specified, detailed set of privacy policy norms and regulations. Find out how all types of data are used, collected, and stored by our mobile development training experts.</p>
                
                <h2>What Data Does Teks Learning Collect From You?</h2><br>
                
                <p>We only ask for personal information when you are:</p>
                
                <ul class="liststyle">
                	
                	<li>Requesting for more information.</li>
                	
                	<br>
                	
                	<li>Sending us any particular query, and</li>
                	
                	<br>
                	
                	<li>Joining a web development or app development training course.</li>
                	
                </ul>
                
              <p>We have separate series of training courses and classes for general students and corporate learners. The data that we collect includes:</p>
              
              <ul class="liststyle">
                	
                	<li>Your full name.</li>
                	
                	<br>
                	
                	<li>Your contact number.</li>
                	
                	<br>
                	
                	<li>Your email id.</li>
                	
                	<br>
                	
                	<li>Specific company details, like organization name, nature and manpower strength.</li>
                	
                	<br>
                	
                	<li>Overview of the query you wish to get resolved by us.</li>
                	
                </ul>
                
                <p>Please note that the above is not an exhaustive list of information that representatives from our app/web training agency can ask from you. Additional data can be asked for, on a case - by - case, as - needed basis.</p>
                
                <h2>Data Encryption & Usage</h2>
                
                <p>All the information that you choose to share on this website are encrypted and stored in the secure database of Teks Learning/Teknowledge Software. We never use such personal data for commercial purposes (selling, promotions, etc.). There is no scope of any unauthorized access of the data you provide whatsoever.</p>
                
                <h2>Use Of Cookies</h2>
                
                <p>We use internet cookies (small internet files) to track and personalize your visits to our website. This invariably facilitates a more custom site-browsing experience for users. You, of course, have the right to delete cookies from your browser - which will take away these advantages.</p>
                
                <h2>Email Security</h2>
                
                <p>Although we employ the latest firewall and data encryption methods to protect information, you are advised to use your discretion while deciding what information to share with us via email. Also, be wary of fake calls - and make sure that you are interacting/communicating with an authorized Teks personnel at all times.</p>
                
                <p>You need not worry about data security at our web and mobile development training center. Select the right course, and concentrate on the lessons...we shall take care of all the formalities!</p>
                
              </div>
              <!-- End Single Post Content -->
            </div>
            <!-- End Single Post Area -->
          </div>
        </div>
      </div>
    </div>
    <!-- End content -->
  
  </div>



  <?php include 'footer.php' ?>

<style>
	.liststyle{
		list-style: circle;
		margin: 25px;	
	}
</style>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>