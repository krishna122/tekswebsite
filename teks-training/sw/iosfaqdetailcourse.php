<div class="section courses" style="display: non;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><span style="font-size: 40px; text-transform: uppercase;">Wish To Know More?</span></h1>
              
              <!-- Some Text -->
              <p>Before joining a part time/full time app development training course, it is only natural that you would have certain queries. Here are the answers to some common student questions:</p>
              
              <div class="margin-top"></div>
              
          <div class="panel-group" id="accordion" >

                  <!-- Start Accordion 1 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Why Do I Need To Join An iOS Training Course?
						</a>
					  </h4>						
                    </div>
                    <div id="collapse-7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Good question. Getting trained at a recognised mobile and web training institute like Teks will help you learn smart coding techniques and how to make an iOS app in a professional manner. We help you develop in a more streamlined, efficient manner.
						</p>
                    </div>
                  </div>
                  <!-- End Accordion 1 -->

                  <!-- Start Accordion 2 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Do I Need To Have Any Particular Academic Degree?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-8" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 No prior iOS programming knowledge is required to join our training courses. However you should have a B.Tech. degree from a recognised college/university. Our instructors will take care of all the requisite training on Objective-C, Swift and Cocoa/Cocoa Touch.
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 2 -->

                  <!-- Start Accordion 3 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Who Will I Be Learning From & Who Will I Be Learning With?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-9" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 At Teks, you get to learn from some of the best iPhone app developers in India. We have a strong in-house team of trainers, while external delegates are regularly invited at our iOS seminars. The opportunity to interact with fellow students from across the country is also beneficial.
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 3 -->

                </div>
        </div>
</div>
      </div>
      <!-- .container -->
</div>