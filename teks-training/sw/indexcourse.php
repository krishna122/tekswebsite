<?php include_once 'mysqlconnect.php'; ?>

<div class="section courses">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span style=" text-transform: uppercase; ">SEMINARIER OCH WORKSHOPS MED PRAKTISKA ÖVNINGAR</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Håll koll på de senaste webb och mobil applikationsutveckling verktyg, tekniker och metoder genom att delta i våra interna workshops och seminarier.</p>
              
               <div class="margin-top"></div>
              
              <a href="courses&classes.php" class="btn-system btn-large border-btn btn-black">Attend Workshop</a> 

            </div>

            <div class="col-md-6">
				
		<div class="team-member">

                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/3.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
    
<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >STOCKHOLM <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li><a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> "><?php echo $result['locationname']; ?></a></li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <div class="row">

            <!-- Start Image Service Box 1 -->
            <div class="col-md-6 image-service-box">
              <img class="img-thumbnail" src="images/iosworkshop.jpg" alt="">
              <h4><a href="ios-workshop.php">iOS-Con 2016: Training & Development</a></h4>
              <p>Coming Soon</p>
            </div>
            <!-- End Image Service Box 1 -->

            <!-- Start Image Service Box 2 -->
            <div class="col-md-6 image-service-box">
              <img class="img-thumbnail" src="images/androidworkshop.jpg" alt="">
              <h4><a href="android-workshop.php">Deal With Droid 2016: The Android Workshop</a></h4>
              <p>Coming Soon</p>
            </div>
            <!-- End Image Service Box 2 -->

          </div>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-7">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span >HELTIDSSTUDIER</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Ge din karriär ett uppsving genom att gå på någon av våra kurser.<br> ( Längd: 8 - 12 veckor )</p>
              
              <!-- <div class="margin-top"></div>
              
              <a href="trainingplace.php" class="btn-system btn-large border-btn btn-black">View Full-Time Courses</a> -->

            </div>

            <div class="col-md-5">
				
	      <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/1.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
   
<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >STOCKHOLM <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li>
			      	<a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> ">
			      		<?php echo $result['locationname']; ?>
			      	</a>
			      </li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <ul class="list-group">
              	<?php 

					  $result = array();
					  $query = "SELECT * FROM  coursename";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			     ?>
			      <a href="detailcourse.php?detailcourse=<?php echo $result['name_sw']; ?>&coursetime=FULL-TIME COURSES">
			      	<li class="list-group-item">
			      		<span>
			      			<strong style="font-size: 18px;"><?php echo $result['name_sw']; ?></strong>
			      		</span> 
			      		<span style="float: right">Coming Soon</span>
			      	</li>
			      </a>
			      <?php 
					   }
					}
			      ?>
			    </ul>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-7">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span >Deltidskurser</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Anmäl dig till vår Teks kurs och bli en mer effektiv webb och apputvecklare<br> ( Längd: 4 - 6 veckor )</p>
              
              <!-- <div class="margin-top"></div>
              
              <a href="trainingplace.php" class="btn-system btn-large border-btn btn-black">View Part-Time Courses</a> -->

            </div>

            <div class="col-md-5">
				
				<div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/2.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
   
</div>

<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >STOCKHOLM <span class="caret" ></span></div>
			      <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li>
			      	<a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> ">
			      		<?php echo $result['locationname']; ?>
			      	</a>
			      </li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
               <ul class="list-group">
              	<?php 

					  $result = array();
					  $query = "SELECT * FROM  coursename";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <a href="detailcourse.php?detailcourse=<?php echo $result['name_en']; ?>&coursetime=PART-TIME COURSES">
			      	<li class="list-group-item">
			      		<span>
			      			<strong style="font-size: 18px;"><?php echo $result['name_en']; ?></strong>
			      		</span> 
			      		<span style="float: right">Coming Soon</span>
			      	</li>
			      </a>
			      <?php 
					   }
					}
			      ?>
			    </ul>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span>APPLE APPUTVECKLING - FRÅN WIREFRAMING TILL TESTER</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Lära sig att arbeta med Xcode 7 och Swift 2. Jobba med globalt kända utvecklare på olika iOS- (app)projekt .</p>
              
              <div class="margin-top"></div>
              
              <a href="contactus.php" class="btn-system btn-large border-btn btn-black">Kontakta oss</a> 

            </div>

            <div class="col-md-4">
				
		<div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/5.png">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>