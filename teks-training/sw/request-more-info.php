<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Request more info</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>MER OM VÅRA FÖRETAGSUTBILDNINGAR</span></h4>
            
            <p>Om du har en fråga om våra företagsutbildningar,  kontakta oss här.</p>
            
            <div class="margin-top"></div>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="NAMN" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="FÖRETAG" name="company">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="E-POST" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="TELEFON" name="phone">
                </div>
              </div>
              <div class="form-group">
              	<label style="color: #a9a9a9;"><strong>ANTALET ANSTÄLLDA</strong></label>
                <div class="controls">
                  <select name="employee">
					  <option value="1-250">1-250</option>
					  <option value="250-5000">250-5000</option>
					  <option value="More than 5000">More than 5000</option>
				 </select>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea rows="10" placeholder="BERÄTTA FÖR OSS VAD DITT FÖRETAG BEHÖVER" name="message"></textarea>
                </div>
              </div>
              <input type="hidden" name="submit" value="request">
              <button type="submit" id="submit" class="btn-system btn-large" style="background: #ff6106;">SKICKA</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>
          
          <div class="col-md-1"></div>

          <div class="col-md-4">

             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fdeba3;">
	              <div class="plan-name" style="background:#fbd334;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">NÄR DU KONTAKTAR OSS SÅ :</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Får du svar inom 24 timmar</li>
	                  <li style="border-bottom: 0px;">Detaljerad information om våra företagsutbildningar</li>
	                  <li style="border-bottom: 0px;">Schema gällande kommande workshops, webb och app-seminarier.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>
        	
        	<div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#dadada;border-bottom: 0px;">
	                <h3 style="color: #515151;">VILL DU VETA MER...? </h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Vänligen besök vår  <a href="corporatefaq.php" style="text-decoration: underline;">FAQ</a> sida.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>

          </div>

        </div>

      </div>
    </div>
    
    <script type="text/javascript">               
                    
					    $('#contact-form').on('submit', function(e) {
					        e.preventDefault(); 
					        var form = $(this); 
					      
					        var post_data = form.serialize();
					
					        $.ajax({
					            type: 'POST',
					            url: 'submitrequest.php',
					            data: post_data,
					            success: function(msg) {
					                $('#contact-form').fadeIn(500, function(){
					            	alert("Thank you for contacting us we will get back to you very soon");
					             	form.trigger('reset');
					                });
					            }
					        });
					    });

   				</script>
   				
   				

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>