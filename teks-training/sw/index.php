<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

  <!-- Basic -->
  <title>Teks Learning | Home</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider.php' ?>
  
  <?php include 'coursebytopic.php' ?>

    <!-- Start Purchase Section -->
    <div class="section purchase">
      <div class="container">

        <!-- Start Video Section Content -->
        <div class="section-video-content text-center">

          <!-- Start Animations Text -->
          <h1 class="fittext wite-text tlt" >
                      <span class="texts" style="text-transform: capitalize;">
                        <span style="text-transform: capitalize;">Skräddarsydd</span>
                        <span style="text-transform: capitalize;">Djuplodande</span>
                        <span style="text-transform: capitalize;">Värdefull</span>
                        <span style="text-transform: capitalize;">Praktisk</span>
                        <span style="text-transform: capitalize;">Interaktiv</span>
                      </span>
                        MOBIL OCH WEBBUTVECKLINGSKURSER SOM HJÄLPER DIG ATT BLI EN FRAMGÅNGSRIK OCH PROFFESSIONELL MJUKVARUUTVECKLARE.
                    </h1>
          <!-- End Animations Text -->

        </div>
        <!-- End Section Content -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Purchase Section -->
   
   <?php include 'indexcourse.php' ?>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
  <script type="text/javascript">
	$(document).ready(function(){
		$('#index').addClass('active');
	});

</script>

</body>

</html>