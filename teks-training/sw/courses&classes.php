<?php include_once 'mysqlconnect.php'; ?>
<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Courses & Classes</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
    <div class="section courses">

      <div class="container" >
     
     <div class="col-md-12">
            
            <div class="margin-top"></div>
            
            <!-- Classic Heading -->
              <h1 class="big-title" style="text-align:center;">APPUTVECKLINGSKURSER</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="text-align: center;">Här lägger vi upp kommande utvecklingskurser för Webb, iOS och Android. Välj stad och kurs. </p>
              
             <div class="margin-top"></div>
             
             
              
              
              <div class="margin-top"></div>
              
              <ul class="list-group">
			    <a href="ios-workshop.php"><li class="list-group-item"><span >iOS-Con 2016: Training & Development</span> <span style="float: right">9th May to 13th May 2016 </span></li></a>
			    <a href="android-workshop.php"><li class="list-group-item"><span style="text-align: left">Deal With Droid 2016: The Android Workshop</span> <span style="float: right">9th May to 13th May 2016</span></li></a>
			  </ul>

        </div>
        
          <div class="col-md-12">
              <div class="margin-top"></div>
              <!-- Classic Heading -->
              <h1 class="big-title" style=" text-align: center;">APP OCH WEBBUTVECKLINGSUTBILDNING</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="text-align: center;">Bläddra genom vårt utbud av iOS, Android och Webbutvecklingsutbildningar - och välj en som är bäst för dig. 
              	 </p>
              
             <div class="margin-top"></div>

            </div>
            
            <div class="col-md-12">
            	
            	<div class="margin-top"></div>

               
              <h1 class="big-title">VÄLJ DIN KURS</h1>
              
              <div class="margin-top"></div>
              
          <div class="row pricing-tables">

          <div class="col-md-4 col-sm-4">
            
            	<div class="pricing-table">
	              <div class="plan-name">
	                <h3>KURSER IOS- UTVECKLING</h3>
	              </div>
	              <div class="plan-price">
	                <img src="images/course/6.png" />
	              </div>
	              <div class="plan-list">
	                <ul>
	                  <li>Du kan välja mellan olika branschorienterade IOS apputvecklingmoduler och handledda klasser och workshops. Delta i informativa seminarier, workshops och paneldiskussioner. Klasserna leds av kända Apple utvecklare och app-entreprenörer. Du får jobba med dummy projekt av varierande komplexitet, lära dig Objective-C och Swift programmering, och påbörja med att göra appar professionellt.</li>
	                </ul>
	              </div>
            	</div>
            
          </div>

          <div class="col-md-4 col-sm-4">
          	
            <div class="pricing-table">
              <div class="plan-name">
                <h3>KURSER ANDROID-UTVECKLING</h3>
              </div>
              <div class="plan-price">
                <img src="images/course/8.png" />
              </div>
              <div class="plan-list">
                <ul>
                  <li>Lära sig att göra Androidappar från googleutvecklare med rik erfarenhet av branschen. Få praktisk utbildning på Java-programmering, få fördjupad kunskap om testning, app felsökning och den övergripande processen att omvandla livskraftiga idéer till effektiva mobila applikationer. Greppa chansen att arbeta i samverkan med några av de bästa Androidutvecklarna på marknaden.  </li>
                </ul>
              </div>
            </div>
            
          </div>


          <div class="col-md-4 col-sm-4">
          	
            <div class="pricing-table">
              <div class="plan-name">
                <h3>KURSER WEBBUTVECKLING</h3>
              </div>
              <div class="plan-price">
                <img src="images/course/7.png" />
              </div>
              <div class="plan-list">
                <ul>
                  <li>Välj en Teks webbutbildning, och lär dig att skapa webbplatser från grunden. Delta i speciellt organiserade HTML5 och CSS workshops, lära dig hur sidor bör utformas, anpassas, underhållas och optimeras. Ta reda på hur man använder online-verktyg för att analysera webbplatsens prestanda, och hur du kan planera för att förbättra effektiviteten i webbportalerna.</li>
                </ul>
              </div>
            </div>
            
          </div>

        </div>
     </div>

      	</div>
      <!-- .container -->
	</div>
	
   

  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>GE DIN karriär " TEKS EDGE " !</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Undrar varför du bör gå en Teks learning ? Nåväl, det finns skäl i överflöd ! Till att börja med , vi är ett namn att räkna med när det gäller globala mobila applikationsutveckling - med över 900 applikationer ( iOS och Android i kombination ) i vår portfölj . Vi erbjuder även skräddarsydda utbildningar och handledning - så att du kan få maximal nytta . Förekomsten av vårt erfarna team av webb och mobila app utvecklare är en stor hjälp - och vi också bjuda in gästföreläsare , utvecklare och testare vid våra applikationsutveckling seminarier . God teoretisk kunskap med praktisk kompetensutveckling - det är vad du får genom att fylla i en kurs på Teks .
</p>
              
              <!-- <div class="margin-top"></div> -->
              

            </div>

            <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-02.jpg">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>