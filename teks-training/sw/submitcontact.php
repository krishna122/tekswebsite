<?php

if (!isset($_POST['submit']) || $_POST['submit'] != 'contact') {
    exit;
}
	
// get the posted data
$contactname = $_POST['contact_name'];
$company = $_POST['company'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

$headers = "From: $email\r\n"; 
$headers .= "Reply-To: $email\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

// write the email content
$email_content = "<strong>Name:</strong> $contactname <br>";
$email_content .= "<strong>Company:</strong> $company <br>";
$email_content .= "<strong>Email Address:</strong> $email <br>";
$email_content .= "<strong>Phone Number:</strong> $phone <br>";
$email_content .= "<strong>Message:</strong> $message";
	
// send the email

mail ('info@teksmobile.se',  'Teks Training - Contact', $email_content, $headers);
	
// send the user back to the form
header('Location: index.php');

?>