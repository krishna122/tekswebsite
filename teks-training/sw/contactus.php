<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Contact Us</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>HAR DU EN FRÅGA GÄLLANDE NÅGON AV VÅRA WEBB OCH APP-KURSER? </span></h4><br>
            
            <p>Skriv till oss och få svar inom 24 timmar. </p>
            
            <div class="margin-top"></div>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="NAMN" name="contact_name" required>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="FÖRETAG" name="company">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email"  placeholder="E-POST" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="TELEFONNUMMER" name="phone">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea rows="10" placeholder="VAD GÄLLER DITT ÄRENDE" name="message"></textarea>
                </div>
              </div>
              <input type="hidden" name="submit" value="contact">
              <button type="submit" id="submit" class="btn-system btn-large" style="background: #ff6106;">SKICKA</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>
          
          <div class="col-md-1"></div>

          <div class="col-md-4">

             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fdeba3;">
	              <div class="plan-name" style="background:#fbd334;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">VI SVARAR DIG INOM KORT</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Du får ett detaljerat svar från någon av våra erfarna in - houseutvecklare.</li>
	                  <li style="border-bottom: 0px;">Uppdaterad kursplan för våra webb, Android och iOS - utvecklingskurser.</li>
	                  <li style="border-bottom: 0px;">En flödesschema över hur en Teks kurs kan lyfta dina karriärsmöjligheter. </li>
	                </ul>
	              </div>
	            </div>
          
        	</div>
        	
        	<div class="row pricing-tables">
	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#dadada;border-bottom: 0px;">
	                <h3 style="color: #515151;">HAR DU FLER FRÅGOR?</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Gå till <a href="generalfaq.php" style="text-decoration: underline;">FAQ</a>.</li>
	                </ul>
	              </div>
	            </div>
        	</div>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">               
                    
					    $('#contact-form').on('submit', function(e) {
					        e.preventDefault(); 
					        var form = $(this); 
					      
					        var post_data = form.serialize();
					
					        $.ajax({
					            type: 'POST',
					            url: 'submitcontact.php',
					            data: post_data,
					            success: function(msg) {
					                $('#contact-form').fadeIn(500, function(){
					            	alert("Thank you for contacting us we will get back to you very soon");
					             	form.trigger('reset');
					                });
					            }
					        });
					    });

   				</script>
   				
  <?php include 'footer.php' ?>

<?php include 'bottom.php' ?>
  </div>
  <!-- End Full Body Container -->
  
  

 
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#contact').addClass('active');
	});

</script>



</body>

</html>