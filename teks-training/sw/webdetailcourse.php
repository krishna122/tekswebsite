<!-- Start Services Section -->
    <div class="section service courses" id="web" style="display: non;">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="font-size: 40px; text-align: center;text-transform: uppercase;">Web Development @ Teks Learning Center</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/15.png" />
            </div>
            <div class="service-content">
              <h4>Multiple Platforms</h4>
              <p>Get personalised training sessions on website development on Wordpress, Drupal, Joomla and Magento.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/16.png" />
            </div>
            <div class="service-content">
              <h4>HTML, CSS & more…</h4>
              <p>Join our web development tutorial course and learn how to work with HTML and CSS stylesheets. Eminent web designers are available as mentors.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/17.png" />
            </div>
            <div class="service-content">
              <h4>Responsive Websites</h4>
              <p>Know how to work with tools like PHP, MySQL and other updated tools to make responsive, optimised websites. Learn to build websites for personal and professional purposes.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    




<!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style="font-size: 40px;">Become An Expert Web Developer</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">And we will help you in that, at every step. Let’s take a brief tour of some salient features of the web development courses that we offer:</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/1.png" />
            </div>
            <div class="service-content">
              <h4>Learn From The Best</h4>
              <p>Excellence is what we strive for at our mobile and web training institute. In addition to our in-house trainers, top professional web developers are regularly invited to interact with students.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/2.png" />
            </div>
            <div class="service-content">
              <h4>Guidance When You Need</h4>
              <p>At Teks Learning, we have experienced mentors to guide you on a constant basis. Clarifying all doubts and queries regarding website development is easier than ever.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/3.png" />
            </div>
            <div class="service-content">
              <h4>Industry-Oriented Learning</h4>
              <p>The units and modules of our web development/designing/HTML/CSS courses are arranged in a manner to brighten your career prospects. Join us and let us help you enhance your development skills.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->