<!-- Start Services Section -->
    <div class="section courses-white">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1><span>Kurser på <strong>Teks</strong> utbildningscenter</span></h1>
            <p class="title-desc">Hos Teks kan du anmäla dig till följande kurser</p>
          </div>
          <!-- End Big Heading -->
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <i class="fa fa-apple icon-large"></i>
            </div>
            <div class="service-content">
              <h2>iOS - utbildning</h2><br>
              <p>Anmäl dig till vår serie av utvecklingskurser för iPhone. Internationella seniora utvecklingsexperter från branchen håller i kurserna. Efter utbildningen har du "Hands on" - erfarenhet om hur man gör en iOS-app.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <i class="fa fa-android icon-large"></i>
            </div>
            <div class="service-content">
              <h2>Android - utbildning</h2><br>
              <p>Anmäl dig till en av våra kurser och bli en professionell Android - utvecklare. Vi håller seminarier, workshops, utbildningar och mycket mer.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <i class="fa fa-television icon-large"></i>
            </div>
            <div class="service-content">
              <h2>Webb - utbildning</h2><br>
              <p>Lär dig att skapa, utveckla, designa och underhålla personliga och professionella hemsidor. Specialiserade kurser för plattformarna: Wordpress, Drupal, Joomla och Magento.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->