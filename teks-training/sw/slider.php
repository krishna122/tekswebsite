<!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="images/slider/bg1.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span>Mobila <strong>och</strong> webbbaserade plattformar</span>
                              </h2>
                <h3 class="animated3">
                                <span>Utbildningscenter</span>
                              </h3>
                <p class="animated4"><a href="courses&classes.php" class="slider btn btn-system btn-large" style="color:#515151;">kommande workshop</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->