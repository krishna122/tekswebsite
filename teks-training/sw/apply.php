<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Apply For</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Apply For <strong style="text-transform: uppercase;">Course Name</strong></span></h4>
            
            <p>Request More Information about Teks's Training Solutions</p>
            
            <div class="margin-top"></div>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Company" name="company">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Phone" name="phone">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea rows="12" placeholder="Tell us a bit about your query" name="message"></textarea>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system btn-large" style="background: #ff6106;">Submit</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>
          
          <div class="col-md-1"></div>

          <div class="col-md-4">

             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fdeba3;">
	              <div class="plan-name" style="background:#fbd334;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">What You'll Get ?</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Response Within 24 Hours</li>
	                  <li style="border-bottom: 0px;">Detailed Syllabus Of The Course(s) You Want To Join.</li>
	                  <li style="border-bottom: 0px;">Information Sheet On How The Course Will Help Your Career.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>
        	
        	<div class="row pricing-tables">
	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#dadada;border-bottom: 0px;">
	                <h3 style="color: #515151;">HAVE ANOTHER QUERY?</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;"><a href="contactus.php" style="text-decoration: underline;">Contact Us</a> and get your answers!</li>
	                </ul>
	              </div>
	            </div>
        	</div>
          </div>
        </div>
      </div>
    </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 

</body>

</html>