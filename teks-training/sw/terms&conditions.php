<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Terms Of Service</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
    
    <div id="content">
      <div class="container">
        <div class="row blog-post-page">
          <div class="col-md-12 blog-box">

            <!-- Start Single Post Area -->
            <div class="blog-post gallery-post">

              <!-- Start Single Post Content -->
              <div class="post-content">
                <h1 style="text-align: center;">Terms Of Service</h1><br>
                
                <p>Welcome to the official website of Teks Learning - the premier iOS, Android, Web, HTML and CSS training center in India. Before proceeding further, we request you to kindly go through the commercial terms & conditions for using our learning services:</p>
                
                <ul class="liststyle">
                	
                	<li>Terms like ‘We’, ‘Our’, and ‘This Institute’ refer to one and the same entity - Teks Learning (organized by Teknowledge Software). On the other hand, ‘you’, ‘your’, ‘students’, ‘candidates’, etc. refer to all readers - people who have already registered in our tutorial classes and those who are planning to, but are yet to join.</li>
                	
                	<br>
                	
                	<li>All images, content, designs, logos, trademarks, symbols and other visual material present on this website are copyrighted. Unauthorized reproduction, sale, copy, or usage for any other purpose is prohibited, and amounts to a chargeable offence.</li>
                	
                	<br>
                	
                	<li>Kindly note that when you join any of our web or mobile app development training courses, you do so out of your OWN FREE WILL. We advise you to go through the curriculum of each course carefully, and then select the one that would be most suitable for you.</li>
                	
                	<br>
                	
                	<li>The syllabus, tutorial modules, learning material (print version and electronic version) of the courses are proprietary material of Teks Learning. You DO NOT HAVE THE PERMISSION to sell them to any other individual/agency. Neither can you POSE as a representative of Teks Learning, without prior consent from us.</li>
                	
                	<br>
                	
                	<li>We earnestly request you to make the course payments (as applicable) on time and in the right manner. Failure to submit the fees within the stipulated deadline can lead to your name being struck off the records in a particular class.</li>
                	
                	<br>
                	
                	<li>Plans change, needs change - and at Teks Learning, we appreciate that. That’s precisely why we have a CANCELLATION/REFUND policy. If you decide to not join a course you had originally signed up for, please inform us to the effect within seventy-two (72) hours of the initial registration. No refund requests will be entertained after that.</li>
                	
                	<br>
                	
                	<li>At our app and web training center, we rely on strong business ethics and transparency. While we arrange for the best developers, testers, designers, app experts and other IT professionals in India to collaborate with you as guides and mentors - it is your responsibility to follow class routines and get the maximum benefits from your chosen course. We have the setup all ready, you only have to use it optimally!</li>
                	
                	<br>
                	
                	<li>Participation in the app development seminars, workshops and other events organized by Teks Learning is optional. However, we strongly urge you to not miss them - since they complement the classroom teaching in the best possible manner.</li>
                	
                	<br>
                	
                	<li>Shifting from one full-time course to another is permitted for an enrollee, provided (s)he informs us about the same in advance. Please note that learners CANNOT shift from a full-time course to a part-time course (or vice versa).</li>
                	
                	<br>
                	
                	<li>In case of any conflict between the information present on this <a href="index.php" style="text-decoration: underline">website</a> and other online/offline sources - the former would prevail.</li>
                	
                </ul>
                
              <p>Kindly check out our <a href="privacypolicy.php" style="text-decoration: underline">Privacy Policy</a> as well, before starting to browse <a href="index.php" style="text-decoration: underline">courses</a>. If you wish to become a successful mobile / web developer, Teks Learning is there to help you take the best foot forward.</p>
                
              </div>
              <!-- End Single Post Content -->
            </div>
            <!-- End Single Post Area -->
          </div>
        </div>
      </div>
    </div>
    <!-- End content -->
  
  </div>



  <?php include 'footer.php' ?>

<style>
	.liststyle{
		list-style: circle;
		margin: 25px;	
	}
</style>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>