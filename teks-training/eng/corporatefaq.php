<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

	<head>

		<!-- Basic -->
		<title>Teks Learning | General FAQ</title>
		<!-- Page Description and Author -->
		<meta name="description" content="">
		<meta name="author" content="">

		<?php include 'head.php' ?>

	</head>

	<body>

		<!-- Full Body Container -->
		<div id="container">

			<?php include 'header.php' ?>

			<div class="section courses" style="display: non;">

				<div class="container">

					<div class="col-md-12">

						<!-- Classic Heading -->
						<h1 class="big-title text-center"><span style="font-size: 40px; text-transform: uppercase;">FAQ - Corporate Learning</span></h1>

						<!-- Some Text -->
						<p>
							Teks Learning brings to you the best corporate training courses in India, in the domain of web and mobile app development. Do go through the following common questions, find the answers, and then proceed with the selection of a course.
						</p>

						<div class="margin-top"></div>

						<div class="panel-group" id="accordion">

							<!-- Start Accordion 1 -->
							<div class="panel panel-default" style="background: #fff;border:0px;">
								<div class="panel-heading">
									<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 1. I Do Not Need To Start From The Basics. Can I Start From An Advanced Level? </a></h4>
								</div>
								<div id="collapse-1" class="panel-collapse collapse in" aria-expanded="false" >
									<div class="panel-body" style="background: #fff;">
										<p>
											Absolutely...in fact, that’s what our corporate training sessions and modules are all about. Just as we have app training courses for <a href="generalfaq.php" style="text-decoration: underline;">beginners</a>, we provide senior-level training to our corporate enrollees.
										</p>
									</div>
								</div>
								<!-- End Accordion 1 -->

								<!-- Start Accordion 2 -->
								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a data-toggle="" data-parent="#accordion" aria-expanded="false" class="collapsed" style="background: #fff;"> 2. What Platforms & Coding Languages Will You Train Me In? </a></h4>
									</div>
									<div id="collapse-2" class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												Depending on your requirements and preferences, we will show you how to make an app with Swift 2 (and Xcode 7), or Objective-C, or Java. Special Cocoa and Cocoa Touch training sessions are also organized. We also have training courses on web development for business.
											</p>
										</div>
									</div>
								</div>
								<!-- End Accordion 2 -->

								<!-- Start Accordion 3 -->
								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 3. Is There Any Provision For Part-Time Courses? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												As working professionals, we appreciate the fact that you might not get the time to join a full-time course on web training or mobile app development training. That’s the main reason why we have come up with the part-time courses, which you can join easily. The courses are shorter in duration, but are equally beneficial.
											</p>
										</div>
									</div>
								</div>
								<!-- End Accordion 3 -->

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 4. Will I Get Any Opportunities To Network With Peers? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												As much as you want. You will be collaborating with leading mobile app developers, website designers and developers, and other top IT professionals from India, Australia and Sweden. Your fellow-learners would also be corporate professionals. There will be more than ample opportunities for networking, idea exchange and knowledge transfer.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 5. I Want To Organize A Series Of Training Classes For My Employees. Will That Be Possible? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												Of course. All that you have to do is <a href="contactus.php" style="text-decoration: underline;">contact us</a> and share a few details about your company. We will get back to you with custom proposals for app/web training classes, within 24 hours.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 6. Why Is Joining A Teks Learning Course Necessary For Me?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												 The field of mobile app development, or website development & maintenance for that matter, is vast. It is virtually impossible for a busy corporate professional like yourself to keep abreast of all the changes that are constantly happening in these fields. When you join a Teks course, it is our responsibility to keep you informed and regularly updated, at all times.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 7. You Mean That Completing A Course Will Be Good For MY Business Too?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												Definitely. Irrespective of whether you operate as a freelance indie developer, or are a senior person at a mobile app company - completing a formal corporate training course will take up your expertise by several notches. Our training modules will help you serve clients better - and that, as we all know, is great news for business.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 8. Will I Get To Work On Project Simulations & Dummy Projects?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												The corporate training courses and classes at Teks are focused to familiarize you with more varieties, techniques and methods of making mobile apps and websites. During the course workshops, you will work on dummy projects - on the platform you are interested in. All such sessions are helmed by internationally recognized app developers, who would ensure that you are doing things the right way.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 9. What If I Sign Up For, But Do Not Attend A Workshop?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												We would strongly recommend you to not do this - but well, emergencies happen. Provided that you inform us about your inability to participate in an event you had registered for - you will get a full refund of the registration fee. Please note that this refund policy will lapse if you fail to give us this information within 72 hours of registering for the first time.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 10. So, I Will Have To Provide My Company Details. Will Such Information Remain Safe With You?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												Trust us on that. We follow a stringent set of <a href="privacypolicy.php" style="text-decoration: underline;">Privacy Policy</a> guidelines. All the personal and professional data you provide are kept secure in our center database. There is no chance of any unauthorized access at all.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 11. How Experienced Are You Guys? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												Oh well - it is only natural to be curious about the qualifications and relevant experience of the people you will be learning from. We have been in the business of website creation and making mobile apps for close to a decade. Each mentor in charge of the Teks Learning courses have rich industry experience - with many of them having won prestigious professional awards as well.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 12. How Can I Keep Track Of The Workshops and Seminars You Organize?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												That’s simple enough. We regularly update the details (topic, venue, timings, etc.) of all our upcoming seminars, discussions and workshops - in various cities across India. Keep visiting this website, and you’ll get all the information you need. Joining special app/web training events will be easy.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 13. Can You Guys Arrange For Training Sessions Customized For My Company? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												We most certainly can. Just head to the <a href="request-more-info.php">Request More Information</a> page, and leave the details of the type of app or web development training session(s) you wish to arrange for your company. You will receive personalized training course syllabus and other relevant information within one working day.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 14. Can I Share My Ideas And Experiences During The Courses?</a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												We would look forward to it. It is a constant endeavor of ours to make the website development, or the iOS/Android development training classes as interactive as possible. When you and your fellow-learners start sharing their own industry experiences - it will be great for the entire batch.
											</p>
										</div>
									</div>
								</div>

								<div class="panel panel-default" style="background: #fff;border:0px;">
									<div class="panel-heading">
										<h4 class="panel-title"><a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;"> 15. What Are The Course Fees? </a></h4>
									</div>
									<div  class="panel-collapse collapse in" aria-expanded="false" >
										<div class="panel-body" style="background: #fff;">
											<p>
												That varies slightly with the nature of (iOS, Android or Web) and type of (full-time or part-time) training course you opt for. One thing we can assure you about though - our course fees are the most competitive in India. You will always get more than adequate value for money for us.
											</p>
										</div>
									</div>
								</div>

							</div>
						</div>
						
						<p>Startups to MNCs, our corporate training courses are open for professionals associated with all types of organizations. Get in touch with us, choose courses, and let us give your career and your company a lift.
</p>

					</div>
				</div>
				<!-- .container -->
			</div>

		</div>

		<?php include 'footer.php' ?>

		<style>
			.liststyle {
				list-style: circle;
				margin: 25px;
			}
		</style>

		</div>
		<!-- End Full Body Container -->

		<?php include 'bottom.php' ?>

	</body>

</html>