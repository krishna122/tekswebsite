<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

  <!-- Basic -->
  <title>Teks Learning | Home</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider.php' ?>
  
  <?php include 'coursebytopic.php' ?>

    <!-- Start Purchase Section -->
    <div class="section purchase">
      <div class="container">

        <!-- Start Video Section Content -->
        <div class="section-video-content text-center">

          <!-- Start Animations Text -->
          <h1 class="fittext wite-text uppercase tlt">
                      <span class="texts">
                        <span>Customised</span>
                        <span>In-Depth</span>
                        <span>Valuable</span>
                        <span>Interactive</span>
                        <span>Practical</span>
                      </span>
                        Mobile And Web Development Training Courses For Helping You Become A Successful Software Professional
                    </h1>
          <!-- End Animations Text -->


          <!-- Start Buttons -->
          <a href="#" class="btn-system btn-large btn-wite" style="color: #515151; font-size: 16px;"><i class="fa fa-thumbs-o-up"></i>Join A Teks Training Course</a>

        </div>
        <!-- End Section Content -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Purchase Section -->
   
   <?php include 'indexcourse.php' ?>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
  <script type="text/javascript">
	$(document).ready(function(){
		$('#index').addClass('active');
	});

</script>

</body>

</html>