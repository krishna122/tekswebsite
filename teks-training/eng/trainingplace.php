<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Detail of Courses</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div class="section" style="background: #ececec;">

      <div class="container" >

        <div class="col-md-12">
				
				
			<div class="col-md-8">
				<!-- Some Text -->
              <p style="font-size:30px; line-height: 30px;">Find Web/Mobile Development Training Classes</p>
              
              <div class="margin-top"></div>
              
              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">KOLKATA</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">PUNE</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">STOCKHOLM</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">SYDNEY</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">COPENHAGEN</a></li>
			    </ul>
			  </h1>
              </div>
              
              <div class="col-md-4">
              	
              	<div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fff;">
	              <div class="plan-name" style="background:#fff;border-bottom: 0px;">
	                <p style="color: #515151; text-align: center"><strong>Become A Developer</strong></p>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px; text-align: center;">Browse Courses, Workshops & Seminars</li>
	                  <li style="border-bottom: 0px;border-top: 0px; text-align: center;">
	                  	<a class="animated4 slider btn btn-system btn-large btn-min-block" href="#" style="color: #515151;">JOIN A COURSE</a>
	                  	</li>
	                  	<li style="border-bottom: 0px;border-top: 0px; text-align: center;"></li>
	                </ul>
	              </div>
	            </div>
        	</div>
              	
              	</div>
              
            </div>

      	</div>
      	<!-- .container -->
	</div>
    
    
    <div class="section courses-white">

      <div class="container" >
       
       <div class="col-md-12">
       	
        <div class="col-md-6">

              <!-- Classic Heading -->
              <h4 class="big-title"><span style="">Sort Courses/Sessions By Type</span></h1>
              
              <div class="margin-top"></div>
              
              <div class="select-style">
				  <select>
				  	<option value="All Formats">All Sessions</option>
				    <option value="Full Time Corses">Part­Time Courses</option>
				    <option value="Part Time Courses">Full­Time Courses</option>
				    <option value="Classes & Workshops">Workshops & Seminar</option>
				    <option value="Events">Special Events</option>
				  </select>
				</div>
              <div class="margin-top"></div><br>

            </div>

            <div class="col-md-6">
				
                <!-- Classic Heading -->
              <h4 class="big-title"><span style="">Sort Courses/Sessions By topic</span></h1>
              
              <div class="margin-top"></div>
              
              <div class="select-style">
				  <select>
				  	<option value="All Topics">All Courses</option>
				    <option value="iOS App Development">iOS App Development Courses</option>
				    <option value="Android App Development">Android App Development Courses</option>
				    <option value="Web development">Web development Courses</option>
				  </select>
				</div>
              <div class="margin-top"></div><br />
            </div>
            
        <p>Explore all of our upcoming offerings, from 90-minute classes to full-time courses.</p>
        
        <div class="margin-top"></div>
        
        
        <h3>Today</h3>
        
        <br />
        
        <p style="border-bottom: 1px solid #dadada;"></p>
        
        <div class="margin-top"></div>
        
        <div class="col-md-4"><img src="images/course/14.png" /></div>
        
        <div class="col-md-4">
        	<h3>WEB DEVELOPMENT IMMERSIVE</h3>
        	<br>
        	<p>Learn the most sought-after skills in tech, from JavaScript to Rails, with guidance from our team of experienced instructors.</p>
        	
        </div>
        
        <div class="col-md-4"></div>
         	 <p>Mon, 2 Nov</p>
          </div>
          
          <!-- --------------------------------------------------------------------------------------------------- -->
          
          
         <h3>Tue, 19 Sept </h3>
        
        <br />
        
        <p style="border-bottom: 1px solid #dadada;"></p>
        
        <div class="margin-top"></div>
        
        <div class="col-md-4"><img src="images/course/14.png" /></div>
        
        <div class="col-md-4">
        	<h3>WEB DEVELOPMENT IMMERSIVE</h3>
        	<br>
        	<p>Learn the most sought-after skills in tech, from JavaScript to Rails, with guidance from our team of experienced instructors.</p>
        	
        </div>
        
        <div class="col-md-4"></div>
         	 <p>Mon, 2 Nov</p>
          </div>
          
             
      </div>
      <!-- .container -->
</div>

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>