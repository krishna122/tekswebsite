<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Learn HTML</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div class="section" style="background: #d3668b;">

      <div class="container" >

        <div class="col-md-12">
				
				<h4 style="text-align: center;color: #fff;">10 Weeks | PART-TIME | FULL-TIME Course</h4>
				
				<div class="margin-top"></div>
				
              <!-- Classic Heading -->
              <h1 class="big-title" style="font-size: 50px; text-align: center; color: #fff; line-height: 50px; text-transform: uppercase;">LEARN HTML, CSS & WEB DESIGN</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="color:#fff;text-align: center;">LEARN TO DESIGN AND BUILD BEAUTIFUL, RESPONSIVE WEBSITES USING HTML AND CSS.</p>
              
              <div class="margin-top"></div>
              
              <div class="" style="text-align: center;">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="#" style="color: #fff;background: #ff8947;">Request Syllabus</a>
                  <a class="animated4 slider btn btn-default btn-min-block" href="#">Enroll Now </a>
                </div>
              
            </div>

      	</div>
      	<!-- .container -->
	</div>

<!-- --------------------------------- WEB ---------------------------------------------------------------------- -->
		
	<!-- Start Services Section -->
    <div class="section service courses" id="web">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="font-size: 40px; text-align: center;">OVERVIEW</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/10.png" />
            </div>
            <div class="service-content">
              <h4>Skills & Tools</h4>
              <p>Use the command line, Sublime Text, GitHub, and Ruby on Rails in concert.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/11.png" />
            </div>
            <div class="service-content">
              <h4>Production Standard</h4>
              <p>Build a web application that authenticates login, responds to user input, and connects to services like Reddit and Facebook.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/12.png" />
            </div>
            <div class="service-content">
              <h4>The Big Picture</h4>
              <p>Gain a working knowledge of what powers modern web applications: databases, routing, web views, APIs, and more.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    

    
    
          
    <!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style="font-size: 40px;">MEET YOUR SUPPORT TEAM</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">Our educational excellence is a community effort. When you learn at GA, you can always rely on an in-house team of experts to provide guidance and support, whenever you need it.</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/1.png" />
            </div>
            <div class="service-content">
              <h4>Instructors</h4>
              <p>Learn industry-grade frameworks, tools, vocabulary, and best practices from a teacher whose daily work involves using them expertly.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/2.png" />
            </div>
            <div class="service-content">
              <h4>Teaching Assistants</h4>
              <p>Taking on new material isn’t always easy. Through office hours and other channels, our TAs are here to provide you with answers, tips, and more.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/3.png" />
            </div>
            <div class="service-content">
              <h4>Course Producers</h4>
              <p>Our alumni love their Course Producers, who keep them motivated throughout the course. You can reach out to yours for support anytime.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    
    
    <div class="section" style="background: #ffdf00;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="Big-title text-center" style="font-size: 40px;line-height: 40px;">EMBRACE THE DETAILS</h1>
              
              <div class="margin-top"></div>
              
              <h4>Unit 1: Ruby</h4>
              
              <div class="margin-top"></div>
              
              <div class="panel-group" id="accordion">

              <!-- Start Toggle 1 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-up control-icon"></i>
						Introduction to Web Programming and GitHub
						</a>
				  </h4>					
                </div>
                <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
                  </div>
                </div>
              </div>
              <!-- End Toggle 1 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Ruby Basics
						</a>
				  </h4>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 2 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Everything is an Object
					  </a>
				  </h4>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 3 -->

            </div>
            
            
            
            
              <div class="margin-top"></div>
              
              
              
              <h4>Unit 2: Ruby on Rails</h4>
              
              <div class="margin-top"></div>
              
              <div class="panel-group" id="accordion">

              <!-- Start Toggle 4 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-up control-icon"></i>
						Introduction to Web Programming and GitHub
						</a>
				  </h4>					
                </div>
                <div id="collapse-4" class="panel-collapse collapse" aria-expanded="true" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
                  </div>
                </div>
              </div>
              <!-- End Toggle 4 -->

              <!-- Start Toggle 5 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Ruby Basics
						</a>
				  </h4>
                </div>
                <div id="collapse-5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 5 -->

              <!-- Start Toggle 6 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Everything is an Object
					  </a>
				  </h4>
                </div>
                <div id="collapse-6" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 6 -->

            </div>
            
            
            <div class="margin-top"></div><br><br>
            
            
            <div class="call-action call-action-boxed call-action-style3 clearfix">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="#" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Get Syllabus</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">Request a detailed syllabus</h2>
          </div>

        </div>
	
	</div>
      <!-- .container -->
</div>
 
 
   <div class="section courses-white">

      <div class="container" >
      	
      	<div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/team/4.png">
                </div>
              
            </div>

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>
              </div>
              <div class="testimonial-author text-center"><span>Joe Leo</span> / President, Def Method</div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<div class="section" style="background: #ffdf00;">

      <div class="container" >
      	
      	<div class="col-md-12">
            
            <div class="margin-top"></div>
            
            <!-- Classic Heading -->
              <h1 class="big-title" style="font-size: 40px; text-align: center;">TUITION & DATES</h1>
              
             <div class="margin-top"></div>
             
             
              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">KOLKATA</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">PUNE</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">STOCKHOLM</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">SYDNEY</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">COPENHAGEN</a></li>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 16px;">We’re currently scheduling more sessions in Singapore <BR> Be the first to know when we put something on the calendar. 
			    	<a href="" style="text-decoration: underline;">Get Notified.</a></li>
			  </ul>

        </div>

      </div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><span style="font-size: 40px;">GET ANSWERS</span></h1>
              
              <!-- Some Text -->
              <p>We love questions, almost as much as we love providing answers. Here are a few samplings of what we’re typically asked, along with our responses:</p>
              
              <div class="margin-top"></div>
              
          <div class="panel-group" id="accordion" >

                  <!-- Start Accordion 1 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Why is this course relevant today?
						</a>
					  </h4>						
                    </div>
                    <div id="collapse-7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body" style="background: #fff;">
                      	<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>
                    </div>
                  </div>
                  <!-- End Accordion 1 -->

                  <!-- Start Accordion 2 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 What practical skill sets can I expect to have upon completion of the course?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-8" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 2 -->

                  <!-- Start Accordion 3 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Who will I be sitting next to in this course?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-9" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>

						<p>A web developer that creates client-side web sites can only go so far without back-end logic.</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 3 -->

                </div>
        </div>
</div>
      </div>
      <!-- .container -->
</div>

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>