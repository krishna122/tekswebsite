<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Contact Us</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Have A Query About Any Of Our Web Or App Development Courses?</span></h4><br>
            
            <p>Write in to us...and get your answers within 24 hours!</p>
            
            <div class="margin-top"></div>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Company" name="company">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Phone" name="phone">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea rows="10" placeholder="Tell us a bit about your query" name="message"></textarea>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system btn-large" style="background: #ff6106;">Submit</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>
          
          <div class="col-md-1"></div>

          <div class="col-md-4">

             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fdeba3;">
	              <div class="plan-name" style="background:#fbd334;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">We Will Email You:</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Detailed answer to your query, directly from our in­house app developers.</li>
	                  <li style="border-bottom: 0px;">Updated syllabus of our web, Android and iOS development courses.</li>
	                  <li style="border-bottom: 0px;">A flowchart of how a Teks course can brighten your career prospects.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>
        	
        	<div class="row pricing-tables">
	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#dadada;border-bottom: 0px;">
	                <h3 style="color: #515151;">Have More Questions?</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Head over to our <a href="generalfaq.php" style="text-decoration: underline;">FAQ</a> section.</li>
	                </ul>
	              </div>
	            </div>
        	</div>
          </div>
        </div>
      </div>
    </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#contact').addClass('active');
	});

</script>

</body>

</html>