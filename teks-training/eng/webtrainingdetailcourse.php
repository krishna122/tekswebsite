 <!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style="font-size: 40px;">Become An Expert Web Developer</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">And we will help you in that, at every step. Let’s take a brief tour of some salient features of the web development courses that we offer:</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/1.png" />
            </div>
            <div class="service-content">
              <h4>Learn From The Best</h4>
              <p>Excellence is what we strive for at our mobile and web training institute. In addition to our in-house trainers, top professional web developers are regularly invited to interact with students.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/2.png" />
            </div>
            <div class="service-content">
              <h4>Guidance When You Need</h4>
              <p>At Teks Learning, we have experienced mentors to guide you on a constant basis. Clarifying all doubts and queries regarding website development is easier than ever.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/3.png" />
            </div>
            <div class="service-content">
              <h4>Industry-Oriented Learning</h4>
              <p>The units and modules of our web development/designing/HTML/CSS courses are arranged in a manner to brighten your career prospects. Join us and let us help you enhance your development skills.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->