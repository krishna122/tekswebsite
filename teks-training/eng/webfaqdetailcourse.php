<div class="section courses" style="display: non;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><span style="font-size: 40px; text-transform: uppercase;">Wish To Know More?</span></h1>
              
              <!-- Some Text -->
              <p>Here are some common questions that are probably on the top of your mind. Go through the answers, and find out how our web development and designing classes can help you:</p>
              
              <div class="margin-top"></div>
              
          <div class="panel-group" id="accordion" >

                  <!-- Start Accordion 1 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-7" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 What do the web courses include?
						</a>
					  </h4>						
                    </div>
                    <div id="collapse-7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Everything that you would need. From basic website registration and domain hosting, to developing personal/professional portals (including ecommerce sites) and day-to-day maintenance - our training modules include everything.
						</p>
                    </div>
                  </div>
                  <!-- End Accordion 1 -->

                  <!-- Start Accordion 2 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-8" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							 Who would be in charge of my web training programs?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-8" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 The best website development trainers in India will be at the helm of the classroom sessions, assignments, workshops and development seminars organised by us. There are extra modules for web app development training.
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 2 -->

                  <!-- Start Accordion 3 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-9" aria-expanded="false" class="collapsed" style="background: #fff;">
							<i class="fa fa-angle-up control-icon"></i>
							How will this course benefit me?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-9" class="panel-collapse collapse" aria-expanded="false">
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 The objective of all our mobile and web development tutorial classes are the same - bolstering your career prospects. We help you master the latest tools and techniques for making responsive, immersive websites on multiple platforms. With the knowledge, landing your dream job becomes easier!
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 3 -->

                </div>
        </div>
</div>
      </div>
      <!-- .container -->
</div>