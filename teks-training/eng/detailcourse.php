<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Detail of Courses</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div class="section" style="background: #FE6F5E;">

      <div class="container" >

        <div class="col-md-12">
				
				<h4 style="text-align: center;color: #fff;">10 Weeks | <?php echo $_REQUEST['coursetime']; ?></h4>
				
				<div class="margin-top"></div>
				
              <!-- Classic Heading -->
              <h1 class="big-title" style="font-size: 50px; text-align: center; color: #fff; line-height: 50px; text-transform: uppercase;"><?php echo $_REQUEST['detailcourse']; ?></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="color:#fff;text-align: center; text-transform: uppercase; display: none;" id="ios">Holistic Training Courses To Help You Learn How To Make Custom iPhone Apps</p>
              
              <p style="color:#fff;text-align: center; text-transform: uppercase; display: none;" id="android">Learn To Work With Android Development Tools And Make Android Apps</p>
              
              <p style="color:#fff;text-align: center; text-transform: uppercase; display: non;" id="web">Modular training courses on website creation, development and maintenance - including sessions on CSS and HTML.</p>
              
              <div class="margin-top"></div>
              
              <div class="" style="text-align: center;">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="#" style="color: #515151;">Request Syllabus</a>
                  <a class="animated4 slider btn btn-default btn-min-block" href="#">Join A Course </a>
                </div>
              
            </div>

      	</div>
      	<!-- .container -->
	</div>

<!-- --------------------------------- iOS ---------------------------------------------------------------------- -->
 
 
 
		
	<?php ($_REQUEST['detailcourse']=='iOS App Devlopment')?include 'iosdetailcourse.php' :'';?>
    
<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->

<!-- --------------------------------- Android ---------------------------------------------------------------------- -->
		
	<?php ($_REQUEST['detailcourse']=='Android App Development')?include 'androiddetailcourse.php' :''; ?>
    
<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->

<!-- --------------------------------- Web ---------------------------------------------------------------------- -->
		
	<?php ($_REQUEST['detailcourse']=='Web Devlopment')?include 'webdetailcourse.php' :''; ?>
    
<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->

    
    
    <!-- ---------------------------------SYLLABUS ---------------------------------------------------------------------- -->
    
    <div class="section" style="background: #ffdf00;">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="Big-title text-center" style="font-size: 40px;line-height: 40px;text-transform: uppercase;">Syllabus At A Glance</h1>
              
              <div class="margin-top"></div>
              
              <h4>Unit 1: Ruby</h4>
              
              <div class="margin-top"></div>
              
              <div class="panel-group" id="accordion">

              <!-- Start Toggle 1 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-1" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-up control-icon"></i>
						Introduction to Web Programming and GitHub
						</a>
				  </h4>					
                </div>
                <div id="collapse-1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
                  </div>
                </div>
              </div>
              <!-- End Toggle 1 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Ruby Basics
						</a>
				  </h4>
                </div>
                <div id="collapse-2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 2 -->

              <!-- Start Toggle 3 -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="collapsed" aria-expanded="false">
						<i class="fa fa-angle-down control-icon"></i>
						Everything is an Object
					  </a>
				  </h4>
                </div>
                <div id="collapse-3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                  <div class="panel-body">
                  	
                  	<ul>
                  		<li>Write and call parameterized custom methods</li>
                  		<li>Describe and use object­-oriented programming</li>
                  		<li>Apply instances of built­-in classes</li>
                  		<li>Create and implement custom classes in your program</li>
                  		<li>Control the scope of class functions and variables</li>
                  	</ul>
                  	
				  </div>
                </div>
              </div>
              <!-- End Toggle 3 -->

            </div>
            
            
            
            
              <div class="margin-top"></div>
              
             
            
            <div class="margin-top"></div><br><br>
            
           <?php if ( $_REQUEST['detailcourse']=='iOS App Devlopment' ) { ?>
            	
            <!-- ---------------------------------iOS ---------------------------------------------------------------------- -->
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="#" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Get Syllabus</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">View Detailed iOS Training Syllabus</h2>
            </div>
            
            <!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->
            
           <?php  } 
          
             if ( $_REQUEST['detailcourse']=='Android App Development' ) {
          	
            ?>
            
            <!-- ---------------------------------Android ---------------------------------------------------------------------- -->
            
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="#" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Get Syllabus</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">View Detailed Android Training Syllabus</h2>
            </div>
          
            <!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->
            
            
            <?php  } 
          
             if ( $_REQUEST['detailcourse']=='Web Devlopment' ) {
          	
            ?>
          
            <!-- ---------------------------------Web ---------------------------------------------------------------------- -->
            
            <div class="call-action call-action-boxed call-action-style3 clearfix" style="display: non">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:10px;"><a href="#" class="btn-system border-btn btn-medium btn-wite"><i class="icon-gift-1"></i> Get Syllabus</a></div>
            <!-- Call Action Text -->
            <h2 class="primary">View Detailed Web Development Training Syllabus</h2>
            </div>
            
            <!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->
             
             <?php  } ?>
          
        </div>
	
	</div>
      <!-- .container -->
</div>
 
 
 <?php if ( $_REQUEST['detailcourse']=='iOS App Devlopment' ) { ?>
 
 <!-- ---------------------------------iOS ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	<div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/team/4.png">
                </div>
              
            </div>

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>The iOS app development courses launched by Teks are a wonderful effort. The learning modules here are comprehensive - starting from the basics of app coding and wireframing, to advanced Xcode sessions, on-topic seminars and workshops. What’s more, the courses are regularly updated and industry-oriented. If you wish to become an expert iPhone app developer, joining a Teks course is certainly the right way to start.</p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->

<?php  } 
          
if ( $_REQUEST['detailcourse']=='Android App Development' ) {
          	
?>

<!-- ---------------------------------Android ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	<div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/team/4.png">
                </div>
              
            </div>

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>It has been a privilege to be a part of the team of Android app development instructors at Teks. From my professional experience, I can vouch for the fact that these modules are indeed well-designed...ideal for helping beginners make a mark as expert Android app developers. There are considerable opportunities for knowledge exchange, both with students and my peers. This is one of the finest Android training institutes in India in my book.</p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->



<?php  } 
          
if ( $_REQUEST['detailcourse']=='Web Devlopment' ) {
          	
?>
            
            
<!-- ---------------------------------Web ---------------------------------------------------------------------- -->
 
   <div class="section courses-white" style="display: non;">

      <div class="container" >
      	
      	<div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/team/4.png">
                </div>
              
            </div>

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><img alt="" src="images/team/testimonial.png"></h1>
              
              <div class="margin-top"></div>
              
              <div class="classic-testimonials">
              <div class="testimonial-content">
                <p>I feel that there is a genuine absence of quality web development training institutes in India. In that regard, Teks is doing an excellent job - grooming promising young talents into smart, well-informed web developers and site designers. Attention to detail is one of the high points of all the training courses offered by this institute. If you are passionate about making a career in web development, I strongly suggest joining Teks Learning Center.</p>
              </div>
              <div class="testimonial-author text-center"><span>Name</span> / Designation </div>
            </div>

              <!-- <div class="margin-top"></div> -->
              

            </div>

      </div>
      <!-- .container -->
</div>

<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->

<?php } ?>




<div class="section" style="background: #ffdf00;">

      <div class="container" >
      	
      	<div class="col-md-12">
            
            <div class="margin-top"></div>
            
            <!-- Classic Heading -->
              <h1 class="big-title" style="font-size: 40px; text-align: center; text-transform: uppercase;">Course Schedule & Itinerary</h1>
             
              
              <div class="margin-top"></div>
              
              
             <?php if ( $_REQUEST['detailcourse']=='iOS App Devlopment' ) { ?> 
<!-- ---------------------------------iOS ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	We have multiple iPhone development courses (full-time and part time) coming up. Kindly <a href="contactus.php" style="text-decoration: underline;">subscribe</a> here to get notifications on upcoming courses. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->
              <?php  } 
          
				if ( $_REQUEST['detailcourse']=='Android App Development' ) {
          	
			  ?>

<!-- ---------------------------------Android ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	The dates for the next series of Android development training courses will be announced soon. Watch this space, or <a href="contactus.php" style="text-decoration: underline;">subscribe</a> here to get notifications about the important dates. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF Android ---------------------------------------------------------------------- -->

              <?php  } 
          
              if ( $_REQUEST['detailcourse']=='Web Devlopment' ) {
          	
              ?>
<!-- ---------------------------------Web ---------------------------------------------------------------------- -->              
              <ul class="list-group">
			    <li class="list-group-item" style="font-size: 15px;">
			    	The next batch of web development tutorial classes will be starting soon. <a href="contactus.php" style="text-decoration: underline;">Subscribe</a> here to get notified about important dates. 
			    </li>
			  </ul>
<!-- ---------------------------------END OF Web ---------------------------------------------------------------------- -->
              <?php } ?>

        </div>

      </div>
      <!-- .container -->
</div>


<!-- ---------------------------------FAQ ---------------------------------------------------------------------- -->

<!-- ---------------------------------iOS ---------------------------------------------------------------------- -->

<?php ($_REQUEST['detailcourse']=='iOS App Devlopment')?include 'iosfaqdetailcourse.php' :''; ?>


<!-- ---------------------------------END OF iOS ---------------------------------------------------------------------- -->


<!-- ---------------------------------ANDROID ---------------------------------------------------------------------- -->

<?php ($_REQUEST['detailcourse']=='Android App Development')?include 'androidfaqdetailcourse.php' :''; ?>


<!-- ---------------------------------END OF ANDROID ---------------------------------------------------------------------- -->


<!-- ---------------------------------WEB ---------------------------------------------------------------------- -->


<?php ($_REQUEST['detailcourse']=='Web Devlopment')?include 'webfaqdetailcourse.php' :''; ?>

<!-- ---------------------------------END OF WEB ---------------------------------------------------------------------- -->

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>