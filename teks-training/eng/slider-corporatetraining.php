<!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span> <strong>Mobile & Web Development Training</strong> </span>
                              </h2>
                <h3 class="animated3">
                                <span>Join iOS, Android & Web Development Courses For Corporates</span>
                              </h3>
                <p class="animated4"><a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151;">Find Out More</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->