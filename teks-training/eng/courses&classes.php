<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Courses & Classes</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
    <div class="section courses">

      <div class="container" >
     
     <div class="col-md-12">
            
            <div class="margin-top"></div>
            
            <!-- Classic Heading -->
              <h1 class="big-title" style="text-align:center;">App Coding & Development Workshops</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="text-align: center;">Here is the itinerary of the upcoming web development, Android development and iOS development training workshops lined up for the next few days. Select your city, and pick the one(s) you would like to join.</p>
              
             <div class="margin-top"></div>
             
             
              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">KOLKATA</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">PUNE</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">STOCKHOLM</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">SYDNEY</a></li>
			      <li><a tabindex="-1" href="#" style="color: #515151;font-weight: 300;font-size: 16px;">COPENHAGEN</a></li>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <ul class="list-group">
			    <a href="detailed-classes&workshop.php"><li class="list-group-item"><span >IOS HANDS-ON  Workshop 1</span> <span style="float: right">Tuesday, 29 September 6:00pm EDT</span></li></a>
			    <a href="detailed-classes&workshop.php"><li class="list-group-item"><span style="text-align: left">IOS HANDS-ON  Workshop 2</span> <span style="float: right">Tuesday, 29 September 6:00pm EDT</span></li></a>
			  </ul>

        </div>
        
          <div class="col-md-12">
              <div class="margin-top"></div>
              <!-- Classic Heading -->
              <h1 class="big-title" style=" text-align: center;">Mobile & Web Development Training</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p style="text-align: center;">Browse through our holistic range of iOS, Android and Web Development training courses - and select the one that’s best suited for you. Part-time and full-time, classroom and correspondence
              	 <br><br> 
              	 <strong>- We have them all!</strong>
              	 </p>
              
             <div class="margin-top"></div>

            </div>
            
            <div class="col-md-12">
            	
            	<div class="margin-top"></div>

               
              <h1 class="big-title">Select Your Course</h1>
              
              <div class="margin-top"></div>
              
          <div class="row pricing-tables">

          <div class="col-md-4 col-sm-4">
            
            	<div class="pricing-table">
	              <div class="plan-name">
	                <h3>iOS Development Courses</h3>
	              </div>
	              <div class="plan-price">
	                <img src="images/course/6.png" />
	              </div>
	              <div class="plan-list">
	                <ul>
	                  <li>Take your pick from our industry-oriented series of iOS app development training modules and tutorial classes. Participate in informative seminars, workshops and panel discussions - helmed by renowned Apple developers and app entrepreneurs. Work on dummy projects of varying complexities, learn Objective-C and Swift programming, and get set to start making apps professionally.</li>
	                </ul>
	              </div>
            	</div>
            
          </div>

          <div class="col-md-4 col-sm-4">
          	
            <div class="pricing-table">
              <div class="plan-name">
                <h3>Android Development Courses</h3>
              </div>
              <div class="plan-price">
                <img src="images/course/8.png" />
              </div>
              <div class="plan-list">
                <ul>
                  <li>Learn how to make Android apps from Google developers with rich industry experience. Get hands-on training on Java programming, gain in-depth knowledge on testing, app debugging and the overall process of transforming viable ideas into efficient mobile applications. Grab the chance to work in collaboration with some of the best Android developers in India. Both full-time and part-time courses available.</li>
                </ul>
              </div>
            </div>
            
          </div>


          <div class="col-md-4 col-sm-4">
          	
            <div class="pricing-table">
              <div class="plan-name">
                <h3>Web Development Courses</h3>
              </div>
              <div class="plan-price">
                <img src="images/course/7.png" />
              </div>
              <div class="plan-list">
                <ul>
                  <li>Pick a Teks web training course, and get a thorough idea about creating websites from scratch - for personal and professional purposes. Attend specially organized HTML5 and CSS training workshops, learn how sites should be designed, made responsive, maintained and optimized. Find out how to use online tools to analyze website performance, and how you can plan to improve the effectiveness of web portals.</li>
                </ul>
              </div>
            </div>
            
          </div>

        </div>
     </div>

      	</div>
      <!-- .container -->
	</div>
	
   

  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>Give Your Career The ‘Teks Edge’!</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Wondering why you should join a Teks learning course? Well, there are reasons galore! For starters, we are a name to reckon with in the field of global mobile app development - with over 900 applications (iOS and Android combined) in our portfolio. We also offer customized training classes and tutorials - so that you can get the maximum advantage. The presence of our experienced team of web and mobile app developers is a big help - and we also invite guest lecturers, developers and testers at our app development seminars. Sound theoretical knowledge with practical development expertise - that’s what you gain by completing a course at Teks.</p>
              
              <!-- <div class="margin-top"></div> -->
              

            </div>

            <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-02.jpg">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>

  <?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>