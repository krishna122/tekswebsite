<?php // session_start();
// session_regenerate_id(true);
// if($_SESSION['isLoggedin']!=1)
// {
	// header("Location:index.php");
// }

?>
<?php
include ('userclass.php');
$userObj = new User();

$returnVal = $userObj -> getAllUnit();
$allCourse = $userObj -> getAllCourse();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php
	include 'header.php';
 ?>
      
      <?php
	include 'sidebar.php';
 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
              		<button class="btn btn-block btn-flat pull-right" style="width:auto; background:#fd702b;color:#fff;" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 data-toggle="modal" data-target="#addUnitModel"><i class="fa fa-plus-circle"></i> ADD UNIT</button>
                </div>
          <BR><BR>
        </section>
        
<!-- Modal  for add new unit -->
<div id="addUnitModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ADD UNIT</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>UNIT NAME (EN)</label>
           <input type="text" name="addtitle_en" id="addtitle_en" class="form-control" placeholder="ENTER UNIT NAME (EN)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (SW)</label>
           <input type="text" name="addtitle_sw" id="addtitle_sw" class="form-control" placeholder="ENTER UNIT NAME (SW)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (DE)</label>
           <input type="text" name="addtitle_de" id="addtitle_de" class="form-control" placeholder="ENTER UNIT NAME (DE)" required="required">
        </div>
        
        <div class="form-group">
           <label>SELECT COURSE</label>
          <select class="form-control" id="addcoursenameid" name="addcoursenameid">
          	<?php while($course = mysql_fetch_assoc($allCourse)) { ?>
          		<option value="<?php echo $course['coursenameid']; ?>"><?php echo $course['name_en']; ?></option>
          		<?php } ?>
          </select>
        </div>
		
		 <div class="form-group">
           <label>IS FULL TIME</label>
          <select class="form-control" id="isfulltimeadd" name="isfulltimeadd">
          		<option value="0">No</option>
          		<option value="1">Yes</option>
          </select>
        </div>
        
         <div class="form-group">
           <label>IS PART TIME</label>
          <select class="form-control" id="isparttimeadd" name="isparttimeadd">
          		<option value="0">No</option>
          		<option value="1">Yes</option>
          </select>
        </div>
		
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="addUnit();">ADD</button>
      </div>
    </div>
  </div>
</div>


<!------------------- Function to update course name  ------------------------------------------------------------------------>
<div id="editUnitModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >UPDATE COURSE</h4>
      </div>
      <div class="modal-body">
      	    <input type="hidden" name = 'editunitid' id="editunitid" >
      	    <input type="hidden" name = 'courseID' id="courseID" >
      	    
      	    <div class="form-group">
           <label>UNIT NAME (EN)</label>
           <input type="text" name="edittitle_en" id="edittitle_en" class="form-control" placeholder="ENTER UNIT NAME (EN)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (SW)</label>
           <input type="text" name="edittitle_sw" id="edittitle_sw" class="form-control" placeholder="ENTER UNIT NAME (SW)" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT NAME (DE)</label>
           <input type="text" name="edittitle_de" id="edittitle_de" class="form-control" placeholder="ENTER UNIT NAME (DE)" required="required">
        </div>
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="return updateUnit();">UPDATE</button>
      </div>
    </div>
  </div>
</div>
         
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW UNITS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	
    			<?php if($returnVal != 0) { ?>
                		
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10%">UNIT ID</th>
                      <th>COURSE(EN)</th>
                      <th>UNIT NAME (EN)</th>
                      <th>UNIT NAME (SW)</th>
                      <th>UNIT NAME (DE)</th>
                       <th>IS FULL TIME</th>
                       <th>FULL TIME UNIT NO</th>
                       <th>IS PART TIME</th>
                       <th>PART TIME UNIT NO</th>
                      <th>UPDATE ON</th>
                      <th style="width: 5%">EDIT</th>
                      <th style="width: 5%">DELETE</th>
                    </tr>
                    
                    <?php while($data = mysql_fetch_assoc($returnVal)) { ?>
                    <tr id="<?php echo $data['unitid']; ?>">
                      <td><?php echo $data['unitid']; ?>.</td>
                      <td><?php echo $data['name_en']; ?></td>
                      <td><?php echo $data['title_en']; ?></td>
                      <td><?php echo $data['title_sw']; ?></td>
                      <td><?php echo $data['title_de']; ?></td>
                      
                       <td><?php
					if ($data['isfulltime'] == '1') { echo 'Yes';
					} else { echo 'No';
					}
 ?></td>
                       <td><?php
					if ($data['isfulltime'] == '1') { 	echo $data['fullunitno'];
					}else { echo 'Not Available';}
 ?></td>
                       
                       <td><?php
					if ($data['isparttime'] == '1') { echo 'Yes';
					} else { echo 'No';
					}
 ?></td>
                       <td><?php
					if ($data['isparttime'] == '1') { 	echo $data['partunitno'];
					}else { echo 'Not Available';}
 ?></td>
                       
                      <td><?php echo $data['updateon']; ?></td>
                      <td>
                       <i class="fa fa-pencil fa-2x btn getUnitName" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 	data-toggle="modal" data-target="#editUnitModel"  data-unitid="<?php echo $data['unitid']; ?>"
              		 	data-title_en="<?php echo $data['title_en']; ?>" data-title_sw="<?php echo $data['title_sw']; ?>" data-title_de="<?php echo $data['title_de']; ?>"
              			data-coursenameid="<?php echo $data['coursenameid']; ?>"  ></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x btn"  onclick='deleteUnit(<?php echo $data['unitid'] ?>)'></i>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                
                <?php  } else {
					echo '<center><h3>No data found! </h3></center>';
					}
				?>
                </div><!-- /.box-body -->
              </div>
            </div>
            
          </div>   <!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
 
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php
	include 'script.php';
 ?>
    
    <script>
				$("button").click(function() {
			$("p").show("slow");
		});

		//Show pop up (model) with location name and hidden location id to update
		$(document).on("click", ".getUnitName", function() {
			$('#editunitid').val($(this).attr('data-unitid'));
			$('#edittitle_en').val($(this).attr('data-title_en'));
			$('#edittitle_sw').val($(this).attr('data-title_sw'));
			$('#edittitle_de').val($(this).attr('data-title_de'));		
		});

		//function for delete location
		function deleteUnit(unitid) {
			var confirmDel = confirm('Are you sure you want to delete this unit?');
			if (confirmDel) {
				$.ajax({
					url : 'ajax.php',
					data : {
						unitid : unitid,
						type : 'deleteunit'
					},
					type : 'post',
					success : function(output) {
						alert('Unit is deleted successfully!');
						location.reload();
					}
				});
			}
		}

		//function to update location detail
		function updateUnit() {
			var unitid = $('#editunitid').val();
			var title_en = $('#edittitle_en').val();
			var title_sw = $('#edittitle_sw').val();
			var title_de = $('#edittitle_de').val();
			$.ajax({
				url : 'ajax.php',
				data : {
					title_en : title_en,
					title_sw : title_sw,
					title_de : title_de,
					type : 'updateunit',
					unitid : unitid
				},
				type : 'post',
				success : function(output) {
					alert('Unit is updated successfully!');
					location.reload();
				}
			});
		}

		//function for add location
		function addUnit() {
			var title_en = $("#addtitle_en").val();
			var title_sw = $("#addtitle_sw").val();
			var title_de = $("#addtitle_de").val();
			var isfulltime = $("#isfulltimeadd").val();
			var isparttime = $("#isparttimeadd").val();
			var coursenameid = $("#addcoursenameid").val();
			$.ajax({
				url : 'ajax.php',
				data : {
					title_en : title_en,
					title_sw : title_sw,
					title_de : title_de,
					coursenameid : coursenameid ,
					isparttime : isparttime ,
					isfulltime : isfulltime ,
					type : 'addunit'
				},
				type : 'post',
				success : function(output) {
					alert('Unit is added successfully!');
					location.reload();
				}
			});
		}


          $("#unithover").attr('class', 'treeview active');
          $("#unithover").attr('class', 'active');
          $("#unithover").attr('style', 'display:block');
	</script>

  </body>
</html>