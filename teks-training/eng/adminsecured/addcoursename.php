<?php 
// session_start();
// session_regenerate_id(true);
// if($_SESSION['isLoggedin']!=1)
// {
	// header("LANGUAGE:index.php");
// }

require_once("db.php");

$query1 = "SELECT * FROM coursetype";
$rs1 = mysql_query ($query1);

$query2 = "SELECT coursetype_eng,courses.* FROM courses join coursetype on coursetype_id = coursetype";
$rs2 = mysql_query ($query2);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php include 'header.php'; ?>
      
      <?php include 'sidebar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 style="text-align: center;">
           ADD COURSE NAME HERE
          </h1>
          <BR><BR>
        </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">ADD COURSE NAME</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="coursename-backend.php">
                  <div class="box-body">
                    <div class="form-group">
                      <label>COURSE NAME</label>
                      <input type="text" class="form-control" name="coursename_eng" placeholder="Enter Course Name (English)" required="required"><br>
                      <input type="text" class="form-control" name="coursename_sv" placeholder="Enter Course Name (Swedish)" required="required"><br>
                      <input type="text" class="form-control" name="coursename_da" placeholder="Enter Course Name (Danish )" required="required">
                    </div>
                    <div class="form-group">
                      <label>COURSE TYPE</label><br>
                      <select class="form-control" name="coursetype" required="required">
                      	<option>Select</option>
                      	<?php while ($arr = mysql_fetch_array ($rs1)) { ?>
                        <option value="<?php echo ($arr['coursetype_id']); ?>"><?php echo ucwords($arr['coursetype_eng']); ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
              
              
              <!-- VIEW ADDED LOCATION -->
              
              <section class="content-header">
		          <h1 style="text-align: center;">
		           VIEW COURSE NAME HERE
		          </h1>
		          <BR><BR>
		        </section>
        
        
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">VIEW ADDED COURSE NAME</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>COURSE NAME (ENGLISH)</th>
                      <th>COURSE NAME (SWEDISH)</th>
                      <th>COURSE NAME (DANISH)</th>
                      <th>COURSE TYPE</th>
                      
                      <th style="width: 40px">EDIT</th>
                      <th style="width: 40px">DELETE</th>
                    </tr>
                    
                    <?php while ($arr2 = mysql_fetch_array ($rs2)) { ?>
                    <tr>
                      <td><?php echo ucwords($arr2['course_id']); ?>.</td>
                      <td><?php echo ucwords($arr2['coursename_eng']); ?></td>
                      <td><?php echo ucwords($arr2['coursename_sv']); ?></td>
                      <td><?php echo ucwords($arr2['coursename_da']); ?></td>
                      <td><?php echo ucwords($arr2['coursetype_eng']); ?></td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    <?php } ?>
                    
                  </tbody>
                </table>
                </div><!-- /.box-body -->
              </div>
              
              
            </div>
            
          </div>   <!-- /.row -->
        </section>
        
        
      </div><!-- /.content-wrapper -->
      
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php include 'script.php'; ?>
  </body>
</html>