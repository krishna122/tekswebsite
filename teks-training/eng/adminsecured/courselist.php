<?php // session_start();
// session_regenerate_id(true);
// if($_SESSION['isLoggedin']!=1)
// {
	// header("Location:index.php");
// }

?>
<?php
include ('userclass.php');
$userObj = new User();
$returnVal = $userObj -> getAllCourse();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php
	include 'header.php';
 ?>
      
      <?php
	include 'sidebar.php';
 ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
              		<button class="btn btn-block btn-flat pull-right" style="width:auto; background:#fd702b;color:#fff;" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 data-toggle="modal" data-target="#addCourseModel"><i class="fa fa-plus-circle"></i> ADD COURSE</button>
                </div>
          <BR><BR>
        </section>
        
<!-- Modal  for add new location -->
<div id="addCourseModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ADD COURSE</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>COURSE NAME (EN)</label>
           <input type="text" name="addname_en" id="addname_en" class="form-control" placeholder="ENTER COURSE NAME (EN)" required="required">
        </div>
        
        <div class="form-group">
           <label>COURSE NAME (SW)</label>
           <input type="text" name="addname_sw" id="addname_sw" class="form-control" placeholder="ENTER COURSE NAME (SW)" required="required">
        </div>
        
        <div class="form-group">
           <label>COURSE NAME (DE)</label>
           <input type="text" name="addname_de" id="addname_de" class="form-control" placeholder="ENTER COURSE NAME (DE)" required="required">
        </div>
        
      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="addCourse();">ADD</button>
      </div>
    </div>
  </div>
</div>


<!------------------- Function to update course name  ------------------------------------------------------------------------>
<div id="editCourseModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >UPDATE COURSE</h4>
      </div>
      <div class="modal-body">
      	    <div class="form-group">
      	    	<input type="hidden" name="updatedcoursenameid" id="updatedcoursenameid" >
           <label>COURSE NAME (EN)</label>
           <input type="text" name="updatename_en" id="updatename_en" class="form-control" placeholder="ENTER COURSE NAME (EN)" required="required">
        </div>
        
        <div class="form-group">
           <label>COURSE NAME (SW)</label>
           <input type="text" name="updatename_sw" id="updatename_sw" class="form-control" placeholder="ENTER COURSE NAME (SW)" required="required">
        </div>
        
        <div class="form-group">
           <label>COURSE NAME (DE)</label>
           <input type="text" name="updatename_de" id="updatename_de" class="form-control" placeholder="ENTER COURSE NAME (DE)" required="required">
        </div>

      </div>
      <div class="modal-footer">
      	 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;" onclick="return updateCourse();">UPDATE</button>
      </div>
    </div>
  </div>
</div>
       
       
       
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW COURSES</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	
    			<?php if($returnVal != 0) { ?>
                		
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10%">COURSE ID</th>
                      <th>COURSE NAME (EN)</th>
                      <th>COURSE NAME (SW)</th>
                      <th>COURSE NAME (DE)</th>
                      <th>UPDATE ON</th>
                      <th style="width: 5%">EDIT</th>
                      <th style="width: 5%">DELETE</th>
                    </tr>
                    
                    <?php while($data = mysql_fetch_assoc($returnVal)) { ?>
                    <tr id="<?php echo $data['coursenameid']; ?>">
                      <td><?php echo $data['coursenameid']; ?>.</td>
                      <td><?php echo $data['name_en']; ?></td>
                      <td><?php echo $data['name_sw']; ?></td>
                      <td><?php echo $data['name_de']; ?></td>
                      <td><?php echo $data['updatedon']; ?></td>
                      <td>
                       <i class="fa fa-pencil fa-2x btn getCourseName" data-controls-modal="idModal" data-backdrop="static" data-keyboard="false"
              		 data-toggle="modal" data-target="#editCourseModel"  data-coursenameid="<?php echo $data['coursenameid']; ?>"
              		 data-name_en="<?php echo $data['name_en']; ?>" data-name_sw="<?php echo $data['name_sw']; ?>" data-name_de="<?php echo $data['name_de']; ?>"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x btn"  onclick='deleteCourse(<?php echo $data['coursenameid'] ?>)'></i>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                
                <?php  } else {
					echo '<center><h3>No data found! </h3></center>';
					}
				?>
                </div><!-- /.box-body -->
              </div>
            </div>
            
          </div>   <!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
 
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php
	include 'script.php';
 ?>
    
    <script>
		$("button").click(function() {
			$("p").show("slow");
		});

		//Show pop up (model) with location name and hidden location id to update
		$(document).on("click", ".getCourseName", function() {
			$('#updatedcoursenameid').val($(this).attr('data-coursenameid'));
			$('#updatename_en').val($(this).attr('data-name_en'));
			$('#updatename_sw').val($(this).attr('data-name_sw'));
			$('#updatename_de').val($(this).attr('data-name_de'));
		});

		//function for delete location
		function deleteCourse(coursenameid) {
			var confirmDel = confirm('Are you sure you want to delete this course?');
			if (confirmDel) {
				$.ajax({
					url : 'ajax.php',
					data : {
						coursenameid : coursenameid,
						type : 'deletecourse'
					},
					type : 'post',
					success : function(output) {
						alert('Course is deleted successfully!');
						location.reload();
					}
				});
			}
		}

		//function to update location detail
		function updateCourse() {
			var coursenameid = $('#updatedcoursenameid').val();
			var name_en = $('#updatename_en').val();
			var name_sw = $('#updatename_sw').val();
			var name_de = $('#updatename_de').val();
			$.ajax({
				url : 'ajax.php',
				data : {
					name_en : name_en,
					name_sw : name_sw,
					name_de : name_de,
					type : 'updatecourse',
					coursenameid : coursenameid
				},
				type : 'post',
				success : function(output) {
					alert('Course is updated successfully!');
					location.reload();
				}
			});
		}

		//function for add location
		function addCourse() {
			var name_en = $("#addname_en").val();
			var name_sw = $("#addname_sw").val();
			var name_de = $("#addname_de").val();
			$.ajax({
				url : 'ajax.php',
				data : {
					name_en : name_en,
					name_sw : name_sw,
					name_de : name_de,
					type : 'addcourse'
				},
				type : 'post',
				success : function(output) {
					alert('Course is added successfully!');
					location.reload();
				}
			});
		}


          $("#coursehover").attr('class', 'treeview active');
          $("#coursehover").attr('class', 'active');
          $("#coursehover").attr('style', 'display:block');
	</script>

  </body>
</html>