<?php 

		include ('userclass.php');
		$userObj = new User();
		
		$returnVal = $userObj -> adminLogin($email,$password);
		
$query1 = "SELECT * FROM courses";
$rs1 = mysql_query ($query1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php include 'header.php'; ?>
      
      <?php include 'sidebar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-6"></div>
                <div class="col-md-2">
              		<button class="btn btn-block btn-warning btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle"></i> ADD COURSES</button>
                </div>
          <BR><BR>
        </section>
        
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ADD COURSES</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>COURSE TYPES</label>
           <input type="text" name="coursetype_eng" class="form-control" placeholder="" required="required">
        </div>
        <div class="form-group">
           <label>COURSE NAME</label>
           <input type="text" name="coursename_eng" class="form-control" placeholder="" required="required">
        </div>
        <div class="form-group">
           <label>COURSE COUNTRY</label>
           <input type="text" name="unitid" class="form-control" placeholder="" required="required">
        </div>
   
        <div class="form-group">
           <label>COURSE LANGUAGE</label>
           <input type="text" name="coursetype_eng" class="form-control" placeholder="" required="required">
        </div>
        <div class="form-group">
           <label>START DATE</label>
           <div class="input-group">
             <div class="input-group-addon">
               <i class="fa fa-calendar"></i>
             </div>
             <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
           </div><!-- /.input group -->
        </div>
        <div class="form-group">
           <label>END DATE</label>
           <div class="input-group">
             <div class="input-group-addon">
               <i class="fa fa-calendar"></i>
             </div>
             <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
           </div><!-- /.input group -->
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;">DONE</button>
      </div>
    </div>

  </div>
</div>

       
       
       
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
       
       
       
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW COURSES</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>COURSE TYPE</th>
                      <th>COURSE NAME</th>
                      <th>COURSE COUNTRY</th>
                      <th>COURSE LANGUAGE</th>
                      <th>START DATE</th>
                      <th>END DATE</th>
                      
                      <th style="width: 40px">EDIT</th>
                      <th style="width: 40px">DELETE</th>
                    </tr>
                    
                    <?php while ($arr = mysql_fetch_array ($rs1)) { ?>
                    	
                    <tr>
                      <td><?php echo ucwords($arr['course_id']); ?>.</td>
                      <td><?php echo ucwords($arr['coursetype']); ?></td>
                      <td><?php echo ucwords($arr['coursename']); ?></td>
                      <td><?php echo ucwords($arr['courselocation']); ?></td>
                      <td><?php echo ucwords($arr['languagecode']); ?></td>
                      <td><?php echo ucwords($arr['startdate']); ?></td>
                      <td><?php echo ucwords($arr['enddate']); ?></td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    
                    <?php } ?>
                    
                  </tbody>
                </table>
                </div><!-- /.box-body -->
              </div>
              
              
            </div>
            
          </div>   <!-- /.row -->
        </section>
        
        
      </div><!-- /.content-wrapper -->
      
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php include 'script.php'; ?>
    
    <script>
		$( "button" ).click(function() {
		  $( "p" ).show( "slow" );
		});
	</script>

  </body>
</html>
