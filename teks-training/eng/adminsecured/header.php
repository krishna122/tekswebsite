<header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="logo.png" style="height:50px; width: 50px;" /></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="logo.png" style="height:67px; width: 100px;" /></span>
        </a>
        
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="user user-menu">
                <a href="index.php">
                  <span class=""><i class="fa fa-power-off fa-2x" style="color:#2b2b2b;"></i></span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
    </header>