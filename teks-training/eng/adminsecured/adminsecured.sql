-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 27, 2015 at 01:21 PM
-- Server version: 5.6.23
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `adminsecured`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `adminid` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminid`, `email`, `password`) VALUES
(1, 'govind@gmail.com', 'teks123');

-- --------------------------------------------------------

--
-- Table structure for table `coursename`
--

CREATE TABLE IF NOT EXISTS `coursename` (
  `coursenameid` bigint(20) NOT NULL,
  `name_en` varchar(11) NOT NULL,
  `name_sw` varchar(200) NOT NULL,
  `name_de` varchar(200) NOT NULL,
  `updatedon` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coursename`
--

INSERT INTO `coursename` (`coursenameid`, `name_en`, `name_sw`, `name_de`, `updatedon`) VALUES
(2, 'g1', 'gg1', 'ggg1', '2015-10-26 11:45:51'),
(4, 'en2', 'sw2', 'de2', '2015-10-26 11:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `locationid` int(11) NOT NULL,
  `locationname` varchar(1000) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`locationid`, `locationname`) VALUES
(11, 'Delhi'),
(3, 'Stockholm'),
(4, 'Sydney'),
(5, 'Copenhagen'),
(9, 'Howrah'),
(10, 'Mumbai');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
  `unitid` int(11) NOT NULL,
  `coursenameid` bigint(20) NOT NULL,
  `partunitno` int(11) NOT NULL,
  `fullunitno` int(11) NOT NULL,
  `title_en` varchar(250) NOT NULL,
  `title_sw` varchar(200) NOT NULL,
  `title_de` varchar(200) NOT NULL,
  `isfulltime` enum('0','1') NOT NULL DEFAULT '1',
  `isparttime` enum('0','1') NOT NULL DEFAULT '1',
  `updateon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unitid`, `coursenameid`, `partunitno`, `fullunitno`, `title_en`, `title_sw`, `title_de`, `isfulltime`, `isparttime`, `updateon`) VALUES
(2, 2, 1, 1, 's', 'j', 'l', '1', '1', '2015-10-26 14:14:35'),
(6, 2, 0, 2, 'gg', 'sdfs', 'sdfsfsf', '1', '0', '2015-10-26 13:02:27'),
(7, 2, 2, 0, 'dffs', 'sdfs', 'sdfsf', '0', '1', '2015-10-26 13:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `unitsyllabus`
--

CREATE TABLE IF NOT EXISTS `unitsyllabus` (
  `syllabusid` bigint(20) NOT NULL,
  `unitid` bigint(20) NOT NULL,
  `heading_en` varchar(250) NOT NULL,
  `heading_sw` varchar(250) NOT NULL,
  `heading_de` varchar(250) NOT NULL,
  `description_en` text NOT NULL,
  `description_sw` text NOT NULL,
  `description_de` text NOT NULL,
  `updatedon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unitsyllabus`
--

INSERT INTO `unitsyllabus` (`syllabusid`, `unitid`, `heading_en`, `heading_sw`, `heading_de`, `description_en`, `description_sw`, `description_de`, `updatedon`) VALUES
(2, 6, 'h1_en', 'he1_sw', 'he1_de', 'de1_en', 'de1_sw', 'de1_de', '2015-10-27 05:49:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminid`);

--
-- Indexes for table `coursename`
--
ALTER TABLE `coursename`
  ADD PRIMARY KEY (`coursenameid`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`locationid`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unitid`), ADD KEY `unitid` (`unitid`);

--
-- Indexes for table `unitsyllabus`
--
ALTER TABLE `unitsyllabus`
  ADD PRIMARY KEY (`syllabusid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `coursename`
--
ALTER TABLE `coursename`
  MODIFY `coursenameid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `locationid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `unitid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `unitsyllabus`
--
ALTER TABLE `unitsyllabus`
  MODIFY `syllabusid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
