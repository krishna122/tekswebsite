<?php 
// session_start();
// session_regenerate_id(true);
// if($_SESSION['isLoggedin']!=1)
// {
	// header("LANGUAGE:index.php");
// }

require_once("db.php");

$query1 = "SELECT * FROM coursesYLLABUS";
$rs1 = mysql_query ($query1);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminpanel Teks Training | Course Syllabus</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include "head.php" ?>
  </head>
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">

      <?php include 'header.php'; ?>
      
      <?php include 'sidebar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          		<div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
              		<button class="btn btn-block btn-warning btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle"></i> ADD COURSE SYLLABUS</button>
                </div>
          <BR><BR>
        </section>
        
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="color: #fc6220">&times;</button>
        <h4 class="modal-title">ADD COURSE SYLLABUS</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
           <label>COURSE NAME</label>
           <input type="text" name="coursename_eng" class="form-control" placeholder="" required="required">
        </div>
        <div class="form-group">
           <label>UNIT ID</label>
           <input type="text" name="unitid" class="form-control" placeholder="" required="required">
        </div>
        
        <div class="form-group">
           <label>UNIT TITLE</label>
           <input type="text" name="unittitle" class="form-control" placeholder="" required="required">
        </div>
    
        <div class="form-group">
           <button type="button" class="btn btn-default" data-dismiss="modal">SAVE</button>
        </div>
        <div class="" >
        <div class="form-group">
           <label>SYLLABUS HEADING</label>
           <input type="text" name="syllabusheading" class="form-control" placeholder="" required="required">
        </div>
        <div class="form-group">
           <label>SYLLABUS DESCRIPTION</label>
            <div class='box-body pad'>
               <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>          
        </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="background: #fc4b00;color: #fff;border: 0px;">ADD MORE</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #fc6220;color: #fff;border: 0px;">DONE</button>
      </div>
    </div>

  </div>
</div>

       
       
       
  <!-- --------------------------------------------------------------------------------------------------------------------------------- -->     
       
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            	
              <div class="box">
                <div class="box-header">
                  <h3 class="" style="text-align: center;text-decoration: underline;">VIEW COURSE SYLLABUS</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th>COURSE NAME</th>
                      <th>UNIT NUMBER</th>
                      <th>UNIT TITLE</th>
                      <th>SYLLABUS HEADING</th>
                      <th>SYLLABUS DESCRIPTION</th>
                      
                      <th style="width: 40px">EDIT</th>
                      <th style="width: 40px">DELETE</th>
                    </tr>
                    
                    <?php while ($arr = mysql_fetch_array ($rs1)) { ?>
                    	
                    <tr>
                      <td><?php echo ucwords($arr['coursename']); ?></td>
                      <td><?php echo ucwords($arr['unitid']); ?></td>
                      <td><?php echo ucwords($arr['unittitle']); ?></td>
                      <td><?php echo ucwords($arr['syllabusheading']); ?></td>
                      <td><?php echo ucwords($arr['syllabusdescription']); ?></td>
                      <td>
                        <i class="fa fa-pencil fa-2x"></i>
                      </td>
                      <td>
                      	<i class="fa fa-trash-o fa-2x"></i>
                      </td>
                    </tr>
                    
                    <?php } ?>
                    
                  </tbody>
                </table>
                </div><!-- /.box-body -->
              </div>
              
              
            </div>
            
          </div>   <!-- /.row -->
        </section>
        
        
      </div><!-- /.content-wrapper -->
      
      <?php include "footer.php" ?>
      
    </div><!-- ./wrapper -->

    <?php include 'script.php'; ?>
    
    <script>
		$( "button" ).click(function() {
		  $( "p" ).show( "slow" );
		});
	</script>

  </body>
</html>
