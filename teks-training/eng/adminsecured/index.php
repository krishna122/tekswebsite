<?php
// session_destroy();
// session_start();
ob_start();
error_reporting('E_ERROR|E_WARNING');
//include the class file
include ('userclass.php');

$message = "";

if (!empty($_POST['Submit']) && $_POST['Submit'] == "login") {
	$email = $_POST['email'];
	$password = $_POST['password'];

	/* making object of hr class to access the function  */
	$userObj = new User();
	$returnVal = $userObj -> adminLogin($email, $password);
	if ($returnVal != 0) {
		$_SESSION['employee_id'] = $returnVal['adminid'];
		$_SESSION['email'] = $returnVal['email'];
		/*----------------------------------------------------------------------------------------------
		 Insert the Login Details With IP , Browser and Login Time
		 ----------------------------------------------------------------------------------------------*/

		$IP_Address = getenv('REMOTE_ADDR');

		$browser = "";
		if (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), strtolower("MSIE"))) {$browser = "ie";
		} else if (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), strtolower("Presto"))) {$browser = "opera";
		} else if (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), strtolower("CHROME"))) {$browser = "chrome";
		} else if (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), strtolower("SAFARI"))) {$browser = "safari";
		} else if (strpos(strtolower($_SERVER["HTTP_USER_AGENT"]), strtolower("FIREFOX"))) {$browser = "firefox";
		} else {$browser = "other";
		}
		echo "<script>window.top.location='courselist.php'</script>";
	} else {
		$message = "Login Incorrect";
	}
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Adminpanel Teks Training</title>
		<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="plugins/iCheck/square/blue.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page" style="background: #515151;">
		<div class="login-box">
			<div class="login-logo">
				<img src="logo.png">
			</div><!-- /.login-logo -->
			<div class="login-box-body" style="border-top: 3px solid #fd702b;">
				<p class="login-box-msg">
					Sign in to start your session
				</p>

				<?php if($message != '') { ?>
				<div class="alert alert-danger alert-login" id="dv_alert" style="text-align: center;">
					<?php echo $message; ?>
				</div>
				<?php } ?>
				<form action="" method="post">
					<div class="form-group has-feedback">
						<input type="email" class="form-control" placeholder="Email" name="email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="Password" name="password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button type="submit" name="Submit" class="btn btn-primary btn-block btn-flat" value="login" style="background: #fd702b;border: 0px;">
								Sign In
							</button>
						</div><!-- /.col -->

						
					</div>
				</form>

			</div><!-- /.login-box-body -->
		</div><!-- /.login-box -->

		<!-- jQuery 2.1.4 -->
		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>
