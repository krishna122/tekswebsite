<?php

include ('userclass.php');
$userObj = new User();

//check location exist or not (add location accordingly)
if (!empty($_POST['type']) && $_POST['type'] == "addlocation") {
	$locationname = $_POST['locationname'];
	$returnVal = $userObj -> isLocationExist(0, $locationname);
	if ($returnVal == 0) {
		$addVal = $userObj -> updateLocation(0, $locationname);
		echo $addVal;
	} else {
		echo 0;
	}
}

//check location exist or not(update location accrodingly)
if (!empty($_POST['type']) && $_POST['type'] == "updatelocation") {
	$locationname = $_POST['locationname'];
	$locationid = $_POST['locationid'];
	$returnVal = $userObj -> isLocationExist($locationid, $locationname);
	if ($returnVal == 0) {
		$addVal = $userObj -> updateLocation($locationid, $locationname);
		echo $addVal;
	} else {
		echo 0;
	}
}

//delete location
if (!empty($_POST['type']) && $_POST['type'] == "deletelocation") {
	$locationid = $_POST['locationid'];
	$returnVal = $userObj -> deleteLocation($locationid);
	echo $returnVal;
}

//dleete course
if (!empty($_POST['type']) && $_POST['type'] == "deletecourse") {
	$coursenameid = $_POST['coursenameid'];
	$returnVal = $userObj -> deleteCourse($coursenameid);
	echo $returnVal;
}

//function to add course
if (!empty($_POST['type']) && $_POST['type'] == "addcourse") {
	$name_en = $_POST['name_en'];
	$name_sw = $_POST['name_sw'];
	$name_de = $_POST['name_de'];
	$addVal = $userObj -> updateCourse(0, $name_en, $name_sw, $name_de);
	echo $addVal;
}

//check location exist or not(update location accrodingly)
if (!empty($_POST['type']) && $_POST['type'] == "updatecourse") {
	$coursenameid = $_POST['coursenameid'];
	$name_en = $_POST['name_en'];
	$name_sw = $_POST['name_sw'];
	$name_de = $_POST['name_de'];
	$addVal = $userObj -> updateCourse($coursenameid, $name_en, $name_sw, $name_de);
	echo $addVal;
}

//ajax to add an unit
if (!empty($_POST['type']) && $_POST['type'] == "addunit") {
	$coursenameid = $_POST['coursenameid'];
	$title_en = $_POST['title_en'];
	$title_sw = $_POST['title_sw'];
	$title_de = $_POST['title_de'];
	$isfulltime = $_POST['isfulltime'];
	$isparttime = $_POST['isparttime'];
	$addVal = $userObj -> addUnit($coursenameid, $title_en, $title_sw, $title_de, $isfulltime, $isparttime);
	echo $addVal;
}

//dleete unit
if (!empty($_POST['type']) && $_POST['type'] == "deleteunit") {
	$unitid = $_POST['unitid'];
	$returnVal = $userObj -> deleteUnit($unitid);
	echo $returnVal;
}

//update unit detail
if (!empty($_POST['type']) && $_POST['type'] == "updateunit") {
	$unitid = $_POST['unitid'];
	$title_en = $_POST['title_en'];
	$title_sw = $_POST['title_sw'];
	$title_de = $_POST['title_de'];
	$returnVal = $userObj -> updateUnitDetail($unitid, $title_en, $title_sw, $title_de);
	echo $returnVal;
}

//function to add syllabus
if (!empty($_POST['type']) && $_POST['type'] == "addsyllabus") {
	$unitid = $_POST['unitid'];
	$heading_en = $_POST['heading_en'];
	$heading_sw = $_POST['heading_sw'];
	$heading_de = $_POST['heading_de'];
	$description_en = $_POST['description_en'];
	$description_sw = $_POST['description_sw'];
	$description_de = $_POST['description_de'];
	$returnVal = $userObj -> updateSyllabus(0, $unitid, $heading_en, $heading_sw, $heading_de, $description_en, $description_sw, $description_de);
	echo $returnVal;
}

//delete unit syllabus
if (!empty($_POST['type']) && $_POST['type'] == "deletesyllabus") {
	$syllabusid = $_POST['syllabusid'];
	$returnVal = $userObj -> deleteUnitSyllabus($syllabusid);
	echo $returnVal;
}
?>