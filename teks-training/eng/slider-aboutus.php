<!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="images/slider/bg-about.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2" style="line-height: 55px;">
                              <span> <strong>Teknowledge Software</strong> </span>
                              </h2>
                <h3 class="animated3" style="line-height: 36px;">
                                <span>The No.1 Mobile & Web Development Training Institute In India</span>
                              </h3>
                <p class="animated4"><a href="contactus.php" class="slider btn btn-system btn-large" style="color:#515151;">Get In Touch</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->