<!-- Start Services Section -->
    <div class="section courses-white">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1><span style="font-size: 40px;">Courses At Teks <strong>Training</strong> Center</span></h1>
            <p class="title-desc">At Teks, You Can Sign Up For The Following Courses</p>
          </div>
          <!-- End Big Heading -->
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <i class="fa fa-apple icon-large"></i>
            </div>
            <div class="service-content">
              <h4>iOS Training</h4>
              <p>Sign up for our exclusive series of iPhone app development courses. Work with senior industry experts, and get hands­on experience on how to make an iOS app.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <i class="fa fa-android icon-large"></i>
            </div>
            <div class="service-content">
              <h4>Android Training</h4>
              <p>Get set to become a professional Android app developer by joining our tutorial courses. Participate in classroom discussions, seminars, workshops, and a lot more.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <i class="fa fa-television icon-large"></i>
            </div>
            <div class="service-content">
              <h4>Web Training</h4>
              <p>Learn how to create, develop, design and maintain personal and professional websites. Specialised courses on Wordpress, Drupal, Joomla and Magento platforms.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->