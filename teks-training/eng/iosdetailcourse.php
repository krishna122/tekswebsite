<!-- Start Services Section -->
    <div class="section service courses" id="ios" style="display: non;">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="font-size: 40px; text-align: center; text-transform: uppercase;">Become A Trained iPhone / iPad App Developer</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/10.png" />
            </div>
            <div class="service-content">
              <h4>Xcode Training</h4>
              <p>Get hands-on experience of working with latest iteration of the Xcode tool.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/11.png" />
            </div>
            <div class="service-content">
              <h4>Learn iOS Programming</h4>
              <p>Gain thorough knowledge on coding for iPhone/iPad apps with Objective-C and Swift 2 programming languages.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/12.png" />
            </div>
            <div class="service-content">
              <h4>Work In Apple IDE</h4>
              <p>From Xcode and AppCode, to tips and methods for iOS 8 & iOS 9 app development - get trained on everything. Learn the key features behind any app’s success.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    




<!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style="font-size: 40px; text-transform: uppercase;">Participative App Development Courses</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">Teks Learning Center encourages interactivity in all its iOS development training courses and modules. We have special app development courses for beginners and professionals.</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/1.png" />
            </div>
            <div class="service-content">
              <h4>Experienced Guides</h4>
              <p>Learn how to create an iPhone app with a senior-level app developer acting as your guide. Learning support, as and when you need it.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/2.png" />
            </div>
            <div class="service-content">
              <h4>Case Studies</h4>
              <p>Work on dummy iOS development projects and get a hang of working with the latest versions of the platform, as well as the tools, frameworks and APIs.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/3.png" />
            </div>
            <div class="service-content">
              <h4>Peer-to-Peer Learning</h4>
              <p>Share and gain knowledge from your peers and fellow-learners at the iOS courses. Work on group projects, and sort out development issues and glitches together.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->