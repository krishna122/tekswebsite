<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Training | Request more info</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
  <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-7">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>MORE ABOUT OUR CORPORATE DEVELOPMENT TRAINING COURSES</span></h4>
            
            <p>If you have a query about our corporate training modules, contact us here.</p>
            
            <div class="margin-top"></div>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Company" name="company">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Phone" name="phone">
                </div>
              </div>
              <div class="form-group">
              	<label style="color: #a9a9a9;"><strong>Number of Employees</strong></label>
                <div class="controls">
                  <select>
					  <option value="1-250">1-250</option>
					  <option value="250-5000">250-5000</option>
					  <option value="More than 5000">More than 5000</option>
				 </select>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea rows="10" placeholder="Tell us a bit about your Corporate Training needs" name="message"></textarea>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system btn-large" style="background: #ff6106;">Submit</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>
          
          <div class="col-md-1"></div>

          <div class="col-md-4">

             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #fdeba3;">
	              <div class="plan-name" style="background:#fbd334;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">When You Contact Us, You Get:</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Response within 24 hours.</li>
	                  <li style="border-bottom: 0px;">Details on all our corporate learning courses.</li>
	                  <li style="border-bottom: 0px;">Itinerary of upcoming seminars and workshops on web/mobile app development.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>
        	
        	<div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#dadada;border-bottom: 0px;">
	                <h3 style="color: #515151;">If You Wish To Know More...</h3>
	              </div>
	              <div class="plan-list" style="text-align: left;">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;">Kindly visit our <a href="corporatefaq.php" style="text-decoration: underline;">FAQ</a> page.</li>
	                </ul>
	              </div>
	            </div>
          
        	</div>

          </div>

        </div>

      </div>
    </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>