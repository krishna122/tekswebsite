<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Detailed Classes & Workshop</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
  
    <div class="section courses-white">

      <div class="container" >

        	<div class="col-md-12">
        		
             <div class="col-md-6">

	              <!-- Classic Heading -->
	              <h1 class="big-title" style="font-size: 40px;">iOS Hands-On (Workshop)</h1>
	              
	              <div class="margin-top"></div>
	              
	              <!-- Some Text -->
	              <p style="text-decoration: underline;"><strong>At A Glance</strong></p>
	              <p>App development workshop for the iOS 9 platform.</p>
	              
	             <div class="margin-top"></div>
	             
	             <h3>About The Workshop</h3>
	             
	             <br>
	             
	             <p>‘iOS Hands-On’ will involve in-depth discussions among attendees on making apps for iOS platform. Learners (beginners and corporates) will have the opportunity to familiarize themselves with Xcode 7 and all that’s new in Swift 2.</p>
	             
	             <br>
	             
	             <p>As an additional feature in this workshop, we will have a special 1-hour Q&A session on WatchKit app development. Preliminary demonstrations on making apps for Apple Watch will also be presented.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>What’s In It For Learners?</h3>
	             
	             <br>
	             
	             <p>Like all our iOS app development training seminars and workshops, this one will also offer valuable takeaways for attendees. The core focus of ‘iOS Hands-On’ is to provide learners with a practical experience of coding for apps for iOS 9 devices.</p>
	             
	             <br>
	             
	             <ul style="list-style: circle; padding-left: 15px;">
	             	<li>
	             	  Exposure to real-world coding environment.	
	             	</li>
	             	
	             	<li>
	             	  Peer-to-peer interactions.	
	             	</li>
	             	
	             	<li>
	             	  Learning the details about Xcode 7, Swift 2, iOS 9 and WatchKit 2.	
	             	</li>
	             	
	             	<li>
	             	  One-on-one Q&A sessions.	
	             	</li>
	             	
	             	<li>
	             	  Interactions with globally renowned iPhone app developers.	
	             	</li>
	             	
	             	<li>
	             	  Case study presentations and dummy projects.	
	             	</li>
	             </ul>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Eligibility To Attend</h3>
	             
	             <br>
	             
	             <p>The ‘iOS Hands-On’ training workshop is open for everyone. However, to get the most advantage from the session, you should have a working idea of iPhone app development and basic Swift programming. There will be a separate corporate training session as well.</p>
	             
	             <div class="margin-top"></div>
	             
	             <h3>Mentors At iOS Hands-On</h3>
	             
	             <br>
	             
	              <p>The training workshop will be graced by the presence of 9 senior-level iOS developers - who will be interacting with attendees and guiding them on how to make mobile apps.</p>
	             
            </div>
            
            <div class="col-md-6">
              
              <div class="item text-center">
                	<img alt="" src="images/course/13.jpg">
                </div>
              
             
             <div class="row pricing-tables">

	            <div class="pricing-table highlight-plan" style="background: #f6f6f6;">
	              <div class="plan-name" style="background:#ececec;color: #515151;border-bottom: 0px;">
	                <h3 style="color: #515151;">Wednesday, 30 September</h3>
	                
	                <p>7 – 9 pm SGT</p>
	                
	                <p><img src="images/course/line.png" /></p>
	                
	                <h1><i class="fa fa-map-marker fa-2x"></i> GA Singapore @ The Working Capitol</h1>
	                
	                <br />
	                
	                <p>Annexe, The Working Capitol, 1A Keong Saik Rd - Turn left after Lollapalooza, and enter via the white door on the side lane Singapore</p>
	                
	              </div>
	              
	              <div class="plan-list">
	                <ul>
	                  <li style="border-bottom: 0px;border-top: 0px;padding: 40px;"><span style="float: left; font-size: 30px;"><strong>Regular Ticket</strong></span> <span style="float: right; font-size: 20px;">$35 SGD</span></li>
	                  <li style="border-bottom: 0px;"></li>
	                  <a href="#" class="btn-system btn-large" style="color: #515151; font-size: 20px;">Attend</a>
	                  <li></li>
	                </ul>
	              </div>
	            </div>
          
        	</div>

            </div>
            
          </div>
          
          

      	</div>
      <!-- .container -->
	</div>
	
   

<?php include 'footer.php' ?>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>