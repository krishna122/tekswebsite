<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | About Us</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider-aboutus.php' ?>
  
  
  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="big-title"><span style="font-size: 40px; text-transform: uppercase;">The Teks Story</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>We are a team of dedicated mobile and web development training providers, focused on helping you become successful in this domain. We have been arranging classes and offering training courses since 2006 ­ when even the first­generation iPhone was yet to arrive (our mentors used to conduct Java app development training at that time). Cut to the present ­ and we have 60+ courses on iOS and Android training, 15+ web training courses (including HTML and CSS courses), 120+ instructors, and over 10000 successful candidates. It has been an exciting journey ­ a journey which has shaped the careers of many new as well as experienced developers.</p>
              
              <!-- <div class="margin-top"></div> -->
              

            </div>

            <div class="col-md-6">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-02.jpg">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
  
  
  <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div style="text-transform: uppercase;">
            <h1 class="big-title text-center"><span style="font-size: 30px; text-transform: uppercase;">What Sets Our Mobile / Web Training Institute Apart ?</span></h1>
          </div>
          <!-- End Big Heading -->
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/corporate/1.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Choose Your Course </h2><br>
              <p>We present specialized training courses on how to make iOS apps, Android apps, and professional/personal websites. You can select from a vast range of courses that we have on offer.</p>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/corporate/2.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Learn From The Experts </h2><br>
              <p>At our institute, every student gets to interact with the best web and app developers in India. In our team, we also have noted app entrepreneurs from Australia and Sweden.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/corporate/3.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Career Oriented </h2><br>
              <p>Our iOS/Android/Web training modules are designed to help you become a professional developer. Candidates from our institute are currently placed in many leading mobile app companies across the world.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
  
   <div class="section courses-white">

      <div class="container" >
        
        <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/about-02.jpg">
                </div>
              
            </div>
            
        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span style="font-size: 40px;">Getting Trained At The Teks Institute</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Our web and mobile development training institute offers out­of­the­box opportunities to learn app development and website creation. At Teks, you get to work under the guidance of the best IT guides in India, create projects on the latest devices (from iPhone 6S to Mac OS X 10.11 systems), and learn to use the latest frameworks and tools for making apps. We conduct industry research on an ongoing basis, to ensure the best possible value­based learning scopes for each and every student. It’s not without reason that we are regarded as the top mobile and web training institute in India and abroad.</p>

            </div>

      </div>
      <!-- .container -->
  </div>
  
    <!-- Start Services Section -->
    <div class="section service" style="background: #fecc02;">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div style="text-transform: uppercase;">
            <h1 class="big-title text-center"><span style="font-size: 30px; text-transform: uppercase;">Your Training. Your Course. Your Choice.</span></h1>
            <p>At the Teks Learning institute, you get to choose from an exhaustive array of training courses and classes. We appreciate that not everyone has the same learning requirements or the time ­and we believe that there is something for each of you at our training center.</p>
          </div>
          <!-- End Big Heading -->
          <br>
          <!-- Start Service Icon 1 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/about/course.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Full-­Time Courses (8-12 Weeks)</h2><br>
              <p>We offer full­time courses for beginners, to help them learn how to make an app from scratch. There are separate corporate training modules as well.</p>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/about/course.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Part-­Time Courses (2-­6 Weeks)</h2><br>
              <p>These are akin to crash courses on web and mobile app development ­ ideally suited for working professionals who wish to take their app­making expertise a couple of notches higher.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/about/course.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Seminars </h2><br>
              <p>Industry seminars and panel discussions are regular features of all the training courses offered by our institute. Participation in these events helps students to directly interact with top developers.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->
          
          <!-- Start Service Icon 4 -->
          <div class="col-md-6 col-sm-6 service-box service-center" >
            <div class="service-icon">
              <img src="images/about/course.png" width="60%"/>
            </div>
            <div class="service-content">
              <h2>Workshops </h2><br>
              <p>Hands­on workshops are organised by us in different cities at regular intervals ­ to give you the chance to work on dummy projects, test your codes, and become a more well­informed developer.</p>
            </div>
          </div>
          <!-- End Service Icon 4 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    
  
  <div class="section courses">

      <div class="container" >
        <h1 class="big-title" style="text-align: center;"><span style="font-size: 40px; text-align: center;">Okay, That’s Enough About Our Institute...</span></h1>
        
        <div class="call-action call-action-boxed call-action-style1 clearfix">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:8px;"><a href="request-more-info.php" class="btn-system btn-large" id="color">Apply For A Course</a></div>
            <h2 class="primary">Let’s start with the web and mobile app training courses for you.</h2>
          </div>
          
      </div>
      <!-- .container -->
  </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#about').addClass('active');
	});

</script>

</body>

</html>