<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | Corporate Training</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
  <?php include 'slider-corporatetraining.php' ?>
  
  
  <div class="section courses-white">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>Shaping Careers. Grooming Developers. Providing Corporate Training.</span></h1>
              
             
              <!-- Some Text -->
              <p>The mobile/website development training modules offered by Teknowledge are all geared to help you make a mark as expert developers in the professional world. Our instructors - top app developers themselves - acquaint you with the latest industry standards, coding methods and development techniques. Particular attention is placed on making mobile apps for the latest platforms (iOS 9, Android 6.0 Marshmallow), and websites with various tools (Drupal, Wordpress, Joomla, Magento, Prestashop, Opencart). Special sessions on troubleshooting and client query handling also feature in all the courses.</p>
              

            </div>

            <div class="col-md-6">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/corporate/4.jpg">
                </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
  
  
  <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">
         <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1><strong>CORPORATE TRAINING</strong> COURSES AT TEKS TRAINING CENTER</h1>
          </div>
          <!-- End Big Heading --><br>
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/corporate/1.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>iOS App Development</h2><br>
              <p>Advanced application development training courses, classes for the iOS 9 platform, using the Xcode 7 framework.</p>
			  <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151; background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/corporate/2.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>Android App Development</h2><br>
              <p>Join a corporate-level Android development training course, and learn how to make efficient, engaging Android applications.</p>
              <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151;background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/corporate/3.png" width="50%"/>
            </div>
            <div class="service-content">
              <h2>Web Development </h2><br>
              <p>Attend classes, seminars and demonstration sessions on the development and designing of corporate websites. Special focus on CSS and HTML training.</p>
              <!-- <a href="request-more-info.php" class="slider btn btn-system btn-large" style="color:#515151;background: #dadada;">Learn More</a> -->
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
  
   <div class="section courses-white">

      <div class="container" >
        
        <div class="col-md-4">
				
                <!-- Memebr Photo, Name & Position -->
                <div class="item">
                	<img alt="" src="images/corporate/5.jpg">
                </div>
              
            </div>
            
        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="big-title"><span>HOW DOES TEKS LEARNING MAKE A DIFFERENCE?</span></h1>
              
              
              <!-- Some Text -->
              <p>The web and mobile app training courses and tutorials organised by Teks complement the academic knowhow of wannabe developers in the best possible manners. Depending on your convenience and precise learning requirements, you can choose from our full-time (8-12 weeks) and part-time (2-6 weeks) courses. Apart from our app courses for beginners, we bring to you corporate learning courses - covering advanced topics and techniques related to both iOS and Android development. You also get to learn coding for apps from some of the best developers in India. Teks is not just a learning institute - we are your learning partner!</p>

            </div>

      </div>
      <!-- .container -->
  </div>
  
  <div class="section" style="background: #fecc02;">

      <div class="container" >
        
        <h4 class="classic-title"><span>MEET THE MENTORS</span></h4>
        
        <p id="color">At Teknowledge Software, you get to learn from internationally acclaimed app developers, software testers, programmers, website developers, UI/UX experts, and other top-level IT professionals. Let’s meet some of the personnel in charge of the corporate training courses.</p>
        
        <div class="row">

            <!-- Start Memebr 1 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/team/face_1.png">
                  <div class="member-name">NAME <span>DESIGNATION</span></div>
                </div>
                <!-- Memebr Words -->
                <div class="member-info">
                  <p id="color">Mobile app entrepreneur, with nearly a decade of industry experience. Started out with Java apps, and is currently an acclaimed iOS / Android developer.</p>
                </div>
              </div>
            </div>
            <!-- End Memebr 1 -->

            <!-- Start Memebr 2 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/team/face_2.png">
                  <div class="member-name">NAME <span>DESIGNATION</span></div>
                </div>
                <!-- Memebr Words -->
                <div class="member-info">
                  <p id="color">Creator of the multiple award-winning ‘Story Time For Kids’ app (iOS and Android). Regularly conducts app development training classes at institutes across Asia.</p>
                </div>
              </div>
            </div>
            <!-- End Memebr 2 -->

            <!-- Start Memebr 3 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/team/face_3.png">
                  <div class="member-name">NAME <span>DESIGNATION</span></div>
                </div>
                <!-- Memebr Words -->
                <div class="member-info">
                  <p id="color">One of the most promising young app experts in Australia - with the super-successful ‘Stopover’ app among her list of successful creations. She guides beginners on how to start making mobile apps.</p>
                </div>
              </div>
            </div>
            <!-- End Memebr 3 -->

          </div>

      </div>
      <!-- .container -->
  </div>
  
  <div class="section courses">

      <div class="container" >
        <h1 class="big-title" style="text-align: center;"><span style="font-size: 40px; text-align: center;">BOOST YOUR CAREER AT TEKS</span></h1>
        
        <div class="call-action call-action-boxed call-action-style1 clearfix">
            <!-- Call Action Button -->
            <div class="button-side" style="margin-top:8px;"><a href="request-more-info.php" class="btn-system btn-large" id="color">Pick A Course</a></div>
            <h2 class="primary">Join a course, and let us help you become a top-notch professional web / mobile developer</h2>
          </div>
          
      </div>
      <!-- .container -->
  </div>

  <?php include 'footer.php' ?>


  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('#corporate').addClass('active');
	});

</script>

</body>

</html>