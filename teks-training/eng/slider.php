<!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="images/slider/bg1.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span>Mobile <strong>&</strong> Web Platforms</span>
                              </h2>
                <h3 class="animated3">
                                <span>One Stop Learning Center</span>
                              </h3>
                <p class="animated4"><a href="courses&classes.php" class="slider btn btn-system btn-large" style="color:#515151;">Upcoming Workshops</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->