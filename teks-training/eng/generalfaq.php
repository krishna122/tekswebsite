<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Teks Learning | General FAQ</title>
  <!-- Page Description and Author -->
  <meta name="description" content="">
  <meta name="author" content="">

  <?php include 'head.php' ?>

</head>

<body>

  <!-- Full Body Container -->
  <div id="container">

  <?php include 'header.php' ?>
    
    
    <div class="section courses" style="display: non;">

      <div class="container">

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="big-title text-center"><span style="font-size: 40px; text-transform: uppercase;">FAQ - General</span></h1>
              
              <!-- Some Text -->
              <p>Have a query? Need to know more about our mobile and web development training center? Well, we believe that you will get most of your answers right here:</p>
              
              <div class="margin-top"></div>
              
          <div class="panel-group" id="accordion">

                  <!-- Start Accordion 1 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							1. So, What Is Teks Learning?
						</a>
					  </h4>						
                    </div>
                    <div id="collapse-1" class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Teks Learning is an initiative on the part of Teknowledge Software - a leading multinational mobile app development company. We offer training courses on iOS app development, Android app development, Web development and UI/UX design, and advanced HTML/CSS training.
						</p>
                    </div>
                  </div>
                  <!-- End Accordion 1 -->

                  <!-- Start Accordion 2 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a data-toggle="" data-parent="#accordion" aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 2. Fair Enough, But How Do I Know You Guys Are Good?
						</a>
					  </h4>	
                    </div>
                    <div id="collapse-2" class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Our track record speaks for itself. As a mobile app company, we have 900+ applications in our portfolio - over 80% of which have been featured at the app stores. The roster of mentors and guides at Teks Learning feature the names of some of the top developers, testers and other IT experts from around the world. When you learn from the best, the learning has to be good!
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 2 -->

                  <!-- Start Accordion 3 -->
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 3. Wait A Second...Do I NEED To Join A Course?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Yes, you do. It’s not that you must seek help while learning to code for apps, or finding out how to create a website - but performing these tasks at a professional standard requires a formal educational course. And that’s what we have on offer.
						</p>
                    </div>
                    </div>
                  </div>
                  <!-- End Accordion 3 -->
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 4. But What If Your Course Is Not Suitable For Me?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Great question. The answer is a one-liner - we offer COURSES...not a single COURSE. You can take your pick from our myriad of web, Android and iOS development training classes and tutorials. You will find a course that is right up your street...we can promise you that.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 5. I Heard That Joining A Formal Training Course Can Be Very Expensive. Your Take On That?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 It isn’t, at least not at Teks Learning. The course fees we charge are extremely competitive. Training young developers and corporates is a passion for us - and is certainly not a mere business.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 6. It All Sounds Good, But I Do Not Have The Time For A Full-Time Course!
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Worry not, we have just the thing for people who cannot eke out enough time for a full-time course. We have a series of part-time courses - which are equally beneficial, and which you can schedule at your convenience. You learn...in the way you want.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
						7. What If I Do Not Want To Join An App Training Course For Beginners?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 You do not have to. If you are already familiar with the basics of app/web development, or are an corporate entrepreneur yourself - you can join any of our large bouquet of corporate training course. For FAQ on our corporate training classes, <a href="corporatefaq.php" style="text-decoration: underline;">click here</a>.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							8. Are The Classes All About Only Classroom Teaching?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Far from it. To ensure effective knowledge transfer, we organize seminars, hands-on workshops, debates, panel discussions, and a host of other special events related to website and mobile app development. Practical knowhow is vital - and we blend that in each of our training courses.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
					      9. What If I Wish To Shift From One Course To Another?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Yes, you can do that too - but only after providing prior information to us. Also, you can shift from one full-time course to another. Migrating from a full-time to a part-time course, or the other way round, is not allowed.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							10. Okay, an emergency has cropped up and I cannot attend a workshop! Will I get a refund?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 We do have a cancellation/refund policy for such instances. However, you have to let us know within 3 working days of the initial registration, that you will not be able to attend a particular seminar/workshop. No refunds are possible if such intimation is not given within this time.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 11. What If I Am Not Fully Satisfied With The Course?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 We have never faced such complaints till date, thankfully. However, in case you feel that any aspect of our classes/tutorials/seminars are not to your liking - feel free to let us know immediately. We are constantly trying to improve the quality of learning at Teks, and we will be more than happy to listen to your feedback and advice.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							12. Will I Get Trained On Working On The Latest Platforms?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 You bet. Our in-house trainers help you become experts on making apps that are optimized for the iOS 9, watchOS 2, tvOS and Android 6.0 Marshmallow platforms. On the web development training front, our courses include hands-on training on the WordPress, Joomla, Magento and Drupal platforms. Updated, cutting-edge professional training is what you get from us.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							 13. If I Join A Teks Course, Will I Gain Any International Exposure?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 Oh yes, you will. In addition to our in-house mentors and instructors - who are highly reputed web and app developers in their own right - we regularly invite industry experts from Australia and Sweden at our workshops and seminars. At these events, you can interact and pick up valuable tips and coding advice directly from them.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							14. Help! I Do Not Know Anything About Programming. Can I Still Join The Course?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 You can...but a working knowledge of C-programming and/or Java programming will make things easier for you. You do not need to have specialized coding expertise for making apps though - our trainers will provide you with that.
						</p>
                    </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default" style="background: #fff;border:0px;">
                    <div class="panel-heading">
                      <h4 class="panel-title">
						<a  data-parent="#accordion"  aria-expanded="false" class="collapsed" style="background: #fff;">
							
							15. Is There A USP Of The Teks Learning Courses?
						</a>
					  </h4>	
                    </div>
                    <div  class="panel-collapse collapse in" aria-expanded="false" >
                      <div class="panel-body" style="background: #fff;">
                      	<p>
                      	 We do not like to go into comparisons - but yes, our USP is providing practical, industry-standard training courses that help enrollees become professional mobile or web developers. There is a gap between academic knowledge and practical knowhow, and Teks Learning strives to bridge that gap.
						</p>
                    </div>
                    </div>
                  </div>

                </div>
        		</div>
        		
        		<p>If you have any other question, head straight over to our <a href="contactus.php" style="text-decoration: underline;">Contact Us</a> section and let us know. Our app and web development training courses have boosted the careers of many people...and we would love to have you on board!</p>
        		
        		
			</div>
      </div>
      <!-- .container -->
</div>
  
  </div>



  <?php include 'footer.php' ?>

<style>
	.liststyle{
		list-style: circle;
		margin: 25px;	
	}
</style>

  </div>
  <!-- End Full Body Container -->

 <?php include 'bottom.php' ?>

</body>

</html>