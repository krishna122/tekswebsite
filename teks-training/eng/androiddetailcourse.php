<!-- Start Services Section -->
    <div class="section service courses" id="android" style="display: non;">
      <div class="container">
        <div class="row">
        	
        	<h1 class="big-title" style="font-size: 40px; text-align: center;text-transform: uppercase;">Android App Development Training At Teks</h1>
        	
        	<div class="margin-top"></div>
        	
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/course/18.png" />
            </div>
            <div class="service-content">
              <h4>Work With Android Studio</h4>
              <p>Learn how to make custom Android apps with the tools & APIs of the latest version of Android Studio. Expert app developers available as guides.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/course/19.png" />
            </div>
            <div class="service-content">
              <h4>Java Programming</h4>
              <p>Graded Java programming courses for Android app development. Code with Java and learn to use the Android SDK for making software and apps.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/course/20.png" />
            </div>
            <div class="service-content">
              <h4>Android Training For Everyone</h4>
              <p>Teks brings to you personalised Android training courses for beginners and working professionals. Learn how to make Android apps with the latest techniques.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->
    
    
    
    
    
    
<!-- Start Services Section -->
    <div class="section service courses-white">
      <div class="container">
        <div class="row">
        	
        		
        <!-- Start Big Heading -->
	   
	       <h1 class="big-title text-center" style="font-size: 40px;">Android Training At Teks</h1>
	       
	       <div class="margin-top"></div>
	       
	       <p class="title-desc text-center">Industry-oriented, practical Android app development training is what we offer at Teks. Take a look around at the professional help you will get after joining a course:</p>
	     
	    <!-- End Big Heading -->
           
           <div class="margin-top"></div>
           
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <img src="images/team/1.png" />
            </div>
            <div class="service-content">
              <h4>Developers As Guides</h4>
              <p>Get mentored, trained and helped at every stage by the top Android app developers in India. Clarify doubts, work on dummy Android projects, and enhance your coding skills.</p>

            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <img src="images/team/2.png" />
            </div>
            <div class="service-content">
              <h4>More Than Classroom Training</h4>
              <p>Our interactive seminars, panel discussions and workshops are instrumental in helping you get a hands-on experience of developing Android apps.</p>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-4 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <img src="images/team/3.png" />
            </div>
            <div class="service-content">
              <h4>Flexible Courses & Timings</h4>
              <p>For your convenience, we have both full time and part time Android development courses and modules. View the syllabus, and join the course that suits you best.</p>
            </div>
          </div>
          <!-- End Service Icon 3 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->