<?php include_once 'mysqlconnect.php'; ?>

<div class="section courses">

      <div class="container" >

        <div class="col-md-6">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span style="font-size: 40px; text-transform: uppercase; ">Seminars, Workshops & Tutorial Classes</h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Keep track of the latest web and mobile app development tools, techniques and methods by participating in our in-house workshops and seminars.</p>
              
              <!-- <div class="margin-top"></div>
              
              <a href="trainingplace.php" class="btn-system btn-large border-btn btn-black">Attend a Class or Workshop</a> -->

            </div>

            <div class="col-md-6">
				
		<div class="team-member">

                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/3.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
    
<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li><a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> "><?php echo $result['locationname']; ?></a></li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <div class="row">

            <!-- Start Image Service Box 1 -->
            <div class="col-md-6 image-service-box">
              <img class="img-thumbnail" src="images/iosworkshop.jpg" alt="">
              <h4>iOS-Con 2016: Training & Development</h4>
              <p>Coming Soon</p>
            </div>
            <!-- End Image Service Box 1 -->

            <!-- Start Image Service Box 2 -->
            <div class="col-md-6 image-service-box">
              <img class="img-thumbnail" src="images/androidworkshop.jpg" alt="">
              <h4>Deal With Droid 2016: The Android Workshop</h4>
              <p>Coming Soon</p>
            </div>
            <!-- End Image Service Box 2 -->

          </div>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-7">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span style="font-size: 40px;">FULL-TIME COURSES</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Give Your Career A Boost By Joining Our Full Time Mobile/Web Training Courses (Duration: 8 - 12 Weeks)</p>
              
              <!-- <div class="margin-top"></div>
              
              <a href="trainingplace.php" class="btn-system btn-large border-btn btn-black">View Full-Time Courses</a> -->

            </div>

            <div class="col-md-5">
				
	      <div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/1.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>
   
<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			    <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li>
			      	<a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> ">
			      		<?php echo $result['locationname']; ?>
			      	</a>
			      </li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
              <ul class="list-group">
              	<?php 

					  $result = array();
					  $query = "SELECT * FROM  coursename";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			     ?>
			      <a href="detailcourse.php?detailcourse=<?php echo $result['name_en']; ?>&coursetime=FULL-TIME COURSES">
			      	<li class="list-group-item">
			      		<span>
			      			<strong style="font-size: 18px;"><?php echo $result['name_en']; ?></strong>
			      		</span> 
			      		<span style="float: right">9th May 2016 <strong style="font-size: 16px;">to</strong> 13th May 2016</span>
			      	</li>
			      </a>
			      <?php 
					   }
					}
			      ?>
			    </ul>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-7">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span style="font-size: 40px;">PART-TIME COURSES</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Sign Up For Our Part Time Courses And Become A More Efficient Web/Mobile App Developer (Duration: 4 - 6 Weeks)</p>
              
              <!-- <div class="margin-top"></div>
              
              <a href="trainingplace.php" class="btn-system btn-large border-btn btn-black">View Part-Time Courses</a> -->

            </div>

            <div class="col-md-5">
				
				<div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/2.jpg">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
   
</div>

<div class="section courses-white">

      <div class="container" >

        <div class="col-md-12">

              <!-- Classic Heading -->
              <h1 class="dropdown classic-title">
			    <div class= "dropdown-toggle" id="menu1" data-toggle="dropdown" >KOLKATA <span class="caret" ></span></div>
			      <ul class="dropdown-menu" aria-labelledby="menu1">
			    	<?php 

					  $result = array();
					  $query = "SELECT * FROM  location";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <li>
			      	<a tabindex="-1" style="color: #515151;font-weight: 300;font-size: 16px; text-transform: uppercase;" href="locationwise.php?locationname=<?php echo $result['locationname']; ?> ">
			      		<?php echo $result['locationname']; ?>
			      	</a>
			      </li>
			      <?php 
					   }
					}
			      ?>
			    </ul>
			  </h1>
              
              <div class="margin-top"></div>
              
               <ul class="list-group">
              	<?php 

					  $result = array();
					  $query = "SELECT * FROM  coursename";
					  $sqlObj = new MySqlconnect();
					  $sqlObj->fetch($query);	


				      if(mysql_num_rows($sqlObj->mysql_result) > 0)
			            {                                  
	                               
				       while($result = mysql_fetch_array($sqlObj->mysql_result))
				       {
				       	
						
			       	?>
			      <a href="detailcourse.php?detailcourse=<?php echo $result['name_en']; ?>&coursetime=PART-TIME COURSES">
			      	<li class="list-group-item">
			      		<span>
			      			<strong style="font-size: 18px;"><?php echo $result['name_en']; ?></strong>
			      		</span> 
			      		<span style="float: right">9th May 2016 <strong style="font-size: 16px;">to</strong> 13th May 2016</span>
			      	</li>
			      </a>
			      <?php 
					   }
					}
			      ?>
			    </ul>

        </div>
	
	</div>
      <!-- .container -->
</div>

<div class="section courses">

      <div class="container" >

        <div class="col-md-8">

              <!-- Classic Heading -->
              <h1 class="classic-title"><span style="font-size: 40px;">Apple App Development - From Wireframing To Testing</span></h1>
              
              <div class="margin-top"></div>
              
              <!-- Some Text -->
              <p>Learn to work with Xcode 7 and Swift 2. Work with globally renowned developers on iOS app projects.</p>
              
              <div class="margin-top"></div>
              
              <a href="contactus.php" class="btn-system btn-large border-btn btn-black">Contact Us</a> 

            </div>

            <div class="col-md-4">
				
		<div class="team-member">
                <!-- Memebr Photo, Name & Position -->
                <div class="member-photo">
                  <img alt="" src="images/course/5.png">
                </div>
                
              </div>
              
            </div>

      </div>
      <!-- .container -->
</div>