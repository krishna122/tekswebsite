<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Awards We Get</title>
<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>

<header class="award" style="height: 60%;">
    <div class="award-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">PRIZE-WINNING APPS BY TEKSMOBILE</span></h1>
	                    <p>Over the years, our apps have won many important professional awards and accolades. Here are some of our ‘winners’</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
    
<section style=" padding-top:20px; padding-bottom:20px;" class="app1" id="award">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				  <div class="col-lg-6">
				 
				  	<span style="text-align: center;"><img alt="Story Time For Kids" src="awards/img/app/logo-storytime.png"></span>
                                        <h5 style="color:#1F1F21">Story Time For Kids</h5>
				 	<br><br>	
				  	<ul style="padding: 0px;color: #1F1F21; list-style: none;text-align: left;">
				  	<li>(Won) Adobe Design Award, 2013</li>
					<li>(Won) GMASA 2015 - Best Books & Reference App</li>
					<li>(Nominated) mBillionth South Asia Award, 2014</li>
					</ul>
				  	<br><br>
				  </div>
				  
				<div class="col-lg-6">
				  <img src="awards/img/app/storytime.png" align="right">
				</div>
		  </div>
		</div>
	</div> 
</section>

<section style=" padding-top:20px; padding-bottom:20px;" class="app2">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
				  <img src="awards/img/app/stopover.png" align="left">
				</div>
				  <div class="col-lg-6">
				  	<span style="text-align: center;"><img alt="Stopover" src="awards/img/app/stopoverlogo.png">  </span>
				 	<br><br>	
				  	<ul style="padding: 0px;color: #1F1F21; list-style: none;text-align: left;">
				  	<li>(Won) <strong>Rising Star</strong> Award at Talent International 2014</li>
					<li>Prize Awarded By Richard Branson & Steve Wozniak</li>
					<li>Available for iOS & Android platforms</li>
					</ul>
				  	<br><br>
				  </div>
		  </div>
		</div>
	</div> 
</section>

<section style=" padding-top:20px; padding-bottom:20px;" class="app3">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				  <div class="col-lg-6">
				 
				  	<span style="text-align: center;"><img alt="SpeedyKey" src="awards/img/app/sklogo.png">  </span>
				  	<h5>Speedy Key</h5>
				 	<br><br>	
				  	<ul style="padding: 0px;color: #fff; list-style: none; text-align: left;">
				  	<li>Top rank in the Danish App Store</li>
					<li>Featured among the top-10 list in Faroe Island.</li>
					<li>Highlighted in several app magazines & journals.</li>
					</ul>
				  	<br><br>
				  </div>
				  
				<div class="col-lg-6">
				  <img src="awards/img/app/speedykey.png" align="right">
				</div>
		  </div>
		</div>
	</div> 
</section>

<section style=" padding-top:20px; padding-bottom:20px;" class="fav7">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  		
	  			<div class="col-lg-6">
				  <img src="awards/img/app/icbf.png" align="left">
				</div>
				
				  <div class="col-lg-6">
				 
				  	<span style="text-align: right;"><img alt="Hello Mind" src="img/hellomind.png">  </span><br><br>
				  	<h5 style="text-align: left;">Hello Mind</h5>
				 	
				  	<ul style="padding: 0px;color: #fff; list-style: none;text-align: left;">
				  	<li>Featured at Apple App Store.</li>
					<li>Positive Reviews & Ratings from multiple leading app magazines & portals.</li>
					<li>Separate appreciation for each individual app</li>
					</ul>
				  	<br><br>
				  </div>
		  </div>
		</div>
	</div> 
</section>

<?php include "map.php";?>
<?php include "footer.php";?>
<?php include 'script.php';?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#awards').addClass('active');
	});
</script>

</body>

</html>