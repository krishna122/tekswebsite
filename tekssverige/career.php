<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Looking For a Job</title>

	<?php include 'head.php';?>

</head>


<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="career">
        <div class="career-body">
            <div class="project-container" style="margin-top: 8%; margin-bottom: 2%;">
                   <div class="row">
                    <div class="col-md-12">
                        <h1 style="color:#fff; font-weight: 900;">Work @ Teks</h1>
                        <div style="text-align: center;"><img alt="" src="img/icon.png" ></div><br/>
                        <p style="color:#fff;">Working at our mobile app company is more than a job at just another IT firm. At Teknowledge, we shape careers, hone app development/designing skills, provide regular training... and CARE for each member of our family. Oh, and our compensation packages are right at par with the best in the industry!</p><br/>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                        <a href="#career" class="page-scroll" style="text-align: center;">
                        <span class="animated"><img src="img/scrollbutton.png"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
	
<section id="career">
	<div class="container">
	  <div class="row">
	  <h1>Join The Teks Team</h1>
	  <p style=" text-align: center; color:#9c9c9c;font-size: 30px; margin-top: -50px;">Varied Opportunities. Multiple Openings. Ample Scope To Do What You Love.</p><BR></BR>
	  <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                    <img src="img/c1.png">
                    <div class="info">
                        <h4 style="font-size:25px;" class="title">Ui/Ux Designer</h4>
                    </div>
                </div>
                <div class="space"></div>
           
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
         
                <div class="icon">
                    <img src="img/c2.png">
                    <div class="info">
                        <h4 style="font-size:25px;" class="title">Android Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
          
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c3.png">
                    <div class="info">
                        <h4 style="font-size:25px;" class="title">Project Coordinator</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c4.png">
                    <div class="info">
                        <h4 style="font-size:25px;" class="title">PHP Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
    </div>
 </div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-6"> 
				<div class="panel panel-default job1" style=" border: none; cursor: pointer;" onclick="job1()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Graphics</span><br>
			    			<span style="font-weight: 200;color:#fff;">UI/UX Designer</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px; font-weight: 300;">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job2" style=" border: none; cursor: pointer;" onclick="job2()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Android</span><br>
			    			<span style="font-weight: 200;color:#fff;">App Developer</span>
		    			</h3> <br> 
	    			<center>
	    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
	    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job3" style=" border: none; cursor: pointer;" onclick="job3()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Operation</span><br>
			    			<span style="font-weight: 200;color:#fff;">Project Coordinator</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job4" style=" border: none; cursor: pointer;" onclick="job4()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Coding</span><br>
			    			<span style="font-weight: 200;color:#fff;">Php Developer</span>
		    			</h3> <br> 
			    			<center>
			    			<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
			    			</center> 
	    			</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
function job1() {
    window.location.assign("applydesigner.php")
}

function job2() {
    window.location.assign("applyandroid.php")
}

function job3() {
    window.location.assign("applypm.php")
}

function job4() {
    window.location.assign("applyphp.php")
}
</script>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

<script type="text/javascript">
$(document).ready(function(){
	$('#careers').addClass('active');
});

</script>

</body>
</html>