<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hyr appspecialister i Sverige på “molnbasis”</title>

    <?php include 'head.php';?>
    <link href="css/appstoriesnew.css" rel="stylesheet">

    <style>
        p {
            font-size: 1em
        }
    </style>
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

    <?php include 'header.php';?>

    <!-- Intro Header -->
    <header class="api" style="height: 60%;">
        <div class="api-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
							<span style="color:#fff; text-transform: uppercase;font-size:2rem;">Hyr appspecialister i Sverige på “molnbasis”</span>
						</h1>
                        <p>Höj värdet på ditt projekt genom att använda våra utvecklingstjänster via molnet</p>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="offwhite-background">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="txt-feature txt-fly-over bg-2">
                        <h3 class="color">Offshore App & API Utveckling</h3>
                        <p class="color">Teks Mobile Sverige erbjuder dig molnbaserade tjänster inom mjukvaruutveckling. Dela dina projektuppgifter med oss, berätta vilken typ av team du vill hyra - och låt oss förbereda allt som krävs för att komma igång med projektet. När du samarbetar med Teks, har du som kund alltid sista ordet!</p>
                        <p class="color">Vi erbjuder hög kompetens inom app/ API utveckling, 2D / 3D spelutveckling, webbdesign, utveckling och underhåll, och (IoT). Vi har utvecklat över 1000 för iOS och Android,  och du kan vara trygg när det gäller kvaliteten på vårt jobb och service.</p>
                    </div>
                </div>
            </div>
        </div>
        
    </section>    

    <section class="offwhite-background" style="background:#F2F3EE;">
        
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="color">
						Varför anställa ett offshore team från Teks Mobile?
                     </h3>
                    <p class="color">När du jobbar med en “molnanställd” förvandlas vårt kontor till din alldeles egna anpassade arbetsplats. Vi ser till att du får de mest de bäst lämpade mjukvaruutvecklarna för ditt projekt. Du  hålls konstant uppdaterad under det pågående arbetet. Logga in och få tillgång till fjärrstyrning och bli en del av produktutvecklingen redan från början.</p>
                    <p class="color">Att hyra en expert och anställa en molnbaserad utvecklare har många fördelar.</p>
                </div>

                <div class="col-lg-12">
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <center>
                        <img alt="api_design" src="img/api/api_design.png">
                    </center>
                    <h2 class="apih2">Rekrytering och urval</h2>
                    <p class="text-center color apip">Vi ser till att vi har den nödvändiga kvalificerade arbetskraften för olika typer av mjukvaruutveckling. Du slipper kostnader från rekryteringsföretag. Du får alltid en möjlighet handplocka de utvecklare som du anser vara bäst för just ditt projekt.</p>
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <center>
                        <img alt="api_development" src="img/api/api_development.png">
                    </center>
                    <h2 class="apih2">Bygg ditt eget team</h2>
                    <p class="text-center color apip">Du får full frihet att forma ditt eget projekt-team. Beroende på vad uppdraget kräver, så kan du välja allt från en enskild utvecklare, eller ett stort team. Vi finns tillgängliga för startups, små, medelstora och stora  företag.</p>

                </div>
                </div>
                
                <div class="col-lg-12">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <center>
                        <img alt="api_strategy" src="img/api/api_strategy.png">
                    </center>

                    <h2 class="apih2">Vi samarbetar globalt</h2>
                    <p class="text-center color apip">Släpp oron att inte vara på plats fysiskt. Den senaste tekniken gör det möjligt med fjärr-projektledning och andra synkroniseringsverktyg gör dig till en del av teamet oavsett var du befinner dig. Har du en fråga, ring oss på Skype!</p>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <center>
                        <img alt="api_testing" src="img/api/api_testing.png">
                    </center>

                    <h2 class="apih2">Kvalitetssäkring</h2>
                    <p class="text-center color apip">Efter över 10 års bransch-närvaro har Teks Mobile varit synonymt med kvalitativa projekt samt hög arbetsmoral. Teamet har gedigna kvalitetsparametrar och vi lägger största vikt på säkerhet och autentisering av data. Dina idéer är alltid säkra hos oss.</p>
                </div>
                </div>    
                    
                <div class="col-lg-12">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <center>
                        <img alt="api_design" src="img/api/api_design.png">
                    </center>
                    <h2 class="apih2">Tidszoner</h2>
                    <p class="text-center color apip">Teks Mobile erbjuder sina tjänster till användare från mer än 18 olika tidszoner. Vi har ett stort antal yrkesverksamma från olika delar av världen. Vi ser till att det inte finns några språkliga hinder.</p>
                </div>
                </div>    

            </div>

        </div>

    </section>
   
    <section class="offwhite-background">
        <div class="container">
            <h3 class="color text-center">Hur går det till?</h3>
            <p class="color text-center">Att anlita/anställa våra utländska frilansare är extremt lätt. Här är stegen:</p>
            
            <div class="row bs-wizard" style="border-bottom:0;">
                
                <div class="col-xs-3 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum">Projektbeskrivning</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Förklara projektet i detalj för oss. En riktigt specifikation ger oss möjligheten att leverera den bästa servicen.</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Rekrytering</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Vi bygger tillsammans teamet efter kundens önskemål. Vi rekryterar de bästa mjukvaruutvecklarna för ditt projekt.</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Projektledning</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Du blir tilldelad en projektledare som har huvudansvaret för uppdraget. Han/hon förser dig med 
regelbunden rapportering och feedback. Du kan själv välja hur delaktig du vill vara i det dagliga arbetet. Du interagerar enkelt via molnet. Projektrapporter och status uppdateras varje dag.</div>
                </div>
                
                <div class="col-xs-3 bs-wizard-step active"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Avtalsform</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Vi jobbar på löpande kontrakt eller tidsbestämda uppdrag. Vi undertecknar gärna sekretessavtal (NDA) om det krävs för uppdraget.</div>
                </div>
                
            </div>
            
        </div>
    
    </section>

    <?php include "map.php";?>

    <?php include 'footer.php';?>

    <?php include 'script.php';?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#cloudemployee').addClass('active');
        });
    </script>

</body>

</html>