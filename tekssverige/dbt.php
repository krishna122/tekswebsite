<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of DBT</title>

	<?php include 'head.php';?>
    <link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="dbt_story" style="height: 34%;">
        <div class="dbt_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                        <br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">DBT Selfhelp & Diary Card</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Self-help apps are a dime a dozen at the Apple App Store. Do a Google search on “iPhone self-help apps”, and you will find several ‘top-X’ results displayed immediately. We have already created many such applications, and some of them like <a href="icba.php"><strong>I Can Be Anything</strong></a> have gone on to become big winners. DBT is yet another self-help application that we feel can turn out to be every bit as successful.
	         </p>
	         <blockquote class="color blockdbt">
		  	    Life is all about acquiring new skills and coping with day-to-day crisis. The DBT app looks to make these tasks easier for every user - via regular monitoring, self-evaluation and tracking. It is an app that, I believe, everyone would like to have on their devices.
		  	</blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
<!-- 		  <div class="col-lg-12"> -->

			 <!-- 		  	 <div class="col-lg-6 hidden-xs"> -->
<!--		  	 	<video autoplay loop muted  poster="2016-04-16.png" style="border: 5px solid #D07A67;"> -->
<!-- 				   <source src="video/appstories/DBT.mp4" type="video/mp4">  -->
<!-- 				</video> -->
<!-- 		  	 </div>    -->
		  	
		  	 <div class="col-lg-12">
		  		<h3 class="color">Platform Support</h3>
		  	    <p class="color">
		          From the very start, DBT was meant to be a dedicated iPhone self-help application. We only had to take a call on what the backward compatibility (i.e., the older versions of iOS that the app supports) would be. After a couple of discussion sessions with the client, it was decided that DBT will be compatible with iPhone handsets working on iOS 7, iOS 8, and of course, iOS 9.
		         </p>
		         
		  	    <blockquote class="color blockdbt">
		  	    	The thing I personally liked best about this project was the underlying thought...of allowing people monitor and enhance themselves while managing their busy daily schedules. DBT would certainly rank among the apps that I would love to use on my iPhone. Many of my colleagues are looking forward to using it as well!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          A detailed free app quote was sent to the concept owner of DBT within 24 hours after he had got in touch with us with the idea. As soon as all the formalities were completed, our team of iPhone developers started working in right earnest on the project. It was one of the most interesting app projects of this quarter.
		         </p>
		  	 </div>
		  	 
<!-- 		  </div> -->
		</div>
		
<p></p>
	
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Skills & Crisis</h3>
		  	    
		         <p class="color">
		         	Indeed, the list of personal ‘Skills’ and ‘Crisis’ can be updated on a daily basis on the DBT application. Reminders can also be set, and users can check out all reminders on one screen. In addition, one CRISIS NUMBER number can be set by going to the ‘Edit’ section of ‘Emergency Calling’ feature of the app. Keeping people in the loop...the easy way!
		         </p>

                 <blockquote class="color blockdbt">
		  	    	No one can be more honest about the skills that a person actually acquires and the problems (s)he faces than, well, the person him or herself. I wanted DBT to be an app where users could easily list their skills and crisis. As new skills are mastered and new crisis crop up, the lists can be updated.
		  	    </blockquote>
		  	       
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/dbt6.png" alt="DBT" style="width:90%;" ></center>
		  	</div>
		  
		  </div>
		</div>
		
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/dbt10.png" alt="DBT" style="width:60%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7">
		  		<h3 class="color">Monitoring Health With DBT</h3>
		  	    <p class="color">
		            Regular usage of the DBT app would automatically boost health-consciousness levels among individual users. In the ‘Health Data’ screen of this iOS self-help application, a user can set any number of health parameters that (s)he would like to monitor. Separate ‘Period Length’ and ‘Cycle Length’ can be set, for more customized monitoring of the selected health metrics.
		         </p>
		         
		  	    <blockquote class="color blockdbt">
		  	    	Health monitoring is one of the most essential features of any self-help app - at least that’s what I feel. While working on DBT, we were aware that health data have to be bundled in the app. With valuable inputs from the client, my team of iPhone app developers managed to do this with ease.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		            To ensure that a user does not miss out on any health data report, a daily alarm is triggered in the app - from 7 days before the conclusion of a ‘Period Length’. There is not a chance of anyone missing the health information they wish to track.
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-7">
		  		<h3 class="color">Custom Pre-Defined Skill Lists</h3>
		  		<p class="color">
		          As and when a person learns a new skill (learn a new language, master a new dance step, etc.), (s)he can simply add it to the list of pre-defined skills created earlier. There are question papers based on the added skills which the user can then solve, as many times as (s)he wants. It’s like a bit of gaming fun thrown inside a self-help application. 
		         </p>
		         
		         <blockquote class="color blockdbt">
		  	    	If a person uses DBT and does not actually learn all the things about a skill he thinks he has mastered, that’s an opportunity lost. To counter this, Hussain and I went with the idea of adding a ‘Practice’ section in the app - where users could answer simple questions about the skills they add to their lists.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          As new skills are learnt, more question papers become available - and the scope of enhancing one’s overall knowledge pool goes up gradually. 
		         </p>
		  </div>
		  
		  <div class="col-lg-5">
		  		 <center><img src="appstories/dbt9.png" alt="DBT" style="width:55%;"></center>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">A Personal Self-Help Diary</h3>
		  	    
		         <p class="color">
		         	That’s precisely what the DBT app is meant to be. With regular additions to skill lists and crisis, practice questions, reminders and emergency contact numbers, this software doubles up as a cool diary companion app for all users. With ‘Diary Card Settings’, we have made sure that people do not forget to update their ‘Diary Card’ for long.
		         </p>
                 
                 <blockquote class="color blockdbt">
		  	    	In the Diary Card Settings of DBT, a special notification is generated whenever a user misses his/her diary card for even one day. As more days pass, the notification message gets updated. For example, if the user has missed the Diary Card for, say, X days - the notification will read…’Missed Diary Card X days’ 
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt1.jpg" alt="DBT"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt2.jpg" alt="DBT"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt3.jpg" alt="DBT"></div>
		    
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt4.jpg" alt="DBT"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt7.jpg" alt="DBT"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dbt8.jpg" alt="DBT"></div>
		  </div>
		</div>
		<p></p>
		<div class="row">
			  <div class="col-lg-12">
			  	   <p class="color">
					  Behaviours and Emotions can be separately added on the app by individual users. The latter can also rate the emotions and behaviour patterns they add (all ratings are out of 5). Based on the self-assigned ratings, a graphical representation of the emotions and behaviours get prepared and can be viewed by users.
				   </p>
				   <blockquote class="color blockdbt">
					  At times, people act on an urge, and it would be a mistake to classify such acts as regular behaviour. DBT factors in this consideration, and lets users specify whether any of their recent behaviours was prompted by an urge. It’s all about considering everything in the right perspective.					
				   </blockquote>
			</div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-7">
		  		<h3 class="color">Managing Crisis from Personal Section</h3>
		  		<p class="color">
		          From the personal section of this unique iPhone self-help and evaluation application, a person can directly call the CRISIS NUMBER (s)he had added at the start. The number can, of course, be edited from here as well. Separate ‘crisis pictures’ can be added too - for a more visual representation of the problems faced on a day-to-day basis. 
		         </p>
		         
		         <blockquote class="color blockdbt">
		  	    	The priority of different crisis or problems do not remain the same as we move on in life. We face new issues, and older problems somewhat move down our pecking order of concerns. To reflect this, we added the crisis reordering feature in DBT.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          A user can reorder his/her list of crisis at any time, based on the relative importance of each of the issues. Just as in a diary app, posts can be added in the app - and each post can contain ‘Notes’, ‘Comments’, ‘Insights’ and ‘Life Events’. From ‘Calendar’, daily reminders and alerts can be set. 
		         </p>
		  </div>
		  
		  <div class="col-lg-5">
		  		 <center><img src="appstories/dbt5.png" alt="DBT"></center>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Managing <strong>My Health</strong></h3><br>
		  		
		  		 <blockquote class="color blockdbt">
		  	    	DBT is nothing short of an excellent personal health monitoring app for users. They can add and monitor many personal health parameters, track their regular medications, and even add emergency contact numbers...that is, the number of a user’s regular health practitioner. 
		  	    </blockquote>
		  	    
		         <p class="color">
		         	Under the “Well Being” section of ‘My Health’, 2 questions are present by default. Our developers were constantly in touch with the client, while finalizing the other features that are present in this section of the app. A self-help application that attaches prime importance to personal health - that what DBT is supposed to be.
		         </p>
		  </div>
		</div>
		<div class="row">
			  <div class="col-lg-12">
				   <div class="box">
				  	  <p class="color">
				  	     DBT will soon be launched at the Apple App Store. It is an iPhone self-help app with a difference - and we are more than sure that it will rack up impressive download figures over the long-run. After all, who doesn't want easy self-evaluation, monitoring and health tracking...right at their fingertips?
				  	  </p>
				  </div>
			  </div>
		</div>
	</div>
 </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>