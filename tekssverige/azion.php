<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Azion Team Challenge</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Azion Team Challenge</span>
                        </h1>
                        <a href="https://itunes.apple.com/au/app/team-challenge/id771353026" target="_blank"><img alt="Azion Team Challenge" src="img/appstore.png"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.teamchallenge" target="_blank"><img alt="Azion Team Challenge" src="img/play-store.png"></a>
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
          <blockquote class="color">
              Children, these days, are not active enough!
          </blockquote>
        
          <p class="color">
              ...and hence goes the common refrain of parents all over the globe. It’s a fair enough observation - since the advancement of digital tools and games (read: addictive gaming apps) are gradually replacing actual physical activities of kids and youngsters. Truth be told though, there aren’t too many apps out
          </p>
	   </div>
        <p class="color">there that encourage young people to adopt a more active lifestyle, in a fun, exciting manner either. That was precisely why we were delighted beyond a point to receive the Azion Team Challenge project. Here was, we felt, a chance to make the lives of users more…’active’!</p>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
        
        <div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	It’s all very well to rant about how people nowadays...particularly students...are becoming more and more cooped up in homes. For all the advancements in mobile app technology, we are yet to come up with a nice application that blends systematic activity routines with interesting, fun elements. Azion Team Challenge looks to fill this gap, and I feel it has every chance of becoming a big success
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">Starting Off With The App</h3>
                 <p class="color">
                     As per our policy, a free app quote was sent to the concept-owner of the project, within 48 hours after we had been contacted. The paperwork were all settled without a hitch, and we could start working on the app from the next week itself. This one had piqued our interest in a big way, and we were eager to take up the challenge of making it in the best way possible.
                 </p>
		  	 </div>
              
		  </div>
		</div>
		
        <div class="row">
            <div class="col-lg-12"><br>
                <div class="col-lg-6">
                    <center><img src="appstories/azion4.png" alt="Azion Team Challenge" ></center>
                </div>
                <div class="col-lg-6">
                    <h3 class="color">On iOS. On Android.</h3>
                    <p class="color">
                        Azion Team Challenge, as the very name indicates, is an app about fitness and wellness tracking. As such, it only made sense that it should be built for both the iOS and the Android platforms. The client advised us accordingly, and our teams of iOS and Android app developers started out with the preliminary wireframing simultaneously.
                    </p>
                    <blockquote class="color">
                        I wanted the Azion Team Challenge app to be present on both Android and iOS, and I wanted the same company to make both versions. Teksmobile fitted the bill perfectly...and in Hussain, I found a partner I could rely on. The developers in this company were sincere, skillful and really cared for what I wanted
                    </blockquote>
                    
                </div>
            </div>
            <p class="color">
                This innovative team activity management app was made compatible for iOS 8 and above (both iPhone and iPad) and Android 4.0.3 and above. This made it possible for us to target a wider audience, and make the benefits of the app reach out to a larger cross-section of people.
            </p>
        </div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Multiple User Roles & Secure Login</h3>
		  	    
		  	    <p class="color">
		          Although primarily meant for kids and youngsters, users with other profiles can also log on to Azion Team Challenge and enjoy its features. There are, in total, 4 different user roles in the app - with individuals having the option to sign in as a student, as a parent, as a teacher, and as an organization (team entrant). The user profiles are created from the backend.
		         </p>
                 
                 
		  	    <blockquote class="color">
		  	    	The presence of several high-performance APIs ensure the smooth functionality of Azion Team Challenge. A lot of things are done from the backend - right from creating users, to generating usernames and passwords for each user. This is one activity tracker app that focuses on security...big time
		  	    </blockquote>
                 
		  	 </div>
		  	 
		  	 <div class="col-lg-6"><br><br>
		  	 	<center><img src="appstories/azion2.png" alt="Azion Team Challenge"></center>
		  	 </div>   
              
		  </div>
            <p class="color">
                  Once a user logs in with the credentials provided by the app admin (backend), (s)he has to agree to the terms of use (compiled on a dedicated screen) before proceeding further.
              </p>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-5">
                  <center><img src="appstories/azion3.png" alt="Azion Team Challenge" style="width:60%;"></center>
              </div>
                  
              <div class="col-lg-7">
		  		<h3 class="color">Choosing Avatars...The Customized Way</h3>
		  		
                <p class="color">
                    As already highlighted right at the start, we were out to make an activity management app that was interesting, one which came with more than its fair share of interactive features. The app developers working on this project had several rounds of meetings with the app-owner, on how such customizations were to be included in the application. Giving users the option to choose, and in fact, create their own profile avatars was a start.
		         </p>
              
		         <blockquote class="color">
                     For an app like Azion Team Challenge, I felt that interactive features should be present right through. Even the seemingly routine process of setting up user profiles should not seem boring. After consulting with Hussain and his team, we decided to go ahead with fully customizable profile avatars in the app.
		  	    </blockquote>
		  	    
                <p class="color">
                    Right from opting for a male or female avatar, to choosing its face (you can even ‘generate a face’) and hair colour - a user has full control over deciding exactly exactly how (s)he would ‘look like’ on the the Azion Team Challenge community. The avatars of other team members can also be viewed.
                </p>    
            </div>  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Smooth Onboarding With Tutorials</h3>
		  	    
                 <blockquote class="color">
                     To be honest, I have lost count of apps that seem real cool at the stores, but end up confusing users...simply because they do not provide adequate guidance. Azion Team Challenge does not present any such hassles. There are as many as 4 different tutorial screens for users to check out, after they have logged in
		  	    </blockquote>
                 
                 <p class="color">
                 	Each of the tutorial screens explains a specific feature or functionality of this all-new iOS/Android team activity app. The first showcases how the app doubles up as an exciting virtual adventure tool - across exotic locations around the world (how about a virtual trek across the Gobi Desert?). The second one highlights how unique avatars can be created/selected by users. The manner in which steps (activities) taken in the real-world are reflected as progress on the virtual map is indicated in the next screen. Finally, the fourth tutorial is all about integrating Azion Challenge with third-party applications. We come to that next.
                 </p>
            
            </div>
		</div>
		
        <div class="row">
		  <div class="col-lg-8"><br>
				<h3 class="color">Integration With Third-Party Fitness Apps</h3>
				<p class="color">
					People can fill up the ‘Get Steps’ field manually on Azion Challenge. However, this is not mandatory - and the app makes things a lot easier through its seamless integration with other fitness tracker apps, both on iOS and Android. That way, the required activity fields are filled and updated real-time.
				</p>
				<blockquote>
                    The iOS version of Azion Team Challenge pulls in activity information from Fitbit as well as Apple Health. The Android version, on the other hand, is integrable with Fitbit. Steps can either be tracked on a daily basis, or based on the dates the user selects on the app
				</blockquote>
                <p class="color">
					Apart from steps, users can also track weight (in kgs or pounds) and waist size (in centimeters or inches) on the app. The figures - tracked daily - are visually represented in a graph on the same screen. Comprehensive health and activity information - that’s what users can read off from this app.
				</p>
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/azion1.png" alt="Azion Team Challenge" style="width:70%;">
              </center>
		  </div>
	   </div>
        
        <div class="row">
            <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/azion5.png" alt="Azion Team Challenge" style="width:70%;">
              </center>
		  </div>
		  <div class="col-lg-8"><br>
				<h3 class="color">The Mood Factor</h3>
				<p class="color">
                    The features of Azion Team Challenge are not limited to simply tracking the number of steps taken by users everyday, and other health parameters. On tapping a small ‘+’ tab (from where the ‘Get Steps’ and ‘Weight Tracking’ options can be accessed), the icons for MOOD and SLEEP MOOD become viewable. Users need to rate both on a daily basis.
				</p>
				<blockquote>
                    Physical well-being has a lot to do with mental well-being, and I was happy to find that the owner of the project wanted to focus on this. After due deliberation, we included two scales - Mood and Sleep Mood - in the app. Users have to drag the slider to rate themselves on a 10-point scale on both counts daily
				</blockquote>
                <p class="color">
                    Users can select their favourite activities on the Azion Team Challenge application, adding an additional layer of personalization to it. Activities are arranged alphabetically on a scrolling drop down list - and all that people have to do is tap on their favourite ones. There are no limits on the number of favourite activities that can be chosen (at any time, 8 activities are shown on the screen).
				</p>
		  </div>
		  
	   </div>
        
        <div class="row">
		  <div class="col-lg-12">
				<h3 class="color">Taking Up Challenges. Tracking Teams & Team Members.</h3>
				<p class="color">
					All the challenges on this revolutionary mobile fitness app are related to the number of steps taken by users (e.g., “take 16000 steps in 4 days”). They are generated by the app admin. Each challenge is accompanied with a countdown timer, which gets activated as soon as that particular challenge is accepted.
				</p>
				<blockquote>
                    Azion Team Challenge is not just another of those apps where one user challenges another to do some task. In here, challenges are generated from the backend - and individual users have to accept them, and complete them within the given deadlines
				</blockquote>
                <p class="color">
					A user can easily check his/her position vis-a-vis the other team members directly from the app screen. The names of all the teams participating in a challenge can be seen. To view the profile of any team, a person has to tap on its name. If the activity of another team has to be tracked, the button next to its name has to be toggled to ‘On’.
				</p>
		  </div>
	   </div>
        
        <div class="row">
            <div class="col-lg-6"><br>
              <center>
                  <img src="appstories/azion6.png" alt="Azion Team Challenge">
              </center>
		  </div>
		  <div class="col-lg-6"><br>
				<h3 class="color">The Nutrition Factor</h3>
				<p class="color">
                    From the very outset, it was decided that Azion Team Challenge will be a comprehensive fitness and wellbeing app, for students and adults alike. Nutrition, obviously plays a big part in overall health and hygiene - and tracking it was an absolute must in this application.
				</p>
				<blockquote>
                    On the app, users can specify how healthy or otherwise their meals are...or if they have skipped a meal. The nutrition history of individual users are also graphically represented, making the daily monitoring easy. With proper nutrition, it becomes simpler for people to reach their fitness and activity targets
				</blockquote>
                <p class="color">
                    On the virtual map in the application, important health and nutrition facts and tips are shared with users, at regular intervals.
				</p>
		  </div>
		  
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="box">
                  <p class="color">
                      To add to the fun activity training that forms the very essence of Azion Team Challenge, the app also has an exciting ‘Quiz’ feature. The challenges keep changing - for students, teachers and parents - keeping things engaging throughout. We paid particular attention towards optimizing the user-end experience (UX) of the app. Azion Team Challenge is driven by a noble concept, and we did not want any design glitch to ruin it.
					</p>
                  <p class="color">
                      It offers users - particularly kids - a fun way to stay fitter, and it is rapidly scaling up the popularity charts.	
					</p>		
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>