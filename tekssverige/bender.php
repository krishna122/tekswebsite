<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Bender</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="bender_story" style="height: 50%;">
        <div class="bender_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Bender</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>Every big city has several nice public venues. The problem is, not everyone are aware of their existence (particularly true for those who have newly relocated to a city). Spending hours in front of the computer often does not help in getting reliable, updated information about venues either. We had worked on a fair few mobile location-based apps before 2015 - but none of them focused on local venues and public places per se. And then, Bender changed things around.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/b1.png" alt="Bender">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
We had a rough idea of making an iOS application that would provide holistic, and more importantly, accurate information about public venues in...well, as many cities around the world as possible. While browsing for a suitable mobile app company which could work on this idea, we chanced upon the name of Teknowledge Software. We took a look at the many different genres of apps in their portfolio, and felt that they should be able to justice to our app idea.
                                <br><br>
                                <center>
				<span style="font-size: 25px;">Owner, Concept Developer, Bender App</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>The Project Takes Off</h4>
	      <P>It was like the proverbial case of like-minded people finding each other. Our iPhone app developers were looking forward to work on a location/navigation app with a difference - and when the basic idea of Bender was submitted to us, we found just the thing we were pining for. It was a new challenge, something that we were more than willing to take on. The free app quote was sent along on the same day, and the preliminary stages of development kicked off from the week after.</p>	
	   </div>
	</div>
     </div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <h4>Being A Part Of The Bender Community</h4>
	  		<div class="col-lg-6">
	      <P>A standout feature about this mobile venue locator app is that it makes the task of searching for public venues a type of a social process. To make sure that as many people as possible could use the app, we included a Facebook login option (along with, of course, the regular registering with email/password option). The initial wireframes that we were given served as handy reference points as we got down to framing the core features of the app later.</p>
	     </div>
	     <div class="col-lg-6">
               <img src="appstories/b2.png" alt="Bender">
             </div> 	
	   </div>
	</div>
     </div>	
</section>



<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
The concept of Bender was something new, and something that I instantly felt had the potential of becoming big. After all, there are not too many apps around that provide extensive information about venues around the world. With this app, we were plugging this loophole, making something unique.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>That Thing About Location-Sharing</h4>
             <div class="col-lg-6">
	      <P>From the very outset (yes, even before we had discussed about the cities to be included in Bender), there was a slight dilemma regarding whether the physical location of users should be shared on this iPhone venue finder app. After a couple of consultations with the client, it was decided that the final users would get to decide this.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/b7.png" alt="Bender">
             </div>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I wanted Bender to provide information on venues and location in a systematic, user-friendly manner. Randomly plugging in too many cities and places would have only confused people more, instead of helping them. It's a good thing that the developers at Teknowledge grasped the idea pretty quickly, and the app turned out just as I had envisaged it to be from the start.
                                <br><br>
                                <center>
				<span style="font-size: 25px;">Owner, Concept Developer, Bender App</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>Viewing Venues, Getting Information</h4>
              <p>Searchability was one of the first features included in the Bender application. Users were given the option to look up different cities (a totally great option if someone is planning to hire or visit a venue at a foreign city). The search results include detailed map of the city for which the query had been made. All the leading venues at that place would be listed on the map - all that users had to do was tap to get the data they needed.</p>
              <br>
	         <img src="appstories/b5.png">
           
	   </div>
	</div>
     </div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <h4>Everything About Every Venue</h4>
	      <div class="col-lg-6">
	          <P>That's what we wanted Bender to provide - and let's just say we have been successful in achieving this target. A full week was dedicated by our iOS app development team to find out the types of information that people generally wanted to know about venues, if they were looking to visit/hire one. At the end of it all, we decided to include even things like how many people are actually present at any time at a particular venue, and the distance between a user and the venue (s)he wants to know more about. More basic information, like postal addresses and directions, and opening/closing timings were, of course, made available as well.</p>	
	      </div>
	      <div class="col-lg-6">
               <img src="appstories/b4.png" alt="Bender">
             </div>
	    </div>
	</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
A good app is one that does not provide vague, half-baked information. Bender would get high marks on that regard - simply because it provide all types of pertinent information about venues - and the venue database itself is so large. I personally believe that a person using this application will never have any doubts about the details of the venue(s) he or she is interested in.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <P>Our UI/UX designing team took special care to ensure that the layouts and overall in-app navigation was smooth and user-friendly. The last thing we wanted was for people starting to look for venues...and getting lost in the maze within the app!</p>	
	    </div>
	</div>
     </div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>And Now, For The Other Half Of The Story</h4>
             <div class="col-lg-6">
	      <P>The Bender app has two main tabs - 'Venues' and 'Events'. Just as we had done with the former, all types of important information about the biggest events in any city were included under the 'Events' tab. In addition to the date(s) and timing of events, users can also check out stuff like the attendee count at any event. Putting it in another way, with Bender it is possible to find out how 'popular' an event is, before one decides to join it.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/b3.png" alt="Bender">
             </div>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
What comes first to the mind when you hear the word 'venues'? That's right - 'events'. I had conceptualized Bender as an application that provided information on both of these. Yes, venues were the starting point - but as soon as details about venue-inclusion were sorted out, the focus shifted on how we could provide information on social events via the app.
                                <br><br>
                                <center>
				<span style="font-size: 25px;">Owner, Concept Developer, Bender App</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>Participating In Events With Bender</h4>
	  		<div class="col-lg-4">
               <img src="appstories/b6.png" alt="Bender">
             </div>	
             <div class="col-lg-8">
	      	<P>To refer to the process of joining events via Bender as 'easy' would be an understatement. All that willing attendees have to do is send along join requests to the host/manager of the event (as listed in the details of the concerned event). As soon as the host views and approves your request, your name will be included in the audience list. We included a separate section called 'My Past Events' as well - where users could view all the events they have previously attended, all in one screen. Yes, that easy!</p>
          </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Ever experienced the frustrations of wanting to attend a big event and being clueless about how to get tickets for it? I have, and I am sure many others have faced the same problem as well. I was delighted to find out that the concept developers of Bender had planned to provide the provision for joining events directly through the app. It was a classic case of the client and my app development team being on the same page.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
  <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>There's More About The App</h4>
	  	<P>Bender is a seamless mobile venue finder and social events app, and it packs in a great array of additional features. A good example of this would be the real-time chat option provided to users, directly from the inbox. There is also a provision of inviting Facebook friends to try out the app. The benefits of features like these are two-fold - they bolster the usability of the Bender application, and blends in social networking with the task of searching for events/venues.</p>
	   </div>
		</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Hussain and his team brought a lot to the table when it came to deciding the accessory features of Bender. While I had the core idea that it should be an app for finding public venues and looking for events - the additional features definitely take up the advantages and the fun-quotient of using the app quite a bit.
                                <br><br>
                                <center>
				<span style="font-size: 25px;">Owner, Concept Developer, Bender App</span>
				</center>
                </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
  <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>Something For The Hosts</h4>
	  	<P>Under the 'Host A Bender' tab, users could, firstly, check out their previous and upcoming events. For venue owners, the same screen shows up the list of public venues they own. New venues can be added with ease, events can be promoted, and join requests sent by interested attendees can be approved/declined from this section. The app makes management of events and venues easier than ever before.</p>
	   </div>
		</div>
     </div>	
</section>

<section style="background:#999123;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
With Bender, we have tried to help general people looking for venues to visits and events to attend, along with the hosts and managers of the same. After all, if people could not send out the word about the events they were hosting or venues they owned, there would be no real benefit for anyone. The app gives hosts the opportunity to publicize their events or venues...and that too, to a potentially large audience.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
  <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	<P>Bender is a one-of-its-kind application (just as we had thought from the very first), and it was fun working on it. We particularly enjoyed the extensive research required to include updated venue and event details in the app. This is one app which is backed up by a lot of thought, precision, and user-orientation - and we do believe it will emerge as one of our most successful applications. Ever.</p>
	   </div>
		</div>
     </div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>