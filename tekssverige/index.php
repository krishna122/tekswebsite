    <!DOCTYPE html>
        <html lang=en>

        <head>
            <meta charset=utf-8>
            <meta http-equiv=X-UA-Compatible content="IE=edge">
            <meta name=viewport content="width=device-width, initial-scale=1">
            <title>
                Mobile App Development | APIs | IoT | Teks Mobile Australia
            </title>
            <meta name=description content='We specialize in mobile app development services, API development and management, and IoT. Our developers work on iOS and Android apps.' />
            <?php include 'head.php';?>
                <style>
                    .letterpress {
                        text-shadow: 0 1px 1px #4d4d4d;
                        color: #fff;
                        font-size: 28px;
                        padding: 10px;
                    }
                </style>
                <link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel=stylesheet type=text/css>
        </head>

        <body data-spy="scroll" ondragstart="return false">
            <?php include 'headerindex.php';?>
                <header class="intro fullscreen-bg">
                    <video loop muted autoplay poster="video/intro-video.jpg" class=fullscreen-bg__video>
                        <source src=video/teks_aus.mp4 type=video/mp4></source>
                    </video>
                    <div class=intro-body style=display:initial;padding-left:0;padding-right:0>
                        <div class=container style="padding-top:5%;">
                            <div class=row>
                                <div class=col-md-12 style="vertical-align: middle;">
                                    <a href=./><img style="outline:none;" src=img/logo.png alt="Teksmobile India"></a>
                                    <br>
                                    <h1 class=pageheader style="font-weight:700;text-align:center;">GET THE BEST <br> MOBILE APP, API, WEB & IOT SOLUTIONS</h1>
                                    <p class=letterpress>Work with us and transform your ideas into high-quality mobile and web applications. We also provide cutting-edge API and IoT services</p>
                                    <a href=#cbp-so-scroller class="page-scroll col-md-12" style=text-align:center;margin-top:0px;outline:0>
                                        <img src=img/scrollbutton.png>
                                        <!-- <i class="fa fa-angle-down" aria-hidden="true" style="font-size:100px;"></i> -->
                                    </a>
                                </div>
                            </div>
                        </div>
                </header>
                <?php include "favourite.php";?>
                    <?php include "map.php";?>
                        <?php include "footer.php";?>
                            <?php include 'script.php';?>
        </body>

        </html>