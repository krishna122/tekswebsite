<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Appbattles</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="appbattles_story" style="height: 50%;">
        <div class="appbattles_story-body">
            <div class="container" style="margin-top: 12%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">App Battles</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">

	  			<div class="col-lg-6">
	  			<blockquote style="line-height: 1;margin-bottom: -31px;font-style:italic;">Come on then...let’s meet face to face, and fight it out!</blockquote>
				<p>Nopes, this is not a good old battle-cry! It is, well, the core essence of one of the best mobile multiplayer games we have worked on - App Battles. In this real-time iOS fighting game, players can choose their favourite characters - select opponents, and engage in a fight to the finish. A virtual ‘finish’, of course!</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab1.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I could almost tell that the mobile game developers in my team were itching to take on a project that had something new about it, a project that presented fresh, exciting challenges. And then, the App Battles project came to us - and the next few weeks were pure creative fun.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<h4>What Exactly Is App Battles?</h4>
				<p>Including real-time multiplayer features in App Battles was one of the two biggest challenges while we were working on the project. The other, you ask? Adding enough creative touches to it - so that it did not seem anything like the hundreds to shooting and fighting games that are already present at the App Store.</p>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<h4>Platform Compatibility</h4>
	  			<div class="col-lg-6">
				<img src="appstories/ab2.png" alt="App Battles">
		  		</div>
	  			<div class="col-lg-6">
				<p>As with most of the apps developed by our company (at least the ones that are not too niche), we wanted to make App Battles available to a wide range of smartphone users around the world. After due deliberation, it was decided that the game should debut on the iOS platform. Our in-house app developers made sure that App Battles was completely compatible with the iOS 9 platform.</p>
		  		</div>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
For a new mobile game, selecting the right platform is tricky business. My creative personnel and software analysts did a lot of research - before finalizing Apple iOS as the best platform for the launch of App Battles. Given the fast start that iPhone 6S is already off to, it was a given that the app had to be optimized for iOS 9.
                <br><br>
                <center>
					<span style="font-size: 25px;">Project Owner, App Battles</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
				<p>We created App Battles with the open-source Cocos2d-x game development engine. At our mobile app company, we have iOS developers with years of experience of working with the engine - so the actual coding was a bit of a breeze.</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab3.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>


<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
It is just plain sad when a perfectly good, innovative app idea goes to waste - simply because there is no one to actually transform it into a workable piece of software. Thank goodness that did not happen to App Battles, and we were fortunate enough to start working on it. If the concept behind this lovely game had been lost, that would have been a shame.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<h4>Moving On To The Creatives</h4>
				<p>Once the basic framework for App Battles was ready, the next task was to make it...very, very colourful, interactive and personalized (after all, isn’t that what any good mobile multiplayer game should be?). A full fortnight passed, before our iPhone game development experts reached an agreement with the owner of the project regarding the name, number, appearance and powers of the characters in the app. The results, thankfully, indeed look nice!</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I am an enthusiastic gamer myself - and I will say this...even the most promising games seem slightly boring after some time. I am fairly confident that this won’t be the case with App Battles though. There are enough fun features in it to keep users...hopefully of all ages...occupied for any amount of time.
                <center>
					<span style="font-size: 25px;">Project Owner, App Battles</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<p>Since App Battles is an online real-time multiplayer game, there were some initial concerns regarding the effect it might have on the battery life of a user’s iPhone. There were several rounds of app testing done - until we were absolutely sure that the app did not cause any battery drain or overheating (two common problems with many otherwise nice games). After all, people will play App Battles for fun - they should not have to worry about anything else.</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Sometime back, our mobile game development department had conducted a survey. It revealed that most users preferred to choose their own avatars, while playing an action-based mobile game. We implemented this idea in App Battles. Players can take their pick from a fairly large series of characters...and well, fight their fights!
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
                <h4>Meet The Characters</h4>
	  			<div class="col-lg-6">
				<p>AngryBird, Ninja, Bear and Doodle are four of the most popular avatars that the users of App Battles can choose. Each of the characters have the power to jump, kick and punch...and most importantly, all of them are a lot of fun!</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab4.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I had a feel that giving the characters only run-off-the-mill powers would not make the game exciting enough. The game developers at Teks floated the idea of providing each character with a single unique power. That, I felt, made the game complete.
				<center>
					<span style="font-size: 25px;">Project Owner, App Battles</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
				<p>After rounds of brainstorming and Skype discussions, the nature of the unique powers was decided. A player playing as AngryBird can spill bombs, Ninja fighters have (you guessed it!) special swords, and Doodles could fire bullets at a frighteningly rapid clip. And what about the Baer avatar? Well, it can bite...and bite hard!</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab5.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
                <h4>Let The Fight Begin</h4>
				<p>Next up was the question of how the characters would go to battle against each other. Our team of game developers decided to give players the option to invite opponents online - and once an invitation is accepted, move with his/her avatar to the ‘playground’ (or shall we call it ‘battleground’?). The client came up with the idea of creating several ground levels on the battle screen, across which the characters could jump - an idea that our UI/UX designers implemented with ease.</p
		</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Moving the characters smartly, and clever usage of the available weapons is key to winning a fight in App Battles. From the very start, I had a rough idea of what the actual playground would look like - with plenty of room for the players to maneuver. Hussain and his team did a great job of working on this vision of mine. Two thumbs up to Teks!
				<center>
					<span style="font-size: 25px;">Project Owner, App Battles</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
				<p>The fights in the App Battles game are well and truly skirmishes to the finish. A player ‘wins’ a battle when his/her opponent’s character either falls off the playground, or gets killed. We also included ‘Bonus Lifetimes’ within the game play. The player who manages to grab it first receives a boost of extra health. Mighty handy during an intense fight!</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab6.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section style="background: #b3c444;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Initially, we were in a dilemma about whether a game like App Battles needed any in-app purchase feature. We finally decided to include it - for the simple reason that gamers are likely to be interested in selecting from a wider range of characters than what is available to them by default.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
                <h4>What Are The In-App Purchases About?</h4>
	  			<div class="col-lg-6">
				<p>Via in-app purchases, users of the App Battles multiplayer iPhone game can do two things. Firstly, they can get rid of in-app advertisements - often a source of irritation for many people. Secondly, new and more interesting characters/avatars can be unlocked - by paying nominal amounts. A chance to move beyond the default settings - that’s what the in-app purchases in App Battles are all about.</p>
		  		</div>

		  		<div class="col-lg-6">
				<img src="appstories/ab7.png" alt="App Battles">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<p>Via in-app purchases, users of the App Battles multiplayer iPhone game can do two things. Firstly, they can get rid of in-app advertisements - often a source of irritation for many people. Secondly, new and more interesting characters/avatars can be unlocked - by paying nominal amounts. A chance to move beyond the default settings - that’s what the in-app purchases in App Battles are all about.</p>
		  		<p>App Battles is an iOS game that we created with a lot of enthusiasm, detailing...and yes, love and devotion. It will arrive at the App Store soon - and we are sure that lovers of gaming apps will take to it in a big way.</p>
		  	</div>
		</div>
	</div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>
