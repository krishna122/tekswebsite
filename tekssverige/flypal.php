<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Flypal</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
		<!-- Intro Header -->
<header class="flypal_story" style="height: 34%;">
        <div class="flypal_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Flypal</span></h1>
                        <center>
                        <a href="https://itunes.apple.com/us/app/flypal-crs/id1124048406?ls=1&mt=8" target="_blank"><img alt="" src="img/appstore.png"></a> 
                        <a href="https://play.google.com/store/apps/details?id=com.flypal.crs" target="_blank"><img alt="" src="img/play-store.png"></a></center> 
                    </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Just as there are some people who manage to make their mark as ‘first among equals’, certain mobile apps are ‘more unique than others’. FlyPal CRS is a classic example of such applications. It is a custom aviation management app - and it is certainly different from almost all the other apps that we have worked on to date. We hope that such winning app ideas keep coming to us, and we get the chance of transforming them into fully functional mobile software.
	         </p>
	         
	          <blockquote class="color blockFlypal">
		  	    	I feel that the global aviation industry can benefit a lot with proper use of mobile technology. In particular, apps can go a long way in facilitating real-time communication among employees of different designations. Just when we were wondering how to create such an app...FlyPal CRS happened!
		  	   </blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
<!-- 		  <div class="col-lg-12"> -->
		  	
		  	 <div class="col-lg-12">
		  		<h3 class="color">FlyPal In A Nutshell</h3>
		  	    <br>
		  	    
		  	    <p class="color">
		          FlyPal is an app meant for ‘Administrators’ (Admin), ‘Operators’, and ‘Crew’ - in decreasing order of control. The application aims at seamless, real-time information access and sharing across these three levels in the flight industry.
		         </p>
		         
		  	    <blockquote class="color blockFlypal">
		  	    	It is one thing to conceptualize an app...and completely another to find an app developer company that can actually give shape to it. It’s a good thing that I came upon the name of Teks Mobile pretty soon - these guys turned out to be absolute experts!
		  	    </blockquote>
		  	    
		         <p class="color">
		           The FlyPal app will be available on both the iOS (iPhone and iPad) and Android platforms. Our in-house mobile app designers have ensured that the layout of the app is perfectly customized for different screen sizes of devices. The app has a professional feel about it, just as any aviation-related mobile application should.
		         </p>
		  	 </div>
		  	 
<!-- 		  	 <div class="col-lg-6 hidden-xs"> -->
<!--		  	 	<video autoplay loop muted  poster="2016-04-16.png" style="border: 5px solid #D07A67;"> -->
<!-- 				   <source src="video/appstories/currently.mp4" type="video/mp4">  -->
<!-- 				</video> -->
<!-- 		  	 </div>    -->
		  	 
<!-- 		  </div> -->
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Hierarchy Of Information In FlyPal</h3>
		  	    
		         <p class="color">
		         	Users can log in as ‘Admin’, ‘Operator’ or ‘Crew’ on the FlyPal CRS application. An ‘Admin’ can view information of all ‘operators’ and ‘crews’, an ‘Operator’ can check out the status and data related to ‘crews’, while a ‘Crew’ can only access information pertinent to him/herself.
		         </p>

                 <blockquote class="color blockFlypal">
		  	    	Not everyone needs all the available information. The key lies in providing people in the aviation industry the data that they actually need. In FlyPal, we have gone with a three-tier information control system, with ‘Admin’ right at the top and ‘Crew’ at the bottom - with ‘Operators’ slotted in between.
		  	    </blockquote>
		  	    
		  	    <p class="color">
                 	Users logged in as ‘Admin’ and ‘Operator’ can filter the available data on ‘operators’ and ‘crews’. A ‘Crew’, as already explained, can only see and filter his/her own information. 
                 </p>
                 
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/flypal10.png" alt="currently" style="width:60%;"></center>
		  	</div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-4">
		  	 	<center><img src="appstories/flypal3.png" alt="currently" style="width:100%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-8"><br>
		  		<h3 class="color">Nature Of Crew Information Viewable On The App</h3>
		  	    <br>
		  	    
		  	    <blockquote class="color blockFlypal">
		  	    	Inordinate, and perfectly avoidable, delays crop up when senior aviation staff do not have precise information on the availability of flight crews at any time. FlyPal promises to be a really good tool to remove such hassles and bottlenecks. With it, officials can track crew status in a matter of seconds.
		  	    </blockquote>
		  	    <p class="color">
		          In keeping with the nature of the app and the requirements of target users, the nature of crew information that FlyPal CRS provides is extensive. Right from the names of individual crew members, to the duties assigned to them and their job timings - everything can be viewed by users logged in as ‘Admin’ or ‘Operator’ on this innovative iOS/Android flight management app.
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">Documentation Storage & Access</h3>
		  		<br>
		  		
		  		<p class="color">
		          Important documents related to the aviation industry or to particular crew members can be viewed directly on the FlyPal CRS application. There are two alternative modes through which users can interact with these documents - ‘Alert’ and ‘Entry’.
		         </p>
		         
		         <blockquote class="color blockFlypal">
		  	    	FlyPal would have been incomplete without the its ‘Crew Training & Document’ section. There are so many documents that people from the aviation industry need to access...and access quickly - and that’s precisely what this app allows them to do. In addition to viewing docs, users can edit them too.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          By tapping on ‘Alert’, users can view the documents stored in the app. If any information needs to be edited or deleted, that can be done through the ‘Entry’ tab. The high-end app security features of FlyPal CRS makes sure that the control of the documents always remains with authorized personnel.
		         </p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Visual Representation Of Information</h3>
		  	    
		  	    
		         <p class="color">
		         	In addition to detailed reports on crew status and availability, informative ‘Graph Report’-s are also viewable on FlyPal. For instance, senior-level officers can check out the ‘flight time’ of individual pilots - and manage their tasks accordingly. Stats and reports of people involved in the aviation industry can also be filtered according to requirements.
		         </p>
                 
                 <blockquote class="color blockFlypal">
		  	    	I’d say that the best thing about the Graph Reports section in FlyPal CRS is its quick data filtering property. For example, while checking the flight time of pilots, users can toggle between different time periods - 7 days, 30 days, 365 days, etc. Filtering can be done by designation as well.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		         	The graphs, reports and other information available on the FlyPal app score big on the accuracy front. For efficient aviation activity management, people involved in the industry need to have access to updated, and completely error-free data. Availability of such authenticated reports (with additional filtering options) bolsters the overall utility of the application by several notches.
		         </p>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/flypal1.png" alt="currently"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/flypal7.png" alt="currently"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/flypal2.png" alt="currently"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/flypal4.png" alt="currently"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/flypal8.png" alt="currently"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/flypal9.png" alt="currently"></div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">Interacting With Crew Members</h3>
					<p class="color">
						The section is displayed as ‘Notify Crew’ in the FlyPal CRS mobile app. Crew members can be called directly from within the app, while email messages can be sent too. The recipients are notified as and when such calls/emails come in - ensuring that the entire process of information transfer is not delayed under any circumstance. 
					</p>
					
					<blockquote class="color blockFlypal">
		  	    		A point of debate while FlyPal was being made was how it could help senior officials get in touch with crew members, for passing on important information. Hussain and I had several consultation sessions, following which the idea of having a separate section for this was finalized.
		  	        </blockquote>
					
					<p class="color">
						Via the ‘Notify Crew’ section, ‘Admin’-s and ‘Operator’-s can quickly assign tasks to individual crew members as well.<br>
						Note: Full crew profiles can be viewed on the ‘Crew Profile & FDTL’ screen.					
					</p>		
				</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-5">
		  	 		<center><img src="appstories/flypal6.png" alt="currently" style="width:85%;"></center>
		  	    </div>	
		  	<div class="col-lg-7"><br>
				  <h3 class="color">Information on FDT/FDTL Status</h3>
				  <p class="color">
					FDTL, or Flight and Duty Time Regulations, is one of the most important standards in the field of aviation. In our new iPhone flight management app, accurate FDTL status can be viewed and tracked by users. That, in turn, rules out any chance of confusions and resultant legal issues. 
				  </p>
					
			     <blockquote class="color blockFlypal">
		  	    	FlyPal is not limited to being a communication and information-exchange app for the aviation industry. From the very outset, the concept developer had been interested to include information on the legal standards as well - something that I really approved of. We finally decided to include FDTL status information in the app.
		  	    </blockquote>
		  	    
		  	    <p class="color">
					FlyPal CRS is a one-of-its-kind mobile application, created exclusively for employees of the aviation industry. It has one primary aim - to improve the day-to-day workflow by facilitating better, quicker, smarter communication between workers at different levels. Developing this app was a challenge for us - and let’s just say our teams of iOS and Android app developers enjoyed the challenge at every point. 
				  </p>
				  
				  
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
					 Given its sheer range of useful features and rich information database, it has every potential to be a big success. This is one app that is destined to...fly! 
				    </p>
				</div>
		  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>