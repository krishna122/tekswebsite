<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Apps made for Bluetooth by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
    
    
<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Bluetooth Apps</span></h1>
	                    <p>Take a look at some of our advanced Bluetooth-powered mobile apps, created to serve a vast array of functions</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
<section id="appstory" class="dataworks">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">DataWorks</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Bluetooth Connectivity App</span>
				  	<br><br>	
				  	<p style="padding: 0px;" class="appcolor">DataWorks taps into the power of Bluetooth technology to the fullest. With this powerful app, devices can be connected with other compatible gadgets, and data can be measured, calibrated, maintained and managed.</p>
				  <br><br>
				  <a href="dataworks.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dataworkslogo.jpg" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</div> 
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>