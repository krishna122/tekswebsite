<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Productivity Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>

<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Productivity Apps</span></h1>
	                    <p style="font-size: 25px;">We create apps that bolster productivity levels, both in the personal and the professional domain. Here are some apps that fall in this category</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
<section id="appstory" class="speedykey">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">SpeedyKey</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Virtual Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">A third-party iOS keyboard app par excellence. Available in three different languages and four different themes (via in-app purchase), SpeedyKey makes typing on iPhones/iPads quicker, easier and more accurate. Its USP? The host of custom 'Speedy Replies'!</p>
				  <br><br>

				  <a href="speedykey.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/speedykey.png" align="center">
				</div>

		  </div>
		</div>
        <hr>
	</div>
</section>

<section id="appstory" class="veeaie">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Veeaie Keyboard</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">The key USP of Veeaie Keyboard is its host of pre-written custom replies, for emails and messages. With this third-party iOS keyboard app, staying in touch and responding to messages becomes quicker than ever.</p>
				  <br><br>
				  <a href="veeaie.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/veeaie.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>