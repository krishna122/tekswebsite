<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Dataworks</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="dataworks_story" style="height: 50%;">
        <div class="dataworks_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Dataworks</span></h1>
                    </div>
                 </div>
            </div>
        </div>
</header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>We are proud of the sheer range and variety of apps that are present in our portfolio. From games and navigation apps, to chat applications and mobile cloud storage storage software - we have worked on practically all types of projects. However, creating a purely connectivity-based app was not something we had done before. When the DataWorks project came along, we knew this was a golden opportunity to add a new genre to our app portfolio. It was a fresh challenge, and we were up for it!</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/d1.png" alt="Dataworks">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
We could not afford to take a chance with the development of the DataWorks app. Hiring just any app developer was out of the question...and after about a week’s search, we found the name of Teknowledge Software. Their portfolio looked impressive, we had a chat with the CEO Mr.Hussain, and decided that they would be able to create the Android version of DataWorks in the best possible manner.
                                <br><br>
                                <center>
				<br> 
				 
				<span style="font-size: 25px;">Head, Instrument Works Pty Ltd</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Getting Connected</h4>
          <div class="col-lg-5">
               <img src="appstories/d2.png" alt="Dataworks">
             </div>
           
           <div class="col-lg-7">
               <P>No, we have not started discussing the Bluetooth-connectivity features (that’s the heart and soul of Dataworks) of the app. While browsing through suitable mobile app development companies for handling the project, the personnel at Instrument Works came across the name of our agency. Fair play to them - they were prompt in contacting us and asking for a free app quote. We provided the quote within 24 hours, and everything was finalized by the next week.</p>
             </div>  	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
During my first consultation with the team of Instrument Works, I realized how much of research these guys have put in for creating the idea behind DataWorks. I had always known that a lot of things could be done with a strong, reliable Bluetooth connection - but the way in which the app brought together so many functionalities was a surprise, even for me.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">So, The App Is About</h4>
             <div class="col-lg-6">
	      <P>In a nutshell, DataWorks is an Android app that helps users connect their devices with other compatible, low-energy Bluetooth gadgets. The purpose of this is the measurement and maintenance of a wide range of parameters - like temperature or the pH level of water. Simultaneous recording and managing data from multiple third-party Bluetooth devices is another key feature of this rather unique and interesting mobile application.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/d3.png" alt="Dataworks">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
DataWorks is a high-end Bluetooth-connection-based app, and it would have been a folly to try to optimize it for low-end devices. Just like the iOS version of the app, both Hussain and I agreed that it would be compatible with only the devices that have Bluetooth 4.0 or above.
                                <br><br>
                <center>
				<br> 
				 
				<span style="font-size: 25px;">Head, Instrument Works Pty Ltd</span>
				</center>
               </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">Compatibility Requirements Of DataWorks</h4>
	  		<div class="col-lg-6">
               <img src="appstories/d4.png" alt="Dataworks">
             </div>
             <div class="col-lg-6">
	      <P>On the third-party compatibility front, DataWorks really aces it. It can be used in collaboration with devices like Health Thermometers, SensorTags, BlueBox pH-units, and several other sophisticated gadgets. Users get an unified app-using experience while working with DataWorks, irrespective of the type/model of Android handset they have installed it on.
	      <br><br>
	      With Apple Watch finding acceptance among users across the world, it was only natural that we created an optimized watchOS version of the DataWorks application as well. Our in-house WatchKit app developers worked in tandem - ensuring that all versions of the app finished within the pre-specified deadline. The app, when released, will be an interesting addition to our portfolio of embedded applications.
	      </p>
          </div>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I can say this with confidence...very few apps at the store are as user-oriented as the DataWorks Bluetooth app. From the very start, we had decided to create a dynamic UI for the application - which would make the usage of, and indeed, getting used to the app, an absolute breeze.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">Focus On User-Requirements</h4>
             <div class="col-lg-6">
	      <P>During the wireframing stage for the app, we organized multiple discussion sessions with the senior representatives of Instrument Works. One of the main agenda at this point was determining how the probes will be detected by the app, prior to taking measurements and managing/monitoring the same. Finally, we zeroed in upon the idea of automatic detection of probes. What’s more, the dynamic user-interface also allowed generation of real-time graphs and charts in landscape mode. We felt, and the concept developers agreed, that if the interface of the app has automatic orientation features, it will be just that bit more user-friendly.</p>
             </div>	
             <div class="col-lg-6">
               <img src="appstories/d5.png" alt="Dataworks">
             </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
General users are often unaware of how powerful a Bluetooth-based app can be. Even before I had started thinking about the finer details of the app, I was certain about one thing - the app had to perform functions that are not, or at least not satisfactorily, performed by other similar applications. Tapping into the powers of Bluetooth technology - that’s what both the iOS and Android versions of the app are both about.
                                <br><br>
                                <center>
				<br> 
				 
				<span style="font-size: 25px;">Head, Instrument Works Pty Ltd</span>
				</center>
              </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>What Does The DataWorks App Do?</h4>
            <div class="col-lg-7">
	      <P>A lot - there is no other way of putting it! To get into specifics, some of the capabilities of this Android Bluetooth application are recording and storing data from connected devices, scan data quickly, and display data from compatible devices on a real-time basis. For efficient management and maintenance of the collected data, separate datasets can be created. Users also have the option of specifying the time intervals at which data will be recorded by the app.
			<br><br>
			One-touch calibration is yet another high point about the DataWorks application. For calibrating any probe, all that users have to do is tap the ‘Start’ tab once. The entire calibration tutorial will be displayed - and users are not required to learn any further controls for that either.	      	
	      	</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/d6.png" alt="Dataworks">
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
                Apart from the calibration mechanism, another thing that’s very intriguing about DataWorks is the way the app itself keeps track of the status of any calibration process. Notifications are generated as and when probes near the end of their lives. The app presents zero hassles for users - just as any good mobile application should do.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Cloud Support</h4>
            <div class="col-lg-6">
                <img src="appstories/d7.png" alt="Dataworks">
              </div>
            <div class="col-lg-6">
	      <P>We were in agreement with the vision of those at Information Works - DataWorks needed to be equally beneficial for those doing off-site, on-field work. For this, we implemented seamless cloud storage features in the app. Since all the calibration and other data can be quickly uploaded and stored in the cloud, users need not bother about manual data exporting. The cloud storage, of course, happens real-time.</p>
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
It’s just plain bad when someone takes the trouble of collecting data from different sources, and everything gets corrupted while manually exporting it. This risk can be effectively tackled when a user registers for the Information Cloud. Data will be stored real-time on the cloud, and will remain accessible as and when required.
                                <br><br>
                                <center>
                <br>                	
				 
				<span style="font-size: 25px;">Head, Instrument Works Pty Ltd</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	<h4>Standards Maintained During The Development Process</h4>
            <div class="col-lg-6">
	      <P>The DataWorks project complies with the ‘Good Laboratory Practice’ standard. This, in turn, speaks volumes about the authenticity, accuracy, and timeliness of the data/calibration information collected by this app. All the collected data can be tagged according to the location where they were collected (geo-tagging ensures that locations are automatically detected by the app). Time-stamps can be added to the data as well. </p>
              </div>
            <div class="col-lg-6">
                <img src="appstories/d8.png" alt="Dataworks">
              </div>   
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
Before work started on DataWorks, our Android app developers were briefed about the importance of maintaining very high quality standards at every stage of making this application. The app also satisfies important international quality standards. I am quietly confident that this app will turn out to be a winner. A big one.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	<h4>And Wait, There’s More</h4>
            <p>In order to enhance the convenience factor of users while collecting data, both visual and audio alarms can be set in the app. In addition, notes and labels can be attached with the data, for ready reference at a later point in time. DataWorks has been made in a way so that users never feel confused while using it.</p>
	      
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
In an app like DataWorks, it is important that users are able to customize the collected data as much as possible. I had created an outline of the data-management features that should be present in the Android version of the application. Hussain and his team of developers made sure that all of them were properly implemented.
                        <br><br>
                        <center>
			<br> 
			 
			<span style="font-size: 25px;">Head, Instrument Works Pty Ltd</span>
			</center>
                            </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>