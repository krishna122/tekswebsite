<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Onebrand</title>

	<?php include 'head.php';?>
    <link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">
    
<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="onebrand_story" style="height: 34%;">
        <div class="onebrand_story-body">
            <div class="container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1>
	                        <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
	                        <br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Onebrand</span>
                        </h1>
                        <center><a style="cursor: pointer" data-toggle="modal" data-target="#onebrand"><img alt="" src="img/btn_beta.png"></a></center>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
  <section class="offwhite-background">
    <div class="container">
	   <div class="row">
	       <div class="col-lg-12">
	          <blockquote class="color blockonebrand">
		  	    	Wow! I could use this app for some shopping!
		  	   </blockquote>
	       </div>
	    </div>
     </div>
  </section>
  
  <section class="offwhite-background">
    <div class="container">
	   <div class="row">
	       <div class="col-lg-12">
	          <p class="color">
		  	     That was the reaction of one of our junior iOS developers, when the initial briefing for the One Brands application was being given. It’s not that there were no mobile shopping apps present at the store already - but this one packed in so many excellent features that it really stood out from the crowd. Kudos to the team which thought up the idea of One Brands!
		  	   </p>
	       </div>
	    </div>
     </div>
  </section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
 		  <div class="col-lg-12"> 
 		  
 		  	<div class="col-lg-5 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/onebrand.jpg" style="border: 5px solid #2CADEB;">
				   <source src="video/appstories/onebrand.mp4" type="video/mp4"> 
				</video>
		  	 </div>
		  	
		  	 <div class="col-lg-7">
		  		<h3 class="color">When We Took Up The One Brands Project</h3>
		  	    
				<p class="color">
		          Creating a mobile commerce app presents an entirely different set of challenges for mobile app developers. From arranging the showcased products and updating product catalogs real-time, to facilitating payments and maintaining security standards - there are a lot of things that need to be taken care of. We were happy when the project was delegated to Team Teks - and we made a resolution right at the time: <strong>this app had to excel</strong>.
		         </p>
		         
		  	    <blockquote class="color blockonebrand">
		  	    	I had visualized One Brands as an app that included all the aspects of mobile shopping. There was no question of compromising on any feature or qualitative aspects of the application - and that’s why I spent quite a bit of time hunting for a good mobile app agency. Finally, I selected this company called Teknowledge to do the job.
		  	    </blockquote>
		  	    
		  	 </div>
		  	 
		  	 <div class="col-lg-12"> 
		  	 	<p class="color">
		           We send along free app quotes to clients within a maximum of 24 hours of them contacting us with their app idea. We made an exception for this project though. The idea was so intriguing that we got in touch with the brains behind the One Brands app inside a couple of hours, with quotes and the project plan. Things fell in place quickly enough, and our developers started working on the app two days later. From day 1, One Brands was one of our most promising apps.
		         </p>
		  	 </div>
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color"><strong>Intelligent</strong> Buying With One Brands</h3>
		  		<p></p>
                 <blockquote class="color blockonebrand">
		  	    	The concept of ‘buying the best product’ is an ambiguous one. Unless a buyer gets to see ALL the options in a particular category, how on earth can (s)he decide which one is the best? I was delighted to find that One Brands would indeed allow users to browse through a huge array of products - so that they could make the right choice. On a more personal front, the enthusiastic shopper in me was also excited!
		  	    </blockquote>
		  	    
		  	    <p class="color">
                 	There were multiple brainstorming sessions organized, to finalize the list of product categories that would be featured on this unique iOS shopping application. Some time was set aside to determine how the products will be showcased on the app screens (our in-house app designing team provided valuable inputs, as did the client). It was finally decided that while some products will be displayed individually, others will be viewable in batches/groups. End-users would have the option of toggling between the two options. 
                 </p>
                 
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/onebrand1.png" alt="onebrand" width="60%"></center>
		  	</div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/onebrand2.png" alt="onebrand" style="width:65%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7"><br>
		  		<h3 class="color">Getting Information From Fellow-Shoppers</h3>
		  	   <p></p>
		  	    <blockquote class="color blockonebrand">
		  	    	Most, if not all, buyers look for the user-ratings and reviews on a product, before deciding whether or not to purchase it. To satisfy this important requirement, I had planned to include a system that would allow users to find out how many times a particular product has been bought, and how many ‘likes’ it has garnered. The app, in a nutshell, was all about making informed shopping decisions.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		           	One look at the number of times an item has been purchased and how many ‘likes’ it has generated gives a buyer a fair idea of whether the product under question is worth buying or not. One Brands is not an app that simply throws up a large number of products, and leaves it to the users to guess which one(s) they should spend their money on. Instead, it makes sure that the purchase-decision is based on concrete information and indicators. Getting value for money becomes more likely...and definitely easier!
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">Finding Out About New Arrivals</h3>
		  		
				<p class="color">
					Barring a few (read: very few), every buyer wants to find out about new products, the latest arrivals. This is something One Brands really aces at. In a matter of a few taps, shoppers can view the details of all the new products in the different categories that are available for sale.
				</p>
				
		         <blockquote class="color blockonebrand">
		  	    	A mobile shopping application simply cannot afford to be static - this was something I and the owner of One Brands agreed upon from the start. There had to be a way in which we could present real-time information on the newest products to the shoppers. Contrary to what we had thought before, including this feature was easy.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          	Finding out about new product arrivals is often tricky for people who are...well...not really fond of visiting crowded malls and standing in long queues. The One Brands app brings to them all the information they need, right at their fingertips. Quite literally.
		         </p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Device Compatibility Of One Brands</h3>
		  	    
		         <p class="color">
		         	Our iPhone app development team customized One Brands for iPhone 6 and iPhone 6 Plus - just as the client wanted. Several rounds of testing were done (run-time as well as after the completion of the app prototype) on simulators and actual devices. The app was meant to reach out to focused people - people who were indeed likely to make purchases through their smartphones.
		         </p>
                 
                 <blockquote class="color blockonebrand">
		  	    	I wanted to make sure that my app should not suffer performance issues due to the limitations in older devices. That was the sole reason that I told Hussain and his team of developers that One Brands was to be optimized only for the latest flagship iPhones. The guys over there also thought this to be a good idea.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob5.jpg" alt="onebrand"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob7.jpg" alt="onebrand"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob8.png" alt="onebrand"></div>
		    <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob9.jpg" alt="onebrand"></div>
		    
		  </div>
		</div>
		<p></p>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob1.jpg" alt="onebrand"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob2.jpg" alt="onebrand"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob3.jpg" alt="onebrand"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/ob4.jpg" alt="onebrand"></div>
		  
		  </div>
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">Multi-Lingual Support</h3>
					<p class="color">
						The One Brands app is available in three different languages - English, Russian and Azerbaijani. The range and type of showcased products varies with the language option that any particular user chooses. We concurred with the view of the concept developer that tastes and preferences varied from one person to another, and language-based changes in product display was our way to address this issue. The multi-language support of One Brands, understandably, increased its reach as well.
					</p>
					
					<blockquote class="color blockonebrand">
		  	    		People from different countries and continents can use One Brands. It hardly made sense to show the same products to everyone, and ignoring the geographical factors altogether would have been a folly. It was smart on the part of the concept-owners to make sure that users from different places could view different products.
		  	        </blockquote>		
				</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-5">
		  	 		<center><img src="appstories/onebrand3.png" alt="onebrand" style="width:70%;"></center>
		  	    </div>	
		  	<div class="col-lg-7">
		  		<div class="box">
					<h3 class="color">Moving Over To Making Payments</h3>
					<p class="color">
						All of One Brand’s product database and options to browse through items would have been of no use if people could not purchase things directly through the app. After all, hardly anyone would want to search and select an item on their mobile screens, and then take the trouble of visiting a store and making the actual purchase. On One Brands, this problem does not crop up - since this mobile app for buyers comes with a secure payment gateway. 
					</p>
					
					<blockquote class="color blockonebrand">
		  	    		Apps that involve monetary transactions need to have top-notch security features - that’s basically a rule of thumb. I was initially wary that some loopholes in this regard might ruin my dream project. Thankfully, the developers at Teknowledge were up to the task of including a secure payment gateway in the app. It is probably the most vital cog in One Brands.
		  	        </blockquote>
				</div>
			</div>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
					<h3 class="color">Cash-On-Delivery Options</h3>
					<br>
					<blockquote class="color blockonebrand">
		  	    		While creating the payment channel in One Brands, one of our app developers raised the question regarding whether people buying products from local outlets would be interested in making payments through the app. It was an important question, and we finally decided to include an additional COD option too.
		  	        </blockquote>
		  	        
		  	        <p class="color">
						Anyone who use the One Brands app to purchase products from local stores that offer home delivery options do not need to use the payment gateway of the app at all. Instead, all that such buyers have to do is pick the ‘Cash-On-Delivery’ option. The stuff they buy will be delivered at their doorstep. All of us agreed that this feature would enhance the convenience factor of buyers just that bit more. 
					</p>
					
					<blockquote class="color blockonebrand">
		  	    		I believe that the flawless payment security features and the promptness of the entire browsing-choosing-buying process will be the two key reasons for the success of One Brands. I wanted to make an iPhone app that allowed people to make purchases and complete transactions quickly, easily, and without any apprehensions. One Brands has turned out to be just that. Fair play to the developers of Teks here.
		  	        </blockquote>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
				<h3 class="color">The Simplicity Of It All</h3>
		  	    <p class="color">
					Simple, easy-to-use apps succeed, the more confusing ones don’t - it’s as straightforward as that. Our team of iOS developers have collaborated with the graphic designers and UI/UX experts to make One Brands an application that people would actually ENJOY using. Every feature, every screen, every single aspect of the app is neatly organized and streamlined. Shopping on it is safe and as easy as a breeze. 
				</p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
						At our mobile app company, we have this unwritten rule - we only take up app projects that excite us...the ones we look forward to use ourselves as well. As pointed out right at the start of this app story, One Brands certainly falls in this category. Mobile shopping companions do not get better than this! 
					</p>
				</div>
		  </div>
		</div>
	
     </div>
	
</section>



<!-- Beta form modal -->

<div class="modal fade" id="onebrand" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" style="outline:0;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h5 class="modal-title color" id="lineModalLabel" style="color:#555">Check out the FREE beta version of Onebrand</h5>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form>
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Enter email" id="email" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="fn" class="form-control" placeholder="Enter First Name" id="userfn" style="color:#555">
              </div>
              <div class="form-group">
                <input type="text" name="ln" class="form-control" placeholder="Enter Last Name" id="userln" style="color:#555">
              </div>
              <input type="submit" class="btn btn-default" id="submitonebrand" style="background: #212B34;color:#fff;" value="Submit">
            </form>
            
		</div>
	</div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script>
		$(document).ready(function () {
          $("#submitonebrand").click(function() {
        	  var email = $("#email").val();
              var userfn = $("#userfn").val();
              var userln = $("#userln").val();
              var appID = '1111154921';

             // alert(email + userfn + userln + appID);exit;

  			$.post("https://staging.teks.co.in/testflightbetainvite/phpinvititionscriptrun.php", {email:email, appID:appID, userfn:userfn, userln:userln}, function(data){
  				alert('Please Check you email for beta app link');
  			  });
              
             });
          });
        
    </script>





<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>