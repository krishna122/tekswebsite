    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
	<link href="css/style.css" rel="stylesheet">
	<link href="css/appstories.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
   <!-- <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'> -->
    <script src="js/favourite.js"></script>
    <script src="js/main.js"></script>
    <script src="js/modernizr.js"></script>
    <link href="css/yamm.css" rel="stylesheet">
   <!--  <link rel="stylesheet" type="text/css" href="css/default.css" /> -->
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    
    <script>javascript:(function(){function ats(){var styles='*,p,div{user-select:text !important;-moz-user-select:text !important;-webkit-user-select:text !important;}';jQuery('head').append(jQuery('<style />').html(styles));var allowNormal=function(){return true;};jQuery('*[onselectstart], *[ondragstart], *[oncontextmenu], #songLyricsDiv').unbind('contextmenu').unbind('selectstart').unbind('dragstart').unbind('mousedown').unbind('mouseup').unbind('click').attr('onselectstart',allowNormal).attr('oncontextmenu',allowNormal).attr('ondragstart',allowNormal);}function atswp(){if(window.jQuery){ats();}else{window.setTimeout(atswp,100);}}if(window.jQuery){ats();}else{var s=document.createElement('script');s.setAttribute('src','http://code.jquery.com/jquery-1.9.1.min.js');document.getElementsByTagName('body')[0].appendChild(s);atswp();}})();
</script>
     
     