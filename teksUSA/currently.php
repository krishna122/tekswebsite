<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Currently</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
		<!-- Intro Header -->
<header class="currently_story" style="height: 34%;">
        <div class="currently_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">currently</span></h1>
<!--                         <center><a><img alt="" src="img/appstore.png"></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            At Teks Mobile, we are always on the lookout to make something new, something that pushes our skills and expertise. Of course we love to create mobile apps that become successful - but not by churning out the same type of applications over and over again. The Currently app project gave us the perfect opportunity to delve into something new. This project was of a type that we had not handled in the recent past.
	         </p>
	         
	          <blockquote class="color blockcurrently">
		  	    	Same is boring. What drives me and my team of developers is newness in app projects. That way, Currently was right up our streets. It’s an out-of-the-box application...that’s for sure.
		  	   </blockquote>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
<!-- 		  <div class="col-lg-12"> -->
		  	
		  	 <div class="col-lg-12">
		  		<h3 class="color">The App Is All About</h3>
		  	    <br>
		  	    <blockquote class="color blockcurrently">
		  	    	From the very start, I wanted to make the Currently app available to as many users as possible. Hussain suggested that we should create separate versions of the app for the iOS and Android platforms. It’s a good thing that Teknowledge had separate teams for Apple and Android app development. They were very professional about it.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          listening, sharing, saving audio clips. That’s right, Currently is a dedicated mobile audio clip sharing app - optimized for smartphones and tablets. On the app, users can record, share, receive and download/save 1-minute audio clips. We lovingly refer to it as a ‘one minute story app’ too.
		         </p>
		         <p class="color">
		           Currently can be downloaded and installed on all iPhones and iPad-s running on iOS 7 and above. It can also be used on Android phones and tablets (minimum requirement: Android 3.0 Honeycomb).
		         </p>
		  	 </div>
		  	 
<!-- 		  	 <div class="col-lg-6 hidden-xs"> -->
<!--		  	 	<video autoplay loop muted  poster="2016-04-16.png" style="border: 5px solid #D07A67;"> -->
<!-- 				   <source src="video/appstories/currently.mp4" type="video/mp4">  -->
<!-- 				</video> -->
<!-- 		  	 </div>    -->
		  	 
<!-- 		  </div> -->
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">To Sign In On Currently</h3>
		  	    
		         <p class="color">
		         	users can either use a unique email/password combination, or simply use their Facebook/Twitter credentials to log in. The app developers assigned on the project made doubly sure that there were no hiccups in the login stage of the app. It was our wholehearted effort to let Currently provide a seamless user-experience right through. Once a person successfully signs in, (s)he is redirected to the ‘Profile’ screen.
		         </p>

                 <blockquote class="color blockcurrently">
		  	    	There is a lot of information neatly arranged on the Profile page of the Currently app. You have to enter your username, location, upload a photo, and fill in the other stuff. There is a provision to edit profile information any time as well. Nothing about this app is static.
		  	    </blockquote>
		  	    
		  	    <p class="color">
                 	From the ‘Profile’ screen itself, users can view their follower-stats on the Currently network. There are three different counts - ‘Follows Me’ (the number of followers/friends a user has), ‘I Follow’ (the number of other users that person follows) and ‘Pending Followers’ (the number of requests that a user has received but not yet approved). 
                 </p>
                 
                 <blockquote class="color blockcurrently">
		  	    	To make things easier for people, we decided to display a summary of personal audio-clip collection on the profile screen. There are two sections - ‘My Waves’ refers to the sound clips any particular user records, while ‘Saved Waves’ are the clips that he or she receives and downloads.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		  	    	Only when a person actually downloads a received audio clip, does it go to the ‘Saved Waves’ section. Simply playing the clip does not save it.
		  	    </p>
                 
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/c9.png" alt="currently" ></center>
		  	</div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/c8.png" alt="currently" style="width:65%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7"><br>
		  		<h3 class="color">To Record Sound Clips On Currently</h3>
		  	    <br>
		  	    <p class="color">
		          users have to go to the ‘Record’ screen of the mobile audio clip sharing app. There, the designated recording area has to be tapped, to start recording sound clips. The minimum and maximum duration of any audio clipping on Currently are 3 seconds and 1 minute respectively.
		         </p>
		         
		  	    <blockquote class="color blockcurrently">
		  	    	I wanted to add a voice animator to the recording page of Currently. It would, in essence, reflect the volume of the recordings on a real-time basis. It was implemented in a smart manner by the guys at Teks...and it adds just that additional bit of fun to the app too.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          As soon as ‘Stop’ is tapped on this screen, users are taken to the clip-sharing page. There are four different sharing options - on Facebook and Twitter, and on ‘Stream’ and ‘Ocean’. People can choose any of the 4 channels to share their sound ‘waves’. There is one condition though - at least one out of ‘Stream’ and ‘Ocean’ has to be selected.
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">The ‘Stream’ And ‘Ocean’ In Currently Are A Lot Like</h3>
		  		<br>
		         <blockquote class="color blockcurrently">
		  	    	The presence of ‘Stream’ and ‘Ocean’ in Currently gives users greater control on with whom sound clips are to be shared. If someone wants sound waves to be displayed mainly to the people he follows, sharing on ‘Stream’ is recommended...while sharing clips on ‘Ocean’ is more like sending clips out to the entire Currently community.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          the ‘Friends’ and ‘Public’ options in Facebook for sharing posts. On the iPhone/Android sound recording app, when a user shares a ‘Wave’ in his/her ‘Stream’, it shows up in the ‘Stream’ of everyone who follow that user. By the same token, the waves of all ‘followers’ show up in the ‘Stream’ of any particular user.
		         </p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">To Add/Invite Friends, Users Have To</h3>
		  	    
		  	    
		         <p class="color">
		         	head to the ‘Find’ screen of this unique one-minute audio story app. From here, contact lists can be synced, and friend invitations can be sent - for joining the Currently platform. The contact list displayed to users on the app is prepared from the backend. This makes sure that people won’t have to draw up their list of contacts from scratch.
		         </p>
                 
                 <blockquote class="color blockcurrently">
		  	    	Existing users of Currently have the option to invite friends either by email or phone - from within the app itself. For emailing, the normal messaging feature of devices is used. They can also browse through the entire user-database of Currently, search for friends, and start following them on the app.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/c1.png" alt="currently"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/c3.png" alt="currently"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/c2.png" alt="currently"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/c4.png" alt="currently"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/c5.png" alt="currently"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/c6.png" alt="currently"></div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">‘Waves’ Remain In The Currently App For</h3>
					<p class="color">
						a period of 7 days, provided that they are present in the ‘Stream’ of the app (i.e., sound clips remain in the app server for a maximum of 7 days). Hence, users need to save their clips within that period. On the other hand, ‘Ocean’ waves can be listened to only once - and they play back continuously, unless paused (that is, one audio clipping after another).
					</p>
					
					<blockquote class="color blockcurrently">
		  	    		When I first got the hang of the app, I was like...whoa, if every sound clip gets saved, there will soon be too much of data clutter in it. I shared this concern with the client, and together, we came up with the idea of giving people the option of whether or not they want to save a clip. If yes, they have to download the clip inside 7 days. Even after saving, the waves get auto-deleted after 6 months.
		  	        </blockquote>
					
					<p class="color">
						This system effectively ensures that the Currently audio app does not get ‘too heavy’ at any point in time. Its performance does not get affected, as a result.					
					</p>		
				</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-5">
		  	 		<center><img src="appstories/c7.png" alt="currently" style="width:60%;"></center>
		  	    </div>	
		  	<div class="col-lg-7">
		  		<div class="box">
					<h3 class="color">To Reply To And Rate Sound Clips</h3>
					<p class="color">
						First-time users of this audio-sharing app receive a popup message, indicating the Wave to which they are replying (happens for the first 3 replies). The ‘Rating’ system (called STARFISH) is simple enough too - users can rate any sound clip present on the app from -2 to +2. It’s all about expressing oneself on Currently...by sharing audio clips, by rating them, and by posting replies/comments on waves. 
					</p>
					
					<blockquote class="color blockcurrently">
		  	    		On Currently, people have all the opportunity to interact with fellow-users of the app. They can send audio replies to Stream waves...which get displayed only on the concerned friend’s/follower’s Stream. There is an option to rate each clip on a five-point scale too.
		  	        </blockquote>
				</div>
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">To Report A Wave</h3>
					<p class="color">
						Oh yes, there is an option for that too. By tapping on ‘Report Wave’, a user can instantly file a report to the app admin, on any sound wave shared on Currently. On being reported, the clipping goes under admin review. 
					</p>
					
					<blockquote class="color blockcurrently">
		  	    		Like in any media-content sharing app, there is the risk of users uploading misleading, fraudulent or objectionable sound files on Currently. We included the provision of reporting waves...to make sure that such clippings are brought to notice, reviewed and if required, removed, as quickly as possible.
		  	        </blockquote>
		  	        
		  	        <p class="color">
		  	        	Once ‘Report Wave’ is tapped. a dialog box appears - prompting the user to indicate whether the concerned profile name is spam, or the username is spam, or if the the full audio clip is spammy. If the report is upheld, the profile pic or the username or the entire wave shared by the guilty party gets blocked (depends on the type of spam offence). When someone’s profile image gets blocked, the Currently logo is displayed, while people with blocked usernames are shown as ‘Mr.Currently’.
		  	        </p>
				</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">Ads Appear In Currently</h3>
					<p class="color">
						after 6 sound ‘Waves’ are played. A single video advertisement is shown - before which there is a popup to indicate that revenues from the ads are channelized towards charity. Our mobile app developers included the ads in a way that they do not hamper the overall user-experience on the app in any way. 
					</p>
					
					<blockquote class="color blockcurrently">
		  	    		In-app ads are often messy, and they can ruin up the overall set up of applications. For Currently, we decided to display a single ad after every 6 waves. The ad revenues go for a noble purpose...and the ads are not, well, a disturbance.
		  	        </blockquote>
		  	        
		  	        <p class="color">
		  	        	The Currently mobile audio-clip sharing app for iPhone/iPad and Android arrives in Apple App Store and Google Play Store within the next few weeks. It is easily one of the most interesting new apps we have worked on so far this year, and users would surely love it!
		  	        </p>
				</div>
		  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>