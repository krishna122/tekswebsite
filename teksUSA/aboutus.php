<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Us</title>
    <meta name='description' content='' />
    <meta name='keywords' content='' />

	<?php include 'head.php';?>

	<style type="text/css">

		h4{
			font-size: 18px;
		}
	</style>
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

	<?php include 'header.php';?>

    
        	<!-- Intro Header -->
	<header class="about" style="height: 60%;">
        <div class="about-body">
            <div class="container" >
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">TEKSMOBILE - AN ’APPY COMPANY</span></h1>
	                           <p>Teksmobile started operations in the United States in 2007, a year after the launch of our first office. At present, we have over 1000 mobile apps (iOS/ Android) in our portfolio, 400+ customers across the globe, and have offices in Sweden, Australia and India as well</p>
                       </div>
                 </div>
            </div>
        </div>
	</header>


<!-- Services Section -->
     <section id="services">
         <div class="container">
             <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="service-section-heading">SERVICES</h2>
                </div>
            </div>
            <div class="row text-center"><br>
                <div class="col-md-4 col-sm-4">
                    <img src="img/app_development.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">Mobile Apps</h3>
                    <p class="main_text">Build custom, multi-featured apps based on your ideas. Our developers work on the Apple (iOS, watchOS, tvOS, macOS) and Android platforms</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="img/game_development.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">2D/3D Games</h3>
                    <p class="main_text">Teksmobile US offers specialized mobile game development services. Discover how our graphic designers and animators blend efficiency with creativity in your game projects</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="img/api_development.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">RESTful APIs</h3>
                    <p class="main_text">We handle all aspects of API services - from API development and monetization, to lifecycle management and API strategy optimization. Teks brings to you the finest BaaS services</p>
                </div>
            </div>
            <div class="row text-center"><br>
                <div class="col-md-4 col-sm-4">
                    <img src="img/design_animation.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">Web</h3>
                    <p class="main_text">Discover creativity and efficiency at the highest level, as we create and manage personal and business websites. Avail the services of top web designers, developers and analysts</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="img/web_design_development.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">Internet of Things (IoT)</h3>
                    <p class="main_text">Smart car APIs and home automation software/hardware feature prominently in our IoT service portfolio. We place prime focus on scalability, reliability and overall functionality</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="img/IoT.png" alt="" class="img-circle img-thumbnail">
                    <h3 class="main_title">Designing/Animation</h3>
                    <p class="main_text">At Teks, you get to work with the best UI designers and 2D/3D computers in the United States. Several of our apps have won international app awards</p>
                </div>
            </div>
        </div>
     </section>

    <!-- Team Section -->
    <section id="team" style="background: #5d5d5d;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">THE BRAINS BEHIND TEKS</h2>
                    <h4 class="section-subheading text-team">A snapshot of the globally appreciated Teks Family</h4>
                </div>
            </div>
            <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
		    <!-- use jssor.slider.debug.js instead for debug -->
		    <script>
		        jQuery(document).ready(function ($) {

		            var jssor_1_SlideoTransitions = [
		              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
		              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
		              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
		              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
		              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
		              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
		              [{b:10000,d:2000,x:-379,e:{x:7}}],
		              [{b:10000,d:2000,x:-379,e:{x:7}}],
		              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
		            ];

		            var jssor_1_options = {
		              $AutoPlay: true,
		              $SlideDuration: 800,
		              $SlideEasing: $Jease$.$OutQuint,
		              $CaptionSliderOptions: {
		                $Class: $JssorCaptionSlideo$,
		                $Transitions: jssor_1_SlideoTransitions
		              },
		              $ArrowNavigatorOptions: {
		                $Class: $JssorArrowNavigator$
		              },
		              $BulletNavigatorOptions: {
		                $Class: $JssorBulletNavigator$
		              }
		            };

		            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		            //responsive code begin
		            //you can remove responsive code if you don't want the slider scales while window resizing
		            function ScaleSlider() {
		                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
		                if (refSize) {
		                    refSize = Math.min(refSize, 1920);
		                    jssor_1_slider.$ScaleWidth(refSize);
		                }
		                else {
		                    window.setTimeout(ScaleSlider, 30);
		                }
		            }
		            ScaleSlider();
		            $(window).bind("load", ScaleSlider);
		            $(window).bind("resize", ScaleSlider);
		            $(window).bind("orientationchange", ScaleSlider);
		            //responsive code end
		        });
		    </script>

		    <style>

		        /* jssor slider bullet navigator skin 05 css */
		        /*
		        .jssorb05 div           (normal)
		        .jssorb05 div:hover     (normal mouseover)
		        .jssorb05 .av           (active)
		        .jssorb05 .av:hover     (active mouseover)
		        .jssorb05 .dn           (mousedown)
		        */
		        .jssorb05 {
		            position: absolute;
		        }
		        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
		            position: absolute;
		            /* size of bullet elment */
		            width: 16px;
		            height: 16px;
		            background: url('img/b05.png') no-repeat;
		            overflow: hidden;
		            cursor: pointer;
		        }
		        .jssorb05 div { background-position: -7px -7px; }
		        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
		        .jssorb05 .av { background-position: -67px -7px; }
		        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

		        /* jssor slider arrow navigator skin 22 css */
		        /*
		        .jssora22l                  (normal)
		        .jssora22r                  (normal)
		        .jssora22l:hover            (normal mouseover)
		        .jssora22r:hover            (normal mouseover)
		        .jssora22l.jssora22ldn      (mousedown)
		        .jssora22r.jssora22rdn      (mousedown)
		        */
		        .jssora22l, .jssora22r {
		            display: block;
		            position: absolute;
		            /* size of arrow element */
		            width: 40px;
		            height: 58px;
		            cursor: pointer;
		            background: url('img/a22.png') center center no-repeat;
		            overflow: hidden;
		        }
		        .jssora22l { background-position: -10px -31px; }
		        .jssora22r { background-position: -70px -31px; }
		        .jssora22l:hover { background-position: -130px -31px; }
		        .jssora22r:hover { background-position: -190px -31px; }
		        .jssora22l.jssora22ldn { background-position: -250px -31px; }
		        .jssora22r.jssora22rdn { background-position: -310px -31px; }
		    </style>


		    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
		        <!-- Loading Screen -->
		        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
		            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
		            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
		        </div>
		        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
		            <div data-p="225.00" style="display: none;">
		                <img data-u="image" src="img/team/1.png" />
		            </div>
		            <div data-p="225.00" style="display: none;">
		                <img data-u="image" src="img/team/2.png" />
		            </div>
		            <div data-p="225.00" data-po="80% 55%" style="display: none;">
		                <img data-u="image" src="img/team/3.png" />
		            </div>

		        </div>
		        <!-- Bullet Navigator -->
<!--
		        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
		             bullet navigator item prototype 
		            <div data-u="prototype" style="width:16px;height:16px;"></div>
		        </div>
-->
		        <!-- Arrow Navigator -->
		        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
		        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
		    </div>

		    <!-- #endregion Jssor Slider End -->
            
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="large text-team">At Teksmobile US, we collaborate as a team to add optimal value to every single project. From apps for mobiles, wearables and web, to other specialized software - we LOVE making them all!</p>
                </div>
            </div>
        </div>    
    </section>

	<!-- About Section -->
    <section id="about">
        <div class="container" style="padding-bottom:20px;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#1F1F21;">GROWING OVER THE YEARS</h2>
              <br>  </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline" >
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2006.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2006</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted color">First office of Teksmobile launched. The team starts out with simple Java/J2ME applications</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2007.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2007</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Teksmobile LLC opens office in United States. First iPhone app launched</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2008.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2008</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Android app development processes starts. Number of projects increases steadily</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2011.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2011</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Round-the-clock (24x7) service starts, across 18 time-zones. Clients start to receive free app consultancy service</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2012.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2012</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">A big year for Team Teks, as over 85% of our apps get featured at online stores. Several prestigious app awards received</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2013.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2013</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Story Time For Kids - an educational app for children - bags Adobe Design Awards</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2014.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2014</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Office in Sydney, Australia (as Teksmobile Australia) launched. Total app count crosses 600</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2015.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2015</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">A new year, another new Teks chapter. In collaboration with Maria Bergstrom, Teksmobile Sverige starts operations (headquartered in Sala, Sweden)</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2016.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="color">2016</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Total number of successfully completed mobile apps crosses 1000. Participation in StartCon 2016 exhibition in Sydney</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                        <a href="startproject.php">
                            <div class="timeline-image">
                                <h4>
                                    Click for
                                    <br>a free app<br>
                                    quote
                                </h4>
                            </div>
                           </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#about').addClass('active');
	});

</script>

</body>

</html>
</body>
</html>
