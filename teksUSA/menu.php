<nav class="navbar navbar-custom navbar-fixed-top yamm" role="navigation" style="text-shadow: none;">
        <div class="container-fluid">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-inverse side-collapse in" style="margin-top: 15px;">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="aboutus.php" style="font-size:16px;font-weight: 300; color:#fff;text-transform: uppercase;">About</a>
                    </li>
                    <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="grid" style="font-size:16px;font-weight: 300;color:#fff;text-transform: uppercase;">App Stories<b class="caret"></b></a>
                        <ul class="dropdown-menu" style="background-color:rgba(0,0,0,.8) !important;">
                            <li class="grid-demo">
                                <div class="row">
                                
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="text-align: left;">
                                            <h3 style="font-size: 16px;color:#FF7018; font-size: 16px;font-weight: 300;">OUR MOBILE APPS</h3>
                                                <div class="row">
                                                <div class="col-sm-12" style= "font-size: 16px;">
                                                <div class="col-sm-4"><a href="appstories.php">iPhone</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Android</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Windows</a></div>
                                                </div>
                                                </div>
                                        </div>
                                        <div class="col-sm-4" style="text-align: left;">
                                             <h3 style="font-size: 16px;color:#FF7018;font-size: 16px;font-weight: 300;">CATEGORIES</h3>
                                             <div class="row">
                                                 <div class="col-sm-12" style="font-size: 16px;">
                                                <div class="col-sm-4"><a href="appstories.php">Social</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Fitness</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Game</a></div>
                                                </div>
                                                <div class="col-sm-12" style=" font-size: 16px; text-align: center;">
                                                <div class="col-sm-4"><a href="appstories.php">Business</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Lifestyle</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Finance</a></div>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="col-sm-4" style="text-align: left;">
                                            <h3 style="font-size: 16px;color:#FF7018;font-size: 16px;font-weight: 300;">GADGETS</h3>
                                            <div class="row">
                                                <div class="col-sm-12" style=" font-size: 16px;">
                                                <div class="col-sm-4"><a href="appstories.php">Watch-Kit</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Bluetooth</a></div>
                                                <div class="col-sm-4"><a href="appstories.php">Cloud</a></div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                
                            </li>
                            <div class="col-sm-12" style="background: #000;border-top:1px solid #292929;opacity:1; height: 50px; margin-bottom: 0px;"></div>
                        </ul>
                    </li>
                    
                    <li>
                        <a class="page-scroll" href="howwework.php" style="font-size:16px;font-weight: 300;color:#fff;text-transform: uppercase;">HOW WE WORK</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="startproject.php" style="font-size:16px;font-weight: 300;color:#fff; text-transform: uppercase;">Start Your Project</a>
                    </li>
                    
                    <li>
                        <a class="page-scroll" href="career.php" style="font-size:16px;font-weight: 300;color:#fff; text-transform: uppercase;">Careers</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://teks.co.in/site/blog/" target="_blank" style="font-size:16px;font-weight: 300;color:#fff; text-transform: uppercase;">Blog</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="award.php" style="font-size:16px;font-weight: 300;color:#fff;text-transform: uppercase;">Awards</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <script>
    $("#grid").bind('mouseover',function(){
        $(this).trigger("click");
  });
    </script>