<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of HelloMind</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">HelloMind</span>
                        </h1>
                        <a href="https://itunes.apple.com/us/app/hellomind/id327538172" target="_blank"><img alt="HelloMind" src="img/appstore.png"></a>
<!--                        <a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img alt="HelloMind" src="img/play-store.png"></a>-->
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
	            Making a successful app is not something new for us. Over the 10 years (and counting!) of existence of Team Teks, several of our mobile applications have gone on to win international awards and accolades. Even so, as app developers committed towards excellence - the success of every new app still excites us...and we are lucky enough to enjoy these excitements each quarter. The latest in our lineup of uber-successful mobile apps is HelloMind - a custom, multi-featured self-motivation app - which became the top grosser at the Apple App Store.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	Hussain and his team of iPhone developers had done a great job with ICBF. When I started toying with the idea of making HelloMind - I was always confident about delegating the project to Teksmobile. I mean, their professional expertise is beyond all speculations, and I was already a more than satisfied client of theirs. Why look anywhere else?
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">Working On HelloMind</h3>
                 <p class="color">
                     We had previously worked on a iPhone self-improvement app called I Can Be Anything (link to ICBF app story), which had bagged quite a few prestigious awards, uniformly positive reviews and honourable mentions in many ‘top-10’ lists. When its owner, Jacob Strachotta, floated the idea of making a new application - with major feature upgrades and improvements over ICBF - we literally jumped at the opportunity. Here was an app with a proven track-record of success, and we were going to make it even better.
                 </p>
		  	 </div>   
		  	 
              <p class="color">
                  Once the paperwork was all sorted out, preliminary wireframing for the new application started. We had already shown that we could make good motivational apps, and this was an opportunity to go one better!
              </p>
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/hello1.png" alt="HelloMind" style="width:60%;"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Platform Selection</h3>
		  	    
		         <p class="color">
		         	An app like HelloMind deserved to be made available to as wide an audience as possible. Keeping that under consideration, it was decided that separate versions of the app - for iOS and Android - will be developed. Our in-house Android and iOS app developers started out with the project simultaneously. It was an innovative new app, and everyone was looking forward to it.
		         </p>
                
                <blockquote class="color">
			  	  HelloMind is compatible with iOS 8 and later versions...and on Android 4.0 and later. Cross-platform app development is something my team specializes in, and we were pretty much sure about doing a good job on both the versions. Credit to Jacob...he believed in our abilities throughout.
			  </blockquote>	
		   </div>
		   
		  </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Solving Problems With HelloMind</h3>
		  	    <br>
		  	    <blockquote class="color">
		  	    	Life is a complex affair. It throws up challenges and problems and fears...things which can bring in negativity. As a long-time expert on hypnosis and hypnotherapy, I felt that there was a scope of creating a mobile app that would effectively tackle these problems. And that’s how HelloMind was conceptualized.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Problems of various types - neatly categorized - can be resolved with the help of the therapeutic audio sessions (treatment sessions) in the app. The experience of Jacob in this field, combined with our understanding of the proposed functionalities of the app, ensured that HelloMind would be a really handy mobile self-inspirational app...for everyone.
		         </p>
                 
                 <br>
		  	    <blockquote class="color">
		  	    	The range of life issues that HelloMind handles is mighty impressive. From something as common and pesky as lack of adequate sleep, to fear of animals and low self-esteem levels - there are treatments for everything in this app. A cool personal confidence builder!
		  	    </blockquote>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/hello2.png" alt="HelloMind" style="width:75%;">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-5">
                  <img src="appstories/hello3.png" alt="HelloMind" style="width:80%;">
              </div>
                  
              <div class="col-lg-7">
		  		<h3 class="color">The Magic Of Result-Driven Hypnosis</h3>
		  		
                <p class="color">
		          While we did our bit to make HelloMind as useful and user-friendly as possible, the lion’s share of the overall effectiveness of the app should be attributed to Jacob and his team. A guided form of hypnosis - called Result-Driven Hypnosis, developed by Jacob himself - forms the basis of the new mobile self-help application. It focuses on identifying the underlying causes of each mental block/problem, and then proceeds to eliminate it.
		         </p>
              
		         <blockquote class="color">
		  	    	To get over a mental problem….say a fear or a lack of motivation...one has to understand the root cause for the problem first. Powered by my RDH technique, HelloMind gradually guides the subconscious mind to the factors that are causing the problem. This makes it easier for the conscious mind to get rid of the issue. It’s like...if you really know the problem, you will have the solution ready.
		  	    </blockquote>
		  	    
                <p class="color">
                    Once the user selects a category or theme, the app asks several simple, in-depth questions - to gain a proper insight of the problem to be tackled. For instance, if someone starts with the ‘Fear’ category, (s)he will be asked ‘Fear of what’? Let’s say, the response to this query is ‘fear of animal’. The next question generated by the app would be ‘Fear of which animal?’ The interactions proceed in this manner, and, based on the user-responses, the best treatment session is recommended.
                </p>    
            </div>  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Treatment and Boosters</h3>
		  	    
                 <blockquote class="color">
		  	    	To be of practical use, any lifestyle app should have a systematic way of operations. This was even more important for an app like HelloMind. After several rounds of discussions with Jacob, we finally implemented a system of audio treatments and boosters in the app.
		  	    </blockquote>
                 
                 <p class="color">
                 	Magnus was given the responsibility of recording the lesson narrations (done by Jacob himself), and modify the sounds appropriately. Being an audio engineer of rich experience and considerable reputation, Magnus finds this job fun, challenging, and in a way, very, very, fulfilling.
                 </p>
              
                 <p class="color">
                 	While the ‘treatments’ in HelloMind look to flush out different types of negativities from one’s life, the built in audio ‘boosters’ are all about positive reinforcements. There is a large number of boosters available (including a free complimentary booster) - for bolstering several aspects of the human mind. The boosters are also of 30 minutes length (approximately) and can be listened to any number of times.
                 </p>
                 
                 <blockquote class="color">
		  	    	All that you need to do to start getting the benefits of HelloMind is a peaceful place and a pair of good-quality earphones. I would also advise users to set their devices on flight mode, before starting to hear any treatment session. That way, there will be no distractions and the sessions will deliver maximum advantages.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/hm1.jpg" alt="HelloMind"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/hm2.jpg" alt="HelloMind"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/hm3.jpg" alt="HelloMind"></center></div>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/hm4.jpg" alt="HelloMind"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/hm5.jpg" alt="HelloMind"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/hm6.jpg" alt="HelloMind"></div>
		  </div>
		</div>
		
        <div class="row">
		  <div class="col-lg-8"><br>
				<h3 class="color">An App That Helps You Help Yourself</h3>
				<p class="color">
					The rationale behind the HelloMind application is simple enough. The solution to every mental problem lies within our own minds - and the app helps us change ourselves and unlock the secret to happiness within our minds. Better, stronger thought processes - that’s what this all-new self-help mobile app ushers in.
				</p>
				<blockquote>
					People spend lots of money on psychiatric medications...which are expensive, often not effective enough, and might have side effects. With HelloMind, I have tried to offer an alternative to these medications. It is a free app with loads of treatments and boosters - and they offer customized help to users, whenever and wherever they might need it.
				</blockquote>
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/hello4.jpg" alt="HelloMind" style="width:70%;">
              </center>
		  </div>
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">The Price Factor</h3>
					<p class="color">
						HelloMind is, of course, a free-to-download application - and the built-in treatments and boosters are also priced at very competitive levels. A user can download a treatment for $8.99 (boosters are priced at the same level). What’s more, a monthly subscription plan is available, at $12.99. Individuals can also opt for the annual subscription.
					</p>
					<p class="color">
						The iOS (iPhone and iPad) version of HelloMind is available for free download.It has already received the thumbs-up of appreciation from users across the globe...and we believe HelloMind will be an even bigger success than ICBF. 	
					</p>		
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>