<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Timesnaps</title>

	<?php include 'head.php';?>
<link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="timesnaps_story" style="height: 34%;">
        <div class="timesnaps_story-body">
            <div class="container" style="margin-top: 5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                            <br>
                           <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Timesnaps</span>
                        </h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Life is all about fleeting moments, right? Time is never at a standstill, and the person/thing/tour we love the most grows and changes in a blur and a whiz – somehow heightening a sense of loss in our hearts. These were precisely the thoughts of Luke Holden, as he started toying with the concept of an iPhone application that could actually track the passage of time. And what else to do this better, than via photos?
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	<h3 class="color">The Search For A Developer Company</h3>
		  	
		  	<div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/timesnaps.jpg" style="border: 5px solid #3D3286;">
				   <source src="video/appstories/timesnaps.mp4" type="video/mp4"> 
				</video>
		  	 </div>  
		  	 
		  	 <div class="col-lg-6">
		  		
		  	    <p class="color">
		          Timesnaps (the name was Luke’s brainchild) was going to be the very first mobile app that Luke had ever created. Instead of trying to dabble in coding methods that were alien to him, he decided to hire a professional mobile app company for the purpose.
		         </p>
		         
		         <blockquote class="color blocktimesnaps"> 	    	
					I guess I could have tried to make the app myself, or simply got in touch with a freelance developer. However, I did not want to compromise with any aspect of the vision I had for Timesnaps. Hence began my search for an app company.                                
		  	    </blockquote>
                
                <p class="color">
			     While browsing through the profiles of app development firms online, Luke came across the website of Teknowledge Mobile Studio. He requested for a free app quote, and was pleasantly surprised by the fact that this company indeed responded within 16 hours (that’s one ‘yay!’ for us!). The specified terms were to his liking, and work on the project started the following week.
			   </p>
			   
		  	 </div>
		  	 
		  </div>
	</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">The Early Stage</h3>
		  		
		  	    <p class="color">Luke constantly emphasized that Timesnaps was not going to be like any other already existing app in iTunes. He shared a basic wireframe with the iOS developers at Teknowledge, which served as a preliminary reference point. During this early phase of the Timesnaps project, there were regular interactions to understand the exact proposed nature of the app.</p>
		         
		         <blockquote class="color blocktimesnaps">
					I have been in the app development business for close to 9 years, and even so, I was amazed by Luke’s vision for Timesnaps. He wanted the app to track changes in virtually everything – right from the growth of a baby or a plant, to moments during a honeymoon tour or even the sky and the clouds. It was different from anything I had ever worked on before…and of course it was a challenge, but me and my team decided to take it on.       
		  	    </blockquote>
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/timesnaps1.png" alt="timesnaps" width="55%"></center>
		  	</div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/timesnaps2.png" alt="timesnaps" style="width:50%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7"><br>
		  		<h3 class="color">The Challenge</h3>
		  	    
		  	    <p class="color">
		          	Our team agreed with Luke that making this iOS app was not going to be a difficult task per se (the project was completed in five weeks flat). The biggest challenge, in fact, was regarding something else. Till date, smartphone-users were familiar with image-capturing apps and social networking apps. Capturing the passage of time through a mobile application was an entirely new concept. We had to ensure that people actually liked Timesnaps.
		         </p>
		         
		  	    <blockquote class="color blocktimesnaps">
		  	    	
I knew that I had a winning app-idea, and was quietly confident that the guys from Teks would do a good job with it. But was I sure from the very outset that Timesnaps would emerge such a big hit? Frankly, no…there was a lot of nervousness.
                              
		  	    </blockquote>
		  	 </div>
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">Creating Stories From Snaps</h3>
		  		<br>
		         <blockquote class="color blocktimesnaps">
		  	    	
I have always been a fan of photo-sharing apps, and had downloaded Timesnaps just out of curiosity. The new functions of the app – particularly the slideshare creation and sharing feature – are absolute delights. This is an iPhone photo app done just right!
                                
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          This was, in essence, the key function of the Timesnaps application. After a lot of deliberation among our mobile app developers and idea-sharing with Luke, arranging pictures in slideshows was identified as the main functionality of the app. The flow of the app was customized in such a way that users could start creating slideshows immediately after taking photos and accepting them.
		         </p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">How Does Timesnaps Work?</h3>
		  	    
		  	    
		         <p class="color">
		         	To make sure that people did not get confused by the workflow of Timesnaps, the UI of the app was made as user-friendly as possible. A user could go on a snapping spree after the tapping the ‘New Timestamp’ button (located at the center of the screen). A provision for setting alarms was also included, to remind users to take photos of the person/thing whose progress over time was being tracked.
		         </p>
                 
                 <blockquote class="color blocktimesnaps">
		  	    	
I had personally requested Hussain to let his best developers handle my project. I am happy to say that he readily obliged. I found those in charge of making Timesnaps to be proficient both coding as well as non-technical matters.
                        
		  	    </blockquote>
		  	    
		  	    <p class="color">
		         	Slideshows could be created with the snapped photos (there is a ‘Ghost Button’ to add further customization and stability). The slideshows can also be shared directly on Facebook and/or uploaded on Facebook.
		         </p>
		         
		         <blockquote class="color blocktimesnaps">
		  	    	Social media integration is a key feature in nearly all of our iOS app projects. After discussions with Luke, it was decided that Timesnaps won’t be an exception. 
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/time1.png" alt="timesnaps"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/time2.png" alt="timesnaps"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/time3.png" alt="timesnaps"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/time4.png" alt="timesnaps"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/time5.png" alt="timesnaps"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/time6.png" alt="timesnaps"></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
			  <div class="col-lg-6">
			  	  	<center><img src="appstories/timesnaps3.png" alt="timesnaps" style="width:40%;"></center>
			  	  </div>
				<div class="col-lg-6">
				<h5 class="color">In-app Purchases</h5>
					<p class="color">
						Initially, Luke was not planning to put any limits to the number of projects/slideshows that could be created with the free version of Timesnaps. It was during the final stages of development, that an in-app purchase option was included in this innovative imaging app. The basic version of Timesnaps can store a maximum of 3 projects. Further projects can be unlocked by purchasing the ‘Go Pro’ option (Price: $2.99).
					</p>
					
					<blockquote class="color blocktimesnaps">
Initially, there was a bit of a confusion regarding the camera photostamp visible at the corner of the photos. This is a default feature in the standard version of Timesnaps, and can be removed by opting for the ‘Go Pro’ option.                       
		  	        </blockquote>
					
		  	  </div>
		  	  		
		  </div>
		</div>
<p></p>
		<div class="row">
		  <div class="col-lg-12">
				<div class="col-lg-6">
					<h5 class="color">Other Advantages Of The ‘Go Pro’ Feature</h5>
					<p></p>
					<blockquote class="color blocktimesnaps">
						I asked myself whether the possibility of creating and storing more photo projects was enough to justify the presence of the ‘Go Pro’ option. The gut feeling was ‘no’ – and there had to be something more on offer for the users spending their money for the in-app purchase.                                                       
		  	        </blockquote>
		  	        
		  	        <p class="color">
						Luke sat with the app developers and graphic designers at Teknowledge, to chalk out the additional features that would be given to the pro-users. In addition to giving them access to an unlimited number of projects, the ‘Go Pro’ option offers:
					</p>
					
					<ul class="color">
						<li>Removal of the ‘Rate Us’ popup (visible in Standard version).</li>
						<li>Option to create any number of high-definition, interruption-free slideshows.</li>
					</ul>
					
					<p class="color">
						All pictures taken with Timesnaps are saved inside the app itself, and are not exported to iPhone albums. This, in turn, completely removes the risk of losing any photo.
					</p>
					
					<blockquote class="color blocktimesnaps">
						
Two things about Teknowledge really built my belief that they would make my app a winner. First was, of course, their sincerity and sheer knowledge about iPhone app development. Secondly, and perhaps more importantly for me, they had separate teams for coding, testing and app designing. The project was always systematic, always streamlined.
                                                                               
		  	        </blockquote>
					
		  	  </div>
		  	  <div class="col-lg-6">
		  	  		<center><img src="appstories/timesnaps4.png" alt="timesnaps" style="width:90%;"></center>
		  	  </div>		
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
				<div class="box">
					<p class="color">
						We made it a point to include Luke in the actual development phase. Feedback and suggestions were sought from him from time to time, after sharing mockups and prototypes of the app with him. The focus was on making the app just as he wanted.
					</p>
		  	  </div>	
		  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>