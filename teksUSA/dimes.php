<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Story Of Dimes</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="dimes_story" style="height: 34%;">
        <div class="dimes_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Dimes</span></h1>
<!--                         <center><a><img alt="" src="img/appstore.png"></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Campus fun makes a comeback with the Dimes application for iPhone. We had worked on quite a few college and university apps (<a href="http://teks.co.in/site/blog/myvuwsa-app-teknowledge/">Myvuwsa</a> and a few others were big successes) - but this one was different. It’s more geared towards the fun quotient of campus life, and making the app was an altogether new experience for us. An experience we totally fell in love with!
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  		<h3 class="color">The Idea Behind Dimes</h3>
		  	    <br>
		  	    <blockquote class="color blockdimes">
		  	    	Dimes goes beyond the jokes and puzzles and brain-teasers and instant messaging that most college apps are about. My wish was to create an exciting campus poll application, where users will be able to rank their friends on different criteria. I was always confident that such an app would find favour among Generation Z.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Within 24 hours of contacting us with the app idea, a detailed free app quote was sent to the client. By the next week, the preliminary rounds of wireframing had started, the project was assigned to our iOS development team, and the app was on its way to being ‘born’.
		         </p>
		         <p class="color">
		            <strong>Note:</strong> The Dimes app is customized for all iPhones updated to iOS 8.4 (and later versions of the platform).
		         </p>
		  	 </div>
		  	 
		  	 <div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="2016-04-16.png" style="border: 5px solid #D07A67;"> 
				   <source src="video/appstories/dimes.mp4" type="video/mp4"> 
				</video>
		  	 </div>   
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/dimes2.png" alt="dimes" width="60%"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Getting On The Dimes Network</h3>
		  	    
		         <p class="color">
		         	Easy sign-in one of the many high points of this iPhone campus poll app. Users can download and install Dimes on their handsets, and log in with the credentials of their Instagram accounts. The application ‘fetches’ the friends of individual users on the basis of the last 5 images they have uploaded on the Instagram platform.
		         </p>
                 <p class="color">
                 	It takes only a few minutes to sign in on the Dimes app, following which users can start taking part in the fun campus polls on it.
                 </p>
                 
                 <blockquote class="color blockdimes">
		  	    	We wanted to do something different from the regular method of inviting friends by email or text messages. When the idea of adding friends with the help of their respective Instagram pictures was floated by the concept owner, I was immediately hooked. Here was a person who was genuinely interested in making his app different...something that stands out in the crowd.
		  	    </blockquote>
		   </div>
		  
		  </div>
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Polls In The Dimes App</h3>
		  	    <br>
		  	    <blockquote class="color blockdimes">
		  	    	Hussain and I agreed that the coverage of an app like Dimes should be as wide as possible. Apart from ranking friends from one’s own school or college campus, a user can rank his or her other friends as well, in the given polls. The polls, in turn, are determined and displayed from the backend - and it’s a very interesting collection.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          A vast collection of polls are loaded from the backend inside the Dimes application - each of them adding its own bit to the frolic and laughter of campus life. Users can express their opinions (read: vote) on who among their friends is the ‘hottest, or the ‘most stylish’, or the ‘funniest’. Friends can be ranked on the basis of any of the available poll criterion - and the process of this ranking is quick, simple and a lot of sheer fun.
		         </p>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/dimes3.png" alt="dimes">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">Viewing Activities & The Feed List</h3>
		  		<br>
		         <blockquote class="color blockdimes">
		  	    	There are so many polls in Dimes, and a user might just feel a little dazed while trying to keep track of the ones he or she has voted on. To make things easier, all the rankings given by a person are collected in, and can be viewed from, the Feed List. Everything is organized and systematic on the app.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          While the ‘Feed List’ contains all the ‘my rankings’ data, people can check out all their recent activities by from the ‘Activity’ tab as well. After every post, the rankings are directly sent to the ‘Feed List’.
		         </p>
                 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Real-Time Chat Options</h3>
		  	    
		  	    
		         <p class="color">
		         	Dimes is a unique take on campus social networking apps - and we felt that its target users would appreciate if it doubled up as a chat application as well. The client agreed, and accordingly, our iPhone developers bundled in a seamless real-time chat functionality in the app. Users can chat with other registered Dimes members at any time, directly from the app. It’s called staying in touch!
		         </p>
                 
                 <blockquote class="color blockdimes">
		  	    	There is no room for questioning the popularity of different chat apps...and we felt that an instant messaging option would go well on the Dimes application as well. Apart from chatting with fellow-users of the application, people can also send out invites to friends. It’s easy to build a large network of friends on Dimes, and it’s even easier to chat with them any time!
		  	    </blockquote>
                 
                 <p class="color">
                 	A few relatively minor glitches cropped up when the chat feature was being coded in the Dimes app. Our app testing team systematically identified the errors, and the developers resolved the bugs quickly enough. We had to work extra hours on 4 consecutive days for this - but it was all for making the app work properly. We love creating mobile apps that excel in terms of quality.
                 </p>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/dimesscreen1.png" alt="dimes"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/dimesscreen2.png" alt="dimes"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/dimesscreen3.png" alt="dimes"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dimesscreen4.png" alt="dimes"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dimesscreen6.png" alt="dimes"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/dimesscreen7.png" alt="dimes"></div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					
					<p class="color">
						Dimes is much more than just a polling app for college-goers. There are social networking features and chat options - and the app simply cried out for an engaging design theme. After a couple of false starts, our mobile UI/UX designers came up with the final layouts for the app. We were in search of a design idea that conveyed the essence of Dimes properly, and we feel we have hit upon it.
					</p>
					
					<p class="color">
						As soon as the final round of testing is complete, Dimes will be submitted at the iTunes store. It is a cool campus companion app, comes with an exciting collection of polls - and we are sure that youngsters will take to it in a big way!					
					</p>		
				</div>
		  </div>
		</div>
	
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>