<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Apps made for Cloud by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>


<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">CLOUD-BASED APPS</span></h1>
	                    <p>Many of our apps are backed up by robust, third-party, custom APIs for web connectivity. Here we present some of these cloud apps</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
<section id="appstory" class="">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
			 <h1>UPDATING VERY SOON</h1>
		  </div>
		</div>
	</div> 
</div> 
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>