<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Veeaie Keyboard</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="veeaie_story" style="height: 50%;">
        <div class="veeaie_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                        <br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Veeaie Keyboard</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>Not everyday do developers get the chance of working on apps that blend in fun and creativity in equal measure. At Teks, we have been fortunate enough to get a fair number of such projects over the years - but none of them were quite as unique and interesting as the Veeaie Keyboard app. Right from the moment Tejmur Sattarov explained the concept of the app, we knew we simply could not pass up the opportunity of working on this project. Call it 'love at first hearing'!</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/vk7.png" alt="Veeaie Keyboard">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Through Veeaie, I wanted people to express their creative side, without losing out on the fun factor. There are so many apps that focus too much on creativity, and come up short on the humor quotient. I never wished Veeaie to be like one of those boring apps.
                                <br><br>
                                <center>
				<img src="appstories/veeaieclient.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Tejmur Sattarov</span> <br> 
				<span style="font-size: 25px;">(Senior Art Director; Concept Developer of Veeaie Keyboard)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <P>We consider it a stroke of luck that Tejmur chanced upon Teknowledge, while searching for a suitable mobile app company to handle the project. Taking it up was an unanimous decision, and we sent along a detailed free app quote the very next day after Tejmur had got in touch with us.</p>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
As a mobile app entrepreneur, it has been my constant endeavor to add more verve, more variety, more uniqueness to the portfolio of my company. An app like Veeaie Keyboard was simply great news for us. Kudos to Tejmur for thinking up something so innovative, engaging... and more importantly, workable.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <h4>What Is Veeaie Keyboard All About?</h4>
	      
	      <div class="col-lg-5">
	      <img src="appstories/vk4.png" alt="Veeaie Keyboard">	
	   	  </div>
	   	  
	      <div class="col-lg-7">
	      <P>In essence, Veeaie Keyboard is a third-party keyboard application, that allows users to create, view and use emojis. The phone camera is used to take snaps, which can then be edited by by users. Custom photo effects can be added too, with ease.</p>	
	   	  
	   	  <h5>Emojis... Unlimited!</h5>
	   	  <p>During the initial brainstorming sessions with the team of iOS app developers assigned on the project, Tejmur explained that he wanted end-users to create and use as many custom emojis as they wanted. Accordingly, no cap was put on the number of emojis that could be made. Users could also view emojis created by others - to get some inspiration. A subtle touch of social networking thrown in!</p>
	   	  </div>
	   	  
	  </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Creativity is something that should never be bound by limits. If I was making emojis, the last thing I would want to see is a limit set on the number of faces I could create. Veeaie does not present any such inconvenience to users either.
                                <br><br>
                                <center>
				<img src="appstories/veeaieclient.png" alt="Tejmur Sattarov" style="width:20%;"><br> 
				<span style="font-size: 30px;">Tejmur Sattarov</span> <br> 
				<span style="font-size: 25px;">(Senior Art Director; Concept Developer of Veeaie Keyboard)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <div class="col-lg-7">
	         <p>Accordingly, our mobile app development experts included the provision of adding unlimited volumes of customized emojis in the app. Any emoji created by a user could be marked as 'Favorite'. Purchased emojis could be added to the 'Favorites' page too.</p>
              </div>
              <div class="col-lg-5">
	         <img src="appstories/vk1.png">
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
A good idea does not necessarily translate into a great app. I have personally used many applications that have been ruined by complicated controls and confusing layouts. We were determined to make Veeaie an app that anyone... even kids... could operate on their own.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>The User-Friendliness Of Veeaie Keyboard</h4>
	      <P>The process of downloading, installing, activating and starting to use this cool iPhone app follows an easy, streamlined flow. Once the app is installed on a user's device, a set of instructions is displayed (these instructions are only for first-time users). The 'Allow Full Access' tab has to be tapped next - after which the app becomes activated and ready to use.</p>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
From the very outset, I had a clear idea about how Veeaie would work. What I was worried about was that, the app company I hired might not be able to make the app the way I had conceptualized it. Thankfully, Hussain and his team beautifully grasped the feel of the app, and proceeded to create it just as I had visualized it.
                                <br><br>
                                <center>
				<img src="appstories/veeaieclient.png" alt="Tejmur Sattarov" style="width:20%;"><br> 
				<span style="font-size: 30px;">Tejmur Sattarov</span> <br> 
				<span style="font-size: 25px;">(Senior Art Director; Concept Developer of Veeaie Keyboard)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	     <h4>So, How Can Users Use/Create Emojis In Veeaie?</h4>
            
            <div class="col-lg-5">
                <img src="appstories/vk2.png" alt="Veeaie Keyboard">
              </div>
              
            <div class="col-lg-7">
	      <P>This was the part we enjoyed the most in the mobile app development process of Veeaie Keyboard. The app itself was free, and we decided to include emoji sticker packs as available in-app purchases. The packs were priced at uniformly competitive levels, and users could take their pick from 5 different packages. The idea was simple enough - to add fun effects to one's own (or others') pictures.</p>
              </div>
              
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Providing emoji packs to users was all very well... but what I was more interested in was giving people the option to create their very own emojis. The focus was always on letting users have the freedom of creating stickers that they found funny. I did not want to limit their choices to a few pre-defined sets.
                                <br><br>
                                <center>
				<img src="appstories/veeaieclient.png" alt="Tejmur Sattarov" style="width:20%;"><br> 
				<span style="font-size: 30px;">Tejmur Sattarov</span> <br> 
				<span style="font-size: 25px;">(Senior Art Director; Concept Developer of Veeaie Keyboard)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  
            <div class="col-lg-5">
                <img src="appstories/vk3.png" alt="Veeaie Keyboard">
              </div>
              
            <div class="col-lg-7">
	      <P>The Veeaie Keyboard app uses the built-in camera of the device on which it is installed. Users have to take their own pictures first (yupp, selfies), following which the customization starts. The snap initially appears in a circle, and 4 different effects can be added to it. For further personalization, a person can rub off sections of the picture. Doing so is very simple - the unwanted portions (e.g., blank edges) could be removed by swiping.</p>
            </div>
              
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
The availability of custom emoji packs was a nice idea, with effects like 'Drama' and 'Athletes' bound to tickle the users' imagination. However, I feel that the main USP of Veeaie is going to be the option of creating personal emojis from scratch.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <div class="col-lg-5">
                <img src="appstories/vk6.png" alt="Veeaie Keyboard">
              </div>
            <div class="col-lg-7">
	      <P>While testing the app, our in-house mobile app development team tried out their hands (literally!) on the app. Adding funny effects to our own faces was indeed... funny!</p>
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Emoji-creation is not all that the Veeaie app is about. All purchased emoji packs, as well as the ones self-created, can be used by people, anywhere. After all, it's their creative work, and they have every right to show off a bit.
                                <br><br>
                                <center>
				<img src="appstories/veeaieclient.png" alt="Tejmur Sattarov" style="width:20%;"><br> 
				<span style="font-size: 30px;">Tejmur Sattarov</span> <br> 
				<span style="font-size: 25px;">(Senior Art Director; Concept Developer of Veeaie Keyboard)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Which Apps Could Be Used?</h4> 
            <p>The emojis created with the app and/or the ones bought through in-app purchases can be applied by users on practically any online and offline platform (how about making an emoji the phone desktop?). Emojis made and saved by others can also be viewed, but they cannot be used by a particular user. As soon as an emoji pack is purchased, it is displayed in the user's profile.</p>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#15BFCE;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
Tejmur and I both felt that an app as unique and entertaining as Veeaie deserved perfection in every sense. The first few days went by in creating mockups, and none of them quite seemed right. We kept at it, and finally, the app structure was finalized. I daresay it is a good one.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>The Minor Hiccups</h4> 
            <p>Run-of-the mill apps can be developed in a matter of days. Apps that are more involved, more interesting, require more time to be made. Our developers collectively racked their brains to come up with the right mockup for Veeaie. The inputs from Tejmur helped a lot too. After a week or so, we had a rough idea of how the application would work.</p>
	   </div>
	</div>
     </div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	<h4>Built-In Social Integration In Veeaie</h4>
            <div class="col-lg-5">
                <img src="appstories/vk5.png" alt="Veeaie Keyboard">
              </div>
              
            <div class="col-lg-7">
	      <P>Seamless social integration features have been included in the Veeaie Keyboard application. Emojis created/purchased on the app can be posted on the Facebook and Instagram profiles. Just as Tejmur had wished, users have every opportunity to showcase their emoji-making proficiency to their contacts.</p>
            </div>
              
	   </div>
	</div>
     </div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>