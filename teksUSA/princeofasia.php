<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Prince Of Asia</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="princeofasia_story" style="height: 50%;">
        <div class="princeofasia_story-body">
            <div class="container" style="margin-top: 12%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Prince Of Asia</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">

	  			<div class="col-lg-5">
				<p>Who doesn't like a story filled with magic, fantasy and adventure? Wouldn’t it be just great if a mobile strategy game had generous dollops of such features? Of course it would - and only recently, our mobile app company had the opportunity to work on an app just like that. The name of the app is ‘Prince Of Asia’ - and it won’t be overstating facts if we refer to it as one of the best entries in our overall game development portfolio.</p>
		  		</div>

		  		<div class="col-lg-7">
				<img src="appstories/pa1.png" alt="Prince Of Asia">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section style="background: #f9df6a">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
With all due respect to endless running games and shooting games, I feel that a certain kind of monotony sets in after playing them for a while. With Prince Of Asia, we were going for a game that would keep users engaged throughout.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<h4>The Story Behind Prince Of Asia</h4>
				<p>Any good mobile strategy-based game needs a strong, interesting back story. With Prince Of Asia, we spent several weeks to finetune the underlying fairytale that underlines its gameplay. After several rounds of brainstorming (more like fun discussions on magic!), we decided to make the chief protagonist a prince and a master-archer - with the game being based on the challenges he faces. It was to be made, for the time being, only for the iOS platform.</p>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<h4>The Gameplay</h4>
	  			<div class="col-lg-7">
				<img src="appstories/pa2.png" alt="Prince Of Asia">
		  		</div>
	  			<div class="col-lg-5">
				<p>The overall concept of the Prince Of Asia game has got to be one of the most interesting ones we have worked on in recent years. It involves a fairy prince hero, clearing challenges and defeating villains, in his quest for rescuing the abducted princess from an enchanted underworld castle. It presented several cool new twists on the classic ‘prince-rescuing-princess’ tale - and our even our iPhone game developers fell in love with the idea from the very start.</p>
		  		</div>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #f9df6a">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
There’s this thing I believe - if I don’t like the concept behind a mobile software...be it a game or any other app - I cannot expect other people to like it much either. With Prince Of Asia, I knew that I had a winner on my hands - for the story behind it was time-tested and eternally popular, with some interesting new spins. The belief in the game’s success was always there.
                <br><br>
                <center>
					<span style="font-size: 25px;">Concept Owner, Prince Of Asia</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-5">
				<p>Probably the most fun part during the development stages of this iOS game was creating the characters for it. We sketched out figures, cancelled them, did some more sketching...and this went on until all the characters (right from the hero to the master villain) appeared right in sync with the overall mood and environment of the game. If we can say so ourselves, our in-house UI/UX developers were at their creative best for this one.</p>
		  		</div>

		  		<div class="col-lg-7">
				<img src="appstories/pa3.png" alt="Prince Of Asia">
		  		</div>

		  	</div>
		</div>
	</div>
</section>


<section style="background: #f9df6a;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
A strategy-based mobile game cannot afford to be too easy...unless it is meant only for infants. On the other hand, a convoluted, insanely difficult game would not find many takers either. It was important to strike a balance between the two extremes, and I feel that in Prince Of Asia, we have done it just right.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">

	  		<div class="col-lg-12">
				<h4>The Difficulty</h4>
				<p>After several rounds of deliberation between the client and our game development experts, it was decided that the game would have 7 different levels (which the hero would have to cross, to fulfill his objective). From wind deserts, water worlds and enchanted forests, to caves, dark worlds and snow worlds - Prince Of Asia has one deliciously charming level after another.</p>

				<img src="appstories/pa4.png" alt="Prince Of Asia">

				</div>
		</div>
	</div>
</section>

<section style="background: #f9df6a">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
I had never intended to make Prince Of Asia just another mindless shooting game. Instead, I wanted each level to be woven within the story - each level that would present challenges of its own. Credit to Hussain and his team of app developers - they got what I wanted, and gave shape to my ideas rather brilliantly.
                <center>
					<span style="font-size: 25px;">Concept Owner, Prince Of Asia</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<img src="appstories/pa5.png" alt="Prince Of Asia">
		  		</div>
	  		<div class="col-lg-5">
				<p>Each of the seven levels in the game has its own ‘Master Villain’ (who is lovingly(!) called ‘BOSS’). The hero has to defeat the BOSS at each level, to proceed to the next level. It does sound rather simplistic - but it’s the minute details about the game that makes Prince Of Asia so interesting. Things are not as easy as it seems!</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #f9df6a">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
Apart from the generic magic powers and potions, the Prince Of Asia game has a secret trick for players to clear the levels. In every level, there is a ‘Power Arrow’, which a player needs to collect. That weapon, and that weapon only, can kill the BOSS in the next level and help the hero survive.
                <br><br>
                <center>
					<span style="font-size: 25px;">Concept Owner, Prince Of Asia</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
                <h4>The Tricks & Strategies</h4>
	  			<div class="col-lg-5">
				<p>Our iPhone app developers, graphic designers and testers  had to bring their heads together as a team - to list out the range of hidden tricks and moves we had to include in each level. We received valuable inputs from the owner of the app idea as well. The protagonist of Prince Of Asia had was a great archer and all that - but we had to add some magical elements to the game too, without going overboard.</p>
		  		</div>

		  		<div class="col-lg-7">
				<img src="appstories/pa6.png" alt="Prince Of Asia">
		  		</div>

		  	</div>
		  	<div class="col-lg-12">
		  		<p>The idea of having a ‘Power Arrow’ in each level, which HAS to be collected by the hero, was indeed a cool addition to the game. It gave the players a definite objective in each level, at every stage of gameplay. It was not only about beating the main villain and rescuing the princess.</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #f9df6a">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote style="color: #282828;">
I have come across many free iOS and Android games, which have a handful of easy levels to get things started - and then prompt users to purchase stuff, in order to proceed to higher levels. In Prince Of Asia, we have not gone for any such shortcut money-earning strategy. The in-app purchases that are available are helpful, but users can play along without opting for them.
				<center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>
		  	</div>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<h4>The In-App Purchases</h4>
	  			<div class="col-lg-5">
				<p>There are special coins that can be bought via in-app purchase in Prince of Asia. With these coins, players can buy more potent, powerful weapons to take on the increasingly more threatening ‘BOSS’-es in the different levels. Our iOS game developers made sure that users would face no glitches whatsoever - while completing these purchases.</p>
		  		</div>

		  		<div class="col-lg-7">
				<img src="appstories/pa7.png" alt="Prince Of Asia">
		  		</div>

		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
                <h4>The Final Word</h4>
				<p>Prince Of Asia is not, and was never intended to be, an ‘endless’ game. Instead, we focused on making each and every aspect of the game - from the get-up and powers of the chief protagonist and the story behind his quest, to the screens for each levels and the overall gameplay architecture - user-friendly, engaging, delightful. It is not just another mobile app for kids - people from all ages can try their hands on it.</p>
		        <p>The Prince Of Asia game will soon be available for download at the app store. It was fun creating this iOS app - and we are more than certain of its popularity among users.</p>
		        <p>Fairytale magic - right on iPhones!</p>
		   </div>
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>
