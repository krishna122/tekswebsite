<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'team'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <?php include 'headtag.php';?>
</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>

 <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Team</h2>
                </div>
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3 animate-hover-slide-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">   
                	<div class="col-md-6">
                        <div class="w-box inverse">
                            <div class="figure">
                              <center>  <img alt="" src="images/prv/hussain.png" class="img-responsive" style="height:50%; width:50%;"></center>
                                <div class="figcaption">                                    
                                    <ul class="social-icons text-right">
                                    	<li class="text pull-left">Reach Hussain on:</li>
                                        <li class="facebook"><a href="https://www.facebook.com/hussain.fakhruddin" target="blank"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/hussulinux"><i class="fa fa-twitter" target="blank"></i></a></li>
                                        <li class="linkedin"><a href="https://www.linkedin.com/in/hussainfakhruddin" target="blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <center><h2>Hussain Fakhruddin<small>CEO - Teknowledge Software</small></h2></center>
                            <p>
                            Entrepreneur and mobile applications architect. Hussain has been heading the India chapter of Teknowledge for more than 8 years.

                            </p>
                        </div>
                    </div>

               
                    
                  
                    
                    <div class="col-md-6">
                        <div class="w-box inverse">
                            <div class="figure">
                              <center>  <img alt="" src="images/prv/blumanis.jpg" class="img-responsive" style="height:50%; width:50%;"></center>
                                <div class="figcaption">                                    
                                    <ul class="social-icons text-right">
                                    	<li class="text pull-left">Reach Amber on:</li>
                                        <li class="facebook"><a href="https://www.facebook.com/amber.grace.14" target="blank"><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/AmberBlumanis" target="blank"><i class="fa fa-twitter"></i></a></li>
                                        <li class="linkedin"><a href="https://www.linkedin.com/pub/amber-blumanis/96/638/978" target="blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                           <center> <h2>Amber Blumanis<small>COO - Teks Mobile Australia Pty Ltd</small></h2></center>
                            <p>
                            Award-winning mobile app expert, and the chief operating officer of Teks Mobile Australia. Amber is an authority in online advertising, marketing and client relations. 
                            </p>
                        </div>
                    </div>
        
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-5 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <blockquote>
                            Great things in business are never done by one person, they are done by a team of people – Steve Jobs
                        </blockquote>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-four btn-lg pull-right" title="" href="requestquote.php" target="blank">
                        Request A Quote
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
       
    <section class="slice bg-3 animate-hover-slide-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">   
                
                
                <div class="col-md-12">
                        <div class="w-box inverse">
                            <div class="figure">
                                <img alt="" src="images/prv/teks.jpg" class="img-responsive">
                                
                            </div>
                         
                        </div>
                    </div>
     </section>
     
                      
    <section class="slice bg-5 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <blockquote>
                            A group becomes a team when each member is sure enough of himself and his contribution to praise the skill of the others.
                        </blockquote>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-four btn-lg pull-right" title="" href="testimonial.php" target="blank">
                        What Client Says
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
     <section class="slice bg-3 animate-hover-slide-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                               
                	<div class="col-md-6">
                        <div class="w-box inverse">
                            <div class="figure">
                                <img alt="" src="images/prv/collage1.jpg" class="img-responsive">
                                
                            </div>
                         
                        </div>
                    </div>

                    
                    
                    <div class="col-md-6">
                        <div class="w-box inverse">
                            <div class="figure">
                                <img alt="" src="images/prv/collage2.jpg" class="img-responsive">
                                
                            </div>
                            
                        </div>
                    </div>
                    
                  
        
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>
                            