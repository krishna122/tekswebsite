  <section id="asideMenu" class="aside-menu" >
	<form class="form-inline form-search">
        <div class="input-group">
            
            <span class="input-group-btn" style="text-align: right;">
                <button id="btnHideAsideMenu" class="btn btn-close" type="button" title="Hide sidebar"><i class="fa fa-times"></i></button>
            </span>
        </div>
    </form>
	
    <div class="nav">
    	<ul>
        	<li>
            	<a href="index.php">Home</a>
            </li>
            <li>
            	<a href="aboutus.php">About us</a>
            </li>
            <li>
            	<a href="iphone.php">Portfolio - iPhone</a>
            </li>
            <li>
            	<a href="android.php">Portfolio - Android</a>
            </li>
            <li>
            	<a href="blog" target="_blank">Blog</a>
            </li>
            <li>
            	<a href="work.php">How We Work</a>
            </li>
            <li>
            	<a href="advantage.php">Teks Advantage</a>
            </li>
             <li>
            	<a href="requestquote.php">Request A Quote</a>
            </li>
            <li>
            	<a href="faq.php">Hire Us- FAQ</a>
            </li>
        </ul>
    </div>
    
    <h5 class="side-section-title">Social media</h5>
    <div class="social-media">
        <a href="https://www.facebook.com/TeksMobileAustralia" target="blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/u/0/b/109837041891616325544/" target="blank"><i class="fa fa-google-plus"></i></a>
                    <a href="https://www.linkedin.com/pub/teks-mobile-australia/9a/b07/44a" target="blank"><i class="fa fa-linkedin"></i></a>
                    <a href="https://twitter.com/TeksMobileAus" target="blank"><i class="fa fa-twitter"></i></a>
                    <a href="http://instagram.com/teks_mobile_australia" target="blank"><i class="fa fa-instagram"></i></a>
                    <a href="http://www.pinterest.com/teksmobileaustr/" target="blank"><i class="fa fa-pinterest"></i></a>
                    <a href=""><i class="fa fa-youtube-play"></i></a>
                    <a href="https://www.behance.net/teknowledge" target="blank"><i class="fa fa-flickr"></i></a>
    </div>
    
    <h5 class="side-section-title" >Contact information</h5>
    <div class="contact-info">
        <h5 style="text-decoration:underline;">Address:</h5>
        <p>13 Maxwell Crescent, Stanwell Park, NSW, 2508, Australia</p>
        
        <h5>Email</h5>
        <p>amber@teksmobile.com.au</p>
        
        <h5>Phone</h5>
        <p>+61 04 235 21035</p>
    </div>
</section>





                            
                            
                            