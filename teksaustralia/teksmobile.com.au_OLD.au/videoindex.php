    <section class="slice bg-banner-1">
    	<div class="mask-dark"></div>
        <div class="w-section inverse">
        
        <div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4><i class="fa fa-quote-left fa-3x"></i></h4>
              <h2>Stay Ahead Of The Game</h2>
            <p>
                Teks Mobile Australia focuses on making iPhone/Android apps that would help you in every walk of life. Find out some of the exclusive app benefits we offer - right here.
            </p>
            
        </div><!--.col -->
        
        <div class="col-md-8">
            <div class="vid">
                <iframe width="560" height="315" src="//www.youtube.com/embed/k9XnbReyn0w" allowfullscreen=""></iframe>
            </div><!--./vid -->
            
        </div><!--.col -->
        
    </div><!--./row -->
    
</div><!--./container -->


            
            </div>
        </div>
    </section>
    
    <style>
    
    .vid {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px; height: 0; overflow: hidden;
}
 
.vid iframe,
.vid object,
.vid embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

    </style>
    
    