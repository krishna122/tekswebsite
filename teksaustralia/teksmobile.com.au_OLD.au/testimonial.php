<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'testimonial'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="css/testimonial.css" type="text/css">
   <?php include 'headtag.php';?>


</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>What Clients Say </h2>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <br><br>
    <!-- Testimonials -->
	    <div class="section">
			<div class="container">
				
				<div class="row">
					<!-- Testimonial -->
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<a href=""><img src="images/prv/pocket.png" alt="Pocket Navigator"></a>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<!-- Quote -->
								<p class="quote">
		                            "I got an app called Pocket Navigator from this iPhone app company in Australia, about three months back. It’s easy-to-use - and the best thing about it is that, it provides detailed route maps to and from any place! For a frequent traveler (some call me overenthusiastic!), the app has really done away with risks of getting lost in an unknown land. In May, I visited Birmingham - and Pocket Navigator was my road-guide at every point. Am off to Florida next month - and yepp, I will again be relying on it for directions. Thanks Teks for thinking about compulsive travelers like me!"
                        		</p>
                        		<!-- Author Info -->
                        		<cite class="author-info">
                        			- Myra Forrest,<br>Ascot, Brisbane, Queensland
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
                    <!-- End Testimonial -->

                    <div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<a href="https://itunes.apple.com/au/app/stopover/id874807308?mt=8" target="blank"><img src="http://www.teksmobile.com.au/teksmobileau/uploadlogo/logo_1399292064.png" alt="Stopover"></a>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "Ever arrived at an airport nice and early - only to hear the announcement that your flight has been delayed by a couple of hours? Well, I have - and let me tell you it’s a frustrating experience. I would have probably died of boredom while waiting for a flight at the Vietnam Airport last month - if I did not have the Stopover app on my handset. Since my flight was delayed by four hours, I connected with a pretty local lady via the application and had a lovely luncheon date with her. Now I know one thing for certain: I won’t ever get bored again at airports!"
                        		</p>
                        		<cite class="author-info">
                        			- Kiara O’Keefe,<br>Dunedin, New Zealand
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<a href="https://itunes.apple.com/us/app/story-time-for-kids/id410788584?mt=8" target="blank"><img src="http://www.teksmobile.com.au/teksmobileau/uploadlogo/story-time_1389273044.png" alt="Storytime for kids"></a>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "My colleagues kept saying that I should download educational apps for my preschool kid - but till even two months back, I was not convinced. While casually browsing through the portfolio of this iPhone app development company in Sydney, I came across the Story Time For Kids app. The illustrations looked interesting, and since it was free - I decided to get it. To my surprise and Angela(my daughter)’s delight - there were more than 30 original shorts in digital format available on the app. I also found a HUGE stock of classic fairytale books, priced at low-ish levels. Let’s just say Angela now has a new best friend, and I have started believing that mobile apps for kids are good."
                        		</p>
                        		<cite class="author-info">
                        			- Samantha Jones,<br>Balgowlah Heights, Sydney, New South Wales
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
				</div>
			</div>
	    </div>
	    <!-- End Testimonials -->
	    
	    
	    <br><br>
    <!-- Testimonials -->
	    <div class="section">
			<div class="container">
				
				<div class="row">
					<!-- Testimonial -->
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<a href="https://itunes.apple.com/us/app/gymking/id883230138?mt=8" target="blank"><img src="images/prv/144.png" alt="Gym King"></a>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<!-- Quote -->
								<p class="quote">
		                            "I get a kick from finding out how my fitness levels are increasing over time. Gym King - an app created by Teks Mobile Australia - allows me to do just that. With it, I can compare my fitness rank with that of the others exercising at the same gym. The best thing about this mobile application? Erm...that has to be the feature by which it allows me to brag about my fitness on Facebook!"
                        		</p>
                        		<!-- Author Info -->
                        		<cite class="author-info">
                        			-  Gavin Cottrell,<br> Lancashire, England, United Kingdom
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
                    <!-- End Testimonial -->
                    <div class="testimonial col-md-4 col-sm-6">
					<div class="author-photo">
							<a href="https://itunes.apple.com/us/app/draw-apart/id807485063?mt=8" target="blank"><img src="http://www.teksmobile.com.au/teksmobileau/uploadlogo/152_1395314776.png" alt="Drawapart"></a>
						</div>	
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "My son is a big - no, huge - fan of new mobile apps. He downloads practically every app that arrives at iTunes, and more often than not, he starts cribbing about their slowness or technical glitches within a day. Last month, he excitedly informed me about this new iPhone app company in Australia, which had launched four new applications. With no complaints coming in for more than a fortnight (something not in sync with Alex’s nature!) - I took a peek at the apps. There were four of them - Real Talk, Draw Apart, Melhor Oferta and Pocket Navigator. Later on, my son informed me that he had finally chanced upon apps that met up to his high quality expectations. Good for him...and muchas gracias, Teks!"
                        		</p>
                        		<cite class="author-info">
                        			- Felix Sanchez,<br>Carmona, Sevilla, Spain
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
					<div class="testimonial col-md-4 col-sm-6">
						<div class="author-photo">
							<a href="https://itunes.apple.com/us/app/timesnaps/id843757664?ls=1&mt=8" target="blank"><img src="http://www.teksmobile.com.au/teksmobileau/uploadlogo/logo_1399291901.png" alt="Timesnaps"></a>
						</div>
						<div class="testimonial-bubble">
							<blockquote>
								<p class="quote">
		                            "I have used the Timesnaps app created by Teks - and it took my idea about photography to a whole new level. Now, I travel quite a lot, and this app lets me set reminders about the pictures I wish to click. I generally share my snaps directly on Facebook. During my January trip to Madrid, I also created a fun travel video with this app. At first, I was slightly confused regarding how to upload this video on YouTube. I called up the app developers at Teks, and they gave me step-by-step directions. Timesnaps is now an app that is, and will always be, present on my iPhone. It’s a work of wonder!"
                        		</p>
                        		<cite class="author-info">
                        			- Patricia Lagarde,<br>Ano Poli, Patras, Greece
                        		</cite>
                        	</blockquote>
                        	<div class="sprite arrow-speech-bubble"></div>
                        </div>
                    </div>
				</div>
			</div>
	    </div>
	    <!-- End Testimonials -->
	    

<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                           
                            