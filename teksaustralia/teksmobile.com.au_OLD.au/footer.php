<footer>
<div class="container">
        <div class="row">
            <div class="col-md-3">
            	<div class="col">
                   <h4>Contact us</h4>
                   <ul>
                        <li>13 Maxwell Crescent, Stanwell Park, NSW, 2508, Australia</li>
                        <li>Phone: +61 280 152 840</li>
                        <li>Email: <a href="mailto:info@teksmobile.com.au" title="Email Us">info@teksmobile.com.au</a></li>
                        <li>Creating great Apps is our passion</li>
                    </ul>
                 </div>
            </div>
            
            <div class="col-md-3">
            	<div class="col">
                    <h4>Mailing list</h4>
                    <p>Sign up if you would like to receive occasional treats from us.</p>
                    <form class="form-inline">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Your email address..." id="newsletter">
                            <span class="input-group-btn">
                                <button class="btn btn-two" type="button">Go!</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            
   
            <div class="col-md-3">
            	<div class="col col-social-icons">
                    <h4>Follow us</h4>
                    <a href="https://www.facebook.com/TeksMobileAustralia" target="blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/u/0/b/109837041891616325544/" target="blank"><i class="fa fa-google-plus"></i></a>
                    <a href="https://www.linkedin.com/pub/teks-mobile-australia/9a/b07/44a" target="blank"><i class="fa fa-linkedin"></i></a>
                    <a href="https://twitter.com/TeksMobileAus" target="blank"><i class="fa fa-twitter"></i></a>
                    <a href="http://instagram.com/teks_mobile_australia" target="blank"><i class="fa fa-instagram"></i></a>
                    <a href="http://www.pinterest.com/teksmobileaustr/" target="blank"><i class="fa fa-pinterest"></i></a>
                    <a href=""><i class="fa fa-youtube-play"></i></a>
                    <a href="https://www.behance.net/teknowledge" target="blank"><i class="fa fa-flickr"></i></a>
                </div>
            </div>

             <div class="col-md-3">
             	<div class="col">
                    <h4>About us</h4>
                    <p>
                    Teks Mobile Australia brings to you a wealth of customized mobile apps. We care for your projects.
                    <br><br>
                    <a href="testimonial.php" class="btn btn-two">View Our Testimonial</a>
                    </p>
                </div>
            </div>
        </div>
        
        <hr>
        <br>
        <div class="row">
        	<div class="col-lg-9 copyright">
            	2014 &copy; Teks Mobile Australia Pty Ltd. All rights reserverd. 
                <a href="terms.php">Terms & Conditions</a> | 
                <a href="policy.php">Privacy policy</a> |
                <a href="vision.php">Vision & Mission</a>
            </div>
            <div class="col-lg-3 footer-logo">
            	
            </div>
        </div>
    </div>
    <br><br><br>
</footer>



                                     
<script type='text/javascript'>(function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://widget.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({ c: 'ca68bb8f-8840-4bc0-9252-05c554c3b0d6', f: true }); done = true; } }; })();</script>