<section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="section-title"><span>Our Company</span></h3>
                        <div class="widget">
                            <div class="tabs">
                                
                                <div class="tab-content tab-content-inverse">
                                    <div class="tab-pane active" id="tab2-1">
                                        <p>
                                        Teks Mobile is one of the most trusted mobile app companys in Australia. We specialize in developing high-quality, user-friendly applications for the iOS and Android platforms. All apps are developed as per the budget specifications, preferred layouts, and other considerations of clients. We work on mobile projects across 18 time zones. Our in-house app developers are dedicated to serve people from all over the world.
                                        </p><br>
                                        <blockquote>
                                        Together with our India office, our mobile app company presently has a total application count of more than 600. We promise quality service, smarter apps, and unmatchable prices.
                                        </blockquote>
                                    </div>
                                    
                                </div>							
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <br><br>
                        <div class="widget">
                            <div id="photoCarousel" class="carousel carousel-4 slide" data-ride="carousel">
                                
                                <div class="carousel-inner">
                                    <div class="item active">
                                    	<img src="images/prv/teksaustraliacake.jpg" alt="" class="img-responsive">
                                        
                                    </div>
                                    <div class="item">
                                        <img src="images/prv/new.png" alt="" class="img-responsive">
                                       
                                    </div>
                                </div>
                                   
                                <a class="left carousel-control" href="#photoCarousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right carousel-control" href="#photoCarousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>    
    </section>
                            
                            