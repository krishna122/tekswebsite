<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'vision'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <?php include 'headtag.php';?>
</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>

 <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">Our Vision</h3>
                        <p>
                        “To become the most trusted Australian mobile application development company, by making optimized, user-friendly and reasonably priced iPhone and Android apps for clients across the globe, and by ensuring that all our apps deliver excellence in terms of quality.”
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">Mission Statement</h3>
                        
                        	<ul class="list-check">
                                        <li><i class="fa fa-check"></i>To constantly expand our portfolio of iOS and Android apps.</li>
                                        <li><i class="fa fa-check"></i> To stay updated about all the latest frameworks, techniques and coding methods for mobile app development.</li>
                                        <li><i class="fa fa-check"></i> To recruit and retain the best mobile app developers in our team.</li>
                                        <li><i class="fa fa-check"></i> To respond to all client queries within a maximum of 24 hours (including delivery of free app quotes).</li>
                                        <li><i class="fa fa-check"></i>To keep track of app development cost figures at all stages. </li>
                                        <li><i class="fa fa-check"></i>To take full responsibility of delegated projects. </li>
                                        <li><i class="fa fa-check"></i>To maintain due integrity and transparency about all our operations. </li>
					<li><i class="fa fa-check"></i>To remain accessible to our clients on a 24x7 basis. </li>
					<li><i class="fa fa-check"></i>To actively seek feedback, opinions and suggestions from the users of our apps. </li>
					<li><i class="fa fa-check"></i>To pursue mobile app testing routines that are at par with the best industry practices. </li>
					<li><i class="fa fa-check"></i>To share knowledge from the domain of mobile technology, among peers and customers. </li>
                         	</ul>
                        
                  <p>Adherence to ethical business principles and practices has been one of our biggest strong points over the last 8 years. We are confident that, with the continuous support from our partners, developers, and of course, clients - we will achieve our vision of becoming Australians most trusted mobile application development company.</p>
                    </div>
                    
                    
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    
    </section>
  
<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>