<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'terms'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

   <?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Terms & Conditions</h2>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5>
</h5>
                        <ul class="list-check">
                                        <li>Welcome to the official website of Teks Mobile Australia. Our mobile app agency follows a clearly specified code of ethics, to ensure transparency in all our operations. In return, we expect you to abide by our terms of service, as listed below. If you have any further doubts, kindly email us at <strong>hello@teksmobile.com.au.</strong></li>

                                        <li><i class="fa fa-check"></i> By accessing our website, you inherently agree to all our terms and conditions. We suggest that you go through all the clauses here, before navigating to any other page. In case you do not agree to any of the terms, it would be advisable to not proceed on our website.</li>

                                        <li><i class="fa fa-check"></i> Kindly note that the terms ‘Teks Mobile Australia Pty Ltd.’, ‘Teks Mobile Australia’, ‘Teks Mobile’ and ‘Teks’ refer to one and the same entity - our mobile app company in Australia. It is not to be confused with the name of our Indian chapter - ‘Teknowledge Software’/’Teknowledge Mobile Studio’.</li>

                                        <li><i class="fa fa-check"></i> Although we track all information published about Teks Mobile Australia on the World Wide Web, we do not take any responsibilities for misleading/erroneous published on any other site/portal/blog. The information provided on the website will prevail if any conflict of interest arises.</li>

                                        <li><i class="fa fa-check"></i> You have to be at least 18 years of age to request app quotes from us. We do create customized mobile apps for kids as well - but when getting quotes, you will need to specify your age.</li>

                                        <li><i class="fa fa-check"></i> We welcome feedback, suggestions and app-reviews on our website as well as on our social media profile pages. Kindly refrain from posting spam content, profanities, and abusive language anywhere. Violation of this policy can lead to offending users getting permanently banned.</li>

                                        <li><i class="fa fa-check"></i>Teks Mobile Australia Pty Ltd., together with Teknowledge Software (our Indian chapter), has an overall app-approval rate of more than 80% (at iTunes and Play Store). However, our Android and iPhone app developers do not provide any guarantee about any new applications’ acceptance at the stores.</li>

                                        <li><i class="fa fa-check"></i>We take pride in delivering mobile applications of the finest quality. If any app developed by us turns out to be faulty, we take full responsibility of replacing it - at no extra cost. No replacements/refunds will be done though, if any anomalies are found in the usage/installation procedure of the apps. </li>

                                        <li><i class="fa fa-check"></i>The final payments you will have to make will always be in accordance with the free app quote provided earlier. Additional carrier charges and other add-on costs are not accounted for in our estimates. </li>

                                        <li><i class="fa fa-check"></i>You will need to have valid user-accounts at iTunes and Google Play Store, to be able to download our apps. </li>

                                        <li><i class="fa fa-check"></i>In order to get the best mobile apps in Australia from Teks Mobile, kindly make sure that all the details you provide on our Request A <a href="requestquote.php" style="text-decoration:underline;">Quote</a> form are accurate. We cannot take any responsibility for erroneous/false information submitted by users. If you are submitting app layouts, please make sure that you have the legal rights to use them. </li>

                                        <li><i class="fa fa-check"></i>Teks is a mobile app agency in Australia that serves human clients and not bots (you probably already knew that, right?).</li>

                                        <li><i class="fa fa-check"></i>Every app project of Teks Mobile Australia Pty Ltd. is unique. We retain all intellectual rights on the concepts, UI/UX designs, wireframes and beta-prototypes - until the final version of the app is delivered to the client. Any willful replication of our designs/concepts is punishable in a court of law. </li>

                                        <li><i class="fa fa-check"></i>We have a fixed payment structure (30% upfront, 30% midway through the project, and the rest upon completion) for our mobile app development projects. Failure to comply with this payment structure or the payment structure agreed to prior to starting the project would lead to projects being shelved/scrapped. We believe in treating every client on an equal footing - so no exceptions, on any ground, are made. </li>

                                        <li><i class="fa fa-check"></i> Our mobile app portfolio has a large number of applications on which we have trademark and sole copyrights. You can download them from stores and use them on your mobile devices, of course. Refrain from violating any copyright law. Such activities amount to violation of intellectual property rights.</li>

                                        <li><i class="fa fa-check"></i>Teks Mobile offers mobile software development services in Sydney and all other major Australian cities, in accordance with the latest app development frameworks, systems and device settings (as per research conducted across Australia). If our apps do not function properly due to defects in your mobile device(s) - we cannot be held responsible for that. </li>

                                        <li><i class="fa fa-check"></i> You have the rights to ask for a non-competing agreement being issued on behalf of our iPhone and Android app developers. Please note that such agreements would be binding evidence of transfer of intellectual rights (from Teks to you).</li>

                                        <li><i class="fa fa-check"></i>Clients can also cancel projects after we have started working on them. In such cases, the upfront payment (and other further payments, if any) would be non-refundable. Fortunately, such a scenario has never occurred. </li>

                                        <li><i class="fa fa-check"></i>Teks Mobile Australia is linked with other websites/press release portals/classified ad sites and social media channels. We are, in no way, related to any objectionable/erroneous advertisements or other content that might be present in such third-party websites. </li>

                                        <li><i class="fa fa-check"></i>To provide you with personalized services, we collect certain personal information about you. All the collected details are stored in our secure database - with zero risks of unauthorized access. You can check the type of information we collect by heading over to our <a href="requestquote.php" style="text-decoration:underline;">Privacy Policy</a> page.</li>

                                        <li><i class="fa fa-check"></i>Users need not go through elaborate registration/account-creation processes on our website. However, you are solely responsible for making sure that no one else submits your details to us without your consent.</li>


                                         <li>Help us ensure you receive the best client experience by abiding by these simple and user-oriented terms and conditions. At Teks Mobile Australia, business ethics is prime importance.</li>


                         </ul>
                        
                  
                    </div>
                    
                    
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    
    </section>

<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                           
                            