<div class="wrapper">
	<!-- This section is only for demonstration purpose only. Just remove the div "divStyleSwitcher" -->
<!-- Top Header -->
<div class="top-header">
	<div class="container">
        <div class="row">
            <nav class="top-header-menu">
                    <ul class="menu" >
                        <li>Welcome to Teks Mobile Australia Pty Ltd : info@teksmobile.com.au or +61 280 152 840</li>
                        
                    </ul>
</nav>
        </div>
    </div>
</div>
<!-- Header: Logo and Main Nav -->
<header>    
	<div id="navOne" class="navbar navbar-wp" role="navigation">
        <div class="container">
            <div class="navbar-header">
            	<button type="button" class="navbar-toggle navbar-toggle-aside-menu">
                    <i class="fa fa-outdent icon-custom"></i>
                </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" title="Teks Mobile Australia">
                	<img class="img-responsive" src="images/teksaustralialogo.png" alt="Teks Mobile Australia">
                </a>
            </div>
       <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                	<li class="dropdown">
                 <a href="aboutus.php" >About Us</a>
                	</li>
                	
                    <li class="dropdown" >
                    	<a href="portfolio.php" class="dropdown-toggle" data-hover="dropdown">Portfolio</a>
                    	<ul class="dropdown-menu" style="text-align:center;">
                                	<li><a tabindex="-1" href="iphone.php">iPhone</a></li>
                                    <li><a tabindex="-1" href="android.php">Android</a></li>
                                   <li><a tabindex="-1" href="testimonial.php">Testimonials</a></li> 
                        </ul>
                	</li>
                	<li class="dropdown">
                 		<a href="mobile-apps-and-apis.php" >API</a>
                	</li>
                    <li class="dropdown">
                    	<a href="advantage.php" >Teks Advantage</a>
                	</li>
                    <li class="dropdown">
                    	<a href="work.php" >How We Work</a> 
                	</li>
                    <li class="dropdown">
                    	<a href="team.php" >Our Team</a>
                	</li>
                	
                	
                     <li class="dropdown">
                    	<a href="blog"  target="_blank" >Blog</a>
                	</li>

                	<li class="dropdown">
                    	<a href="contactus.php" class="dropdown-toggle" data-hover="dropdown">Contact Us</a>
                    	<ul class="dropdown-menu" style="text-align:center;">
                               <li><a tabindex="-1" href="requestquote.php">Request A Quote</a></li>
                        </ul>
                	</li>
                	
                    <li class="dropdown">
                    	<a href="faq.php" >FAQs</a> 
                	</li>
                	
                	
                    
          
                    
                    <li class="hidden-xs">
                        <a href="#" id="cmdAsideMenu" class="dropdown-toggle dropdown-form-toggle" title="Open sidebar">
                        	<i class="fa fa-outdent"></i>
                        </a>
                    </li>
                </ul>
               
            </div><!--/.nav-collapse -->
        </div>
    </div>
</header>	
                            
                            
                            