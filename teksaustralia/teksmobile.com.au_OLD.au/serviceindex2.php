<section class="slice bg-5">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">What We Promise</h3>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            
                            
                            <h2>Timely Completion Of Projects</h2>
                            <p class="text-center">Here at Teks we pride in being able to complete every app development project within pre-mentioned deadlines. In all probability, you will get your app a few days before the deadline!</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            
                            
                            <h2>Competitive Pricing</h2>
                            <p class="text-center"> We believe that making an iPhone app or an Android app need not be an expensive task. Our budget plans are extremely reasonable - we trim out all possible additional expenses. You need not fork out hefty advance payments either, We provide a flexible, milestone based payment schedule for all clients. </p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            
                            
                            <h2>Commitment To Quality</h2>
                            <p class="text-center">Mobile app development is much more than a business for us - it’s a passion. That’s precisely why our app developers make sure that there is no scope for complaints about the mobile software/apps they create. Our app testing routines are rigorous, ensuring that you get flawless apps...every time.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            
                            <h2>24x7 Availability</h2>
                            <p class="text-center">We love to hear from you anytime, from any place. Our clientele includes individuals, business houses, academic institutions and other entities - from  diverse geographical locations and time zones. You can chat with one of our representatives directly from our website, right round the clock. </p>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>    
    </section>