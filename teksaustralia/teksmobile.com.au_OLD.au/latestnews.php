<section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h1>
                            Why Settle For Less When You Have The Best Apps...Right Here? 
                        </h1>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-one btn-lg pull-right" title="" href="requestquote.php">
                      Get A Free Quote
                        </a>
                    </div>
                </div>
            </div>
        </div>
</section>
