<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'requestquote'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Request A Quote</h2>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <p>
                        Please fill out the form below to request a free quote. We will get back to you with our Client Questionnaire in order to gain greater scope of your project and provide you with an accurate & detailed quote, usually within 24 hours!
                        </p>
                        
                        <form class="form-light mt-20" role="form" enctype="multipart/form-data" name="forminfo" id="forminfo">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Your name" name="quote_name" id="quote_name" required>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email address" name="quote_email" id="quote_email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" placeholder="Phone number" name="quote_phone" id="quote_phone" >
                                    </div>
                                </div>
                            </div>
                            
                                                       
                            <div class="row">
                            
                            <div class="col-md-3">
                                    <div class="for-group">
                                        <label>Need Wireframe</label><br>
                                         
  					&nbsp;&nbsp;<input type="radio" id="checkbox1 " value="yes" name="quote_wireframe">&nbsp;&nbsp; Yes
					
 					&nbsp;&nbsp;<input type="radio"  id="checkbox2 " value="no" name="quote_wireframe">&nbsp;&nbsp; No
					

                                    </div>
                                </div>
                                
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                     <label>Layouts</label>
                                         <input type="file" class="filestyle" placeholder="Browse" name="file" id="file" >
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Approximate budget</label>
                        <select id="cmbNavigationStyle" class="form-control" title="Approximate budget" name="quote_budget">
                            <option value="NO Budget">Select your budget</option>
                            <option value="Less than US $3k ( Simple 2-3 screens with back-end )" >Less than AUS $3k ( Simple 2-3 screens with back-end )</option>
                            <option value="AUS $5k ( under 10 screens with back-end)" >AUS $5k ( under 10 screens with back-end) </option>
                            <option value="AUS $8k-10k ( up to 20 Screens with back-end )" >AUS $8k-10k ( up to 20 Screens with back-end )</option>
                            <option value="AUS $10k+ ( Robust App with back-end )" >AUS $10k+ ( Robust App with back-end )</option>
                            <option value="AUS $20k+ ( Full product development )" >AUS $20k+ ( Full product development )</option>
                        </select>
                                    </div>
                                </div>
                            </div>
                            
                      
                    <div class="form-group">
                                <label>Describe App</label>
                                <textarea class="form-control" placeholder="Write about app..." style="height:100px;" name="quote_describe" id="quote_describe"></textarea>
                            </div>
                            <input type="hidden" name="save" value="quote">
                            <button type="submit" class="btn btn-lg btn-two ">Request Quote</button>
                        </form>
                    	</div>
                        </div>     
	               </div>
                </div>
            </div>
        </div>
    
    </section>

<?php include 'footer.php';?>

<?php include 'script.php';?>

<script>	

    $('form').on('submit', function(e) {
        e.preventDefault(); 
        var form = $(this); 
        var formdata = new FormData(document.getElementById("forminfo"));
        formdata.append('file', $('#file')[0].files[0]);
       // var post_data = form.serialize()+'&'+formdata;
       formdata.append('quote_budget', $('#cmbNavigationStyle').val());
       formdata.append('quote_phone', $('#quote_phone').val());
       formdata.append('quote_email', $('#quote_email').val());
       formdata.append('quote_name', $('#quote_name').val());
       formdata.append('quote_describe', $('#quote_describe').val());
       formdata.append('quote_wireframe', 'yes');

        $.ajax({
            type: 'POST',
            url: 'quotesubmit.php',
            data: formdata,
            success: function(msg) {
                $(form).fadeIn(500, function(){
             //       $("#thank").fadeIn();
             		alert("Thank you for your quote we will be back to you soon");
             		form.trigger('reset');
                });
            },
         cache: false,
         contentType: false,
         processData: false
        });
    });

   </script>
    
</body>
</html>                           
                            