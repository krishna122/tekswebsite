<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'portfolio'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>
   
</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>

<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Portfolio</h2>
                </div>     
            </div>
        </div>
    </div>


<section class="slice bg-3 animate-hover-slide">
        <div class="w-section inverse work">
            <div class="container">
               
              
                
                <div class="row">  
                
<p>Teks Mobile Australia specializes in providing end-to-end cross-platform mobile application development services. Our app development portfolio currently has more than 600 apps - developed for the iOS and Android platforms</p>

<p>Be it a sports app, a travel application, a mobile IM software or any other customized app - you’ll find them all in our app portfolio. And what if you need something that we have not already developed? That’s easy - contact our app developers in Sydney, and we will make your ideas and concepts come alive.</p>




   <ul class="list-check">
                                        <li>Each of the apps in our portfolio is:</li>
                                        <li><i class="fa fa-check"></i> Developed with the latest app development SDKs.</li>
                                        <li><i class="fa fa-check"></i> Focused to serve a precise, important need.</li>
                                        <li><i class="fa fa-check"></i> Tested for bugs at multiple stages.</li>
                                        <li><i class="fa fa-check"></i> Easy to install and operate.</li>
                                        <li><i class="fa fa-check"></i> Very economical in their consumption of mobile bandwidth.</li>
                                       	<li><i class="fa fa-check"></i> Of an attractive appearance - thanks to the efforts of our mobile app designers.</li>
					<li><i class="fa fa-check"></i> Easily visible at the app stores (iTunes/Play Store).</li>
					<li><i class="fa fa-check"></i> Compatible with the latest mobile OS platform versions.</li>
					<li><i class="fa fa-check"></i> Geared to add value to your smart device.</li>
					
                         </ul>

<p>Catch a glimpse of our best-received Android apps.</p>

<p>Check out the most popular iPhone applications created by us.</p>
<h2>Get in touch with us...Teks Mobile Australia will make things happen!</h2>

            </div>
                                    
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>