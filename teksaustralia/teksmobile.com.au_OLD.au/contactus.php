<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://www.teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'contactus'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php include 'script.php';?>
    
   <?php include 'headtag.php';?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script type="text/javascript">
function initialize() {
  var myLatlng = new google.maps.LatLng(-34.227783, 150.991770);
  var mapOptions = {
    zoom: 15,
	scrollwheel: false,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
	  animation: google.maps.Animation.DROP,
      title: 'Teksmobile Australia'
  });
  
  var contentString = '<div class="info-window-content"><h2>Web Pixels</h2>'+
  					  '<h3>Designing forward</h3>'+
					  '<p>Some more details for directions or company informations...</p></div>';
					  
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Reach Us</h2>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h3 class="section-title">Send Us A Message</h3>
                        <p>
                       Whether you have a question, require more information, would like a quote or would like to provide us with your valued feedback, simply send us a message below we will get back to you, within 24 hours
                        </p>
                        
                        <form class="form-light mt-20" role="form" id="contactform">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Your name" name="contact_name" required>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email address" name="contact_email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="tel" class="form-control" placeholder="Phone number" name="contact_phone" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" class="form-control" id="subject" placeholder="Subject" name="contact_subject" required>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" placeholder="Write you message here..." style="height:100px;" name="contact_message" required></textarea>
                            </div>
                            <input type="hidden" name="submit" value="contact">
                            
                            <button type="submit" class="btn btn-two">Send message</button>
                        </form>
                    </div>
                    
                    
                    
     <script type="text/javascript">               
                    
    $('#contactform').on('submit', function(e) {
        e.preventDefault(); 
        var form = $(this); 
      
        var post_data = form.serialize();

        $.ajax({
            type: 'POST',
            url: 'contactsubmit.php',
            data: post_data,
            success: function(msg) {
                $('#contactform').fadeIn(500, function(){
            	alert("Thank you for contacting us we will get back to you very soon");
             	form.trigger('reset');
                });
            }
        });
    });

   </script>
   
   
   
                    
                    <div class="col-md-5">
                        <div id="mapCanvas" class="map-canvas no-margin"></div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="subsection">
                                    <h3 class="section-title">Business info</h3>
                                    <div class="contact-info">
                                        <h5>Address</h5>
                                        <p>13 Maxwell Crescent, Stanwell Park, NSW, 2508, Australia</p>
                                        
                                        <h5>Email</h5>
                                        <p>info@teksmobile.com.au</p>
                                        
                                        <h5>Phone</h5>
                                        <p>+61 280 152 840</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="subsection">
                                     <h3 class="section-title">Stay connected</h3>
                        <p>
                        Follow us on social media, to get regular updates, app-announcements, special offers and other interesting tidbits.
                        </p>
                        <div class="social-media">
                            <a href="https://www.facebook.com/TeksMobileAustralia" target="blank"><i class="fa fa-facebook facebook"></i></a>
                            <a href="https://plus.google.com/u/0/b/109837041891616325544/" target="blank"><i class="fa fa-google-plus google"></i></a>
                            <a href="https://twitter.com/TeksMobileAus" target="blank"><i class="fa fa-twitter twitter"></i></a>
                        </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    
    </section>

<?php include 'footer.php';?>


    
</body>
</html>                           
                            