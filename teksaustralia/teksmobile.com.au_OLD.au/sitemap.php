<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'aboutus'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>

<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Sitemap</h2>
                </div>
              </div>
        </div>
    </div>
        
        <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        
        			<center>	
					<ul class="list-check">	
							<li><a href="index.php"><i class="fa fa-check"></i>Home</a> </li>
							
							<li><a href="aboutus.php"><i class="fa fa-check"></i>About Us</a> </li>
							
							<li><a href="portfolio.php"><i class="fa fa-check"></i>Portfolio</a> </li>

							<li><a href="iPhone.php"><i class="fa fa-check"></i>Portfolio - iPhone</a> </li>
							
							<li><a href="iPhonedetails.php"><i class="fa fa-check"></i>Portfolio - iPhone Description</a> </li>
							
							<li><a href="android.php"><i class="fa fa-check"></i>Portfolio - Android</a> </li>
							
							<li><a href="androiddetails.php"><i class="fa fa-check"></i>Portfolio - Android Description</a> </li>
							
							<li><a href="advantage.php"><i class="fa fa-check"></i>Teks Advantage</a> </li>
							
							<li><a href="work.php"><i class="fa fa-check"></i>How We Work</a> </li>
							
							<li><a href="team.php"><i class="fa fa-check"></i>Our Team</a> </li>

							<li><a href="contactus.php"><i class="fa fa-check"></i>Contact Us</a> </li>
							
							<li><a href="faq.php"><i class="fa fa-check"></i>Frequently Asked Questions</a> </li>
							
							<li><a href="requestquote.php"><i class="fa fa-check"></i>Request A Quote</a> </li>
							
							<li><a href="testimonial.php"><i class="fa fa-check"></i>What Client Says</a> </li>
							
							<li><a href="terms.php"><i class="fa fa-check"></i>Terms & Conditions</a> </li>

							<li><a href="policy.php"><i class="fa fa-check"></i>Privacy Policy</a> </li>
							
							<li><a href="vision.php"><i class="fa fa-check"></i>Vision & Mission</a> </li>
							
					</ul>		
				</center>
						
	        		</div>
	        	</div>
	        </div>
	    </div>
	</section>    
<?php include 'footer.php';?>

<?php include 'script.php';?>
</body>
</html>	    