<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'faq'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Hire Us - FAQ</h2>
                </div>
         </div>
        </div>
    </div>
    
    <section class="slice bg-2">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2>Frequently asked questions</h2>
                            <p>
                               In keeping with our commitment to provide transparent services, we have prepared a selection of queries with responses - please read through our FAQs and get to know us a little more.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-listings">
                        <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>How long have you been developing mobile apps?</h3>
                                    
                                    <p>
                                    Since 2006 - a time when iPhone and Android were just words. We started out with J2ME projects, and gradually expanded our services to include new platforms. At present, we have over 300 clients from Australia, United States and the United Kingdom.
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>So, you guys have already developed lots of apps ?</h3>
                                    
                                    <p>
                                     Oh yes, we have. In April 2014, the total app-count created by our mobile app company in Australia crossed 600. We have a fairly even distribution of iPhone apps <a href="iphone.php" style="text-decoration: underline;">iPhone</a> and Android apps <a href="android.php" style="text-decoration: underline;">Android</a> in our portfolio.
                                   
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Do you have any presence outside Australia ?</h3>
                                    
                                    <p>
                                    Teks Mobile/Teknowledge Software is a multinational mobile app development agency. Our Indian chapter started operations in 2006. At present, around 40% of our projects are obtained from overseas clients - with a high number of app quote requests coming in from North America and the United Kingdom Teks Mobile Australia’s headquarter is in Sydney, but we serve clients  Worldwide! 
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Where is your office located ?</h3>
                                    
                                    <p>
                                    We are located in Sydney, Australia. If you would like to meet up with a representative from Teks Mobile Australia to discuss your project in greater detail please contact customer service directly at: amber@teksmobile.com.au

                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>I want to talk with the top authority. Who’s in charge ?</h3>
                                    
                                    <p>
                                    First contact Amber Blumanis (COO of Teks Mobile Australia) at 0423521035 or via email at amber@teksmobile.com.au alternatively you can contact Hussain Fakhruddin (CEO of Teks - Indian chapter), by calling at +61-280385078 or via email at hello@teksmobile.com.au.
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>How much does it cost to get app quotes ?</h3>
                                    
                                    <p>
                                    Not a single dollar. We provide free app quotes to all clients and get back to you within 24 hours of receiving queries. Simply specify the type of iPhone and/or Android app you need, select your budget, and fill out any other details on the <a href="requestquote.php" style="text-decoration: underline;">form</a> -  We will follow up with a more detailed questionnaire to gain greater scope of your project and provide you with an accurate quote.
                                    </p>
                                </div>
                              
                            </li>
                            
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>What about the intellectual rights of the apps ?</h3>
                                    
                                    <p>
                                    If you choose Teks Mobile Australia as your partner in app development, you will never have to worry about this. We are more then happy to sign NDAs (non-disclosure agreements) and non-competing agreements (if you wish).You also become the sole owner of all intellectual rights. If you need the source code used for the project, we are happy to hand them over.
                                    </p>
                                </div>
                              
                            </li>
                            
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>It all sounds good. But what about the quality ?</h3>
                                    
                                    <p>
                                     We take pride in producing apps of the highest quality. During each stage of a project, our Android and iPhone app developers conduct tests for coding errors, bugs, malware, and more. The final versions of new apps are tested among focus group experts and on the cloud network. We complete projects well within pre-specified deadlines - but not at the expense of ignoring stringent quality standards.
                                    </p>
                                </div>
                              
                            </li>
                            
                             <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Can you name some of your present/previous clients ?</h3>
                                    
                                    <p>
                                     Of course we can. However, we would greatly appreciate it if you let us know first, before contacting any of them. Some of our biggest clients till date have been Australian University, Freenet UK Ltd., Lucid Technologies, Quadronica S.R.L., Acex Game and McDonalds Norway. As you can see, we serve clients from every part of the world!
                                    </p>
                                </div>
                              
                            </li>
                            
                             <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>When can I contact Teks ?</h3>
                                    
                                    <p>
                                    Anytime! We have regular office opening hours however our representatives are available on a 24x7 basis. Teks Mobile Australia offers services across 18 different time zones, so wherever you are in the world feel free to conact us at any time.
                                    </p>
                                </div>
                              
                            </li>
                            
                            
                            
                            
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Is there any specific payment structure ?</h3>
                                    
                                    <p>
                                    We are flexible with client payment schedules however the usual protocol is for you to deposit only 30% of the estimated costs upfront. Once the app development process is midway we will ask for another 30%. Only upon the completion of the project will you need to pay the remaining 40%. 
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Can my project get delayed for any reason ?</h3>
                                    
                                    <p>
                                    Not a chance. When we make a mobile application, we perform detailed tests at every stage - ruling out chances of errors and technical glitches. If the developers handling your project have to stop midway for any reason, someone else will be assigned to your project (within 2 working days). Making an app is an exciting process and we understand how important deadlines are to our clients.
                                    </p>
                                </div>
                              
                            </li>
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Are you present on social media sites ?</h3>
                                    
                                    <p>
                                    We are, and we would love to hear from you on these platforms. Teks Mobile Australia is present on Facebook, Twitter, Google Plus, LinkedIn, Behance, Pinterest, Tumblr and Instagram. Post your feedback, opinions, suggestions - and help us improve our services, even more.
                                    </p>
                                </div>
                              
                            </li>
                            
                            
                       
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3> Will you hire other third-party companies to handle my projects ?</h3>
                                    
                                    <p>
                                    Absolutely Not, all aspects related to mobile app development are handled by our in-house developers, UI/UX designers, and other employees. Projects are personally supervised by Amber Blumanis, our COO, who will update you with ongoing progress regarding your personal project. When you delegate a project to us - we take full responsibility for completing it from start to finish.
                                    </p>
                                </div>
                              
                            </li>
                            
                            
                            <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>You mentioned UI/UX. That means you offer in-house graphics support too ?
</h3>
                                    
                                    <p>
                                    Absolutely. We have a dedicated team of mobile app designers. Creating splash screens that capture your interest, controls that are easy to operate, and impressive animation themes. Our <a href="testimonial.php" style="text-decoration: underline;">Clients</a> have great things to say about our creativity.
                                   </p>
                                </div>
                              
                            </li>
                            
                             <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>There are so many apps Won’t my app get lost in the crowd ?</h3>
                                    
                                    <p>
                                    When you work with Teks you ensure greater visibility for mobile applications. as well. Sachinism, Story Time For Kids, Stopover, Free Your Mind Hypnosis & I Can Write - are just some of our apps that have been featured at iTunes and/or Play Store.
                                    </p>
                                </div>
                              
                            </li>
                            
                             <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Is there any warranty/guarantee ?</h3>
                                    
                                    <p>
                                    If any app by Teks Mobile Australia is released with bugs we would undertake bug fixes as soon as possible. You would not have to spend anything to download these upgraded versions. We also launch revamped versions of our best-performing apps at frequent intervals. We don’t see our customers simply as clients but more as a member of our expanding and supportive developer community. 
                                    </p>
                                </div>
                              
                            </li>

                         <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Will your apps be compatible on my mobile device(s) ?</h3>
                                    
                                    <p>
                                    There is no reason why they should not. The iPhone apps we create are optimized for the latest iOS 7 devices, including, of course, iPhone 5. Our Android apps are fully compatible with Android 4.4 devices. When going with Teks, there is also no need to worry about excessive bandwidth consumption or battery drain on your device. 
                                    </p>
                                </div>
                              
                            </li>

                        <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>What are the mobile platforms you develop apps for ?</h3>
                                    
                                    <p>
                                      Primarily, iOS and Android. However, we do have plans for Windows development. If the demand is high, we would look into expanding to Blackberry development also, but for now we’re focousing on iOS & Android.
                                    </p>
                                </div>
                              
                            </li>

                           <li class="featured">
                                
                               
                                <div class="listing-bodyfaq">
                                    <h3>Do your mobile app developers work only for third-party clients ?</h3>
                                    
                                    <p>
                                      Handling client projects is our priority. However, we do have a considerably large number of apps developed and launched independently. In fact, Stopover - the award-winning travelers’ app - is the brainchild of our COO, Amber. We love new apps, and release multiple applications every quarter.
                                    </p>
                                </div>
                              
                            </li>
                            
                            </ul>

                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    

                         <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Contact us at amber@teksmobile.com.au or 0423521035 to resolve any other queries. For a free quote, head over to the 'Request A Quote' page.</h4>
                </div>
         </div>
        </div>
    </div>
<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                                                           
                            