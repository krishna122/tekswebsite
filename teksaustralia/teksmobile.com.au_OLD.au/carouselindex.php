<section id="companyCarousel" class="carousel carousel-2 slide bg-3" data-ride="carousel">
        <div class="container" style="position:relative;">
        	<!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#companyCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#companyCarousel" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Designed To Excite. Functions That Delight.</h2>
                            <p>Creativity and imagination merge seamlessly with technical finesse and practical considerations, at Teks Mobile Australia. We create applications belonging to diverse genres - ranging right from mobile apps for kids and mobile fitness tools, to finance manager applications, travel-oriented apps, and a whole lot more. Simplicity and elegance are blended beautifully in the controls and features of each app. Our UI/UX design executives relish the challenge of preparing relevant and intriguing display screens, in-app navigation systems, animations and high-res graphic themes for each of our apps. The tabs and buttons are easy to operate - and we have special kid-friendly features in all our children’s apps. Regular upgrades are released, with enhanced functionality and even better designs. 
                            </p>
                            <p>When you get apps developed by us - you make your mobile device smarter!</p>
                        </div>
                        <div class="col-md-6">
                            <img src="images/prv/device-3.png" class="img-responsive" alt="device1">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Your Budget. Your Preferences. Your App.</h2>
                            <p>Our COO conceived the idea for <a href="iphonedetails.php?appid=112">STOPOVER</a> with the vision to connect like-minded people at Airports. By the same token, each one of our other apps is focused to meet a precise requirement of users. Simply let us know the features your app needs and provide a layout (if possible) – and our iPhone/Android app developers will create your app just the way you want it. On our ‘Request A Quote’ page, you can specify your app development budget. We have separate service plans corresponding to every budget range. That way, you can opt for the mobile software development package that suits you, without having to spend additional amounts for what you don’t need. Unlike other app development agencies in Australia, at Teks you only pay for what you need, with no unnecessary extras. 
                            </p>
                            <p>Great apps are not the ones that have a gamut of complicated features. An app is great only when it can satisfy you - the end-user. And that’s precisely what we are specialists at developing!
                            </p>
                        </div>
                        <div class="col-md-6">
                            <img src="images/prv/device-4.png" class="img-responsive" alt="device2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>