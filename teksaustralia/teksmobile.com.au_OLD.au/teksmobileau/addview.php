<?php session_start();
session_regenerate_id(true);
if($_SESSION['isLoggedin']!=1)
{
	header("Location:index.php");
}

?>

<!DOCTYPE HTML>
<html>
<head>
<title>Add App Details</title>
<style>
.error {color: red;}
</style><link rel="shortcut icon" href=images/zzzz.png >
</head>
<body style="background:radial-gradient(black 15%, transparent 16%) 0 0,radial-gradient(black 15%, transparent 16%) 8px 8px,radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;background-color:#282828;background-size:16px 16px;">
<div align="center" style="margin-left: -70px;" ><a href="teksadminpanelview.php"><img  src="images/logosmall.png" /></a> 

<h3 style= "font-family: sans-serif; font-size: 20px; color: white ;">ADD APP DETAILS</h3>
<div style="width:650px; height:100%; border-color:white; border-width:2px; border-style:inset; -moz-border-radius: 8px; border-radius: 8px;"><br><br> 
<form method="post" action="add.php" enctype="multipart/form-data">
<table  cellspacing="10">
   <tr align="left">
   <td  style= "font-family: sans-serif; font-size: 20px; color: white ;">Category   <br><br></td>
   <td><select  name="category" style= " -moz-margin-top: -0px; font-family: sans-serif;  font-size: 20px; color: #999a96; text-align:center; width:300px; height:30px; m  ">
 <option style="text-align:justify;" value="select">Select</option>
  <option style="text-align:justify;" value="iphone">iPhone</option>
  <option style="text-align:justify;" value="android">Android</option>
  <option style="text-align:justify;" value="blackberry">Blackberry</option>
  </select><br><br></td>
   </tr>   
   <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> App_name  <br><br></td>
   <td><input type="text" name="filename" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;  " required> <span class="error">*</span><br><br></td>
   </tr>
    <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Client  <br><br></td>
   <td><input type="text" name="client" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden; " ><br><br></td>
   </tr>
    <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;">  App_url  <br><br></td>
   <td><input type="url" name="alink" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;"><br><br></td>
   </tr>
   
       <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;">  Preview_url  <br><br></td>
   <td><input type="url" name="previewlink" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;"><br><br></td>
   </tr>

      <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Description  <br><br></td>
   <td><textarea  name="description" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:100px; border-radius:5px; border: hidden; resize: none;"></textarea><br><br></td>
   </tr>
   <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Logo  <br><br></td>
   <td><input name="uploaded" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; " required><br><br></td>
      </tr>

   <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Screenshot1  <br><br></td>
   <td><input name="uploadedfile" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; " required><br><br></td>
      </tr>

      <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Screenshot2  <br><br></td>
   <td><input name="uploadedfile1" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; "><br><br></td>
      </tr>

      <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Screenshot3  <br><br></td>
   <td><input name="uploadedfile2" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; "><br><br></td>
      </tr>

      <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Screenshot4  <br><br></td>
   <td><input name="uploadedfile3" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; "><br><br></td>
      </tr>

      <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Screenshot5  <br><br></td>
   <td><input name="uploadedfile4" type="file" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:25px; border-radius:5px; border: hidden; "><br><br></td>
      </tr>

</table>
    <input type="submit" value="Submit" style= "font-family: sans-serif; font-style:oblique; font-size: 20px; color: white ; text-align:center; width:100px; height:50px; border-radius:10px; background: url(bg1.jpg);  ">
</form><br><br><br>
</div>
</div>
</body>
</html>