<?php session_start();
session_regenerate_id(true);
if($_SESSION['isLoggedin']!=1)
{
	header("Location:index.php");
}

?>
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Add Page Info</title>
<style>
.error {color: red;}
</style>
<link rel="shortcut icon" href=images/zzzz.png >
</head>
<body style="background:radial-gradient(black 15%, transparent 16%) 0 0,radial-gradient(black 15%, transparent 16%) 8px 8px,radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;background-color:#282828;background-size:16px 16px;">
<div align="center" >  <a href="pageview.php"><img  src="images/logosmall.png" /></a> 
<br><br>
<h3 style= "font-family: sans-serif; font-size: 20px; color: white ;">ADD PAGE INFO</h3><br>
<div style="width:650px; height:380px; border-color:white; border-width:2px; border-style:inset; -moz-border-radius: 8px; border-radius: 8px;"><br><br> 
<form method="post" action="addpage.php" enctype="multipart/form-data">
<table  cellspacing="10">
   <tr align="left">
   <td  style= "font-family: sans-serif; font-size: 20px; color: white ;">Page_Name<br><br></td>
   <td><input type="text" name="pagename" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;  " required> <span class="error">*</span><br><br></td>
   </tr>   
   <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Meta_Title  <br><br></td>
   <td><input type="text" name="metatitle" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;  " required> <span class="error">*</span><br><br></td>
   </tr>
    <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;"> Meta_Key  <br><br></td>
   <td><input type="text" name="metakey" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden; " required> <span class="error">*</span><br><br></td>
   </tr>
    <tr align="justify">
   <td style= "font-family: sans-serif; font-size: 20px; color: white ;">  Meta_Description  <br><br></td>
   <td><input type="text" name="metadescription" style= "font-family: sans-serif; font-style:oblique; font-size: 15px; color: #999a96; text-align:left; width:300px; height:30px; border-radius:5px; border: hidden;" required> <span class="error">*</span><br><br></td>
   </tr>
</table>
    <input type="submit" value="Submit" style= "font-family: sans-serif; font-style:oblique; font-size: 20px; color: white ; text-align:center; width:100px; height:50px; border-radius:10px; background: url(bg1.jpg);  ">
</form>
</div>
</div>
</body>
</html>

