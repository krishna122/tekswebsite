<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'aboutus'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>

<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 >About us</h2>
                </div>
                <div class="col-md-6">
                    <h2 style="text-align: right;" ><a href="requestquote">Request A Quote</a></h2>
                </div>
              </div>
        </div>
    </div>
    
 <section class="slice bg-5 p-15">
        <div class="w-section inverse">
            <div class="container">
                <div id="homepageCarousel" class="carousel carousel-1 slide color-two-l" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators hide">
                        <li data-target="#homepageCarousel" data-slide-to="0" class="active"></li>
                    </ol>
                    
                    <div class="carousel-inner" style="height:370px;">
                        <div class="item active">
                            <img src="images/prv/about-img-1.jpg" alt="">
                        </div>  
                        <div class="item">
                            <img src="images/prv/about-img-2.jpg" alt="">
                        </div>        
                    </div>
                    
                    <!-- Controls -->
                    <a class="left carousel-control" href="#homepageCarousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right carousel-control" href="#homepageCarousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>     
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="section-title">Adding Value To Your Mobile Device</h3>
                        <p>
                        That is, and will always be, the key objective of Teks Mobile Australia. We have experienced teams of iOS and Android developers - committed to your projects. Client Satisfaction matters to us, if you’re happy, we’re happy!
                        </p>
                    </div>
                    
                    <div class="col-md-6">
                    <h3 class="section-title">Great Ideas. Great Apps.</h3>
                        <p>
                        We have been in the mobile app development business since 2006 - that’s right, even before the first iPhone had been released. Since then, it has always been a policy of our company to make sure that every viable concept and idea can be transformed into a mobile applications. When you go with Teks you have piece of mind in knowing you will receive a quality product and customised service.
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">Journey Of Our Mobile Apps Company</h3>
                        <p>
                        Our first office was at Kolkata, India. Our India chapter is known as Teknowledge Software - and we are one of the leading mobile app developers in Asia at present. Till date, our developers have successfully created 600+ iPhone and Android apps. In 2009, we ventured into the international markets for the first time, under the guidance of our founder/CEO Hussain Fakhruddin. Within four years, the percentage of overseas projects on our roster had risen to over 40%. With many requests for app quotes coming in from Australia, it was only natural to start mobile software development in Sydney.
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

	<section class="slice bg-5">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-quote-left fa-3x"></i></h4>
                            <h2>COO Speak</h2>
                            <div class="testimonial-text">
                                <p>
                                  I had contacted Hussain at Teknowledge Software for a travel-related app project. The knowledge, dedication and work-ethic of the mobile app developers over there was extremely impressive. When I learnt that this team were planning to start an Australian chapter I knew I wanted to be a part it, and was so excited to get involved.
                                </p>
                                <p class="testimonial-author">-- Amber Blumanis, Teks Mobile Australia </p>
                            </div>
                            <span class="clearfix"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
     <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">In Good Hands</h3>
                        <p>
                        Amber Blumanis, the COO of Teks Mobile Australia is the best person you can connect with, for mobile app development in Australia. A huge collection of professional awards bear testimony of Amber’s expertise, love for app-innovation, and sheer passion for mobile technology. Under her leadership, our Australian chapter has fast emerged as one of the most trusted companies in the country already.
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">Make A Mobile App In Australia...Just The Way You Like It!</h3>
                        <ul class="list-check">
                             <li>We offer free app quotes to all our clients.</li>
                             <li><i class="fa fa-check"></i> Our app development service charges are extremely competitive.</li>
                             <li><i class="fa fa-check"></i> We adhere to stringent quality standards and best practices from the mobile industry.</li>
                             <li><i class="fa fa-check"></i> You are always kept in the loop, during app development projects.</li>
                             <li><i class="fa fa-check"></i> We focus on delivering top-notch UI/UX designing and ensuring high-end user-friendliness in each of our apps.</li>
                             <li><i class="fa fa-check"></i> All projects are completed well within pre-specified deadlines. You need not worry about spiralling costs or undue delays.</li>
                         </ul>
                        
                  
                    </div>
                    
                    
                        </div>
                       <h4>At Teks Mobile Australia, we value your opinions, preferences & suggestions</h4>
                    </div>
                </div>
            </div>
        </div>
    
    </section>



<?php include 'footer.php';?>

<?php include 'script.php';?>
</body>
</html>