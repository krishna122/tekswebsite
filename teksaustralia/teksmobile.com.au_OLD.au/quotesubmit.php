<?php

// check for form submission - if it doesn't exist then send back to contact form
if (!isset($_POST['save']) || $_POST['save'] != 'quote') {
    exit;
}
	
// get the posted data
$name = $_POST['quote_name'];
$email_address = $_POST['quote_email'];
$phone = $_POST['quote_phone'];
$message = $_POST['quote_describe'];
$wireframe = $_POST['quote_wireframe'];
$budget = $_POST['quote_budget'];


// write the email content
$email_content = "Name: $name\n";
$email_content .= "Email Address: $email_address\n";
$email_content .= "Phone Number: $phone\n";

$email_content .= "Wireframe: $wireframe\n";
$email_content .= "Budget: $budget\n";
$email_content .= "Message:$message";

$filename=$_FILES["file"]["name"];

$file_size = $_FILES["file"]["size"];
$fileatt_type= "application/zip";
 
$content = file_get_contents($_FILES['file']['tmp_name']); 
$content = chunk_split(base64_encode($content));
$uid = md5(uniqid(time()));
   
$header = "From: $email_address.\r\n";
$header .= "Reply-To: ".$email_address."\r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
$header .= "This is a multi-part message in MIME format.\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $email_content."\r\n\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-Type:".$fileatt_type."; name=\"".$filename."\"\r\n"; // use different content types here
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
$header .= $content."\r\n\r\n";
$header .= "--".$uid."--";
    
	
// send the email
//ENTER YOUR INFORMATION BELOW FOR THE FORM TO WORK!
mail ('info@teksmobile.com.au, hussain@teksmobile.com.au',  'Teks Mobile Australia - Quote Request', $email_content, $header);
	
// send the user back to the form
header('Location: requestquote.php');

?>