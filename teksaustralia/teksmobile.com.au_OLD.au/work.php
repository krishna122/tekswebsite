<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'work'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <?php include 'headtag.php';?>
</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
            
 <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>How We Work</h2>
                </div>     
            </div>
        </div>
    </div>    
            
     <section class="slice bg-3 p-15">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            <div class="thmb-img">
                                <i class="fa fa-lightbulb-o"></i>
                            </div>
                            
                            <h2>Generating ideas</h2>
                            <p class="text-center">We brainstorm for new & interesting app ideas, that would be of value to prospective users. Regular researches are conducted, to find out the precise type of mobile software development services that people from Sydney and  greater Australia are looking for.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            <div class="thmb-img">
                                <i class="fa fa-keyboard-o"></i>
                            </div>
                            
                            <h2>Wireframes & Prototypes</h2>
                            <p class="text-center">Detailed wireframes and mockups are prepared by our iPhone/Android app developers. They are then shared with clients, for approval, feedback and further suggestions. Prior to making the final version of apps, we create beta-prototypes, to give us a clear understanding of how  the project is shaping up.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white">
                            <div class="thmb-img">
                                <i class="fa fa-code"></i>
                            </div>
                            
                            <h2>Designing & Coding</h2>
                            <p class="text-center">We offer the best mobile apps in Australia, excellence in app designing has a lot to do with our professional success. Our developers work with the latest coding techniques and programming languages. At Teks Mobile Australia, you get apps that are visually pleasing and technically glitch-free.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="w-box white last">
                            <div class="thmb-img">
                                <i class="fa fa-rocket"></i>
                            </div>
                            
                            <h2>Testing & Launch</h2>
                            <p class="text-center">In regards to quality, expect nothing but the very best from us. Each of our apps are rigorously tested in accordance with the latest mobile application testing norms and regulations. Only when an app is certified to be bug-free, it is launched at the stores, It does not take long after release for many of our apps to get featured either!</p>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-5">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">How Does Our Mobile Software Development Company Operate?</h3>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-desktop"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Provide FREE App Quotes</h4>
                                    <p>Within one working day, we send detailed app quotes to prospective customers. No payment is charged for online quotes. There are no chances of the final cost figure exceeding that mentioned in the initial quotes. If you wish to get an Android or iPhone obligation free app quote, please <a href="contactus.php" style="text-decoration:underline;">contact us</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-code"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Round-The-Clock Availability</h4>
                                    <p>We serve clients across 18 time-zones, from 4 continents. You can call up representatives from our mobile app company any time - for free consultation sessions. If you have any particular query, you can <a href="mailto:amber@teksmobile.com.au" title="Email Us" style="text-decoration:underline;">email</a> us at your convenience. Teks Mobile Australia is always by you!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-cogs"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Our Apps Have Top-Notch Compatibility Features</h4>
                                    <p>Mobile applications that are high on compatibility - is what we specialize in making. There are separate teams of iOS and Android developers here at Teks. Each of our apps are optimized for the latest mobile OS versions/platforms. Our portfolio includes a large number of apps with cross-platform compatibility.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Training Sessions</h4>
                                    <p>Once every week, app development training sessions are conducted at Teks. We endeavor to stay updated with all that’s new in the domain of mobile technology - so that you get apps that are sophisticated and technically of the finest order. Ongoing training helps us create the best apps and stay ahead of the game.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-plane"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Apps From Diverse Genres</h4>
                                    <p>With Teks as your partner, you can make mobile apps for any genre and category. Our COO started off with a travel-based application, our kids’ apps have won multiple awards, we have IM apps and mobile fitness tools, as well as a great selection of financial and other personalized apps in our portfolio.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="icon-feature">
                                        <i class="fa fa-wrench"></i>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <h4>Emphasis On Usability</h4>
                                    <p>At the end of the day, you will be the one using the apps - so it’s our duty to ensure that you do not face any hassles while doing so. We implement user-friendly graphics, controls, UI/UX designs and other features in each of our applications. Our mobile apps for <a href="http://www.teksmobile.com.au/iphonedetails.php?appid=46" style="text-decoration:underline;">kids</a> are so user friendly that toddlers can navigate them, with out parental aid!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-2 p-50">
        <div class="w-section inverse">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2>We build it, you rock it</h2>
                            <p>
                                We pay minute attention to each stage of the app development processes.  User-friendliness has been, and will always be, our prime focus.
                            </p>
                            <span class="clearfix"></span>
                            
                            <div class="text-center">
                                <a href="requestquote.php" class="btn btn-lg btn-one " title="">Request A Quote</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">Think Successful Apps. Think Teks.</h3>
                <div class="row">
                    <div class="col-md-6">
                        <img src="images/prv/device-3.png" class="img-responsive" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-feature">
                                        <i class="fa fa-cloud"></i>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <h4>Over 80% Fame Rate</h4>
                                    <p>On an average, 8 out of every 10 apps developed by Teks Mobile Australia become a huge successes at iTunes and on Google Play. When you delegate your project to us, you can bid adieu to risk of app-rejection. We have a 100% approval rate for all our apps submitted to the App Store & Google Play.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-feature">
                                        <i class="fa fa-camera"></i>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <h4>Online Visibility</h4>
                                    <p>Teks Mobile  has a record of getting outstanding reviews and user-feedback. We work hard to ensure Teks is a trusted company and take pride in having so many happy clients from all over the world. We have many return clients, and new clients who hear about Teks through our boundless customer referrals.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="aside-feature">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-feature">
                                        <i class="fa fa-twitter"></i>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <h4>At Teks Mobile Australia, Your Success Is Our Reward</h4>
                                    <p>If you’re happy we’re happy. We love hearing about your journey with Teks; if you would like to let us know what you thought about working with us please head over to the ‘contact us’ page and your testimony will feature on our website as reference for prospective clients. We value your feedback and suggestions and are sure to pass on your thanks to each individual at Teks who put their time and talent into turning you idea into an app.</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            
            </div>
        </div>
    </section>       
            
<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                