<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'policy'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'headtag.php';?>

</head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
<div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Privacy Policy</h2>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <p>Teks Mobile Australia Pty Ltd. is committed to bringing you the finest professional smartphone app development services. We make use of state-of-the-art online data security techniques, to keep your personal information safe. Go through our privacy policy pointers, to get an idea about how we use your details.</p>
                    
                        <p>To avail the services of Teks Mobile Australia, you will have to provide the following details:</p>
                        
                        <ul class="list-check">
                                        <li><i class="fa fa-check"></i>Full Name</li>
                                        <li><i class="fa fa-check"></i>Valid email id</li>
                                        <li><i class="fa fa-check"></i> A one-line description of the iOS/Android app you wish to be developed</li>
                                        <li><i class="fa fa-check"></i> A layout of the proposed app (if available)</li>
                         </ul>
                        
                  <p>In addition, you will have to specify your budget preference for the project at hand (choose from the 5 options provided in the drop-down menu). We also encourage clients to provide their own app-wireframes. Of course, if wireframes are not available, our Android/iPhone app developers will prepare wireframes for you.</p>
                  
                  <p>Please note that this information is collected to create a virtual profile solely as reference for your app project. We do not use personal information for any type of commercial activities. In our secure website database, all the data you supply remain secure.</p>
                  
                  <strong>Plagiarism Of Copyrighted Concepts And Designs</strong>
                  
                  
                  <p>Teks Mobile has a firm stance against all types of willful plagiarism of existing app concepts and ideas. When you submit your ideas, rest assured we will not share them with any external individuals or third-party companies/websites. At Teks Mobile Australia, data integrity is always upheld. We also request users to not submit concepts that are ‘heavily inspired’ from existing apps.</p>
                  
                  <strong>Use Of Cookies</strong>
                  
                  <p>Our website uses cookies (small internet files) which automatically get stored in your internet browser. This helps our site to ‘remember’ you, every time you return to our site. We strongly advise users to not disable cookies on their systems, to enable this convenient feature.</p>
                  
                  <strong>E-newsletter Subscriptions & Promotional Emails</strong>
                  
                  <p>Teks offers FREE subscription to all users on two newsletter services:</p>
                  
                  <ul class="list-check">
                                        <li><i class="fa fa-check"></i>AppBoard Tuesday - A weekly newsletter offering relevant insights into the world of mobile apps.</li>
                                        <li><i class="fa fa-check"></i>Infowatch - A monthly newsletter that rounds up all the highlights from the domain of technology each month.</li>
                                        
                         </ul>
                  
                  
                  <p>When you provide your email address on our website, you are automatically subscribed to both of these newsletters. Although we believe they would be of value to any user - you reserve the rights to ‘unsubscribe’ from the newsletter services.</p>

<p>To keep you updated about our latest apps, recently released upgrades and other pertinent information, we send you emails (at the registered id) from time to time. Kindly add our email id - hello@teksmobile.com.au - to your contact list. That would prevent our mails from mistakenly getting stored in the ‘Spam’ folder.</p>

<p>When you do business with Teks you never have to worry about the security of your personal information. At all times we respect the confidentiality of user data.</p>

                    </div>
                    
                    
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    
    </section>

<?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                           
                                                            
                            