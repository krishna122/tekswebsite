    
    <section class="slice bg-banner-2 animate-in-view">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-bar-chart-o fa-4x"></i></h4>
                            <h2>Teks Mobile - Stats at a glance</h2>
                            
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="pie-chart" data-percent="607" data-color="e74c3c">
                                        <span class="percent"></span>
                                        <div class="chart-text">
                                            <h3>Apps Delivered Successfully</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="pie-chart" data-percent="300" data-color="2ecc71">
                                        <span class="percent"></span>
                                        <div class="chart-text">
                                            <h3>Happy International Clients</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="pie-chart" data-percent="46" data-color="3498db">
                                        <span class="percent"></span>
                                        <div class="chart-text">
                                            <h3>In-house Developers</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="pie-chart" data-percent="08" data-color="3498db">
                                        <span class="percent"></span>
                                        <div class="chart-text">
                                            <h3>Years Of Industry Presence</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>