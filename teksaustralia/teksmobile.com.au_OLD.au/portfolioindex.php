<section class="slice relative animate-hover-slide bg-3">
        <div class="w-section inverse">
            <div class="container">
                <h3 class="section-title">Our portfolio</h3>
                
                <div id="carouselWork" class="carousel-3 slide animate-hover-slide">
                    <div class="carousel-nav">
                        <a data-slide="prev" class="left" href="#carouselWork"><i class="fa fa-angle-left"></i></a>
                        <a data-slide="next" class="right" href="#carouselWork"><i class="fa fa-angle-right"></i></a>
                    </div>
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" >
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                         <a href="iphonedetails.php?appid=46"><img alt="" src="images/prv/wk-img-1.png" class="img-responsive"></a>
                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>STORY TIME FOR KIDS</h2>
                                                <small>A cool and interactive mobile storytelling app for kids - with a fantastic range of classic fairy tales and original shorts. Makes reading fun and has educational values too!</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                        <a href="iphonedetails.php?appid=112"><img alt="" src="images/prv/wk-img-2.png" class="img-responsive"></a>
                                           
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>STOPOVER</h2>
                                                <small>An app that makes you feel at home, wherever you go. Connect with others on the basis of common interests, business requirements, and a host of other criteria. All this - right from airports!</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <a href="iphonedetails.php?appid=31"><img alt="" src="images/prv/words.png" class="img-responsive"></a>
                                            
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>MY FIRST WORDS</h2>
                                               <small>The mobile audio and picture dictionary every child would love. This mobile application comes with a fantastic range of original sound effects. Learning new words has never been easier for the little ones!</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                          <a href="iphonedetails.php?appid=95"> <img alt="" src="images/prv/macdonald.jpg" class="img-responsive"></a>
                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>MCDONALD</h2>
                                                <small>This app was done for McDonalds Norway. The most funny part was that no other McDonalds in the world had an app before and this app became very famous in US. </small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                           <a href="iphonedetails.php?appid=94"> <img alt="" src="images/prv/wk-img-5.png" class="img-responsive"></a>
                                            
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>INSLIDE OUT</h2>
                                               <small>The app that offers real-time tracking of your favorite teams’ matches, programs of the artists you love, and a whole lot more. You can find out which of your buddies is attending what event.</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                           <a href="iphonedetails.php?appid=106"> <img alt="" src="images/prv/wk-img-4.png" class="img-responsive"></a>
                                            
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>BLOOD BANK</h2>
                                                <small>With addresses, contact numbers and all other details of leading blood banks, hospitals and clinics, this is your go-to mobile software at the time of emergencies. Keep your dear ones safe - the smart way!</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <a href="iphonedetails.php?appid=13"><img alt="" src="images/prv/wk-img-7.png" class="img-responsive"></a>
                                            
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h2>EXCLUSIVE RESTURANTS</h2>
                                                <small>The finest French cuisine...right on your mobile screen! Exclusive comes with powerful GPS support, to display location details of the best French restaurants in any locality. Tap to discover great food!</small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="w-box inverse">
                                        <div class="figure">
                                            <a href="iphonedetails.php?appid=107"><img alt="" src="images/prv/wk-img-8.png" class="img-responsive"></a>
                                            
                                            
                                        </div>
                                        
                                          <div class="row">
                                            <div class="col-xs-12">
                                                <h2>EVENTS</h2>
                                                <small>Brings to you live updates of all glam events and concerts happening in your city. You can also connect with your favorite stars on Twitterverse. This app is your one-way ticket to the world of showbiz!</small>
                                            </div>
                                            
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>