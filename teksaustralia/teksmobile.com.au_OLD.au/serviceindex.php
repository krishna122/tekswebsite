<section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="w-box w-box-inverse">
                            <div class="thmb-img" style="cursor: pointer;">
                                <i class="fa fa-lightbulb-o"></i>
                            </div>
                            
                            <h2>Generating ideas</h2>
                            <p class="text-center">We brainstorm innovative mobile app ideas, to come up with concepts that will delight you - always. Teks Mobile Australia offers free detailed quotes.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="w-box w-box-inverse">
                            <div class="thmb-img">
                                <i class="fa fa-keyboard-o"></i>
                            </div>
                            
                            <h2>Creating the model</h2>
                            <p class="text-center">Detailed wireframes and mockups are prepared and quality testing is done at every stage. We value your ongoing feedback and personal preferences, creating a customised product.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="w-box w-box-inverse">
                            <div class="thmb-img">
                                <i class="fa fa-code"></i>
                            </div>
                            
                            <h2>Designing and Coding</h2>
                            <p class="text-center">The latest coding techniques are used at Teks Mobile Australia and we add a dash of imaginativeness and creativity in the UI/UX designs of each app.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="w-box w-box-inverse">
                            <div class="thmb-img">
                                <i class="fa fa-rocket"></i>
                            </div>
                            
                            <h2>Launching and Monitoring</h2>
                            <p class="text-center">Teks knows what it takes to get mobile applications approved at online stores. We keep up to date with regulations and guidelines, track app performance and release regular upgrades.</p>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>    
    </section>