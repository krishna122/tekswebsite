<section id="slider-wrapper" class="layer-slider-wrapper">
    <div id="layerslider" style="width:100%;height:500px;">        
        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
        	<!-- slide background -->
            <img src="images/slider/fw-1.jpg" class="ls-bg" alt="Slide background"/>
            
            <!-- Right Image -->
            <img src="images/prv/human-img-3.png" class="ls-l" style="top:58%; left:70%;" data-ls="offsetxin:0;offsetyin:250;durationin:950;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;" alt="Image layer">
            
            <!-- Left Text -->
            <h3 class="ls-l title" style="width:500px; top:25%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">Get Apps That Deliver Value Like Never Before...</h3>
            <h3 class="ls-l subtitle" style="top:40%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:1500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">Mobile Technology. Redefined</h3>
            <p class="ls-l text-standard" style="width:500px; top:55%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:2500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            Teks Mobile Australia is your one stop-destination for customized mobile app development services. We research innovative ideas, make great apps, and exceed your expectations. Ready to take your smartphone & tablet to the next level ?
            </p>
            <a href="portfolio.php" class="btn btn-two btn-lg ls-l" style="top:75%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:3500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">Visit Our Portfolio</a>
        </div>
        
        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
        	<!-- slide background -->
            <img src="images/slider/fw-1.jpg" class="ls-bg" alt="Slide background"/>
            
            <!-- Right Image -->
            <img src="images/prv/human-img-4.png" class="ls-l" style="top:48%; left:70%;" data-ls="offsetxin:0;offsetyin:250;durationin:950;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;" alt="Image layer">
            
            <!-- Left Text -->
            <h3 class="ls-l title" style="width:500px; top:15%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">Better Ideas. Cooler Apps</h3>
            <h3 class="ls-l list-item" style="top:28%; left:80px;" data-ls="offsetxin: left; rotatein: 90;durationin:1000;delayin:1500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            	<i class="fa fa-check-circle-o"></i> 600+ iPhone & Android apps
            </h3>
            <h3 class="ls-l list-item" style="top:35%; left:80px;" data-ls="offsetxin: left; rotatein: 90;durationin:1000;durationin:1000;delayin:2000;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            	<i class="fa fa-check-circle-o"></i> Proficient team of developers
            </h3>
            <h3 class="ls-l list-item" style="top:42%; left:80px;" data-ls="offsetxin: left; rotatein: 90;durationin:1000;durationin:1000;delayin:2500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            	<i class="fa fa-check-circle-o"></i> Competitive pricing
            </h3>
            <h3 class="ls-l list-item" style="top:49%; left:80px;" data-ls="offsetxin: left; rotatein: 90;durationin:1000;durationin:1000;delayin:3000;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            	<i class="fa fa-check-circle-o"></i> Offices in Australia & India
            </h3>
   
            <p class="ls-l text-standard" style="width:500px; top:65%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:3500;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;">
            We are specialists in making mobile apps that offer the finest combinations of usability, value, and visual elegance. If you wish to get more out of your smartphone/tablet, contact us and find out how.
            </p>
            <a href="advantage.php" class="btn btn-two btn-lg ls-l" style="top:84%; left:80px;" data-ls="offsetxin:0;offsetyin:250;durationin:1000;delayin:4000;offsetxout:0;offsetyout:-8;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;" target="_blank">Teks Difference</a>
        </div>
    </div>
</section>



   <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>
                            <strong style="font-size: 40px; text-align: center; font-family: ubuntu;">We Convert Ideas Into Apps !</strong>
                        </h1>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-one btn-lg pull-right" title="" href="requestquote.php" style="font-family: ubuntu;"><i class="fa fa-hand-o-right"></i>Request a Quote</a>
                    </div>
                </div>
            </div>
        </div>
    </section>