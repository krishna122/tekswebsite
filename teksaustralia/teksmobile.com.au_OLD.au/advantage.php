<?php include_once 'mysqlconnect.php'; ?>
<?php 

  $BASE_URL = "http://teksmobile.com.au/teksmobileau/";
  $query = "SELECT * FROM  metapages where pagename = 'advantage'";
  $sqlObj = new MySqlconnect();
  $sqlObj->fetch($query);	
  
  if(mysql_num_rows($sqlObj->mysql_result) > 0)
    {
	   $result = mysql_fetch_array($sqlObj->mysql_result);
	 }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $result['metatitle']; ?></title>
<meta name='description' content='<?php echo $result['metadescription']; ?>' />
<meta name='keywords' content='<?php echo $result['metakey']; ?>' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <?php include 'headtag.php';?>
   </head>
<body>

<?php include 'sidebar.php';?>

<?php include 'menu.php';?>                                
 
 <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                
                
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-2">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h4><i class="fa fa-users fa-3x"></i></h4>
                            <h2>Why Should You Choose Teks ?</h2>
                            
                            <span class="clearfix"></span>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>600+ apps. Nearly a decade of industry presence. Over 300 international clients - from every part of the globe. <br>If you wish to make a mobile app in Australia, there are plenty of reasons to choose us over the rest:</h2>
                </div>
               
                
            </div>
        </div>
    </div>
    
    <section class="slice bg-3">
        <div class="w-section inverse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                         
                        <ul class="list-listings">
                            <li class="">
                                
                               <div class="listing-image">
                                    <img src="images/prv/advantage3.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="">Free App Quotes In Australia</a></h3>
                                    
                                    <p>
                                    We start off with providing you a detailed, accurate cost estimate for your project. All that you have to do is fill out our client questionnaire  - and our representatives will get back to you within 24 hours. Our quotes are obligation free and you, of course, will not be charged anything until you decide to go with Teks.
                                    </p>
                                </div>
                               
                            </li>
                            <li class="">
                               <div class="listing-image">
                                    <img src="images/prv/advantage1.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="">Focus On Innovation</a></h3>
                                    
                                    <p>
                                   Everyone who works at Teks loves mobile technology and software development. We believe that no idea is too far-fetched to be converted into an application. Every new app idea is an exciting challenge for us - and we have been tackling such challenges with relish for years. <a href="testimonial.php" style="text-decoration:underline;">Check</a> out to see what some of our valued clients have to say about our services.  
                                    </p>
                                </div>
                              
                            </li>
                            <li class="">
                                <div class="listing-image">
                                    <img src="images/prv/advantage6.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="">Usability, Value & Visual elegance</a></h3>
                                    
                                    <p>
                                    Our app concepts are innovative, and the app development graphics and UI/UX design themes always have a fresh feel about them. Teks Mobile Australia emphasizes on making Android and iPhone apps that have a seamless blend of usability, practical value, and elegant interfaces/displays.
                                    </p>
                                </div>
                               
                            </li>
                            <li class="">
                                <div class="listing-image">
                                    <img src="images/prv/advantage5.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="">We Care For Your Budget</a></h3>
                                    
                                    <p>
                                    Strong corporate ethics and transparent operational policies have been two key hallmarks of Teks Mobile Australia. We are dedicated to completing all app development projects within the pre-specified cost estimates (quotes). Till date, we have delivered all our apps to clients well before their respective deadlines. If you are wary about spiralling costs or inordinate delays,   you have piece of mind with Teks.
                                    </p>
                                </div>
                             
                            </li>
                            <li class="">
                                <div class="listing-image">
                                    <img src="images/prv/advantage4.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="listing-body">
                                    <h3><a href="">Quality Assurance</a></h3>
                                   
                                    <p>
                                    At Teks we have thorough testing procedures, when we create iPhone and Android apps, you have 100% bug-free assurance on them. We have an experienced team of mobile app testers - who check the operability of new applications among focus groups and on the cloud network. We release app upgrades and new versions at regular intervals. Expect quality products at the best prices from us, every time.
                                    </p>
                                </div>
                                
                            </li>
                        </ul>
                        
                    </div>   
                </div>
            </div>
        </div>
    </section>
    
    
    <div class="pg-opt pin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>When making  mobile app in Australia, Teks Mobile is your most trusted partner. Our services have delighted clients from all over the globe!</h2>
                </div>
               
                
            </div>
        </div>
    </div>
    
    
    <section class="slice bg-2 p-15">
        <div class="cta-wr">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>
                           Think App Innovation. Think Teks. 
                        </h1>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-one btn-lg pull-right" title="" href="requestquote.php" >
                        <i class="fa fa-bolt"></i> Request A Quote
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'footer.php';?>

<?php include 'script.php';?>
    
</body>
</html>                                  
                            
                                                            
                                                            
                            
                            
                            