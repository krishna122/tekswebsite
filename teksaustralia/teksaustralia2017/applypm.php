<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $result['metatitle']; ?></title>
    <meta name='description' content='<?php echo $result['metadescription']; ?>' />
    <meta name='keywords' content='<?php echo $result['metakey']; ?>' />

	<?php include 'head.php';?>

</head>


<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="career">
        <div class="career-body">
            <div class="project-container" style="margin-top: 8%; margin-bottom: 2%;">
            	<div class="row">
                    <div class="col-md-12">
                        <p style="text-align: center;"><img alt="" src="img/pm.png" ></p>
                    </div>
                 </div>
           
                   <div class="row" style="margin-bottom: 0px;">
                    <div class="col-md-12">
                        <h1 style="color:#fff;">Apply For Project Coordinator</h1>
                        <p style="color:#fff;">We urgently require one (1) senior-level project co-ordinator. (S)he will be in charge of tracking the status of projects assigned to each team at our app development company, along with information transfer across teams. Management of day-to-day organizational workflow at Teknowledge will be the successful candidate’s responsibility.</p>
                    </div>
                </div>
            </div>
        </div>
    </header>
	
   <section >
	<div class="container">
	  <div class="row">
           <h4 style="font-weight:200;">Required Skills & Experience</h4>
        <div style="career-skills"><ul>
	  <li>Experience in preparing test cases, dummy projects, performance monitoring.</li>
          <li>Program bug detection ability (for iPhone apps and Android apps).</li>
          <li>Strong analytical skills.</li>
          <li>Commitment to maintain uniformly high service quality to clients.</li>
          <li>Basic knowledge about mobile app testing (recommended).</li>
          <li>Practical information on XML, JSON, MySQL, APACHE, UML Block Diagram, PHP, and other updated app standards.</li>
          <li>Self-motivated with a go-getter attitude.</li>
          <li>Capability of adding to the value of mobile software as well as the entire organizational culture.</li>
          <li>Fluency in English communication is a MUST. Knowledge of foreign languages will be considered as bonus.</li>
       <ul></div>
         <br /> <div class="dropcv"><p style="text-align:centre;">To apply for this post drop in your resume to <a style="color:#ff7018" href="mailto:hr@teks.co.in?subject= Project Coordinator">hr@teks.co.in</a></p></div><br/>
    </div>
 </div>
</section>

<section id="career">
	<div class="container">
	  <div class="row">
	  <h1>Join The Teks Team</h1>
	  <p style=" text-align: center; color:#9c9c9c;font-size: 30px; margin-top: -50px;">Varied Opportunities. Multiple Openings. Ample Scope To Do What You Love.</p><BR></BR>
	  <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                    <img src="img/c1.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Ui/Ux Designer</h4>
                    </div>
                </div>
                <div class="space"></div>
           
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
         
                <div class="icon">
                    <img src="img/c2.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Android App Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
          
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c3.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">Project Coordinator</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
        <center>
        <div class="col-xs-12 col-sm-6 col-lg-3">
            
                <div class="icon">
                   <img src="img/c4.png">
                    <div class="info">
                        <h4 style="font-size25px;" class="title">PHP Developer</h4>
                    </div>
                </div>
                <div class="space"></div>
            
        </div>
        </center>
        
    </div>
 </div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-6"> 
				<div class="panel panel-default job1" style=" border: none; cursor: pointer;" onclick="job1()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Graphics</span><br>
			    			<span style="font-weight: 200;color:#fff;">UI/UX Designer</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px; font-weight: 300;">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job2" style=" border: none; cursor: pointer;" onclick="job2()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px; color:#fff;">Android</span><br>
			    			<span style="font-weight: 200;color:#fff;">App Developer</span>
		    			</h3> <br> 
	    			<center>
	    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
	    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job3" style=" border: none; cursor: pointer;" onclick="job3()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Operation</span><br>
			    			<span style="font-weight: 200;color:#fff;">Project Coordinator</span>
		    			</h3> <br> 
		    			<center>
		    				<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
		    			</center> 
	    			</div>
				</div>
			</div>
	
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<div class="panel panel-default job4" style=" border: none; cursor: pointer;" onclick="job4()">
	    			<div class="panel-body">
		    			<h3>
			    			<span style="font-size:40px;color:#fff;">Coding (next line)</span><br>
			    			<span style="font-weight: 200;color:#fff;">Php Developer</span>
		    			</h3> <br> 
			    			<center>
			    			<span style="color:#fff; font-size: 22px;font-weight: 300;"">Start Date : Immediately</span>
			    			</center> 
	    			</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
function job1() {
    window.location.assign("applydesigner.php")
}

function job2() {
    window.location.assign("applyandroid.php")
}

function job3() {
    window.location.assign("applypm.php")
}

function job4() {
    window.location.assign("applyphp.php")
}
</script>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>