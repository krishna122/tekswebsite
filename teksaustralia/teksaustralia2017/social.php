<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Social Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

    
        	<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Social Networking Apps</span></h1>
	                    <p style="font-size: 25px;">Meet some of the cool social networking apps created by Teksmobile over here. Each of these apps comes with user-friendly features, robust functionality, and have won favourable reviews.</p>
                </div>
            </div>
        </div>
    </div>
</header>

<section id="appstory" class="dimes">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Dimes</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Social app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Dimes is a custom iPhone app that puts a fresh new fun spin on campus life. There are many exciting polls in the app, and users can rank his/her friends on them. Dimes comes with in-app chat functionality too.</p>
				  <br><br>
				  <a href="dimes.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dimes.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="currently">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Currently</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Audio clip sharing app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Currently is an innovatively conceptualized audio-clip sharing app for iOS and Android devices. On the app, users can record sound clips and share them in their ‘Stream’ or in the ‘Ocean’.</p>
				  <br><br>
				  <a href="currently.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/currently.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="stopover">
	<div class="container">
                <div  style="width:100%;"><br/><br/>
		<div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Stop Over</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Social Networking</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Conceptualized by Ms. Amber Blumanis, Stopover is a breakthrough app for travelers. It helps to remove the 'bore-factor' of long waits at airports. With the app, users can connect with people with common interests, right from the airport. Stopover, unsurprisingly, has won multiple awards.</p>
				  <br><br>
				  	<a href="stopover.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/stopover.png" align="center">
				</div>
		  </div>
		 </div>
		 <hr class="hr-style">
       </div>
</section>

<section id="appstory" class="timesnaps">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		<div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Timesnaps</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Photo/Video</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">For those who wish to capture, preserve and treasure the magic of fleeting moments, Timesnaps is a must-have application.  The app allows people to take images of practically anything at regular intervals, create slideshows with them, and revel at the changes that time can make!</p>
				  <br><br>
				  	<a href="timesnaps.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/timesnaps.png" align="center">

				</div>
		  </div>
		  </div>
		  <hr class="hr-style">
	</div>
</section>

<section id="appstory" class="bender">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Bender</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="bender.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/bender.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="freebird">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Freebird</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="freebird.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/freebird.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="inslideout">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">InslideOut</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Social Networking App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Live performances, sports events, concerts - all these and more arrive right at your fingertips, when you have the Inslide Out application. This is one events app-meets-mobile ticketing app you will love to have.</p>
				  <br><br>

				  <a href="inslideout.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/inslideout.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</section>
   

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>