<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of 365Deal</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
		<!-- Intro Header -->
<header class="deal_story_story" style="height: 34%;">
        <div class="deal_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">365Deal</span></h1>
<!--                         <center><a><img alt="" src="img/appstore.png"></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            2016 has indeed started off on an interesting note for us at Team Teks. We have received many different app and game ideas, and have already started working on many new projects. 365 Deal, a full-fledged sellers’ app, would certainly feature in this list of best new apps from Teknowledge. We had not worked on anything quite like this one in the recent past - and it was a challenge (one that we relished) to make the app in a perfect manner.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  		<h3 class="color">365 Deal Comes To Teks</h3>
		  	    <br>
		  	    <blockquote class="color block365Deal">
		  	    	When I decided to make a mobile app where sellers could post ads, one thing was still dicey. I simply had to find a developer company which would not make a mess of my idea. It’s a stroke of fortune that I chanced upon the name of Teks Mobile on the web. The track record of these guys was impressive, and the decision to delegate the project to them was a wise one in retrospect.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Our iPhone app development team made it a point to get a proper grasp of the entire app concept, before getting started with the coding tasks. The systematic approach helped us, and we did not hit a single roadblock during any of the development phases. Managing to finish a project quickly and properly put a satisfied smile on our, and of course the client’s, face!
		         </p>
		  	 </div>
		  	 
		  	 <div class="col-lg-6 hidden-xs">
		  	 	<video autoplay loop muted  poster="appstories/365bg.jpg" style="border: 5px solid #0099CF;">
				   <source src="video/appstories/365.mp4" type="video/mp4"> 
				</video>
		  	 </div>   
		  	 
		  </div>
		</div>
		
<p></p>
	
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Getting To Know 365 Deal</h3>
		  	    
		         <p class="color">
		         	There is nothing complicated and roundabout in 365 Deal - it is an iPhone app for sellers where ads can be posted. Users have to create accounts and log in, to start posting ads for products and/or services. After signing in, advertisements belonging to various categories and sub-categories can be viewed.
		         </p>

                 <blockquote class="color block365Deal">
		  	    	In an app like 365 Deal, it is very important to make optimal use of the screen real estate. The number of ads displayed has to be as high as possible, without, of course, making things intrusive. For this, we planned to show pop-up ads as well in the app, in addition to the ones present on the screen.
		  	    </blockquote>
		  	       
		   </div>
		   
		   <div class="col-lg-5">
		  		<center><img src="appstories/deal2.png" alt="365Deal" style="width:80%;" ></center>
		  	</div>
		  
		  </div>
		</div>
		
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	 
		  	 <div class="col-lg-5">
		  	 	<center><img src="appstories/deal7.png" alt="365Deal" style="width:90%;"></center>
		  	 </div>
		  	 
		  	 <div class="col-lg-7">
		  		<h3 class="color">Types Of Ads On 365 Deal</h3>
		  	    <br>
		  	    <p class="color">
		            Sellers can use their available credits on 365 Deal to post advertisements. In the app, two different types of ads can be posted: ‘Regular Ads’ and ‘Premium Ads’. ‘Regular ads’ are free, and a maximum of 5 such ads can be published by an individual user in a month. These ads remain active in the app for 30 days. On the other hand, the ‘Premium Ads’ are displayed only for a single day...but there is one big advantage here. For that day, the Premium Ad is guaranteed to be present in the top-five advertisements, ensuring enhanced visibility and higher chance of sales lead generation.
		         </p>
		         
		  	    <blockquote class="color block365Deal">
		  	    	365 Deal works on a system of credits. For each Premium Ad, 2 credits are debited from a seller’s credit balance. No credits are required for the first five Regular Ads in a month. On every additional Regular Ad, 1 credit has to be spent. This system keeps the app systematic and confusion-free. It’s on the sellers to make the best use of the app’s resources.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		            Credits can be bought via in-app purchase within 365 Deal. The process of credit-buying is secure and user-friendly - as is the steps for posting ads. We look at that next.
		         </p>
		         
		  	 </div>
		  	 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-7">
		  		<h3 class="color">Publishing Ads On 365 Deal</h3>
		  		<br>
		         <blockquote class="color block365Deal">
		  	    	As a buyer, I like to see an ad which spells out all the information I might look for. While working on 365 Deal, we made a conscious effort to avoid any type of vagueness in the ads present in the app. A half-baked ad is no good for anyone - not for the buyers, and definitely not for the sellers themselves.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          In addition to the Title and Price of the product in an ad (Regular or Premium), sellers have to provide a detailed description and a genuine product image. The correct category has to be chosen as well - for exposure to the right set of target audience. Complete contact information has to be present in all 365 Deal ads as well. 
		         </p>
		         
		         <blockquote class="color block365Deal">
		  	    	One thing that really captured my attention during the making of 365 Deal was the keen eye for detail that the iOS developers at Teks had. They were always a couple of steps ahead while thinking about how users would interact with the app...and hence, they could prepare the screens and overall layout optimally. Hussain has an expert team going!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          From the backend, certain special offers are provided to users on a regular basis. During the app testing, we did encounter a few minor bugs - which were resolved without much trouble. 
		         </p>
		  </div>
		  
		  <div class="col-lg-5">
		  		 <center><img src="appstories/deal8.png" alt="365Deal" style="width:70%;"></center>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Interacting With Sellers In 365 Deal</h3>
		  	    
		  	    
		         <p class="color">
		         	Right from the individual Product Description pages, viewers can call or message the respective ad owners. Real-time chat functionality is available too, for quick interaction with sellers. Getting in touch promptly and conveniently - that’s what this new iOS sellers’ app promises.
		         </p>
                 
                 <blockquote class="color block365Deal">
		  	    	What can possibly be more frustrating than seeing a nice ad, being interested to purchase the product...only to find that there is no way to contact the seller? We made sure that the general users of 365 Deal would never face such irritations. Every ad comes with multiple options to contact the concerned seller.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d1.png" alt="365Deal"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d2.png" alt="365Deal"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d3.png" alt="365Deal"></div>
		    <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d4.png" alt="365Deal"></div>
		    
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d5.png" alt="365Deal"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d6.png" alt="365Deal"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d7.png" alt="365Deal"></div>
		  	<div class="col-lg-3 col-xs-3 col-md-3 col-sm-3"><img src="appstories/365d8.png" alt="365Deal"></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
			  <div class="col-lg-8">
			  		
						<h3 class="color">Prime Importance To User Preferences</h3>
						<p class="color">
							At any time, the 365 Deal app contains many ads - and it is not the easiest thing to keep things organized. The team of iPhone app developers assigned on the job were extra careful while making the application. Apart from the features and tabs, we implemented a systematic notifications system. Sellers are notified on a real-time basis regarding contact requests, updates and any other pertinent information.
						</p>
						
						<p class="color">
							Let’s just say that our extra efforts have borne fruit. 365 Deal not only functions well for general viewers and sellers - it has turned out to be a good-looking app too.					
						</p>
						
						<div class="box">
				  	        <p class="color">
				  	        	Following a final round of testing, the 365 Deal application will be launched at the Apple iTunes store. Making some of the apps in our 900+ kitty have given us just that bit of extra satisfaction - and this iPhone app for sellers certainly falls in that category!
				  	        </p>
					</div>
			  </div>
			  <div class="col-lg-4">
			  		 <center><img src="appstories/deal6.png" alt="365Deal" style="width:60%;"></center>
			  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>