<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Android Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
    	<!-- Intro Header -->
	<header class="appstories" style="height: 60%;">
        <div class="appstories-body">
            <div class="container" >
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Android Apps</span></h1>
	                           <p style="font-size: 25px;">We have successfully created more than 400 Android applications. Get the inside story about a few of these apps here</p>
                       </div>
                 </div>
            </div>
        </div>
	</header>
		
<section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">BuiltBy</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Project Management App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">BuiltBy is a cross-platform project management mobile app, created with React Native for iOS and Android. Conceptualized by Neesha Alwani, BuiltBy facilitates seamless collaboration among team members of projects.</p>
                      <br><br>

                      <a href="builtby.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/builtbylogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>


    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Awayys</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Social Networking</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Awayys is a multi-featured, innovative travel networking application. It is customized for the iOS (8 and later) platform and has a wide array of cool, interesting features. Users can make new friends and expand their networks with ease on this app.</p>
                      <br><br>

                      <a href="awayys.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/awayyslogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
    </div>
    </section>
    
<section id="appstory" class="stopover">
	<div class="container">
    <br/><br/>
		<div class="row">  
		  <div class="col-lg-12">
				  <div class="col-lg-6">
				  
				  	<span style="font-weight:bold; font-size: 25px; text-align: center;" >Stop Over</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Social Networking</span>
				  	<br><br>	
				  	<p style="padding: 0px;" class="appcolor">Conceptualized by Ms. Amber Blumanis, Stopover is a breakthrough app for travelers. It helps to remove the 'bore-factor' of long waits at airports. With the app, users can connect with people with common interests, right from the airport. Stopover, unsurprisingly, has won multiple awards.</p>
				  <br><br>
				  	<a href="stopover.php" ><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  
				  <img src="appimages/stopover.png" align="center">
				  
				</div>
		  </div>
		 </div>
		 <hr class="hr-style">
    </div>
</section>
		  




<section id="appstory" class="dataworks">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">DataWorks</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Bluetooth Connectivity App</span>
				  	<br><br>	
				  	<p style="padding: 0px;" class="appcolor">DataWorks taps into the power of Bluetooth technology to the fullest. With this powerful app, devices can be connected with other compatible gadgets, and data can be measured, calibrated, maintained and managed.</p>
				  <br><br>
				  <a href="dataworks.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dataworkslogo.jpg" align="center">
				</div>
				
		  </div>
		</div>
		<hr class="hr-style">
	</div> 
</section>


<section id="appstory" class="LMIOpt-InChecker">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">
				  
				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">LMI Opt-In Checker</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">RFID app</span>
				  	<br><br>	
				  	<p style="padding: 0px;" class="appcolor">A cutting-edge RFID app created for the Android platform. Ideal for keeping track of prizes won by users at any time. The app boasts of a new technology, and is further bolstered by its smooth usability features</p>
				  <br><br>
				  <a href="LMIOpt-InChecker.php"><img src="img/view-project.png"></a><br><br>
				  </div>
				  
				<div class="col-lg-6 storiesimg">
				  <img src="appstories/LMIOpt-InCheckerlogo.png" align="center">
				</div>
				
		  </div>
		</div>
	</div> 
</section>





<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>