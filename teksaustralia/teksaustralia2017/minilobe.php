<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Minilobes - The House</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Minilobes - The House</span>
                        </h1>
<!--                        <a href="" target="_blank"><img alt="Minilobes - The House" src="img/appstore.png"></a>-->
<!--                    <a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img alt="HelloMind" src="img/play-store.png"></a>-->
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
	            Making apps for kids is something that has always excited us. A mobile application that will put a smile on the faces of little ones as they happily play along - the very picture adds that extra bit of motivation to our in-house developers. What’s more, we love making children’s app that have some learning elements/educational values...apps that have interesting ‘learn as you play’ elements. In that context, the Minilobes project fit the bill perfectly.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	Minilobes is simplistic and fascinating in equal measure. It does not require little children...or their parents...to sign up or register, and focuses solely on the gameplay elements. I particularly like how this app playfully helps kids learn about so many objects in their homes
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">What Exactly Is Minilobes?</h3>
                 <p class="color">
                     Minilobes is a highly interactive game for kids - geared to make them familiar with the rooms in their house and the common objects in them, in a fun manner. It has been built with Unity, does not have any login requirements, and leads users straight on to the splash screen.
                 </p>
		  	 </div>   
		  	 
              <p class="color">
                  The Teks Team already has a successful track record of making award-winning kids’ apps (Story Time For Kids would be a classic example). There was a right buzz amongst our developers to make Minilobes equally beautiful and similarly educational. A free app quote was sent to the client as soon as we had been contacted with the project proposal, and development started from the next week. This was a project that was meant to excel.
              </p>
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/mini1.png" alt="Minilobes - The House"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Platform Compatibility of Minilobes</h3>
		  	    
		         <p class="color">
		         	Following a couple of discussion sessions, it was finalized that Minilobes will be compatible with iOS 7 (and later versions) and Android (v.2.3.1 and later). Our Android and iPhone app development teams started working on the project simultaneously - and the first set of mockups were ready in a couple of weeks flat.
		         </p>
                	
		   </div>
              
		  </div>
            
            
            <div class="col-lg-12">
              
            <br>
            <blockquote class="color">
			  	  We were making a fun game for kids, and we wanted to make it available to as wide an audience as possible. After a brief deliberation with Hussain, I greenlighted the idea of making custom versions of Minilobes for both iOS and Android. The guys at Teks had the expertise and the experience to make both versions.
			  </blockquote>
            </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Getting Into Minilobes - The EXPLORE Mode</h3>
		  	    
                 <p class="color">
                     There are two built-in ‘Modes’ in Minilobes, offering varying features and options for the young users of the app. The splash screen gives way to the ‘Explore Mode’ screen. The ‘Game Mode’ can be activated by tapping on the ‘Play’ button on the top-left corner of the screen.
                 </p>
                 
		  	    <blockquote class="color">
		  	    	In both the Explore Mode and the Game Mode, the Minilobes game beautifully highlights common household items...helping kids identify them, and even understand associations between objects. There are activities galore in the app...and the learning opportunities are excellent!
		  	    </blockquote>
              
              <img src="appstories/mini2.png" alt="Minilobes - The House">
		  	    
		  	    <p class="color">
                    In the ‘Explore Mode’ of this new interactive mobile game for kids, 7 rooms are shown (each of these rooms are tappable). Inside each room, there are 15 tappable objects - ranging from cuddly toys, to shampoo bottles and kitchen sinks. The rooms displayed in this mode are bedroom, living room, garden, garage, kitchen, bathroom and hallway.
		         </p>
		         
		       
		  	 
		  </div>
            
            <div class="col-lg-12">
                <p class="color">
                     So, how do kids learn things in this mode of Minilobes? That’s easy. Whenever any object in any of the displayed rooms is tapped, that object gets zoomed, its name is shown, and 3 other form factors of the object can be seen. A child gets a proper idea about the object (s)he has tapped on, and check out other varieties of the same object as well.
		         </p>
                 
		  	    <blockquote class="color">
                    I always wanted Minilobes to be more than just another kids’ game. That, in fact, was the main reason behind me wishing for the Explore mode in this app. Credit to Hussain and his team of graphic designers and animators - they made this mode thoroughly engaging...just as I had wished it would be.
		  	    </blockquote>
            </div>
            
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
                  
		  		<h3 class="color">Getting Into Minilobes - The GAME Mode</h3>
                  
                <blockquote class="color">
		  	    	To be considered as good, any children’s app should have a wide variety of activities. In this regard, the Game Mode of Minilobes really comes up trumps. There are multiple level-based games - each with its very own gameplay. From the very first, I felt that this app was a winner all the way!
		  	    </blockquote>
              
               <center> <img src="appstories/mini3.png" alt="Minilobes - The House"> </center>  
		  		
                <p class="color">
		          Four games - ‘Guess What’, ‘Sort Me’, ‘What Goes Together’ and ‘The Mission’ - are present and viewable in the Game Mode of Minilobes. Each of the games has 3 practice rounds (compulsory), following which kids can proceed to the actual game. The games have 20 levels each.
		         </p>
              
		         <blockquote class="color">
		  	    	There are plenty of apps out there which are powered by good ideas, but falter in execution. Children might face difficulties in understanding the type of activity they need to do in the app. Minilobes does away with such problems with its mandatory ‘Practice Mode’. Kids are given detailed instructions on what they have to do...ensuring that they can play on their own later, without any hassles
		  	    </blockquote>   
           
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">The Games Of Minilobes</h3>
                 
                 <p class="color">
                 	The mobile app developers working on this project undertook several rounds of brainstorming to determine the precise features and elements of the 4 in-built games. Each of the games involves active participation from kids, and they are instrumental in helping users learn more about their homes.
                 </p>
              
                 <p class="color">
                 	<strong>Guess What -</strong> In this game (after the three practice rounds), kids are instructed to point at certain objects in the different rooms. Correctly answering one question makes the app display the next instruction.
                 </p>
              
                 <p class="color">
                 	<strong>Sort Me -</strong> In this one, objects come along one by one, and kids have to drag them to the image of the room where it is generally found (for instance, a shampoo can belongs to the bathroom). This is, in essence, a sorting game that helps users associate objects with the rooms they are usually present in.
                 </p>
                 
                 <center> <img src="appstories/mini5.png" alt="Minilobes - The House"> </center>  
              
                <p class="color">
                 	<strong>What Goes Together? -</strong> This game serves as an object-pairing activity as well as a memory game. 4 cards are shown, and on tap, they reveal specific household objects. The kids have to tap the two cards that have images of related objects (i.e., which are found in the same room) in succession.
                 </p>
              
                <p class="color">
                 	<strong>Note:</strong> Players are given 3 chances in this game, for finding the cards with associated objects. If a kid fails to answer that question after three attempts, (s)he is led to the next question/set of objects.
                 </p>
              
                <p class="color">
                 	<strong>The Mission -</strong> Living up to its name, the ‘Mission’ game gives assignments to children. The latter have to first find objects from the seven rooms (it can be present in any room). Once found, that object has to be tapped on. The player has the option to look through a room for the specified object, and if it is not present there, (s)he can continue the search in another room.
                 </p>
                 
                 <blockquote class="color">
		  	    	The best thing about Minilobes is that - all the games in it are inter-related...all of them are engaging, and they have a common goal - to familiarize kids with objects at home. Since children can easily switch from one game to another easily, there are no chances of boredom setting in either.
		  	    </blockquote>
                 
		  </div>
		</div>
		
        <div class="row">
		 
				<h3 class="color">Progressing Through The Levels</h3>
				
				<blockquote class="color">
					To keep kids interested, the game had to show that they were progressing in some way or the other. I had originally planned to make the Minilobes games level-based, and Hussain confirmed that this would be the right way to go. All that remained was deciding the number of questions and other such details.
				</blockquote>
              
                <p class="color">
					There were some initial doubts regarding the number of levels we should include in the four Minilobes games. Ultimately, we fixed 20 levels for each of the games. Each level were to have 5 questions. To add a touch of creativity, a character was also included near the bottom of the screen. It would progress through flowers, as each question is correctly answered.
				</p>
                 
                <center>
                  <img src="appstories/mini4.png" alt="Minilobes - The House">
                </center>
            <br>
                <blockquote class="color">
					Progressing through the levels in the Minilobes games ain’t tough...but children have to play well to move ahead. In each level, at least 3 out of the 5 questions have to be answered correctly, to earn one star - with 4 and 5 correct answers giving two stars and three stars respectively. There is competition...but everything is presented in a fun way
				</blockquote>
              
                <p class="color">
					All the games, obviously, have three mandatory ‘Practice’ rounds before the first level is unlocked.
				</p>
		  
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">Dual Language Support</h3>
					<p class="color">
						The Minilobes app for kids is available in two languages - English and Swedish. The application comes with high-end customizable features from ‘Settings’, interactive graphics and layout, and captivating animations. It is a kids’ game and a learning tool - all bundled into one.

					</p>
                    <center>
                  <img src="appstories/mini6.png" alt="Minilobes - The House">
                </center>
					<p class="color">
						Minilobes is currently in the final rounds of app testing, and will be launched on Apple App Store/Google Play Store soon. To date, each of our kids’ applications have received the thumbs-up from users worldwide, and we expect this new free app to continue that proud record!

					</p>		
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>