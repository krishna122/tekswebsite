<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Awayys</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Awayys</span>
                        </h1>
                        <a href="https://itunes.apple.com/us/app/awayys/id1074219724" target="_blank"><img alt="Awayys" src="img/appstore.png"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.awayys" target="_blank"><img alt="Awayys" src="img/play-store.png"></a>
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
                 Overseas trips can be lonely, boring affairs...if you do not have friends and acquaintances out there. Making new buddies at an unknown land is not particularly easy (or advisable!) either. A couple of years back, we had worked on an iPhone app called <a href="stopover.php">Stopover - a travelers</a>’ app which went on to win big at that year’s Talent Unleashed Awards. Awayys, conceptualized by Doris Llamas, is yet another multi-featured app for travelers - focused on ensuring that users are never ‘alone’, wherever they might be at any time. In essence, Awayys serves as an excellent iPhone travel networking app.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	From my decade-long experience as an app architect, I have learnt one thing...if an idea seems intriguing enough, it has a good potential of being transformed into a workable app. The concept of Awayys was easily among the most interesting I had come across in the last couple of quarters.
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">Impressive Idea To Start With</h3>
                 <p class="color">
                    A detailed free app quote was sent along to Ms. Llamas, within 24 hours of her having contacted us with the app idea. Arrangements were made to put her initial apprehensions (perfectly natural, since Ms. Llamas had agreed to deal with a company not in her country) to rest. The next couple of working days saw all the other formalities being taken care of...and the project started soon after.
                 </p>
		  	 </div>   
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/aways_screen_01.png" alt="Awayys" style="width:60%;"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">A Dedicated iPhone App</h3>
		  	    
		         <p class="color">
		         	We had a couple of brief discussions with the client about the desired device compatibility of the Awayys application. Finally, it was decided that it will be built as a iPhone social networking app for travelers. It will be a free application, and would be compatible with all Apple handsets running on iOS 8 (or later versions of the platform).
		         </p>
                
                <blockquote class="color">
			  	  Awayys was always meant to be an iOS application...an app for iPhone-users who were frequent travelers. The challenge, at least for me, was finding a mobile app company that would be competent enough to make the app in the best possible way. Let’s just say that coming across Hussain’s agency was a stroke of good fortune...These guys were fantastic!
			  </blockquote>	
		   </div>
		   
		  </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">Getting Onboard</h3>
		  	    <br>
		  	    <blockquote class="color">
		  	    	A social login feature seemed to be the right way to go for an app like Awayys. After all, it was a travel connections app - and what better social connection tool can there possibly be, than Facebook? We talked the matter over with Doris, and the Facebook login option was implemented in the app.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Installing Awayys on iPhone takes hardly a few seconds, and starting to use it is even simpler. All that users have to do is log in with their Facebook credentials - and they will be redirected straight to the ‘Friends’ screen. No elaborate registration forms, authentications, and all that jazz!
		         </p>
                 
                 <p class="color">
		          Once a person has logged in, (s)he gets to see three categories of connections in this innovative travel networking application. First, all his/her Facebook friends (who also happen to be fellow-users of Awayys) are listed under ‘Friends’. The ‘Friends of Friends’ tab lists out all the users who are not direct connections, but have mutual friends (for instance, User X is a friend of User Y, and User Y is a friend of User Z - so, User X is a ‘friend of friend’ for User Z). Finally, the ‘Everyone’ tab displays all the users of the Awayys app.
		         </p>
                 
                 <br>
		  	    <blockquote class="color">
		  	    	The categories of friends in Awayys is a lot like the structure we find in Facebook. I wanted to segregate the available connections for any particular user - to avoid confusions and clutter. Kudos to Team Teks for understanding this, and making this section of the app in just the way I had envisioned.
		  	    </blockquote>
                 
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/aways_screen_02.png" alt="Awayys" style="width:95%;">
		  	 </div> 
              
              <p class="color">
                    <strong>Note:</strong> Users can also view the actual physical distance (in kilometers) between themselves and each of their Awayys friends.
                </p>
              
		  </div>
        
        </div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-4">
                  <img src="appstories/aways_screen_03.png" alt="Awayys" style="width:90%;">
              </div>
                  
              <div class="col-lg-8">
		  		<h3 class="color">Sending Out Friend Requests...Awayys-Style!</h3>
		  		
                <p class="color">
		          Awayys is one of those apps that seamlessly blend in a fun quotient with custom functionalities. After due deliberations with Doris, we decided that the app will provide as many as 4 different options to send friend requests (via message). A person can select any of the options, depending on his/her preferences and the nature of the fellow-user (s)he wishes to get connected with.
		         </p>
              
		         <blockquote class="color">
		  	    	On Awayys, sending friend requests is fun. You can send request with a wink...or with a smiley...apart from, of course, the normal message. I feel that for people trying to make new acquaintances, customized options are a big thing - and my app delivers that.
                  </blockquote>
                  
                  <p class="color">
                    On the ‘Requests’ screen, a list of all the sent and received friend requests can be viewed by the user. Provided that (s)he is logged in on the app, real-time notifications will also be generated - as soon as a new request comes in.
                  </p>
                  
                  <blockquote class="color">
		  	    	Easy network-building is a must-have feature for any social application. Awayys makes it easy for individual users to connect with fellow-users. That, in my opinion, is one of the hallmarks of a user-friendly networking app.
                  </blockquote>
              </div>
            </div>
        </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Works As A Cool Chat App As Well</h3>
                 
                 <p class="color">
                 	What’s the point of adding a lot of friends anyway, if you cannot chat with them any time you wanted? Credit to Doris for understanding the necessity of real-time messaging feature in Awayys. With important inputs from her, our in-house iPhone app developers added a seamless chat feature in the application.
                 </p>
              
                 
                 <blockquote class="color">
		  	    	In addition to text chatting, users can also send and receive images through the messaging feature of Awayys. I wanted to make a traveler's’ app which would allow each user to exchange ideas, opinions, pictures with others at any time. For that, presence of the chat feature was important.
		  	    </blockquote>
              
              <p class="color">
                  All chats in the Awayys app are completely secure. There are no risks of unauthorized third-party access whatsoever.
              </p>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/aw1.jpg" alt="Awayys"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/aw2.jpg" alt="Awayys"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/aw3.jpg" alt="Awayys"></center></div>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/aw4.jpg" alt="Awayys"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/aw5.jpg" alt="Awayys"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/aw6.jpg" alt="Awayys"></div>
		  </div>
		</div>
		
        <div class="row">
		  <div class="col-lg-8"><br>
				<h3 class="color">Profile Matters</h3>
				<p class="color">
					The entire profile information (along with the FB profile picture) is fetched by this new iOS travel networking app - for creating the default profiles of users. People can change their pictures later on (from the first 6 profile pictures). A simple swipe gesture is required to change the ordering of photos and set a new profile image. Other sections of the profile can also be edited easily, and whenever required.
				</p>
				<blockquote>
					Robust social media integration is one of the high points of this app. On Awayys, the entire profile information and details of individual users are fetched directly from their respective Facebook profiles. Users do not have to spend time manually setting up their profiles...the app itself takes care of that!
				</blockquote>
              <p class="color">
                  Apart from general profile data, there are a couple of other interesting fields on this screen. The first of them is ‘Spiritual Affiliations’, where a user can specify his/her religious inclinations/preferences - and find friends based on that. The other is the ‘Last Visited Countries’, which displays all the countries any particular user has recently been to. Based on this information, a person can easily find like-minded travelers...people with similar travel preferences and love for specific destinations.
              </p>
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/aways_screen_04.png" alt="Awayys" style="width:70%;">
              </center>
		  </div>
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
              
					<h3 class="color">Filtering & Getting Connected</h3>
                    
                    <blockquote>
                        On Awayys, the ultimate power always remains in the hands of the end-users. He or she can search for friends on the basis of several criteria...and take the final call on whether to be connected with a particular fellow-user or not. Helping people find friends according to their preferences is the main objective of this application.
                    </blockquote>
                    
					<p class="color">
						Among ‘Friends’, a Awayys user can select connections according to gender (male/female/both) or geographical distance (i.e., finding friends from a radius of XX kilometers). In the ‘Friends of Friends’ section, the search can be filtered on the basis of age as well. By default, ‘Both’ is selected in the gender filter in ‘Friends of Friends’. A person can also type in his/her location in the ‘Search Place’ box, and check out the profiles of other users of the app within that area.
					</p>
                    
                    <blockquote>
                        As far as travel networking apps go, Awayys is a power-packed offering. By tapping the ‘Matching Trips’ filter, people heading for common travel destinations can get connected with each other. Individuals can also find new friends on the basis of their nationalities, spiritual affiliations, and even institutions attended. Loads of options, really!
                    </blockquote>
              
              <p class="color">
                    <strong>Note:</strong> All applied filters can be reset any time by the user.
                </p>
              
		  </div>
		</div>
        
        <div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-8"><br>
		  		<h3 class="color">Custom Privacy Options</h3>
		  	    <br>
		  	    <blockquote class="color">
		  	    	A user might wish to reach out to the entire Awayys network...or wish his profile to be visible only to a select group of people. Keeping that in mind, we added customized privacy options in the app.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          Profiles can be set to be viewable to the entire Awayys community, to ‘Friends of Friends’, or only to direct ‘Friends’. Once again, the final decision about his/her profile’s discoverability lies in the hands of the final user. 
		         </p>
                 
                 <p class="color">
		          The all-new iPhone travel networking app also allows people to ‘block’ or ‘report’ users - if necessary. When someone visits the profile screen of any other user, (s)he can check out how (s)he is connected to that person. Every bit of information is clearly mentioned, the layouts and in-app navigation are extremely user-oriented...and this app truly has a host of interactive, interesting features.
		         </p>
                 
		  	 </div>
		  	 
		  	 <div class="col-lg-4">
		  	 	<img src="appstories/aways_screen_06.png" alt="Awayys" style="width:75%;">
		  	 </div> 
              
              
		  </div>
            
        </div>
        
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
                        <strong>Awayys is available for free download from the Apple App Store.</strong> Early reports from users have confirmed our belief that this is a really path-breaking travel networking social application - and we are confident of it becoming really popular among users worldwide.
					</p>		
				</div>
		  </div>
		</div>
        
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>