<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Momo Taxi</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Momo Taxi</span>
                        </h1>
<!--                        <a href="https://itunes.apple.com/us/app/hellomind/id327538172" target="_blank"><img alt="HelloMind" src="img/appstore.png"></a>-->
<!--                        <a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img alt="HelloMind" src="img/play-store.png"></a>-->
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
	            The task of getting a cab right when you want it is no longer as daunting as it used to be even five years back. Apps like Uber and (in India) Ola have found widespread acceptance, and they have ensured that people can book taxis as per their convenience, whenever and wherever they need them. Team Teks also had the opportunity of working on one such innovative on-demand cab service app - named Momo Taxi. The project threw up fair few challenges, and is easily among the most interesting recent entries in our mobile app portfolio. 
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	Momo Taxi was an one-of-its-kind taxi service app for travelers. For the first phase, we decided to build the application exclusively for the Android platform. I looked up for good mobile app companies, came across the profile of Teksmobile, liked what I saw, and delegated the project to them.
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">Momo Taxi For Android</h3>
                 <p class="color">
                     While we have developed more than 1000 apps till date, this was the first time we worked on an application like Momo Taxi. After due deliberations about the backward compatibility of the app, it was decided that Momo Taxi will be compatible with Android 4.0 (Ice Cream Sandwich) and later versions. The preliminary formalities were completed quickly, and development started soon after.
                 </p>
		  	 </div>   
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/momo_screen_01.png" alt="Momo Taxi" ></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Logging In & Booking Taxis</h3>
		  	    
		         <p class="color">
		         	The process of logging on to the Momo Taxi app is pretty much straightforward (users can browse about in the app without signing in, but to book a cab, they need to register first). Apart from the regular email registration option, users also have the option of signing in to Momo Taxi with their Facebook credentials. The latter option, if anything, is quicker.
		         </p>
                
                <blockquote class="color">
			  	  In terms of user-friendliness, Momo Taxi would rank high. All that users have to do is check the availability of cabs nearby, enter the drop-off location, see the fare estimates, and then, confirm their booking. The waiting period is minimal, and people can track the navigation of their rides right on the map.
			    </blockquote>
                
                
		   </div>
		   
		  </div>
            
            <p class="color">
		         	To enhance the convenience factor of end-users, two additional information were made viewable at the time of booking. People can see the total count of available drivers, as well as the exact distance the nearest Momo Taxi driver is at. This gives a fairly clear idea of the length of the waiting period, before the cab arrives.
		         </p>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-7"><br>
		  		<h3 class="color">Two Apps and Dual Language Support</h3>
		  	    <br>
		  	    <blockquote class="color">
		  	    	Life is a complex affair. It throws up challenges and problems and fears...things which can bring in negativity. As a long-time expert on hypnosis and hypnotherapy, I felt that there was a scope of creating a mobile app that would effectively tackle these problems. And that’s how HelloMind was conceptualized.
		  	    </blockquote>
		  	    
		  	    <p class="color">
                    This all-new Android taxi app is available in two languages - English and Burmese. Our in-house team of translators worked in close collaboration with the client, to make sure that there were no linguistic differences between the two versions of the app (errors here might well have hampered the app’s functionality). We wanted to optimize the performance of both the English and Burmese versions of Momo Taxi - and thankfully, we managed to do just that.
		         </p>
                 
                 <br>
		  	    <blockquote class="color">
		  	    	Like most other one-demand taxi apps, Momo Taxi has two separate applications - the app for users, and the driver partner app. Both were built by the Android developers at Teks, under the supervision of Hussain. I am happy to announce that they did a mighty good job with my idea
		  	    </blockquote>
                 
                <p class="color">
                    (We will primarily deal with the users’ app over here. A few features of the Momo Taxi driver partner app will be touched on later in this case study).
		         </p>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-5">
		  	 	<img src="appstories/momo_screen_02.png" alt="Momo Taxi" style="width:85%;">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-5">
                  <img src="appstories/momo_screen_03.png" alt="Momo Taxi" style="width:75%;">
              </div>
                  
              <div class="col-lg-7">
		  		<h3 class="color">The Magic Of Result-Driven Hypnosis</h3>
		  		
                <p class="color">
		          While we did our bit to make HelloMind as useful and user-friendly as possible, the lion’s share of the overall effectiveness of the app should be attributed to Jacob and his team. A guided form of hypnosis - called Result-Driven Hypnosis, developed by Jacob himself - forms the basis of the new mobile self-help application. It focuses on identifying the underlying causes of each mental block/problem, and then proceeds to eliminate it.
		         </p>
              
		         <blockquote class="color">
		  	    	To get over a mental problem….say a fear or a lack of motivation...one has to understand the root cause for the problem first. Powered by my RDH technique, HelloMind gradually guides the subconscious mind to the factors that are causing the problem. This makes it easier for the conscious mind to get rid of the issue. It’s like...if you really know the problem, you will have the solution ready.
		  	    </blockquote>
		  	    
                <p class="color">
                    Once the user selects a category or theme, the app asks several simple, in-depth questions - to gain a proper insight of the problem to be tackled. For instance, if someone starts with the ‘Fear’ category, (s)he will be asked ‘Fear of what’? Let’s say, the response to this query is ‘fear of animal’. The next question generated by the app would be ‘Fear of which animal?’ The interactions proceed in this manner, and, based on the user-responses, the best treatment session is recommended.
                </p>    
            </div>  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Powerful GPS Support</h3>
              
                 <p class="color">
                 	Obviously, for the Momo Taxi app to function as desired, the GPS of the devices on which the app is installed has to be activated. With its powerful location tracking support via GPS, the app automatically detects the current location/pickup location of the users - the spot where the drivers have to come. Once the drop-off location is also entered, the trip is all set.
                 </p>
                 
		  </div>
		</div> 
		
        <div class="row">
		  <div class="col-lg-12"><br>
				<h3 class="color">More Power To Passengers</h3>
                 
                <blockquote class="color">
					From the very outset, I wanted people to get more out of Momo Taxi, than what they can from any other similar mobile application. Accordingly, I greenlighted the implementation of several features that offered extra benefits to users. These included offers and special deals from time to time.
				</blockquote>
              
                 <center>
                  <img src="appstories/momo_screen_04.png" alt="Momo Taxi">
                </center>
              
				<p class="color">
					A big advantage of hiring Momo Taxis is that, the fleet has no concept of ‘surges’ or ‘peak hour pricing’. The fare estimate shown at the time of booking is the amount that has to be paid by the passengers, every single time. Riders can also update their pickup and/or drop-off locations, after booking confirmation. There are no cancellation charges either.
				</p>
              
				<blockquote class="color">
					I travel a lot, and on occasions, I have faced problems with the matter of how much tip to pay to a taxi-driver. Momo Taxi efficiently handles this issue. In this app, passengers can choose the percentage of the fare to pay as tip. I can confidently predict that Momo Taxi will solve a lot of day-to-day problems for commuters.
				</blockquote>
		  </div>
                
	   </div>
        
        <div class="row">
            <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/momo_screen_05.png" alt="Momo Taxi" style="width:70%;">
              </center>
		  </div>
            
		  <div class="col-lg-8"><br>
				<h3 class="color">Customized Taxi Service</h3>
              
                <blockquote class="color">
					With Momo Taxi, users can book just the type of ride they are looking for. The Momo Air option offers AC taxis exclusively for those heading to the airport. They can select the name of the airport they wish to go to, from a drop-down list. The Momo Gold option is for people looking for non-AC cabs. Momo Black, on the other hand, has a separate fleet of luxury AC cars
				</blockquote>
              
				<p class="color">
					Not all taxi rides are the same. Different passengers have different needs, and more importantly, different preferences. At an early stage during the app development process of Momo Taxi, these facts were taken under consideration. There were several meetings with the client regarding this - and finally, we decided to introduce 3 different, custom cab services in the app.
				</p>
              
				<p class="color">
					In case a passenger decides to cancel a booking, (s)he only has to tap on the appropriate tab in the app. Two options are displayed to the user - ‘Momo Not Coming’ and ‘Sorry, Can’t Wait’ - and (s)he has to tap on whichever is suitable. As mentioned earlier, there are no charges on taxi cancellations.
				</p>
              
		  </div>
		  
	   </div>
        
         <div class="row">
		  <div class="col-lg-8"><br>
				<h3 class="color">Trip Ratings</h3>
              
				<p class="color">
					After each trip is complete, riders can rate it on this Android on-demand taxi app. There are multiple parameters (e,g., Ride timing, Driver behaviour, Car condition, etc.) - each of which has to be marked on a five-point scale. In addition, a person can also add comments about any trip. The driver ratings can be seen by users at the time of booking.
				</p>
              
                <blockquote class="color">
					An app that does not bring into play user feedback and ratings is destined to fail. I was delighted to note that the owner of the Momo Taxi project was very meticulous on this count. With the help of the detailed reviews after every trip, the overall service of the fleet could improve, and the app itself can be upgraded over time. It’s all about evolving
				</blockquote>
              
				<p class="color">
					The application also has a dedicated ‘Bulletin Board’ screen. Users can check this regularly, for the latest announcements and offers from the Momo Taxi screen. The app partners with leading outlets and service providers to bring attractive deals for regular riders.
				</p>
              
                <blockquote class="color">
					People can check out his or her Momo Taxi rides under the ‘History’ tab. All trips - completed, cancelled and in progress - can be viewed on the same screen
				</blockquote>
              
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/momo_screen_06.png" alt="Momo Taxi" >
              </center>
		  </div>
	   </div>
        
        
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">Momo Taxi - Driver Partner App</h3>
					<p class="color">
						The drivers’ app for Momo Taxi also has quite a few interesting, insightful features. From the app screen, a driver can read off figures like total fare collected and total distance covered - in daily, weekly or monthly view. Profile information can be updated easily, and drivers can also recharge their Wallet Money from the app.
					</p>
					<p class="color">
						The front-end UI of Momo Taxi was planned out in a way that, individuals (riders and drivers) would never face any difficulties while using the app. At every stage during the development, we worked closely with the client - and it would be fair to say that his valuable suggestions and feedback helped our Android app developers add an extra layer of efficiency to the Momo Taxi application. 	
					</p>	
                    <p class="color">
						Momo Taxi is under testing at present, and we are planning a full release at the Google Play Store pretty soon. This one is an on-demand mobile cab service provider with a difference - and we bet that you will like it when you try it! 	
					</p>
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>