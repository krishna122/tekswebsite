<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of ChildMinder</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">ChildMinder</span>
                        </h1>
<!--                        <a href="" target="_blank"><img alt="Minilobes - The House" src="img/appstore.png"></a>-->
<!--                    <a href="https://play.google.com/store/apps/details?id=eu.hypnosis.android&hl=en" target="_blank"><img alt="HelloMind" src="img/play-store.png"></a>-->
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
	            Sometime back, we had worked on an app called <a href="babysitter.php" target="_blank">Babysitter</a>. It was an app for parents looking for expert local caregivers for the young ones. The logic was simple: parents are busy, and they need to hire nannies for their kids. However, even for the most responsible nanny in the world - it is not the easiest task in the world to be thorough and systematic about how (s)he takes care of children. When we received the project proposal for ChildMinder, the first thought that came to our collective minds was -- “we can now do away with that concern too!”
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	Childminder was conceptualized as an information-rich, highly customizable childcare app, to be used by professional child minders as well as parents. It was supposed to help both parties collaborate effectively, and make the care services for kids more systematic, more comprehensive. Am impressed by the ability of the Teks team to first understand, and then implement my idea so thoroughly.
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">What Is The ChildMinder App All About?</h3>
                 <p class="color">
                     Users can register on the ChildMinder application either as a child caregiver (child minder) or as a parent. In this case study, we will primarily highlight the features of the app when it is being operated by child minders. Using the app as parents will be briefly touched upon later.
                 </p>
		  	 </div>   
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/child_minder_screen_01.png" alt="ChildMinder"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">ChildMinder As A iOS App</h3>
		  	    
		         <p class="color">
		         	The concept behind this application was sound, and our iPhone development team went to work on this right after the basic project formalities were complete. The client had already advised us to make Childminder exclusively for the iOS platform. The strategy did make a lot of sense.
		         </p>
                
                <blockquote class="color">
                The ChildMinder app is compatible with iOS 8 and later versions. People can install it on iPhone 6 and newer handsets. It is user-friendly, has a gamut of useful features, and should prove to be a handy digital tool for childcare services.
			  </blockquote>
                
                <p class="color">
                    Following a couple of rounds of brainstorming, we had a rough idea of what the users’ behaviour flow would be like, on the app. Extensive research was also done, to finalize the types of information to be included in it. For an application like ChildMinder, proper homework was essential - and our app developers were careful about that, before starting out with the actual coding.
                </p>
                	
		   </div>
              
		  </div>
            
            
        
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Adding A Child In ChildMinder</h3>
		  	    
                 <p class="color">
                     On this new iPhone childcare app, the caregivers have to create separate accounts for each kid assigned to his/her care. The procedure of adding children to the app is straightforward enough. What’s more, the parents/guardians of the added children can also be selected, directly from the phone contact list of the users.
                 </p>
                 
		  	    <blockquote class="color">
		  	    	If a caregiver on the ChildMinder app selects a guardian who is a fellow-user of the app, the two will be automatically connected as ‘Associates’. If a guardian is not a ChildMinder-user, the caregiver can send an invite to him...and when the invite is accepted, the two become ‘Associates’.
		  	    </blockquote>
              
              <img src="appstories/child_minder_screen_02.png" alt="ChildMinder">
		  	    
		  	    <p class="color">
                    From the very first phase, the iOS developers in charge of this project closely collaborated with our in-house graphics and UI design team. This was an app with a lot of promise, and it deserved to have just the right mix of practical utility, visual appeal and ease of use.
		         </p>
              
              <blockquote class="color">
		  	    	In my 10-odd years as a mobile app entrepreneur, I have learnt this: good design can often make or break a mobile app. For an app like ChildMinder, this factor was all the more important. The idea behind it was excellent, and we had to back it up with efficient coding and optimized front-end design plans.
		  	    </blockquote>
		         
		  </div>
            
		</div>
		
		<div class="row">
		  <div class="col-lg-6">
                  
		  		<h3 class="color">Lots Of Information. Lots Of Detailing.</h3>
                  
                <blockquote class="color">
		  	    	In ChildMinder, the caregivers can easily log various details about the kids under his or her care. In addition, they can also publish short private posts about a child. Only the parent(s) of that child are able to view, and comment on, such posts.
		  	    </blockquote>
              
                <p class="color">
		          Not every children are the same. They differ in their food habits, nap times, mood swings, and many other things. Nannies have to customize the care service provided to each of them accordingly. ChildMinder allows the caregivers to create, and constantly update, the stock of information about every child. That, in turn, helps in determining the precise type of service required.
		         </p>
              
		         <blockquote class="color">
		  	    	I did not want to have public posts in ChildMinder...simply because there was no need for everyone to read about stuff related to one particular child. Hussain agreed with me on this, and we finalized on allowing child minders to publish private posts. Only the parents of that child would be able to see that post
		  	    </blockquote>
              
              <p class="color">
                Registered child minders can log all the daily activities of kids - right from sleep habits and snack details, to mood changes - right on this custom iPhone application. The posts, on the other hand, can be about practically anything related to the children, that is considered to be important by the caregivers.
              </p>
           
		  </div>
            
            <div class="col-lg-6">
                <center> <img src="appstories/child_minder_screen_03.png" alt="ChildMinder" style="width:80%"> </center>
            </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Real-Time Chat Feature</h3>
                 
                 <p class="color">
                 	Taking proper care of a child needs a joint effort from nannies and parents. Keeping that under consideration, we incorporated a real-time chat functionality in the ChildMinder app. With the help of this feature, child minders can interact with single/both parents of kids (one child at a time). The entire thing would work like group chats.
                 </p>
              
                 <blockquote class="color">
                 	I feel that the chat feature adds extra value to this particular mobile app. The caregivers might have to contact and pass on information to the parents of a child at any time...maybe even on an emergency basis. ChildMinder ensures smooth two-way communication between these caregivers and parents.
                 </blockquote>
              
                 <p class="color">
                 	The child minders can also upload photos of children, or any document related to the little ones, in the app. From the ‘Settings’ tab, they can update their own profiles as well.
                 </p>
                 
                 <center> <img src="appstories/child_minder_screen_04.png" alt="ChildMinder" style="width:80%"> </center>
                 
		  </div>
		</div>
		
        <div class="row">
		      <div class="col-lg-6">
				<h3 class="color">Checking Out Older Information</h3>
				
				<blockquote class="color">
					At times, it becomes necessary to refer to previous reports and activity charts of a child - to chalk out the best care plan for him or her in the present. To facilitate this, I specifically asked the developers to make sure that users could change the date in the app, and view the older logs. In this app, accessing all the information about children is a cinch.
				</blockquote>
              
                <p class="color">
					Child logs can also be seen in calendar view. All that the child minder has to do is tap on a date, to reveal the logs created on that day. This option makes it that much easier for the nannies to keep track of all the old records, while planning out a childcare strategy.
				</p>
                 
                
            </div>
            
            <div class="col-lg-6">
                <center>
                  <img src="appstories/child_minder_screen_05.png" alt="ChildMinder" style="width:70%">
                </center>
            </div>
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<h3 class="color">ChildMinder For Parents</h3>
                    
					<p class="color">
						After registering as a ‘Parent’ on ChildMinder (and becoming an ‘Associate’ of the child caregivers), the guardians can see the daily logs, as well as the posts published by the child minder. To interact and express their opinions/feedback, parents have the option of commenting on these posts. Of course, they can also chat real-time with the nannies - as mentioned earlier.
					</p>
                    
                    <p class="color">
						ChildMinder is in its final stage of development, and it will be launched after our standard app testing procedure. This is one app that organizes the task of taking care of a child in a way like never before - and we believe it can join our long list of successful projects.
					</p>
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>