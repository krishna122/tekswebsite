<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of BabySitter</title>

	<?php include 'head.php';?>
    <link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="babysitter_story" style="height:34%;">
        <div class="babysitter_story-body">
            <div class="container" style="margin-top:5%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">BabySitter</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            There is hardly any feeling as blissful as becoming a parent. Caring for a baby, watching him/her grow, cooing sweet nothings to him/her, getting loving hugs from the little ones - there’s magic in everything related with small children. At Teknowledge, we wanted to make a dedicated mobile app for parents for a long time. The one thing that kept us stalled was the doubt regarding what the app would actually be about (we did not want to make just another gimicky application). The Baby Sitter project showed us the way.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  		<h3 class="color">Starting Out On The Project</h3>
		  	    <br>
		  	    <blockquote class="color blockbabysitter">
		  	    	The first thing I asked myself after receiving this app idea was how it was going to help parents. The answer was easy to find - most parents have busy personal/professional lives, they need to hire babysitters to look after their kids in their absence...and Baby Sitter was going to make the task of finding babysitters easier than ever. From the start, I was convinced that this app will deliver value to the final users, that is, the parents.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          The quote for this iPhone app for parents was sent along within 24 hours after we had received the request for the application. A couple of days went by to complete the formalities of the contract, following which the app development process started out in right earnest.
		         </p>
		  	 </div>
		  	 
		  	 <div class="col-lg-6 hidden-xs">
               <center>  <iframe src="https://appetize.io/embed/5rnh8ck9dgkf9pvx0evgg4tter?device=iphone6splus&scale=40&autoplay=true&orientation=portrait&deviceColor=white&xdocMsg=true" width="345px" height="600px" frameborder="0" scrolling="no"></iframe> </center>
<!--
		  	 	<video autoplay loop muted  poster="appstories/babysitter.jpg" style="border: 5px solid #2E93BF;"> 
				   <source src="video/appstories/babysitter.mp4" type="video/mp4"> 
				</video>
-->
		  	 </div>   
		  	 
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/bs3.png" alt="babysitter" width="90%"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">Two Different <strong>Views</strong></h3>
		  	    
		         <p class="color">
		         	Baby Sitter is a unique app - not only because it is good - but also since it is more than just a parents’ app. There are two different views on the app - the ‘Parent’ view and the ‘BabySitter’ view. For dads and moms looking for expert local caregivers, and for professional babysitters looking for jobs - this free iOS application is equally useful.
		         </p>

                 <blockquote class="color blockbabysitter">
		  	    	What I felt was, if my app only catered to the requirements of parents - it would be doing only half of the job it should do. After all, there are plenty of good babysitters looking for suitable jobs, and my app had the potential to help them in their quest as well. I shared this idea with Hussain and his team at Teks, and they concurred.
		  	    </blockquote>
		   </div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	<p></p>
		  	 <div class="col-lg-6">
		  		<h3 class="color">The Process Of Hiring Babysitters</h3>
		  	   
		  	    <p class="color">
		         	User-friendliness is a high point of the Babysitter application - and for that, all the credit goes to the inputs from the client as well as the commitment and expertise of our iOS development and graphic designing team. The in-app navigation is smooth, and the entire process of fixing appointments with multiple babysitters does not take more than five minutes (if that!).
		         </p>
		         
		         <blockquote class="color blockbabysitter">
		  	    	From the very outset, our focus was on making Baby Sitter an app where parents could find the best babysitter for their kids, as quickly as possible. After multiple consultations with the concept developer of the app, we chalked up and implemented a flow that, we felt, parents would find simple. Initial feedback confirmed that we were doing things right.
		  	    </blockquote>
		  	    
		  	 </div>
		  	 
		  	 <div class="col-lg-6"><br>
		  	 	<img src="appstories/bs4.png" alt="babysitter">
		  	 </div>   
		  	 
		  	  <p class="color">
		  	    	After registering and filling out their profile information, parents can browse through the profiles of a vast database of babysitters. All that they have to do is create a shortlist of the ones who seem to be the most suitable, and send along appointment requests to them. The actual hiring happens only after parents have met with the caregivers and talked things over with them. In a nutshell, Baby Sitter makes the entire process of availing the services of a babysitter more interactive, more informed.
		  	  </p>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">Local Babysitters. Good Babysitters</h3>
		  		<br>
		         <blockquote class="color blockbabysitter">
		  	    	With the Baby Sitter application, I wanted to help parents connect with the best local babysitters. Local is the key word here - for parents are generally, and rightly, wary of hiring caregivers who are not from the same locality. After discussions with the team of developers working on my project, it was decided that Baby Sitter would display profiles of babysitters from within 50 kms (max) from the users’ locations.
		  	    </blockquote>
		  	    
		  	    <div class="col-lg-6">
			  	     <p class="color">
			          	While the idea of hiring local babysitters with the app sounded good in theory - we ran into a slight hitch here. It was very much possible that there were no babysitters available at any point in time within 50 kilometers of the user-specified location. To tackle such situations, we included a special ‘Inform Me’ notification option. As soon as a new babysitter registers him/herself on the app, a notification is sent to parents - who can then proceed with fixing an appointment.
			         </p>
	                 
	                 <blockquote class="color blockbabysitter">
	                 	I would say that the most striking feature about this app is, it is not at a loss even when there are no local babysitters available. It allows people to stay in the loop, get real-time updates as and when new caregivers become available, and makes sure that parents do not have to search for babysitters from more distant places. This app remains useful...always.
	                 </blockquote>
                 </div>
                 
                 <div class="col-lg-6">
                   <img src="appstories/bs7.png" alt="babysitter">
                 </div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">That Thing About Payments</h3>
		  	    
		  	    
		         <p class="color">
		         	Yet another thing buzzing in our heads while we were working on this app for parents was the chance of possible discrepancies between the hourly rates demanded and offered. And how did we tackle it? Simple enough! Parents were made to specify the rate they were willing to pay - right at the time of registering on the app. Based on that information, babysitters who were looking for work for that range of compensation were displayed to them. That, in effect, ruled out the risks of probable altercations over money later on.
		         </p>
                 
                 <blockquote class="color blockbabysitter">
		  	    	Uncertainty about fees and charges often prevents people from being able to hire the best service providers. I was determined to ensure that users of the Baby Sitter app faced no such problems whatsoever. Since parents could view the rates demanded by the different registered babysitters, they could take a call on which ones to contact with ease. Who wants to haggle over money later on, anyway?
		  	    </blockquote>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/babysitterscreen1.jpg" alt="babysitter"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/babysitterscreen2.jpg" alt="babysitter"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/babysitterscreen3.jpg" alt="babysitter"></center></div>
		  
		  </div>
		</div>
		<p></p>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Baby Sitter For Babysitters</h3>
		  	    
		         <p class="color">
		         	After formulating the features that would be present in the ‘Parent View’ of this cool iPhone app for parents, we moved on to the ‘Babysitter View’. We felt that registered babysitters needed to be notified when parents try to get an appointment with them. As such, the ‘Babysitter view’ was created like a dashboard for the caregivers listed on the application. Right from viewing the count and status of appointment requests, to approving/declining requests - everything can be done from this section of the application.
		         </p>
                 
                 <blockquote class="color blockbabysitter">
		  	    	Baby Sitter has all the ingredients of becoming really popular - for it is equally beneficial for parents as well as babysitters. The latter can manage the appointment requests they receive - and get immediate notifications whenever their services are required. Things never get cluttered or confusing - a real high-point for an app that has, in any case, so many useful features.
		  	    </blockquote>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Expert Babysitters At Your Fingertips</h3>
		  	    
                 <blockquote class="color blockbabysitter">
		  	    	I had read and heard about many perfectly good app ideas that get ruined at the execution stage, simply because the developers working on them are not competent enough. As it gradually became evident, I had no reason for such worries regarding the development of my Baby Sitter application. It was being made by Teknowledge...and these guys were real good.
		  	    </blockquote>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="box">
				<p class="color">
					Working on the Baby Sitter project finally gave us the chance to give shape to our wish of making a nice mobile app for parents - one that actually delivered value to its intended users. App ideas like these do not come along everyday…and when they do, it’s a real pleasure to work on them.
				</p>		
			</div>
		  </div>
	  </div>
	  
  </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>