<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of bagpal</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Bagpal</span>
                        </h1>
                        <a href="http://bagpal.se/" target="_blank"><img alt="Bagpal" src="img/web.png"></a>
                    </div>  
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
        
        <div class="row">
          <div class="col-lg-12">
                 <p class="color">
                    Travelling is not always the easiest of tasks. Flight delays happen, problems can crop up during hotel bookings, there are always the odd health problems, and luggage can get misplaced. For people stranded at airports, we have the award-winning <a href="stopover.php">Stopover</a> app. We were keen to take up more projects to assist travelers, and the arrival of the bagpal project to our app company was simply great.
                 </p>
           </div>
        </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	As a frequent traveler myself, I am no stranger to baggage-related hassles...at airports, rail stations, practically anywhere. I strongly believe that the bagpal service will be of great help to all the travel-lovers out there.
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">What Is bagpal All About?</h3>
                 <p class="color">
                    In a nutshell, bagpal is an end-to-end baggage carrier service provider, for tourists to Scandinavia. It takes away one major worry of travelers - that’s for sure!
                 </p>
		  	 </div>   
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	
		  		<center><img src="appstories/bag5.png" alt="bagpal"></center>
		  	
		  		
		  		<h3 class="color">The Uniqueness Of bagpal</h3>
		  	    
		         <p class="color">
		         	At our mobile app and web development company, we are always on the lookout to create something new, something different from what’s already out there in the market. The bagpal project gave us an excellent opportunity in that regard. One of the main USPs of bagpal is that, it is the only Nordic service provider that allows every visitor going to, or returning from, any Nordic country to send along their luggage in advance. A novel endeavour, and a real handy assistance.
		         </p>
                
                <blockquote class="color">
			  	  I look at it this way...luggage-safety services are already available, and there is no point in coming up with another similar service. Many travelers like to send their bags and baggages in advance - and bagpal is oriented to cater to their needs. I was always confident about the idea - but credit to Hussain and his team on how well they have implemented it.
			  </blockquote>	
                
                <p class="color">
		         	A remarkable thing about the bagpal luggage transfer service in Scandinavia is that, the entire thing was conceptualized by a couple of travel-lovers - and not a big corporate house or anything. They did their research and found that Nordic countries did not have a single such service provider (unlike, say, in the United States) - and decided to address this issue with bagpal. An opportunity well identified, we must say.
		         </p>
                
		  </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-5">
		  		<h3 class="color">Not A ‘Weighty’ Service</h3>
		  	    
		  	    <p class="color">
                    Travelers often face a harrowing time at the luggage counters of airlines services. There are limits on the maximum weight allowed as cabin luggage, while the rest have to be checked-in and transferred separately. bagpal works in a different manner - there are no upper limits on the permissible baggage weight whatsoever.
		         </p>
                 
                 
		  	    <blockquote class="color">
                    The focus of bagpal has always been the same...it has to be a couple of notches more convenient than the regular airlines luggage service. From not having to stand at queues and doing away with the risks of damages, to not having any weight limit...I believe our service packs in more than enough to be liked and used by travelers.
		  	    </blockquote>
                 
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-7"><br>
		  	 	<img src="appstories/bag2.png" alt="HelloMind" >
		  	 </div>  
              
               <div class="col-lg-12">
              <p class="color">
                     When it comes to baggage transfer, people (unsurprisingly) look for services that offer reliability and affordability in equal measure. The bagpal service provides just that!
		         </p>
              </div>       
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-7">
                  <img src="appstories/bag3.png" alt="HelloMind">
              </div>
                  
              <div class="col-lg-5">
		  		<h3 class="color">The Rate Advantage</h3>
		  		
                <p class="color">
		          The fees for overweight baggages (on a per-kg basis) is significantly lower on bagpal - when compared to what most airline services charge. Add to that the lesser charges on special as well as general checked luggage - and it is clear why we feel bagpal is one of the most competitively-priced baggage carrier services. Ever.
		         </p>
              
		         <blockquote class="color">
		  	    	Flight travelers often end up paying exorbitant amounts on overweight luggages. The average charge for checked-in baggage at airlines is not exactly low either. With bagpal, people will be able to save quite a bit...without having to worry about the quality of the service.
		  	    </blockquote>    
            </div>  
		  </div>
		</div>
        
        <div class="row">
		  <div class="col-lg-12">
              
              <div class="col-lg-5">
		  		<h3 class="color">Can Send or Cannot Send?</h3>
		  		
                <p class="color">
		          What can and cannot be sent via bagpal is clearly spelt out on their website. That, in turn, removes uncertainties from the minds of travelers to Nordic countries. Vigilance on the nature and type of luggage items transferred is important, and bagpal does well on this count.
		         </p>
              
		         <blockquote class="color">
		  	    	It is easy to promise big things to attract customers, and then fail to live up to one’s own words. For bagpal, we always had a clear stand on the type of things we will never transfer. There are absolutely no chances of miscommunications.
		  	    </blockquote>
                  
               
            </div>  
              
              <div class="col-lg-7">
                  <img src="appstories/bag4.png" alt="HelloMind">
              </div>
              
              <div class="col-lg-12">
                  
                  <div class="col-lg-7"><br>
                      <img src="appstories/bag7.png" alt="HelloMind">
                  </div>
                  
                  <div class="col-lg-5">
                      <p class="color">
                          In keeping with international baggage carrier and transfer standards, transferring inflammable material, lithium batteries, firearms, aerosol products and such other potentially hazardous materials is prohibited on bagpal. The service focuses on personal and property safety, big time.
                      </p>
                      <p class="color">
                          On the other hand, a wide range of special luggages can be sent to Nordic destinations with the help of the bagpal service. Apart from suitcases and bags and trolleys, travelers can send snowboarding and skiing kits, surfboards, golf bags, strollers, and many other safe (and mighty important things for an enjoyable holiday in a Nordic country) items can be sent with the help of this service.
                      </p>
                  </div>
                  
              </div>
              
            </div>
        </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">Availing The bagpal Service</h3>
		  	    
                 <blockquote class="color">
		  	    	At first, me and my developers were just a bit uncertain regarding how the baggage pickup would be done, and whether the service would be user-friendly enough. We had several discussion sessions with the client on this. Finally, we decided to let users book shipments first, by selecting the time and exact place of pickup. All pickups are done between Monday and Friday, within working hours.
		  	    </blockquote>
                 
                 <p class="color">
                 	A bit about how travelers can actually avail the bagpal service here. The entire process is simple enough. All that a user has to do is book a pickup, by providing certain pertinent information (including the country of departure and details about the luggage item(s) that are to be transferred). Pickup can be done from homes, offices, and even golf courses. 
                 </p>
               
		  </div>
            <center><img src="appstories/bag1.png" alt="HelloMind"></center>
            
            <div class="col-lg-12">
                <p class="color">
                 	It is only natural that people who have sent luggages in advance would like to keep a tab on their stuff at all times. That’s the main reason why the shipment-tracking system in bagpal was implemented. From the delivery note or the bagpal website, people can find the Airway Bill/Shipment number. That is the reference number for tracking the location of shipments, as well as finding the approximate time when the baggage(s) will be delivered at the pre-specified destinations. A person, of course, has to be present to receive the consignment, and confirm.
                 </p>
            </div>
		</div>
		


		  	

		
        <div class="row">
		  <div class="col-lg-5">
				<h3 class="color">Choice Of Luggage Bags</h3>
				<p class="color">
					Every traveler to Nordic countries do not need to (and may not even like to!) carry the same amount of luggage for their trips. With that in perspective, bagpal Scandinavia AB offers a fairly impressive range of luggage bag options. Depending on the baggage volume and the duration for which the bag(s) will be in transit, users can take their pick and book shipments.
				</p>
				<blockquote>
					From small suitcases to XL suitcases and oversized bags, bagpal allows people to choose luggage bags according to their precise requirements. Standard and large golf bags can also be added or booked.
				</blockquote>
		  </div>
		  <div class="col-lg-7"><br>
              <center>
                  <img src="appstories/bag6.png" alt="HelloMind" >
              </center>
		  </div>
	   </div>
		
        
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
                        bagpal is a one-of-its-kind baggage carrier service that offers convenience, cost-effectiveness, safety and a host of custom shipment options. For people traveling to Nordic destinations, this is one service that has a lot to offer!    
					</p>		
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>