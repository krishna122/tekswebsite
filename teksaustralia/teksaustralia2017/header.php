<nav class="navbar navbar-custom navbar-fixed-top yamm" role="navigation" style="text-shadow: none;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" class="navbar-toggle pull-right" style="background: #fff;">
                    <i class="fa fa-bars" style="color:#000;"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">
                    <img style="max-width:75px; margin-top: -10px; margin-left:-10%;  outline:0;" src="img/logo.png" alt="Teksmobile India">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-inverse side-collapse in" style="margin-top: 14px;">
                <ul class="nav navbar-nav">
                    <li id="about">
                        <a class="page-scroll" href="aboutus.php" style="font-size:15px;font-weight: 300; color:#fff;text-transform: uppercase;">About</a>
                    </li>
                    
                    <li id="stories" class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle" id="grid" style="font-size:15px;font-weight: 300;color:#fff;text-transform: uppercase;">App Stories<b class="caret"></b></a>
                        <ul class="dropdown-menu" style="background-color:rgba(0,0,0,.8) !important;">
                            <li class="grid-demo">
                                <div class="row">
                                        <div class="col-sm-4 brd-right">
                                            <h3 style="font-size: 18px !important;color:#FF7018; font-size: 16px;font-weight: 300; ">OUR MOBILE APPS</h3><hr />
                                                <div class="row" style="">
                                                <div class="col-lg-8" style= "font-size: 14px; text-align: left;" id="sec1">
	                                                <a href="appstories.php">&nbsp; All Apps</a><br>
	                                                <a href="iphone-apps.php"><i class="fa fa-apple fa-fw"></i>&nbsp; iPhone</a><br>
	                                               	<a href="android-apps.php"><i class="fa fa-mobile fa-fw"></i>&nbsp; Android</a>
                                                </div>
                                                <div class="col-lg-4" style="font-size: 16px; text-align: left !important;"><img alt="..." src="img/cube1.png" id="cube1"></div>
                                                </div>
                                        </div>
                                        <div class="col-sm-4 brd-right" style="text-align: left;">
                                             <h3 style="font-size: 18px !important;color:#FF7018; font-size: 16px;font-weight: 300;">CATEGORIES</h3><hr />
                                             <div class="row">
                                                <div class="col-lg-6" style="font-size: 14px;text-align: left;"  id="sec2">
	                                                <a href="social.php"><i class="fa fa-facebook-official fa-fw"></i>&nbsp; Social</a><br>
	                                                <a href="fitness.php"><i class="fa fa-heartbeat fa-fw"></i>&nbsp; Fitness</a><br>
	                                                <a href="game.php"><i class="fa fa-gamepad fa-fw"></i>&nbsp; Game</a><br>
                                                </div>
                                                <div class="col-lg-6" style=" font-size: 14px;text-align: left;"  id="sec4">
	                                                <a href="business.php"><i class="fa fa-building-o fa-fw"></i>&nbsp; Business</a><br>
	                                                <a href="lifestyle-apps.php"><i class="fa fa-suitcase fa-fw"></i>&nbsp; Lifestyle</a><br>
	                                                <a href="productivity.php"><i class="fa fa-money fa-fw"></i>&nbsp; Productivity</a><br>
                                                </div>
                                             </div>
                                        </div>
                                        <div class="col-sm-4" style="text-align: left;">
                                            <h3 style="font-size: 18px !important;color:#FF7018; font-size: 16px;font-weight: 300; ">GADGETS</h3><hr />
                                            <div class="row">
                                                <div class="col-lg-8" style="font-size: 14px;text-align: left;" id="sec3">
	                                                <a href="watchkit.php"><i class="fa fa-clock-o fa-fw"></i>&nbsp; Watch-Kit</a><br>
	                                                <a href="bluetooth.php"><i class="fa fa-wifi fa-fw"></i>&nbsp; Bluetooth</a><br>
	                                                <a href="cloud.php"><i class="fa fa-cloud-download fa-fw"></i>&nbsp; Cloud</a><br>
                                                </div>
                                                <div class="col-lg-4"><img alt="..." src="img/cube2_lg.png" id="cube2"></div>
                                            </div> 
                                        </div>
                                        
                                    
                                </div>
                            </li>
                           
                        </ul>
                    </li> 
                    
                   <li id="api"> 
                   		<a class="page-scroll" href="api-design-and-development.php" style="font-size:15px;font-weight: 300;color:#fff;text-transform: uppercase; cursor: pointer;">API</a>
                   	</li>
                    
                    <li id="work">
                        <a class="page-scroll" href="howwework.php" style="font-size:15px;font-weight: 300;color:#fff;text-transform: uppercase;">HOW WE WORK</a>
                    </li>
                    
                    <li id="awards">
                        <a class="page-scroll" href="awards.php" style="font-size:15px;font-weight: 300;color:#fff; text-transform: uppercase;">Awards</a>
                    </li>
		            
                    <li id="clients">
                        <a class="page-scroll" href="clients.php" style="font-size:15px;font-weight: 300;color:#fff; text-transform: uppercase;">Clients</a>
                    </li>
                    
                    <li id="start">
                        <a class="page-scroll" href="startproject.php" style="font-size:15px;font-weight: 300;color:#fff; text-transform: uppercase;">Request A Quote</a>
                    </li>
                    
                    <li>
                        <a class="page-scroll" href="http://www.teksmobile.com.au/blog/" target="_blank" style="font-size:15px;font-weight: 900;color:#fff;text-transform: uppercase;">Blog</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1" style="margin-top:-5px;">
       <div class="hiddenscroll">
        <h3 style="font-size: 16px;font-size: 16px;font-weight: 300;">OUR MOBILE APPS</h3>
           <a href="appstories.php">iPhone</a>
           <a href="appstories.php">Android</a>
           <a href="appstories.php">Windows</a>
        <h3 style="font-size: 16px;font-size: 16px;font-weight: 300;">CATEGORIES</h3>
           <a href="appstories.php">Social</a>
           <a href="appstories.php">Fitness</a>
           <a href="appstories.php">Game</a>
           <a href="appstories.php">Business</a>
           <a href="appstories.php">Lifestyle</a>
           <a href="appstories.php">Finance</a>
        <h3 style="font-size: 16px;font-size: 16px;font-weight: 300;">GADGETS</h3>
           <a href="appstories.php">Watch-Kit</a>
           <a href="appstories.php">Bluetooth</a>
           <a href="appstories.php">Cloud</a>
           <a href="appstories.php">Cloud</a>
       </div>
    </div>
    <script>
   $("#grid").on('click',function(){
//         currLeft = $(this).offset().left-50;
//         $('.dropdown-menu').css({
//             left: currLeft + "px",
//             position:"absolute"
//         });

    	$('.dropdown-menu').slideToggle(300);
       
  	});


    </script>

        
        <style>
        .brd-right{
        	border-right: 1px solid #fff !important;
        }
        
        
        @media (max-width:767px) { 
        	.brd-right{
        		border-right: 1px solid #000 !important;
        	}
        	#cube1{
        		display: none;
        	}
        	#cube2{
        		display: none;
        	
        	}
        	#sec1{
/*         		text-align: center !important; */
				
				margin-left: 40px !important;
        	}
        	#sec2{
/*         		text-align: center !important; */
				margin-left: 40px !important;
        	}
        	#sec3{
/*         		text-align: center !important; */
				margin-left: 40px !important;
        	}
        	#sec4{
/*         		text-align: center !important; */
				margin-left: 40px !important;
        	}
        }
        </style>