<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Game Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>


    <!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Game Apps</span></h1>
	                    <p style="font-size: 25px;">Our team specializes in 2D/3D mobile game development. From gaming apps for kids to full-blown action, arcade and strategy games - we make them all.</p>
                </div>
            </div>
        </div>
    </div>
</header>
 
<section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Minilobes - The House</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Educational Game App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Minilobes is an engaging educational game for kids, customized for the iOS and the Android platforms. It has two separate modes - ‘Explore’ and ‘Game’ - and helps children learn about their homes and common household objects...the fun way.</p>
                      <br><br>

                      <a href="minilobe.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/minilobelogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
 
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Top Thaaat</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Selfi Challenge Game</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Top Thaaat is a one-of-its kind mobile selfie challenge game app. Users can either select from the games available in the app, or create their very own selfie challenges.</p>
                      <br><br>

                      <a href="topthaaat.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/topthaaatlogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
    
<section id="appstory" class="moovers">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Moover</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Word Game</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Moover is a two-player iOS word-search application. Users have to swipe along word mazes to find certain words, before their opponents manage to do so. Additional coins are available through in-app purchases.</p>
				  <br><br>

				  <a href="moovers.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/m.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>


<section id="appstory" class="princeofasia">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Prince Of Asia</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">How about donning the role of a prince, and rescuing a charming princess? Get set for Prince Of Asia - the strategy-based, multi-level role-playing game that merges fantasy fun with thrilling adventure. An app our game development agency is very proud of!</p>
				  <br><br>

				  <a href="princeofasia.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/princeofasia.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="rst">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Ready Set Text</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Typing Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Ready Set Text is a fun multiplayer mobile typing game for the iOS and Android platforms. Players earn coins by winning typing games, and can then use the coins to win attractive prizes. Both ‘Crowd Games’ and ‘Two-person games’ can be played in the Ready Set Text app.</p>
				  <br><br>

				  <a href="readysettext.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/rst.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>
