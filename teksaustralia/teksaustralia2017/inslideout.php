<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of InSlideOut</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="inslideout_story" style="height: 50%;">
        <div class="inslideout_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">InSlideOut</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>InSlideOut is one of the most detailed event-based app in the app portfolio of Teknowledge. Aaron Rosenzweig, who conceptualized the app, has got to be lauded for his vision - to help people across the world keep track of their favorite social events and sporting affairs, and attend them with ease. The iOS version of InSlide Out has been a huge hit (optimized for iPhone, iPad and iPod Touch), and plans are already in place for the creation of an Android version of the app pretty soon.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/io5.png" alt="InSlideOut">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
My team has created several event-based apps. However, it was a long-standing wish of mine to create an iPhone application that would double up as an event roster as well as a ticketing tool. This was the precise purpose behind InSlideOut.
                                <br><br>
                                <center>
				<img src="appstories/inslideoutclient.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Aaron Rosenzweig</span> <br> 
				<span style="font-size: 25px;">(Founder, Managing Partner, InSlideOut app)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>The Spirit Of InSlideOut</h4>
	      <P>Unlike many wannabe mobile concept developers, Aaron did not sit on the app idea for long. He looked up good iPhone app development companies online, came across our portal, and requested for a free app quote. Interestingly, this was one of the few cases where we could send along the app quote within a couple of hours. Aaron agreed to our terms and we were suitably impressed by his app idea (a 'win-win situation', so to speak) - and the project started from the following week.</p>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
There are plenty of events happening every day. Getting hold of their itineraries and being able to decide which one to attend is not a particularly easy task. During my rather frequent foreign tours, I have found that out for myself. The prospect of an app that would do all the hard work and keep users up-to-date definitely excited me and the other developers at Teks. We were up for it.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">So, What Type Of Events Does InSlideOut Update Users About?</h4>
             <div class="col-lg-6">
	      <P>Aaron had visualized InSlideOut to be an app that enabled people to find out the details about practically every type of high-profile events - right from concerts and live performances, to sporting face-offs. The iOS app developers at our company worked accordingly, to ensure the availability of real-time event feeds to users. From family events to a wild dance festival at a pub, InSlideOut informs people about, well, everything.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/io1.png" alt="InSlideOut">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
The one thing I positively hate is half-baked apps. I have never compromised with the idea behind any of my earlier applications, and InSlideOut was not going to be an exception. It was chalked out as a holistic event guide for users - and thankfully, the guys at Teks understood this, and were capable enough to customize the app accordingly.
                                <br><br>
                                <center>
				<img src="appstories/inslideoutclient.png" alt="Aaron Rosenzweig" style="width:20%;"><br> 
				<span style="font-size: 30px;">Aaron Rosenzweig</span> <br> 
				<span style="font-size: 25px;">(Founder, Managing Partner, InSlideOut app)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <P>An interesting aspect of this event-based iPhone app was its distinct focus on sporting events in particular. After a couple of brainstorming sessions with Aaron and his partner Henrik Olsson, it was finalized that the application would have the detailed itinerary for as many as 5 different sports (Baseball, Basketball, Football, Hockey and Soccer). People would be given the option to choose and follow as many teams as they wanted. Fan-following made easy!</p>	
	    </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
The sports-oriented feel of InSlideOut was the biggest reason why I downloaded it. I am a big fan of American football, and with this cool app, I haven't missed a game of the Chicago Bears since last October. I could even toggle between college-level and pro-level matches. Two thumbs-up for InSlideOut!
                                <br><br>
                                <center>
				<span style="font-size: 30px;">Phillippe Jones (iPhone 6-user)</span> <br> 
				<span style="font-size: 25px;">(downloaded InSlideOut on September 16, 2014)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4 style="font-size: 40px;">Much More Than 'Just Another Social-n-Sports events app'</h4>
	      <P>Developers at our mobile app company were keen to ensure that there was no 'me-too' feel about the InSlideOut application. When Aaron suggested the idea of having a screen via which people could follow the upcoming events of their favorite celebs - we were delighted. If anything, this featured turned out to be one of the USP's of this iOS social app.</p>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I wanted InSlideOut to be more than an event-information generation app. While thinking how to makeInSlideOut different - it struck me�plenty of people wanted to know only about the events and performances of the celebs they love. It made sense to let them do that via InSlideOut.
                                <br><br>
                                <center>
				<img src="appstories/inslideoutclient.png" alt="Aaron Rosenzweig" style="width:20%;"><br> 
				<span style="font-size: 30px;">Aaron Rosenzweig</span> <br> 
				<span style="font-size: 25px;">(Founder, Managing Partner, InSlideOut app)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>A Smart Virtual Decision-Maker</h4>
            <div class="col-lg-7">
	      <P>We agreed with Aaron that there was no point in putting users through the hassles of registering and/or spend time setting up their profiles on InSlideOut. Instead, the app was given the built-in functionality of creating user profiles automatically - as soon as people logged on. Our mobile app developers made sure that the app could pull in and 'learn' from the information provided by users, and serve as an accurate, timely, reliable social calendar.</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/io4.png" alt="InSlideOut">
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
                It's a dilemma for many to pick and choose the events to attend - particularly if two or more good bands are scheduled to perform at the same time. One of the best things about InSlideOut is that it generates automatic suggestions about the event a person would enjoy more. In a nutshell, the app is a top events guide.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Purchasing Event Tickets Through InSlideOut</h4>
            <div class="col-lg-5">
                <img src="appstories/io2.png" alt="InSlideOut">
              </div>
            <div class="col-lg-7">
	      <P>Unlike other apps in the same category, InSlideOut was neither visualized by Aaron, nor created by our app developers, as a database of only local events. The plan was to generate real-time event info from the city of individual users, as well as from other locations around the world. As per Aaron's advice, a secure mobile ticketing gateway was included in the app (every listed event had a 'Buy Ticket' tab). This went a long way in removing uncertainties regarding whether a person would be able to attend an event.</p>
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Only a socially active person would know the frustrations of having to stand in long queues for getting tickets to much-awaited events. Factors like distance and time-unavailability can also be major headaches. Me and Aaron wanted to make InSlideOut a reliable tool to purchase tickets from. People would no longer have to worry about 'Sold Out' boards at ticket counters!
                                <br><br>
                                <center>
				<span style="font-size: 30px;">Henrik Olsson</span> <br> 
				<span style="font-size: 25px;">(Co-Founding Partner, InSlideOut app)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Value As A Social Networking App</h4> 
            <p style="margin-top:-50px;margin-bottom:50px;">Initial feedback from users confirmed what Aaron and his team had always wished for - the InSlideOut iOS application could serve as a multipurpose social networking tool too. On the pretext of attending events in other cities, users could meet up with new people and visit new destinations�with absolute ease. Attending hot-n-happening events and making new acquaintances at foreign locations - with features like these, the success of InSlideOut was not entirely unprecedented.</p>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
Aaron came across as a person who really had a feel about what made mobile apps user-friendly. He floated the idea that, in addition to ticket-purchase options, InSlideOut should also give low-cost travel ideas to users. Creating groups and sharing event-related information within groups was a breeze as well.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <div class="col-lg-5">
            <img src="appstories/io3.png" alt="InSlideOut">
	      </div>
	      <div class="col-lg-7">
            <p>This app development project was completed in three weeks and a bit. We were particularly careful about the UI and graphic features - since a dull-looking social events app was never going to work. Our in-house UI/UX designers made sure that the app had engaging display features, the in-app navigation was fast and smooth, and the events database was updated on a real-time basis. With InSlideOut, it was all about giving customized information and ticket-buying options to users.</p>
	      </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#00A2A6;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I had envisioned InSlideOut as an app that required minimal personal information from users, and provided the maximum amount of relevant information. Credit to Hussain and his team - they made the app just the way I wanted.
                        <br><br>
                        <center>
			<img src="appstories/inslideoutclient.png" alt="Aaron Rosenzweig" style="width:20%;"><br> 
			<span style="font-size: 30px;">Aaron Rosenzweig</span> <br> 
			<span style="font-size: 25px;">(Founder, Managing Partner, InSlideOut app)</span>
			</center>
                            </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>