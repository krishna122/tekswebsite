<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of Lifestyle Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Lifestyle Apps</span></h1>
	                    <p style="font-size: 25px;">Image-based apps, third-party emoji keyboards, self-motivation applications - we have a diverse array of lifestyle apps in our kitty. Find the details about them here.</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
<section id="appstory" class="lifestyle">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">Lifestyle Travel</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Travel App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">The perfect mobile tour planner, information provider and real-time helper - all rolled into one. Conceptualized by Leonie Spencer - a long-time travel fan herself - this is an app that every travel-lover would love.</p>
				  <br><br>

				  <a href="lifestyle.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/lifestyle.png" align="center">
				</div>

		  </div>
		</div>
        <hr>
	</div>
</section>    
    
<section id="appstory" class="timesnaps">
	<div class="container">
		<div  style="width:100%;"><br/><br/>
			<div class="row">
		  		<div class="col-lg-12">
				  	<div class="col-lg-6">
				  		<span style="font-weight:bold; font-size: 25px; text-align: center;">Timesnaps</span><br>
				  		<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Photo/Video</span>
				  		<br><br>
				  		<p style="padding: 0px;" class="appcolor">For those who wish to capture, preserve and treasure the magic of fleeting moments, Timesnaps is a must-have application.  The app allows people to take images of practically anything at regular intervals, create slideshows with them, and revel at the changes that time can make!</p>
				  		<br><br>
				  		<a href="timesnaps.php" ><img src="img/view-project.png"></a><br><br>
				  	</div>

					<div class="col-lg-6 storiesimg">

				  	<img src="appimages/timesnaps.png" align="center">

				   </div>
		  	 </div>
		 </div>
	   </div>
	</div>
</section>


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>