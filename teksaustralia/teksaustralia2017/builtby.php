<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of BuiltBy</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
    <div class="icbf_story-body">
        <div class="container" style="margin-top: 8%">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h1>
                        <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">BuiltBy</span>
                    </h1>
                    <a href="https://itunes.apple.com/us/app/builtby/id1147981001?ls=1&mt=8" target="_blank"><img alt="BuiltBy" src="img/appstore.png"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.builtby" target="_blank"><img alt="BuiltBy" src="img/play-store.png"></a>
                </div>
                <div class="col-md-3"></div>
            </div><br>
        </div>
    </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
          <p class="color">
              BuiltBy is a customized and multi-featured project management mobile application. It has been created with the advanced React Native cross platform tool, for the iOS and Android platforms. Although practically all types of projects can be added and managed with BuiltBy, the application is particularly focused on architectural projects.
          </p>
        </div>
    </div>
    </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
              <h3 class="color" style="margin: 0em 0;">Platform & Device Compatibility</h3>
              <p class="color">
                  The iOS version of the BuiltBy application is compatible with iOS 8 and later versions. People can install and use it on both iPhone and iPad. Its Android counterpart, on the other hand, has been built for Android 4.0 Ice Cream Sandwich and later versions. Once again, the app is available both on phones and tablets.
              </p>
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/bb2.png" alt="HelloMind" style="width:60%;"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">The Need</h3>
		  	    
		         <p class="color">
		         	In the absence of proper project and team management, it does not take long for inefficiencies to crop up - leading to diminished productivities and resultant delays. For an individual manager, manually keeping tab of each and every aspect of a project is tough...and the task becomes well nigh impossible as the number of projects increase. BuiltBy comes across as a readymade, powerful tool to help users manage all their projects more efficiently, more effectively. A cool new mobile project manager!
		         </p>
                
		   </div>
		   
		  </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  		<h3 class="color">Getting Onboard</h3>
		  	    
		  	    <p class="color">
                    As with any good app, the onboarding process of BuiltBy is easy, quick, and most importantly, secure. After launching the app, a user has to provide his/her mobile number on the screen. A one-time-password (OTP) is generated and sent to that number. The person can now complete the registration with that OTP.
		         </p>
                 
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<img src="appstories/bb1.png" alt="HelloMind" style="width:75%;">
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-5">
                  <img src="appstories/bb4.png" alt="HelloMind" style="width:80%;">
              </div>
                  
              <div class="col-lg-7">
		  		<h3 class="color">Working With React Native</h3>
		  		
                <p class="color">
                    It was a challenge, but a familiar one at that. The Teksmobile Team had already worked with the React Native cross-platform tool for quite a few projects in the second half of 2015. Our mobile app developers performed in-depth research to identify all the opportunities where the tool could be used. The BuiltBy project indeed threw up several scopes where the React Native tool could be utilized. It was all about coding once and making the screens ready for both platforms.
		         </p>   
            </div>  
		  </div>
		</div>
		
		<div class="row">
            <div class="col-lg-12">
                <h3 class="color">Adding Projects</h3>
                <p class="color">
                    One of the biggest USPs of the BuiltBy app is its overall user-friendliness. Registered users can start adding projects without any problems whatsoever. There is a small ‘+’ tab on the Home Screen, which has to be tapped - to include a new project. The screen also has all the existing projects in list view.
                 </p>
                <p class="color">
                    At the time of adding a new project, a wide range of details associated with it can be included as well. Apart from the name of the project, address and locality information can be provided - and users also have the option of selecting from the map view. These data makes each project more well-rounded, and adds an extra layer of relevance to them.
                 </p>
            </div>
        </div>
		
        <div class="row">
		  <div class="col-lg-12">
              <h3 class="color">Collaborating With Team Members</h3>
              <p class="color">
                  Once a project has been created in BuiltBy, team members can be added to it - directly from the user’s contact book. Members who are fellow-users of the application are automatically added to the project under question (i.e., there is an ADD option for them). For non-registered users, there is the option to send invites. When the invitation is accepted by the recipient, (s)he becomes a member of the project.
              </p>
		  </div>
	   </div>
        
        <div class="row">
		  <div class="col-lg-8">
              <h3 class="color">One Project. Six Key Features</h3>
              <p class="color">
                  BuiltBy offers an innovative way to facilitate 360-degree management of projects. Each project on the app has as many as 6 built-in features:
              </p>
              <ul class="color" style="list-style:none;">
                  <li> <strong>i) Activity -</strong> The activities of all the members are logged on the screen. This gives users a clear idea of what everyone else is doing.</li>
                  <li> <strong>ii) To-Do -</strong> With this, a person can add task for him/herself OR assign it to a team-member. </li>
                  <li> <strong>iii) Images -</strong> Any type of photos, related to the project, can be uploaded directly on the app. The uploaded photo(s) are viewable to all the team-members.</li>
                  <li> <strong>iv) Note -</strong> This is more of a private form of communication. A user can create a note, and specify the names of team-members who can view it.</li>
                  <li> <strong>v) Members -</strong> This option allows the user to view the profiles of all team members on the same screen. The designations of everyone involved in a project are also displayed.</li>
                  <li><strong> vi) Chats -</strong> For real-time management and interaction, an app like BuiltBy simply cried out for a built-in live chat feature. People can initiate group chats and invite all (or select) members of the project. Apart from text chatting, other files can be attached in the chats as well.</li>
              </ul>
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/bb3.png" alt="HelloMind" style="width:70%;">
              </center>
		  </div>
	   </div>
        
        <div class="row">
		  <div class="col-lg-12">
              <h3 class="color">Complete Customization & Quality Assurance</h3>
              <p class="color">
                  BuiltBy functions as a dynamic, cross-platform mobile app - with users having the opportunity to personalize things. For instance, the project details originally entered can be changed later. For providing a more compact view of project progress, all the activities can be seen on a single screen (there are similar options for ‘All chats’ and ‘All To-Do-s’). It’s all about checking information at a glance - especially when there is no need to go to the detailed screens.
              </p>
              <p class="color">
                  The app has been created with advanced development tools and resources, and in accordance with the latest industry quality-standards. Build iterations were tested at separate stages, to identify and remove bugs and minor functionality issues. Our developers closely collaborated with the project owner (as we do with all our clients), to ensure that the app was indeed turning out to be as he had imagined it would.
              </p>
		  </div>
		</div>
        
        <div class="row">
		  <div class="col-lg-12">
              <h3 class="color">Role Of React Native In BuiltBy</h3>
              <p class="color">
                  In-depth working knowledge of this tool helped us a great deal during the development stages of BuiltBy. React Native was used in several use cases, among which these are worth a separate mention:
              </p>
              <ul class="color">
                  <li>(a) The screens of BuiltBy have been designed with React - for the implementation of our ‘Code Once. Use Later’ principle. Our developers had to do the actual coding only once, and the tool ensured seamless cross-platform availability.</li>
                  <li>(b) For the Live Chat feature (in projects), Firebase has been used with React.</li>
                  <li>(c) Usage of bridges were minimized in the BuiltBy project, so as to reduce problems with ReactNative</li>
                  <li>(d) React Native was also used by the Teksmobile developers to add enhanced scalability and portability to the app.</li>
              </ul>
              <p class="color">
                  <strong>Note:</strong> React Native is an open source tool for cross-platform (iOS and Android) app development (with React).
              </p>
		  </div>
		</div>
        
        <div class="row">
            <h3 class="color">FounderSpeak</h3>
            <blockquote class="color">
                I have been associated with several architectural firms over the last two decades...and project management related problems were common everywhere. I personally feel that such problems can seriously hamper productivity levels, delaying tasks and messing up with timelines. There was a need for a handy mobile tool for all-round project managing...and that’s precisely why I conceived the idea of BuiltBy.
                <span>
                    -- Neesha Alwani<br>
                    (Co-founder, BuiltBy)
                </span>
            </blockquote>
        </div>
		
		<div class="row">
            <div class="col-lg-6">
                <center>
                  <img src="appstories/bb5.png" alt="HelloMind" style="width:60%;">
              </center>
            </div>
		    <div class="col-lg-6">
		  		<div class="box">
					<h3 class="color">BuiltBy Web</h3>
					<p class="color">
						The features of the BuiltBy app can be accessed from its official website as well. There is a built-in QR code in the application, which has to be scanned, to use it right in the website. Once again, this adds to the convenience-factor of end-users.
					</p>
					<p class="color">
						The BuiltBy project is a revolutionary way to look at project and team management tasks and collaborations for architects and more. Its available at the Apple App Store & Google Play Store.	
					</p>		
				</div>
		    </div>
		</div>
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>