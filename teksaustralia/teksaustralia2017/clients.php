<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>CLIENTS</title>

	<?php include 'head.php';?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href="css/appstoriesnew.css" rel="stylesheet">
    
    <style>
		p {
		  font-size:1em
		}
        
        .yt {
           position: relative;
        }
        .yt a {
           position: absolute;
           display: block;
           background: url(img/ytplaybtn.png);
           height: 64px;
           width: 64px;
           top: 50%;
           left: 50%;
           margin: -20px 0 0 -20px;
        }
        
        .modal-body {
            position: relative;
            padding: 0px;
        }
        .modal-header {
            padding-bottom: 0px;
            border-bottom: 1px solid transparent;
            background:#000;
        }
        
    </style>
    
   
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">
    

<?php include 'header.php';?>
	
		<!-- Intro Header -->
<header class="clients" style="height: 60%;">
    <div class="clients-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">400+ HAPPY CLIENTS. 1000+ MOBILE APPS </span></h1>
                    <p style="font-size: 25px;">We LOVE making apps...and our customers LOVE what we do!</p>
                </div>
            </div>
        </div>
    </div>
</header>

<section class="offwhite-background">
    <div class="container">
		<br>
		<div class="row">
			<h3 class=" color text-center">Great Apps Make For Great Reviews!</h3>
			<p class=" color text-center" style="line-height:0px;">Here's What Our Happy Clients Have To Say:</p>
		</div>
		<br>
        <div class="row">
        
        	<!-- 
			<div class="col-md-4">
                <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;"><iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="560" height="315" src="https://www.youtube.com/embed/rbGnVFydiJw?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
            </div>
            <div class="col-md-4">
                <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;"><iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="560" height="315" src="https://www.youtube.com/embed/STAMmogFJpg?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
            </div>
            <div class="col-md-4">
                <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;"><iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="560" height="315" src="https://www.youtube.com/embed/LAgGx0TeC5M?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
            </div>
 			-->
            
            <div class="col-md-4">
                <div class="yt" data-toggle="modal" data-target="#yt1" style="cursor:pointer;">
                    <img src="img/yt1.png">
                    <a></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="yt" data-toggle="modal" data-target="#yt2" style="cursor:pointer;">
                    <img src="img/yt2.png">
                    <a></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="yt" data-toggle="modal" data-target="#yt3" style="cursor:pointer;">
                    <img src="img/yt3.png">
                    <a></a>
                </div>
            </div>
        </div>
        
        <div class="row">
            
			 <div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a>Inslide Out </a>
						        <p>Technical expertise meets creativity meets commitment to excellence at Teksmobile. They have this amazing teamwork going, to make sure applications are created as per the precise preference of clients like myself. The skill factor is extremely high here, and they have a keen eye for innovation too. Highly recommended!</p>
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/02/aaron.png" style="width:90px; height:81px;"/>
								<a>Aaron Rosenzweig</a>
								<i>Founder, Managing Partner</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Drift Keyboard</a>
						        <p>One of the first things that struck me about Teks was their portfolio. After all, not many companies can claim to have worked on more than 1000 app projects. The developers and designers work with a high level of efficiency, intensity and they have a nice imaginative streak going as well. They more than matched up to my expectations...much appreciated!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/hazlett.png" style="width:90px; height:81px;"/>
								<a>Paul Hazlett</a>
								<i>Lead Software Engineer, Owner</i>
							</div>
						</div>
						
						<div class=" col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Veeaie Keyboard</a>
						        <p>Teksmobile is one of those rare companies that actually put in extra hard yards to add real value to their projects. They are always in touch with clients, carefully chalking out the next round of activities during a project. Not only do they make world-class mobile applications...but they are also available on a 24x7 basis. Great stuff.</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/04/v8tejmur.png" style="width:90px; height:81px;"/>
								<a>Tejmur Sattarov</a>
								<i>Senior Art Director & Concept Developer</i>
							</div>
						</div>            
		</div>
		
		
		<div class="row">
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >I Can Be Anything</a>
						        <p>Easily one of the best mobile app companies in Denmark. The guys at Team Teks have excellent technological and quality standards. We were just a tad apprehensive at first, while looking for an app agency...and we’re so glad to have found Hussain’s company. Forget being mere app developers - these guys are real app architects!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="http://teks.co.in/site/blog/wp-content/uploads/2015/02/unnamed-24.png" style="width:90px; height:81px;"/>
								<a>Jacob Strachotta</a>
								<i>Co-founder</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a>Dataworks</a>
						        <p>An Android app with Bluetooth technology as its main driver - the project we hired Teksmobile for was not the easiest out there. Credit to them...they took time out to understand the essence of the app idea, and then proceeded to use cutting-edge technologies for development. Dataworks was always meant to be an innovative app - and Teks helped us make it just that.</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/willLawler.png" style="width:90px; height:81px;"/>
								<a>Will Lawler</a>
								<i>Instrument Works Pty Ltd.</i>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-6">
        				    <div class="block-text rel zmin">
						        <a >Smarttrips</a>
						        <p>Hussain’s team is an absolute delight to work with. Right from the time of providing the free app quote, to the successive stages of designing, development and testing - they were so sincere about everything...and they kept explaining every single thing to me. I also found them more than eager to resolve all my queries, and discuss how the project was to be taken forward. An app company you can really, really trust on!</p>
							    
					        </div>
							<div class="person-text rel">
				                <img src="appstories/lionei.png" style="width:90px; height:81px;"/>
								<a>Leonie Spencer</a>
								<i>Managing Director</i>
							</div>
						</div>
						
				</div>
			
        </div>
    </div>
</section>


<section class="broun-block">
	
	<div class="container">
		<div class="row">
		  <div class="col-lg-42">
		  		<h2 class=" text-center" style="line-height: 0px !important;">Our Top Projects</h2>
		  	    <p class=" text-center" style="line-height: 0px !important;">
		          Check out some key projects in our 1000+ app portfolio
		         </p>
		  </div>
		</div>
		<br>
		
	    <div class="row">
	  	<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/1_cloud.png">
                </div>
				<span>1Cloud2</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/baby_sitter.png">
                </div>
				<span>babysitter</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/currently.png">
                </div>
				<span>Currently</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/dimes-loops.png">
                </div>
				<span>Looops</span>
        </div>
        </center>
	</div>
		<br>
    <div class="row">    
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/fuel_up.png">
                </div>
				<span>FuelUp</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/icbf.png">
                </div>
				<span>I Can Be Anything</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/inslid_out.png">
                </div>
				<span>Inslide Out</span>
        </div>
        </center>
        
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/kids_tiles.png">
                </div>
				<span>Kids Tiles</span>
        </div>
        </center>
	</div>
		<br>
	<div class="row">	
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/prince.png">
                </div>
				<span>Prince Of Asia</span>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/property_match.png">
                </div>
				<span>Property Match</span>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                   <img src="img/smart_trips.png">
                </div>
				<span>Smarttrips</span>
        </div>
        </center>
        
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/speedy_key.png">
                </div>
				<span>SpeedyKey</span>
        </div>
        </center>
		
    </div>
		
		<br>
		
		<div class="row">
	  	
        <center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/times_snap.png">
                </div>
				<span>Timesnap</span>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/veeie.png">
                </div>
				<span>Veeaie Keyboard</span>
        </div>
        </center>
	
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/vuwsa.png">
                </div>
				<span>VUWSA</span>
        </div>
        </center>
		
		<center>
        <div class="col-xs-3 col-sm-3 col-lg-3">
                <div class="icon">
                    <img src="img/aways.png">
                </div>
				<span>awayys</span>
        </div>
        </center>
		
        
    </div>
		
		
     </div>
</section>
    
  
    
<!--    yt1-->
    

  <div class="modal fade" id="yt1" role="dialog">
    <div class="modal-dialog modal-lg">
    
<!--       Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="text-center">Matt Barrie recommends Teksmobile ( Hussain Fakhruddin ) for all mobile app development</p>
          </div>
          <div class="modal-body">
              <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;">
                <iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="860" height="615" src="https://www.youtube.com/embed/LAgGx0TeC5M" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
      </div>
      
    </div>
  </div>

    
<!--    yt2-->
    
  <div class="modal fade" id="yt2" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="text-center" style="padding: 0px;">Teksmobile : Interview with Leonie from Lifestyle Travel Ballarat, Australia</p>
          </div>
        <div class="modal-body">
            <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;">
                <iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="560" height="315" src="https://www.youtube.com/embed/STAMmogFJpg" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
      </div>
      
    </div>
  </div>
    
<!--    yt3-->
    
  <div class="modal fade" id="yt3" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <p class="text-center">Teksmobile : Interview with Doris Llamas ( Creator of Awayys app )</p>
          </div>
        <div class="modal-body">
            <div class="video" style="height: 0;position: relative;padding-bottom: 56.25%;">
                <iframe style=" position: absolute; left: 0; top: 0; width: 100%; height: 100%;" width="560" height="315" src="https://www.youtube.com/embed/rbGnVFydiJw" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
      </div>
      
    </div>
  </div>    


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#clients').addClass('active');
	});

</script>
    


</body>
</html>