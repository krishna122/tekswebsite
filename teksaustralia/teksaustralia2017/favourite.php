<div class="favouriteapps cbp-so-scroller" id="cbp-so-scroller">
    
<div data-favouriteapp="3" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav7">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="1" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="1" class="phone__reveal__wrap">
								<video poster="video/hellomind.jpg" id="video--1" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/hellomind.mp4"></source>
									<source type="video/webm" src="video/vidWebM/hellomind.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/hellomind.png" alt="Hellomind" class="favouriteapp__logo__img"><br>

<!--
					<a class="" href="hellomind.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
-->

					<br>
                    <a href="https://itunes.apple.com/us/app/hellomind/id327538172" target="_blank">
                        <img width="290" height="99" src="img/appstore.png" alt="Appstore">
                    </a>
					

				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
                    <h1 style="text-align:left;">Hellomind</h1>
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Mobile self-motivational app (iOS/Android)</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Based on Result-Driven Hypnosis (RDH)</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > High-quality ‘Treatments’ (audio sessions) & ‘Boosters’</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Helps to identify root causes of mental problems</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Top grosser at Apple App Store</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>

<div data-favouriteapp="1" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav1">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="1" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="1" class="phone__reveal__wrap">
								<video poster="video/flypal.jpg" id="video--1" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/flypal.mp4"></source>
									<source type="video/webm" src="video/vidWebM/flypal.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav1.png" alt="flypal" class="favouriteapp__logo__img"><br>

					<a class="" href="flypal.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>

					<br>
                    <a href="https://itunes.apple.com/us/app/flypal/id1087898179" target="_blank">
                        <img width="290" height="99" src="img/appstore.png" alt="Appstore">
                    </a>
					

				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
                    <h1 style="text-align:left;">Flypal</h1>
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Makes aviation management easier than ever.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Real-time communication features</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Three separate user roles.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Secure documentation & Graph Reports.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;" > Available for iOS and Android.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>


<div data-favouriteapp="6" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav3">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/topthaat.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/topthat.mp4"></source>
									<source type="video/webm" src="video/vidWebM/topthat.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav3.png" alt="" class="favouriteapp__logo__img"><br>
<!--
					<a class="" href="topthaat.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
-->
						<br>
					<a href="https://itunes.apple.com/us/app/top-thaaat-the-selfie-game/id1097716427" target="_blank">
						<img width="290" height="99" src="img/appstore.png" alt="">
					</a>
				</div><!--
			 --><!-- .favouriteapp__phone
			 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
                    <h1 style="text-align:left;">Topthaaat</h1>
					
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Exciting selfie challenge game (iOS)</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Games with 10 challenges.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Over 20 cool badges.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Two separate game categories.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Secure Media Lobby storage.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>





<div data-favouriteapp="2" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav2">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="2" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="2" class="phone__reveal__wrap">
								<video poster="video/fuelup.jpg" id="video--2" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/fuelup.mp4"></source>
									<source type="video/webm" src="video/vidWebM/fuelup.webm"></source>
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>

		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav2.png" alt="fuelup" class="favouriteapp__logo__img"><br>
					<a class="" href="fuelup.php">
					<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
					<br>
                   <a href="https://itunes.apple.com/us/app/fuelup-your-mobile-gas-station!!/id1104355539" target="_blank">
				   		<img width="290" height="99" src="img/appstore.png" alt="Get beta version">
				   </a>
				</div><!--
			 --><!-- .favouriteapp__phone
		 --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
                <h1 style="text-align:left;">FuelUp</h1>
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> High-utility mobile fuel finder</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Optimized for iOS 8 (and later)</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Quick fuel requests.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Powerful GPS support.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Complete payment security.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>



<div data-favouriteapp="9" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav5">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/awayys.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/awayys.mp4">
									<source type="video/webm" src="video/vidWebM/awayys.webm">
								</video>
							</div>
						</div>
					</div><!-- .phone__responsive__inner -->
				</div><!-- .phone__responsive -->
			</div>
		</div>
        
		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav5.png" alt="SpeedyKey" class="favouriteapp__logo__img"><br>
<!--
					<a class="" href="awayys.php">
						<img width="290" height="99" src="img/learn-more.png" alt="Learn More">
					</a>
-->
					<br>
					<a href="https://itunes.apple.com/us/app/awayys/id1074219724" target="_blank">
						<img width="290" height="99" src="img/appstore.png" alt="Available in appstore">
					</a>
				</div><!--
			 --><!-- .favouriteapp__phone
			  --><div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
                <h1 style="text-align:left;">Awayys</h1>
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> iPhone travel networking app.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Compatible with iOS 8 and later.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Comprehensive city coverage.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Three 'Connection' categories.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Cool chat and filtering options.</li>
					</ul>
				</div><!-- .project__desc -->
			</div><!-- .project__content -->
		</div>
	</div><!-- .center-v -->
</div>


<!-- <div data-favouriteapp="10" class="favouriteapp favouriteapp--iphone">
	<div class="favouriteapp__inner fav6">

		<div class="favouriteapp__clip">
			<div class="favouriteapp__phone">
				
				<div class="phone__responsive">
					<div class="phone__responsive__inner">
						<div class="phone__responsive__bg">
							<div data-reveal-bg="5" class="responsive__bg__wrap">
								<img width="377" height="787" src="img/iphone.png" aria-hidden="true" alt="">
							</div>
						</div>
						<div class="phone__reveal">
							<div data-reveal-wrap="5" class="phone__reveal__wrap">
								<video poster="video/smarttrips.jpg" id="video--5" autoplay="autoplay" loop="loop">
									<source type="video/mp4" src="video/vidMp4/ICBF_x264_002.mp4">
									<source type="video/webm" src="video/vidWebM/ICBF_VP8.webm">
								</video>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
		<div class="wrap">
			<div class="favouriteapp__content cbp-so-section">
				<div class="favouriteapp__logo cbp-so-side cbp-so-side-left">
					<img src="img/fav6.png" alt="SpeedyKey" class="favouriteapp__logo__img"><br>

					<br>
					<a href="" target="_blank">
						<img width="290" height="99" src="img/appstore.png" alt="Available in appstore">
					</a>
				</div>
			
			    <div class="favouriteapp__desc cbp-so-side cbp-so-side-right">
					<h1 style="text-align:left;">Smarttrips</h1>
					<ul>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Personal travel planner app</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Comprehensive city guides.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Chat feature with travel agents.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Secure travel document storage.</li>
						<li style="text-align: left;font-weight: 100;padding-left: 5px;"> Custom travel checklists & check-ins.</li>
					</ul>
				</div>
			</div>
		</div>
	</div> 
</div> -->
		

</div>

<style>
	ul{
		list-style-image: url("img/bullet.png") !important;
	}

</style>

