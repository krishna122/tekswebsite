<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='description' content='' />
    <meta name='keywords' content='' />

    <title>Story Of Freebird</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="freebird_story" style="height: 50%;">
        <div class="freebird_story-body">
            <div class="container" style="margin-top: 12%;">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span>
                            <br>
                           <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Freebird</span>
                        </h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>Tap and swipe, slide and drag - that’s all there is to most mobile apps these days, right? It’s not without reason that so many lament the fact that apps are getting more and more mechanized and run-of-the-mill, leaving little (if at all any) scope for creativity. Months ago, we had done an app called Draw Apart - which was like a proverbial piece of fresh air for our team of developers. The Freebird app, if anything, put a bigger crack on the ‘all apps are boring’ myth.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/f1.png" alt="Timesnaps">
             </div>	
	   </div>
	</div>
     </div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>When Freebird Came To Teks</h4>
	      <P>We were testing a mobile social networking app when the request for this new application for creative people came in. From the time we took a look at it, we knew that this was going to be (if we could indeed properly implement the idea) something different from the projects that we had worked on lately. And boy, were we correct or what!</p>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Freebird is an app for creative people, and I wanted it to be made by people who had a creative streak in them. A brief chat with Hussain and his team gave me the impression that these guys could give shape to my app idea - the way I wanted it. The fact that they were so very prompt in sending along the free app quote also reflected on their eagerness and sincerity.
                                <br><br>
                                <center> 
				<span style="font-size: 25px;">Owner, Freebird app</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>While browsing through the profiles of app development firms online, Luke came across the website of Teknowledge Mobile Studio. He requested for a free app quote, and was pleasantly surprised by the fact that this company indeed responded within 16 hours (that’s one ‘yay!’ for us!). The specified terms were to his liking, and work on the project started the following week.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/f2.png" alt="Timesnaps">
             </div>	
	   </div>
	</div>
     </div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Freebird - Of, For, By Creative People</h4>
             <p>To become a part of the Freebird community, users could either use their Facebook credentials or log in with their email ids. Once in, it was a veritable haven of original, creative, impressive works of art…and of course, the hands, brains, and voices behind them!</p>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
There is no dearth of creative talent among people. Many can sketch well, some are good singers, and there are plenty of individuals with other skills. What they need is a proper platform where they can showcase their creativity, get honest, objective feedback - and gradually improve themselves. I felt that Freebird could become that platform.
                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Using Freebird To Show Off Creative Skills</h4>
	      <P>After an in-depth brainstorming session, it was decided that users would have the option of sharing their poems or pictures or paintings or even vocal pieces directly from the app. The iOS developers working on the project created a dedicated ‘Upload’ screen (after it was approved by the app’s owner).</p>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I wanted Freebird to allow common people...people like you and me...to share their creative work to a large audience. What I was not sure about was how this could be done. A couple of discussion sessions with Hussain and his developers assured me that my app was in good hands - they were making sure that sharing stuff directly on the app would be easy indeed.                                <br><br>
                                <center> 
				<span style="font-size: 25px;">Owner, Freebird app</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>




<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <P>For wannabe singers and vocal artists, the Freebird app has a nice and user-friendly setup. A button called ‘Record Your Song’ is present, tapping which allows users to record their vocal creations real-time. Once that is done, all that remains is sharing the songs/tunes on the application. The entire process is a matter of (few!) minutes.</p>	
	   </div>
	</div>
     </div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Following Artists On Freebird</h4>
            <div class="col-lg-7">
	      <P>From poets to singers, and from painters to composers - Freebird doubles up as a virtual meeting place for an impressively large number of creative professionals. Once a person starts following an artist, (s)he would get updates of the latter’s latest works on their walls. Thanks to the sheer variety of creative fields highlighted in the app, people with a knack in practically anything can find like-minded people on this free iOS application.</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/f3.png" alt="Timesnaps">
              </div>
	   </div>
	</div>
     </div>	
</section>


<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
People with a creative bent of mind generally draw inspiration from the work of their favourite celebrities...noted personalities from their respective fields. Via Freebird, I wanted to give users the opportunity to follow the biggest names from different creative domains. There is no shortage of inspiration on Freebird...that’s for sure.                                <center> 
				<br><span style="font-size: 25px;">Owner, Freebird app</span>
				</center>
                 </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Freebird For Artists. Freebird For Listeners</h4>
	      <P>The concept of this app for creatively oriented people was so interesting that we wanted to have as many people as possible try it out. The option of getting onboard either as an artist or simply as a listener ensures high usability. At the time of registering and creating profiles, people can specify whether (s)he is a composer, artist, singer, or wish to make a mark in any other field. Beside the name and display picture of users, this information is displayed.</p> 
	   </div>
	</div>
     </div>	
</section>


<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Quick and effective networking with people with similar interests, similar passions is a high-point of the Freebird application. On the app, a user can find many fellow-users who share his/her interests and have the same skills. Exchanging ideas, giving and receiving feedback, finding and implementing new ideas - all of these, and much more, is possible...right from this app.                <br><br>
                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Quick Interaction With Fellow Artists</h4>
            <div class="col-lg-7">
	      <P>We conducted an in-house survey during the testing phase of this iPhone app for creatives - and the respondents confirmed that the instant messaging (IM) feature of Freebird was indeed a great way for sharing work, getting feedback and inspiration for new stuff, and for staying in touch with people having common interests. Users also have the option of inviting their Facebook buddies and/or Twitter followers to the app. The larger the Freebird community, the more scope for indie artists to showcase their work and learn.</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/f5.png" alt="Timesnaps">
              </div>
	   </div>
	</div>
     </div>	
</section>


<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
				Freebird was always planned as a mobile platform where people would be able to hone their skills in a group, as a team. Seamless interaction among users was an absolute must for this - and having a built-in chat feature was just the right thing for that.
                <center> 
				  <br><span style="font-size: 25px;">Owner, Freebird app</span>
				</center>
                 </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	      <P>We conducted an in-house survey during the testing phase of this iPhone app for creatives - and the respondents confirmed that the instant messaging (IM) feature of Freebird was indeed a great way for sharing work, getting feedback and inspiration for new stuff, and for staying in touch with people having common interests. Users also have the option of inviting their Facebook buddies and/or Twitter followers to the app. The larger the Freebird community, the more scope for indie artists to showcase their work and learn.</p>
	   </div>
	</div>
     </div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>Who Gets To See What?</h4>
            <div class="col-lg-7">
	      <P>In Freebird, every individual user can view the latest works of all the artists that he or she has decided to follow. Independent composers, singers and painters, on the other hand, can directly share their creations with all the people they have started following. Apart from creations, users can even check out general posts and updates from everyone added to their circles.</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/f4.png" alt="Timesnaps">
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#9e4348;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
What we loved most about Freebird was the sheer flexibility it had on offer for users. People could follow anyone they wanted, share their work with anyone...even alter their own profile information at any time. A person could use the app in the way (s)he liked - and that, I believe, is the recipe for the success of any mobile software.                <center>
					<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br> 
					<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
					<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
              </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	  <h4>Freebird - Where Creative People Meet</h4>
	      <P>That is probably the best way to describe this wonderful mobile application in a sentence. It is a user-friendly composer app, an app where people can upload their pictures and paintings, get new ideas, and derive motivation to pursue their creative capabilities in the ideal manner. It was a privilege to work on this app...and we are certain that everyone, even those who are not creative, would love this one.</p>
	   </div>
	</div>
     </div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>