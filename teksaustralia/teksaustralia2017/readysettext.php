<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Ready Set Text</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
    <header class="rst_story" style="height: 50%;">
        <div class="rst_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Ready Set Text</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<p>That’s what our latest gaming app - Ready Set Text - stands for. It is a multiplayer mobile typing speed game app, with adrenaline-pumping competitions, coin-earning opportunities and cool, lucrative prizes up for grabs. Our team of mobile game developers had a lot of fun while making this application, and we are certain users will have fun playing games on it as well.</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>Simple game ideas do not quite remain simple when they are implemented. In my 10-odd years as a mobile app entrepreneur, I have seen many promising app ideas getting lost in translation - during the development stage. We were determined to make sure that nothing of that sort happened with Ready Set Text. It was just too good to be wasted.<br>
				<center><br>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
				<span style="font-size: 25px;">(CEO, Teknowledge Software)</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<p>The Ready Set Text app is optimized for the iOS (iOS 8 and above; both iPhone and iPad) and Android platforms. Brainstorming and initial rounds of wireframing kickstarted soon after the free app quote had been forwarded to the client, and all other things agreed upon. This game had that elusive ‘newness’ about it - something that gave us just that extra ounce of incentive.</p>

        <center><br>
				<img src="appstories/rst1.png" style="">
				</center>

        </div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>Less time on filling signup forms and more time for playing games - that’s what I wanted the Ready Set Text app to offer. Hussain and his team were on the same wavelength, and they went ahead with implementing a email signup option and an alternative social login feature.
				<center><br>

				<span style="font-size: 30px;">Concept Owner, Ready Set Text app</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>



<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
	  				<h4 style="font-size:18px;text-align:left;margin-left:30px;">Logging On To Ready Set Text</h4>
					<p style="margin-top:-20px;">There were some initial doubts regarding the social accounts that should be included for signing in on this mobile typing game app. Finally, the team decided to keep things simple - and use only Facebook for social login purposes, in the initial release. Users can, of course, use unique email/password combinations to sign in as well.</p>

          <h4 style="font-size:18px;text-align:left;margin-left:30px;">Two Different Gameplay Options</h4>
          <p style="margin-top:-20px;">From the preliminary consultations with the client, it was decided that the Ready Set Text app would have two variants of the same typing game (more on the game later). The fiReady Set Text is the simple ‘two-player game’, where a user joins, waits for an opponent, and the contest starts. The other is the bigger ‘Crowd Game’ - where users from all over the world can join and fight for the prize coins.</p>

          <h4 style="font-size:18px;text-align:left;margin-left:30px;">How To Join A Typing Game</h4>
          <p style="margin-top:-20px;">Oh well, that’s easy. Once a user signs in, (s)he can immediately see a list of Crowd Games (on the ‘Crowd Game’ screen). An active ‘minute countdown timer’ is present next to each of the games - indicating the time left for that game to start. Once a game is tapped on, a screen appears with the specifics of that game, including the prizes on offer. A ‘second countdown timer’ can be viewed on this page, along with a ‘Join’ button. Tap that tab, and you’re in the game!</p>

           <p>In the 2-player mode, all that a user has to do is join a game and wait for a bit for an opponent to get in. As soon as both parties are ready, the typing game starts.</p>

            <p>The ‘Calendar’ view in the app gives users the chance to quickly check whether there are any prizes available on a day. Dates on which there are prizes to be won are marked with a small dot, and images of the prizes are also viewable.</p>
          </div>
				<div class="col-lg-6">
					<img alt="" src="appstories/rst01.png">
				</div>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>Let’s face it - even the best of games can feel repetitive after a  time, if there are are no variations in it. When the owner of Ready Set Text floated the idea of having different gameplays in the app, we were absolutely delighted. After due deliberation, the idea of 2-person games and Crowd games took shape.<br>
				<center><br>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
				<span style="font-size: 25px;">(CEO, Teknowledge Software)</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
					<center><br><img alt="" src="appstories/rst4.png" style="width:80%"></center>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>Users can miss out on perfectly good...and potentially rewarding games, if they are not sure about when the games are scheduled to start, and how they can participate in the games. To ensure that there are no such doubts regarding the crowd games listed on Ready Set Text - there are countdown clocks next to each game AND on the individual game details pages. People can easily select the games they want to join.”				<center><br>

				<span style="font-size: 30px;">Concept Owner, Ready Set Text app</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
					<center><br><img alt="" src="appstories/rst2.png" style="width:80%"></center>
				</div>
	  			<div class="col-lg-6">
	  				<strong style="margin-left: 30px;">Type To Win</strong>
					<p>In both Crowd Games and 2-Player Games, participants have to type out a 3-4 line phrase (80-100 characters) that is displayed on the gameplay screen. An image, related to the text to be typed, is also shown. While typing, progress bars (showing the percentage of text typed) are displayed for each contestant, along with the game timer.</p>
				</div>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<div class="col-lg-6">
					<p>To win a game on this mobile text-typing app, a user has to finish typing the entire text within the stipulated time AND before the other participants. In case the typed text is incomplete, or other(s) finish before the concerned user, (s)he is deemed to have lost the game.</p>
				</div>
				<div class="col-lg-6">
					<img alt="" src="appstories/rst3.png" style="width:100%;">
				</div>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>There was this one time when we had a fun typing speed contest at office. One of the iPhone developers - usually a fast typer - came in last, simply because he had typed the entire given text in the wrong case, and was not alerted about this error. In Ready Set Text, I made it a point that users won’t face similar problems. Wrong cases or characters are not accepted in a game, and notification sounds are generated whenever a mistake is committed.<br>
				<center><br>
				<img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
				<span style="font-size: 25px;">(CEO, Teknowledge Software)</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
          <strong style="margin-left: 30px;">Winning Coins</strong>
					<p>Coming Ready Set Text in typing games is the most obvious way of earning coins in the Ready Set Text application. The ‘winner’ of a game gets 3 coins, along with a bonus question. Provided that (s)he answers the question correctly, 2 further coins are added to his/her account.</p>
				</div>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>Winning is great...but on Ready Set Text, everyone who manages to finish typing within the given time, get the chance to earn coins. 1 coin is given to every user who types the entire text, but fails to do so ahead of everyone else. They can win 1 more coin by correctly answering the question that follows.
				<center><br>
				<span style="font-size: 30px;">Concept Owner, Ready Set Text app</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  		<p>The total count of coins earned can be viewed on the ‘Profile’ page. The number of Games Won and the average CPM (characters-per-minute) are also recorded here.</p>
        <center><br>
				<img src="appstories/rst5.png" style="">
				</center>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>There are many alternative ways to earn coins on Ready Set Text - and I feel users will find this handy. Apart from actually winning games, there are coins to be won by recruiting other people in the Ready Set Text network. On completing personal profile information, 100 coins are awarded to users.
				<center> <br>
          <img src="appstories/hussain.png" alt="hussain fhakruddin" style="width:20%;"><br>
  				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br>
  				<span style="font-size: 25px;">(CEO, Teknowledge Software)</span>

				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
          <strong style="margin-left: 30px;">What Is The ‘Recruit Minions’ Feature All About?</strong>
				  <p>Well, that’s another way of earning more coins. Those on Ready Set Text can send out invitations to their friends, recruit people by email or text message (‘Recruit via email’ and ‘Recruit via text’) - and get a percentage of the coins they earn after joining. A user also gets a share of the winnings of players who are invited by the ones (s)he had initially invited. It’s like building an army of minions, and boosting the total coin count from them!</p>

          <strong style="margin-left: 30px;">The Big Prize</strong>
          <p>Giving players different ways to earn coins is important - for coins are the tickets for entering the contest to earn the prizes available on different days (as highlighted above, the prize-days can be checked on the ‘Calendar’ screen). A user can enter any number of coins from his/her total coin balance for participating in the fight for the prize (fiReady Set Text-time users have to fill up a form fiReady Set Text, indicating age, gender, income, education, ethnicity, and other relevant information). The winner is determined from the backend - and person who wins the prize will find a ‘Redeem’ button in the app.</p>
		  	</div>
		</div>
	</div>
</section>

<section style="background: #0A60CA;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote> The charms of Ready Set Text are not limited to the typing speed games in it. It doubles up as a type of exciting lottery app as well - with coins available to enter competitions, and exciting prizes to be won on different days. Win games to earn coins, and use coins to win prizes - that’s the flow here.
				<center><br>
				<span style="font-size: 30px;">Concept Owner, Ready Set Text app</span>
				</center>
				</blockquote>
		  	</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
	  			<strong style="margin-left: 30px;">Of Boosts & More</strong>
				<p>Boosts are, in a nutshell, a time-bonus for the players. When a Boost is applied, the screen with the text to be typed appears after ‘Ready, ‘Set’...instead of the usual ‘Ready’, ‘Set’, ‘Text’. A few extra seconds to get a headstart!

The Ready Set Text mobile typing app for iOS and Android will arrive at the Apple App Store and the Google Play Store soon. The fun features, vast array of games, and prize-winning opportunities in the app all contribute to its appeal - and it should be a matter of time before Ready Set Text gets featured at the stores.</p>
		  	</div>
		</div>
	</div>
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>
