<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

	<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
        <div class="appstories-body">
            <div class="container" >
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Great Apps. Great Stories.</span></h1>
	                           <p style="font-size: 25px;">Check out these stories...One app at a time!</p>
                       </div>
                 </div>
            </div>
        </div>
</header>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">BuiltBy</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Project Management App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">BuiltBy is a cross-platform project management mobile app, created with React Native for iOS and Android. Conceptualized by Neesha Alwani, BuiltBy facilitates seamless collaboration among team members of projects.</p>
                      <br><br>

                      <a href="builtby.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/builtbylogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>

    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Momo Taxi</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Cab Service App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Momo Taxi is a high-on-accuracy on-demand cab service app, optimized for the Android platform. The GPS-powered app lets users book taxis from any location, without any hassles. Separate types of cab services can be chosen too.</p>
                      <br><br>

                      <a href="momotaxi.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/momotaxilogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">ChildMinder</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Child Care App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">ChildMinder is a customized iPhone childcare app. It allows caregivers to record and update a wide range of information about the kids they are looking after. Individuals can register on ChildMinder as ‘Parent’ too.</p>
                      <br><br>

                      <a href="childminder.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/childminderlogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Minilobes - The House</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Educational Game App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Minilobes is an engaging educational game for kids, customized for the iOS and the Android platforms. It has two separate modes - ‘Explore’ and ‘Game’ - and helps children learn about their homes and common household objects...the fun way.</p>
                      <br><br>

                      <a href="minilobe.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/minilobelogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">bagpal</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Baggage carrier service</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">bagpal is a dedicated baggage carrier service - the first of its kind in Nordic countries. By availing the service, travelers can send along their luggages in advance, in a highly convenient and completely secure manner.</p>
                      <br><br>

                      <a href="bagpal.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/bagpallogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
        </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">HelloMind</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Motivational App</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">HelloMind is a multi-featured mobile self-motivation app, conceptualized by Jacob Strachotta, and available on the iOS and Android platforms. It helps users identify the root cause of their problems, and provides personalized audio treatment sessions and boosters.</p>
                      <br><br>

                      <a href="hellomind.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/icbflogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
    </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Awayys</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Social Networking</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Awayys is a multi-featured, innovative travel networking application. It is customized for the iOS (8 and later) platform and has a wide array of cool, interesting features. Users can make new friends and expand their networks with ease on this app.</p>
                      <br><br>

                      <a href="awayys.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/awayyslogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
    </div>
    </section>
    
    <section id="appstory" class="icbf">
        <div class="container">
            <div class="row">
              <div class="col-lg-12"><br><br>
                      <div class="col-lg-6">

                        <span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Top Thaaat</span><br>
                        <span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Selfi Challenge Game</span>
                        <br><br>
                        <p style="padding: 0px;" class="appcolor">Top Thaaat is a one-of-its kind mobile selfie challenge game app. Users can either select from the games available in the app, or create their very own selfie challenges.</p>
                      <br><br>

                      <a href="topthaaat.php"><img src="img/view-project.png"></a><br><br>
                      </div>

                    <div class="col-lg-6 storiesimg">
                      <img src="appstories/topthaaatlogo.png" align="center">
                    </div>

              </div>
            </div>
            <hr class="hr-style">
    </div>
    </section>
    
    <section id="appstory" class="fuelup">
		<div class="container">
			<div class="row">
			  <div class="col-lg-12"><br><br>
					  <div class="col-lg-6">
	
					  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Fuelup</span><br>
					  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Fuel-buying app</span>
					  	<br><br>
					  	<p style="padding: 0px;" class="appcolor">Fuel Up is a mobile fuel-buying app, optimized for the iOS 8 and iOS 9 platforms. Via this GPS-powered app, users can find fellow drivers in their locality, and put in fuel purchase requests to them. The user-friendly driver's app makes car rides free of fuel-related worries!</p>
					  <br><br>
					  <a href="fuelup.php"><img src="img/view-project.png"></a><br><br>
					  </div>
	
					<div class="col-lg-6 storiesimg">
					  <img src="appstories/Fuelup.png" align="center">
					</div>
	
			  </div>
			</div>
			<hr class="hr-style">
		</div>
	</section>
	
	<section id="appstory" class="dimes">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Dimes</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Social app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Dimes is a custom iPhone app that puts a fresh new fun spin on campus life. There are many exciting polls in the app, and users can rank his/her friends on them. Dimes comes with in-app chat functionality too.</p>
				  <br><br>
				  <a href="dimes.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dimes.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>
    
    <section id="appstory" class="flypal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Flypal CRS</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Aviation app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">FlyPal CRS is an iOS/Android app geared to make aviation management smarter and more efficient than ever before. The app facilitates prompt and real-time communication between administrators, operators and crew members, and provides detailed reports and document access too. A winning app for flight managers!</p>
				  <br><br>

				  <a href="flypal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/flypal.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="deal">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">365deal</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Business app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">365 Deal is an exclusive iOS ad-sharing app for sellers. Two different types of ads can be posted on this user-friendly application. All ads are information-rich, and the contact details of sellers are provided with each advertisement.</p>
				  <br><br>

				  <a href="365deal.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/365.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="dbt">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">DBT</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Lifestyle app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">DBT is a comprehensive, multi-layered iPhone self-help application. Right from creating lists of skills and crisis/problems and updating them on a daily basis, to tracking key health parameters - the app lets users perform a wide range of tasks.</p>
				  <br><br>

				  <a href="dbt.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dbt.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="moovers">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Moover</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Word Game</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Moover is a two-player iOS word-search application. Users have to swipe along word mazes to find certain words, before their opponents manage to do so. Additional coins are available through in-app purchases.</p>
				  <br><br>

				  <a href="moovers.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/m.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="currently">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Currently</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Audio clip sharing app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Currently is an innovatively conceptualized audio-clip sharing app for iOS and Android devices. On the app, users can record sound clips and share them in their ‘Stream’ or in the ‘Ocean’.</p>
				  <br><br>
				  <a href="currently.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/currently.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

	<section id="appstory" class="onebrand">
	<div class="container">
  <div  style="width:100%;"><br/><br/>
	  	<div class="row">
	  		<div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">One Brand</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Mobile Commerce</span>
				 	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Probably the best mobile shopping app in our portfolio. From browsing through product categories to purchasing items and making payments - One Brands has all the requisite m-commerce features. The graphics and design layout make it a really user-friendly app too!</p>
				  	<br><br>
				  	<a  href="onebrand.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/onebrand.png" align="center">
				</div>
		  </div>
		</div>
		<hr class="hr-style">
  </div>
</section>

<section id="appstory" class="stopover">
	<div class="container">
   <div  style="width:100%;"><br/><br/>
		<div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Stop Over</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Social Networking</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Conceptualized by Ms. Amber Blumanis, Stopover is a breakthrough app for travelers. It helps to remove the 'bore-factor' of long waits at airports. With the app, users can connect with people with common interests, right from the airport. Stopover, unsurprisingly, has won multiple awards.</p>
				  <br><br>
				  	<a href="stopover.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/stopover.png" align="center">

				</div>
		  </div>
		 </div>
		 <hr class="hr-style">
  </div>
</section>

<section id="appstory" class="timesnaps">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		<div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Timesnaps</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">Photo/Video</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">For those who wish to capture, preserve and treasure the magic of fleeting moments, Timesnaps is a must-have application.  The app allows people to take images of practically anything at regular intervals, create slideshows with them, and revel at the changes that time can make!</p>
				  <br><br>
				  	<a href="timesnaps.php" ><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/timesnaps.png" align="center">

				</div>
		  </div>
		 </div>
		</div>
		<hr class="hr-style">
		</div>
</section>

<section id="appstory" class="babysitter">
	<div class="container">
<div  style="width:100%;"><br/><br/>
		 <div class="row">
		  <div class="col-lg-12">
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;" class="appcolor">Baby Sitter</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;" class="appcolor">App For Parents/Babysitters</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Hiring an expert professional babysitter becomes easier than ever before with this revolutionary iOS application. Parents can fix appointments with babysitters, specify the hourly rates they are willing to pay, and hire the best caregiver for their children - all with the Baby Sitter app.</p>
				  <br><br>
				  <a href="babysitter.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">

				  <img src="appimages/babysitter.png" align="center">

				</div>
		  </div>
		</div>
		<hr class="hr-style">
		</div>
</section>


<section id="appstory" class="speedykey">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">SpeedyKey</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Virtual Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">A third-party iOS keyboard app par excellence. Available in three different languages and four different themes (via in-app purchase), SpeedyKey makes typing on iPhones/iPads quicker, easier and more accurate. Its USP? The host of custom 'Speedy Replies'!</p>
				  <br><br>

				  <a href="speedykey.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appimages/speedykey.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="veeaie">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Veeaie Keyboard</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Keyboard App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">The key USP of Veeaie Keyboard is its host of pre-written custom replies, for emails and messages. With this third-party iOS keyboard app, staying in touch and responding to messages becomes quicker than ever.</p>
				  <br><br>
				  <a href="veeaie.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/veeaie.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="bender">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Bender</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="bender.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/bender.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="freebird">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Freebird</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Event App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">An efficient mobile venue finder with an extensive and regularly updated database. Bender doubles up as an informative events app as well. It also lets event managers/venue owners in their promotions.</p>
				  <br><br>

				  <a href="freebird.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/freebird.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="inslideout">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">InslideOut</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Social Networking App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Live performances, sports events, concerts - all these and more arrive right at your fingertips, when you have the Inslide Out application. This is one events app-meets-mobile ticketing app you will love to have.</p>
				  <br><br>

				  <a href="inslideout.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/inslideout.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>


<section id="appstory" class="lifestyle">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Lifestyle Travel</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Travel App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">The perfect mobile tour planner, information provider and real-time helper - all rolled into one. Conceptualized by Leonie Spencer - a long-time travel fan herself - this is an app that every travel-lover would love.</p>
				  <br><br>

				  <a href="lifestyle.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/lifestyle.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>



<section id="appstory" class="dataworks">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">DataWorks</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Bluetooth Connectivity App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">DataWorks taps into the power of Bluetooth technology to the fullest. With this powerful app, devices can be connected with other compatible gadgets, and data can be measured, calibrated, maintained and managed.</p>
				  <br><br>

				  <a href="dataworks.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dataworkslogo.jpg" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>


<section id="appstory" class="LMIOpt-InChecker">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">LMI Opt-In Checker</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">RFID app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">A cutting-edge RFID app created for the Android platform. Ideal for keeping track of prizes won by users at any time. The app boasts of a new technology, and is further bolstered by its smooth usability features</p>
				  <br><br>

				  <a href="LMIOpt-InChecker.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/LMIOpt-InCheckerlogo.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

    

<section id="appstory" class="princeofasia">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Prince Of Asia</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">How about donning the role of a prince, and rescuing a charming princess? Get set for Prince Of Asia - the strategy-based, multi-level role-playing game that merges fantasy fun with thrilling adventure. An app our game development agency is very proud of!</p>
				  <br><br>

				  <a href="princeofasia.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/princeofasia.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="rst">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold;font-size: 25px; text-align: center;">Ready Set Text</span><br>
				  	<span style="font-weight:bold;font-size: 16px; text-transform: uppercase;">Typing Game app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">Ready Set Text is a fun multiplayer mobile typing game for the iOS and Android platforms. Players earn coins by winning typing games, and can then use the coins to win attractive prizes. Both ‘Crowd Games’ and ‘Two-person games’ can be played in the Ready Set Text app.</p>
				  <br><br>

				  <a href="readysettext.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/rst.png" align="center">
				</div>

		  </div>
		</div>
	</div>

</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>
