<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Top Thaaat</title>

	<?php include 'head.php';?>
	<link href="css/appstoriesnew.css" rel="stylesheet">

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
<header class="icbf_story" style="height: 50%;">
        <div class="icbf_story-body">
            <div class="container" style="margin-top: 8%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>
                            <span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                            <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Top Thaaat</span>
                        </h1>
                        <a href="https://itunes.apple.com/in/app/top-thaaat-the-selfie-game/id1097716427?mt=8" target="_blank"><img alt="Top Thaaat" src="img/appstore.png"></a>
                    </div>
                    <div class="col-md-3"></div>
                 </div><br>
            </div>
        </div>
</header>

<section class="offwhite-background">
	
	<div class="container">
	<div class="row">
	  <div class="col-lg-12">
	         <p class="color">
	            Every day, more than a million selfies are taken - that’s how big the worldwide selfie-craze has grown. We had been planning to make a selfie-based iPhone application for some time now - something that would be different from the regular image filtering and touch-up apps out there. As luck would have it, we received the Top Thaaat project soon enough - and it gave our team of mobile app developers the perfect chance to really play around with selfies.
	         </p>
	   </div>
	</div>
     </div>
	
</section>


<section class="offwhite-background">
	
	<div class="container">
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6">
		  	    <blockquote class="color">
		  	    	Referring to Top Thaaat simply as an image app or a selfie app would be doing it an injustice. It is much more than that - and I feel it has the potential to become a really popular photo-based gaming app over time.
		  	    </blockquote>
		  	 </div>
		  	 
		  	 <div class="col-lg-6">
		  	 	<h3 class="color" style="margin: 0em 0;">So, What Exactly Is Top Thaaat?</h3>
                 <p class="color">
                     To explain things in a phrase, Top Thaaat is an iPhone selfie-challenge game. It is optimized for iOS 8 and later versions of the platform. With plenty of challenges and badges and the sheer excitement of monitoring the win-loss statistics, this is one mobile game that has loads of cool features.
                 </p>
		  	 </div>   
		  	 
            
		  	 <div class="col-lg-8">
                 <p class="color">
                     In keeping with our business policies, a free app quote was sent along to the client within 24 hours. The approvals and other formalities were completed quickly, and Top Thaaat was soon in our drawing board.
                 </p>
                 <blockquote class="color">
		  	    	Conceiving a nice app idea is one thing, and implementing it properly is totally something else. For Top Thaaat, I was looking for an expert team of iOS game developers - an agency that would be experienced and have the expertise to execute my project in the way I had envisioned it. Teksmobile was the one I finally got in touch with.
		  	    </blockquote>
		  	 </div>
              
              <div class="col-lg-4">
		  	    <center><img src="appstories/topthat_screen_03.png" alt="Top Thaaat" style="width:50%;"></center>
		  	 </div>
              
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-5">
		  		<center><img src="appstories/topthat_screen_01.png" alt="Top Thaaat" style="width:60%;"></center>
		  	</div>
		  	
		  	<div class="col-lg-7">
		  		
		  		<h3 class="color">How To Get On Top Thaaat?</h3>
		  	    
		         <p class="color">
		         	For an iOS selfie challenge game like Top Thaaat, we felt that social login features were absolutely necessary. After a round of discussions, it was decided that users will be given the option to log into the app with their Facebook or Twitter credentials. This was, of course, in addition to the regular email login option.
		         </p>
                
                <blockquote class="color">
			  	  Way too many good apps are ruined by elaborate signup and login requirements. For Top Thaaat, we made sure that logging in took only a couple of minutes...if that. The availability of multiple sign in options makes things easier for users.
			  </blockquote>	
		   </div>
		   
		  </div>
            
		</div>
		
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-8"><br>
		  		<h3 class="color">The Top Thaaat Home Screen</h3>
		  	  
		  	    
		  	    <p class="color">
		          After logging in, users are taken directly to the ‘Home’ screen of this all-new mobile selfie game app. The screen contains the profile image of the player, a view of the ‘Badges’ that can be earned by playing and winning selfie challenges, and a summary stats of the number of Wins/Draws/Losses. People can navigate to the ‘Friends’, ‘Lobby’ and ‘GamePlay’ screens from ‘Home’ as well.
		         </p>
                 
                 <br>
		  	    <blockquote class="color">
		  	    	Top Thaaat offers many personalized features to users, and the fun starts from the Home screen itself. The profile pic can be selected from the in-app library itself, or chosen from Facebook or Twitter photos. Options aplenty!
		  	    </blockquote>
		         
		  	 </div>
		  	 
		  	 <div class="col-lg-4">
		  	 	<center><img src="appstories/topthat_screen_04.png" alt="Top Thaaat" style="width:60%;"></center>
		  	 </div>   
		  	 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
              <div class="col-lg-5">
                  <img src="appstories/topthat_screen_05.png" alt="Top Thaaat" style="width:70%;">
              </div>
                  
              <div class="col-lg-7">
		  		<h3 class="color">Win Challenges. Earn Badges.</h3>
		  		
                <p class="color">
		          That’s, in a nutshell, the modus operandi of the Top Thaaat selfie-taking app. There are more than 20 badges to be earned by accepting and winning challenges. The more a player wins, the more badges (s)he can show off to his/her friends.
		         </p>
              
		         <blockquote class="color">
		  	    	Motivation, I feel, is a vital factor for keeping users engaged to a mobile game for any significant period of time. Some games have coins, some have stars, others have different types of in-game currency for this purpose. In Top Thaaat, we went ahead with badges that people could earn by winning challenges.
		  	    </blockquote>
		  	    
                <p class="color">
                    On the ‘Friends’ screen, all other fellow-users of the Top Thaaat selfie application can be viewed. There is a small ‘+’ tab on the top right. When tapped, it displays all the phonebook contacts who are also on the app. The names of Facebook friends and Twitter followers who use Top Thaaat are also shown.
                </p>    
                  
                <p class="color">
                    <strong>Note:</strong> There is an option to remove friends as well.
                </p> 
                  
            </div>  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">The Media Lobby</h3>
		  	    
                 <blockquote class="color">
		  	    	What should a selfie challenge app for iPhone be all about? That’s right, pictures! The ‘Lobby’ screen of Top Thaaat offers a cool view of all the media content stored in the app. The pictures are neatly categorized...and they are all selfies!
		  	    </blockquote>
                 
                 <p class="color">
                 	To keep the stream of submitted selfies secure and easily accessible, the iPhone app developers (after some deliberation with the client) at our company created a ‘Lobby’ section - where all media would be stored. Viewers can also see the challenges against which each picture/set of pictures have been submitted. Selfies in the ‘Lobby’ can be ‘liked’ too.
                 </p>
                 
                 <blockquote class="color">
		  	    	While making any photos app, developers have to be wary of inappropriate content being submitted. In Top Thaaat, users are allowed to ‘flag’ or ‘report’ any of the lobby pictures - in case they find it to be objectionable. In the app, the final say is always with the players.
		  	    </blockquote>
                 
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/top1.jpg" alt="Top Thaaat"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/top2.jpg" alt="Top Thaaat"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/top3.jpg" alt="Top Thaaat"></center></div>
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/top4.jpg" alt="Top Thaaat"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/top5.jpg" alt="Top Thaaat"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/top6.jpg" alt="Top Thaaat"></div>
		  </div>
		</div>
		
        <div class="row">
		  <div class="col-lg-8"><br>
				<h3 class="color">And Now...The Gameplay</h3>
				<p class="color">
					Okay, enough with the screens and features of Top Thaaat - let’s now get straight into the actual gameplay of Top Thaaat. Under ‘Current Games’ (in the ‘Game’ screen), a vast range of open games are listed. Tapping any of the games shows up the details of the user and the opponent. In this two-player selfie challenge game, it is all about submitting selfies before the other person does.
				</p>
				<blockquote>
					Each game in Top Thaaat has 10 challenges. In each challenge, the person who submits his/her selfie first wins 10 points. The other person is given 5 points. The game proceeds like this, the total points accumulated by either player is calculated at the end, and the winner is decided.
				</blockquote>
              <p class="color">
                  In keeping with the general mood of the Top Thaaat app, the games also have fun names. We have named one of the games ‘I leave the toilet seat up’! Pretty LOL stuff...right?
              </p>
		  </div>
		  <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/topthat_screen_06.png" alt="Top Thaaat" style="width:70%;">
              </center>
		  </div>
	   </div>
        
        <div class="row">
            <div class="col-lg-4"><br>
              <center>
                  <img src="appstories/topthat_screen_02.png" alt="Top Thaaat" >
              </center>
		  </div>
		  <div class="col-lg-8">
				<h3 class="color">Types Of Games In Top Thaaat</h3>
				<p class="color">
					There are two different game types in this selfie-competition app for iOS. The ‘Top Thaaat’ games are pre-defined, while the ‘Custom’ games - as the name suggests - have features that can be modified by users. 
				</p>
				<blockquote>
					One thing I really like about Top Thaaat is the degree of customization options it gives to end users - right from profile photo selection, to the games to be participated in. Flexibility is a must-have feature in any dynamic iPhone app, and this one has it in ample proportions.
				</blockquote>
              <p class="color">
                  On the ‘Game’ screen of Top Thaaat, users can view the ‘New Game’, ‘Pending Game’ and ‘Current Game’ tabs. On tapping ‘New Game’, the two alternative types of selfie-challenge games are displayed, from where users have to take their pick. All the games for which a user has sent out invitation but it has not been accepted yet, can be viewed under ‘Pending Game’.
              </p>
              <blockquote>
					On my selfie challenge app, users can invite opponents from their Facebook and Twitter contacts, or opt to play with their in-app friends. There is an additional option of challenging random opponents as well.
				</blockquote>
              <p class="color">
					<strong>Note:</strong> As soon as a ‘Pending Game’ invite is accepted, it moves to the ‘Current Game’. Each game, as mentioned above, has 10 challenges - and players have a maximum of 24 hours to complete them.
				</p>
		  </div>
		  
	   </div>
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
						<strong>The Top Thaaat application is available for free download.</strong> This is one app we had a lot of fun making, and we are pretty much sure users will enjoy playing the Top Thaaat game as well. 
					</p>
                    <p class="color">
						Pssssst...we also took a lot of selfies while working on this project! 	
					</p>
				</div>
		  </div>
		</div>
        
		
     </div>
	
</section>



<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>