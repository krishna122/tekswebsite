
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $result['metatitle']; ?></title>
    <meta name='description' content='<?php echo $result['metadescription']; ?>' />
    <meta name='keywords' content='<?php echo $result['metakey']; ?>' />
    

	<?php include 'head.php';?>

</head>


<body id="page-TOP" data-spy="scroll" data-target=".navbar-fixed-top" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<section class="project">
		<div class="project-body" >
			<div class="project-container"><br>
                         <div>
                         <?php
                         	if(isset($_POST['submit']))
                         	{
                         		
				// multiple recipients
				$to  = 'info@teks.co.in'; // note the comma
									
									
				// subject
				$subject = 'Quote Request';
									
				// message
				$message = '
					<html>
									<head>
									  <title>Quote Request</title>
									</head>
									<body>
									  <p><strong>Name: </strong> '.$_POST['uname'].'</p>
									  <p><strong>Email: </strong>'.$_POST['uemail'].'</p>
									  <p><strong>Description: </strong>'.$_POST['describe'].'</p>
									 <p><strong>Description: </strong>'.$_SERVER['REMOTE_ADDR'].'</p> 
									</body>
									</html>
				';
									
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
									
				// Additional headers
				
				$headers .= 'From: '.$_POST['uname'].' <'.$_POST['uemail'].'>' . "\r\n";
									
				// Mail it
				$retval = mail($to, $subject, $message, $headers);
				if( $retval == true )
				{
				?>
										
			        <div class="alert alert-success" role="alert">
				<?php echo "Message sent successfully"; ?>
				</div>
				<?php 	
				}
				else
				{?>
									
				<div class="alert alert-danger" role="alert">
				<?php echo "Message could not be sent"; ?>
				</div>
				<?php 		
				}
                         	
                         	}
                               ?>
                         
                         
       <h3 style=" text-transform:uppercase;">REQUEST A FREE APP QUOTE</h3>
       <p>We’ll send you one within 24 hours</p>
     </div>
				<form role="form" style="text-align: left; color: #fff;" padding:10px; method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				
					  	<div class="clearfix"></div>
					  	
					  	
					  	<div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
						  	<div class="form-group">
						      <label for="name" style="font-size: 25px; font-weight: 200; margin-bottom:12px;">Your Name</label>
						      <input type="text" name="uname" class="form-control" id="uname" placeholder="Name" required="required">
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					   </div>
					   
					   <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
						  	<div class="form-group">
						      <label for="email" style="font-size: 25px; font-weight: 200;margin-bottom:12px;">Your Email</label>
						      <input type="email" name="uemail" class="form-control" id="uemail" placeholder="email@domain.com" required="required">
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>

                       <div class="row">
					   <div class="col-md-2"></div>
					  	<div class="col-md-8">
						  	<div class="form-group">
						      <label for="desc" style="font-size: 25px; font-weight: 200;margin-bottom:12px;">Describe The Type Of App / API You Wish to Be Made</label>
						      <textarea name="describe" class="form-control" id="desc" rows="8" placeholder="Description" required="required"></textarea>
						    </div>
					  	</div>
					  	<div class="col-md-2"></div>
					  	</div>
					  
					  <div class="row">
					   <div class="col-md-2"></div>
					    <div class="col-md-8"> <br>
					    <button type="submit" class="btn btn-default" style="width: 100%; font-size: 25px; text-transform: capitalize;" name="submit">Send Us A Quote</button>
					    <p style="color: #CFCFC4; font-size: 15px; text-align: center;">By submitting this form, you agree to our <a href="terms.php" style="text-decoration: underline; cursor: pointer; color: #CFCFC4;">Terms Of Service</a> </p>
					    </div>
					    <div class="col-md-2"></div>
					    </div>
				</div>
				</form>
			</div>
		</div>
	</section>
	
<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">

setTimeout(function() {
    $('#alert-warning').fadeOut('slow');
    $('.alert-danger').fadeOut('slow');
    $('.alert-success').fadeOut('slow');
    $('#error-alert').fadeOut('slow');
}, 3000);
                         </script>
                         
                         <script type="text/javascript">
$(document).ready(function(){
	$('#start').addClass('active');
});

</script>
</body>
</html>
