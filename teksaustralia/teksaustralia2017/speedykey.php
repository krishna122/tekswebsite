<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of SpeedyKey</title>

	<?php include 'head.php';?>
   <link href="css/appstoriesnew.css" rel="stylesheet">
</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="speedykey_story" style="height: 34%;">
        <div class="speedykey_story-body">
            <div class="container" style="margin-top: 5%">
            	<div class="row">
            	   <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br>
                        <span style="color:#fff; font-weight: 900;  text-transform: uppercase;">SpeedyKey</span></h1>
	                         <center>
		                         <a href="https://itunes.apple.com/dk/app/speedykey-keyboard/id957834205?mt=8" target="_blank">
		                         	<img alt="Availablet on the Appstore" src="img/appstore.png">
		                         </a>
	                         </center> 
                       </div>
                    <div class="col-md-3"></div>
                 </div>
            </div>
        </div>
    </header>
	
<section class="offwhite-background">
	
	<div class="container">
        
	<div class="row">
	  <div class="col-lg-12">
	         <p class="font20 color">
	            Typing quickly on mobile touchscreen keyboards is not the easiest task for many. Errors crop up, words have to be deleted and rewritten - resulting in frustrating delays. In the last six months or so, we had received multiple requests for creating an app that would make the task of typing on mobile devices faster, simpler, and most importantly, error-free. The SpeedyKey project gave us the opportunity to finally make such an application.
	         </p>
	   </div>
	</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<h3 class="color">What Is The App All About?</h3>
		  	    <br>
		  	    <blockquote class="color blockspeedykey">
		  	    	The idea behind Fuel Up is simple. Random guy is driving, finds that he has run out of petrol, looks for other drivers in his locality, and sends out a fuel request to them. Someone responds to the request, delivers the fuel, gets paid...and it’s done. No gimmicks, only prompt service!
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          After the app quote had been sent to the client, we organized three consultation sessions to grasp the concept of the Fuel Up app properly. Our in-house team of iPhone app developers, on the basis of given instructions, made the app compatible with iOS 8 and iOS 9 handsets (that is, iPhone 6 and iPhone 6S).
		         </p>
		  </div>
		</div>
		

	
		<div class="row">
		  <div class="col-lg-12">
		  	<br>
		  	<div class="col-lg-6">
		  		<center>
			  		<video autoplay loop muted  poster="appstories/speedykey.jpg" style="border: 5px solid #EE1715;">  
					   <source src="video/appstories/speedykey.mp4" type="video/mp4">  
					</video>
				</center>
		  	</div>
		  	
		  	<div class="col-lg-6">
		  		
		  		 <blockquote class="color blockspeedykey">
		  	    	Ever since the release of iOS 8, we have received plenty of proposals for making custom third-party keyboards. Barring a few, most of the concepts were not exciting enough to take on. SpeedyKey, however, put an entirely new spin on the idea of what a keyboard app for iOS devices should be like. Making it was... fun.
		  	    </blockquote>
		  	    
		         <p class="color">
		         	When the head of Sp/F Meitilberg Arts presented the app idea for the first time, it was clear that it had the potential to be a big-time winner. Apart from making typing quicker, SpeedyKey was slated to pack in a host of keyboard shortcuts, pre-written custom replies, themes, and other fun features. We knew that, if we could successfully make this app - it would find favor among users. And that's precisely what happened.
		         </p>
		         
		   </div>
		   
		   <div class="col-lg-12">
		   		<blockquote class="color blockspeedykey">
		  	    	Before I got in touch with Hussain and his team, I had one thing pretty clear in my mind. I did not want SpeedyKey to be like the other run-of-the-mill mobile keyboard applications. A look at the portfolio of Teknowledge told me that these guys might have it in them to properly implement my ideas in the app. My gut feeling turned out to be correct.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		         	As is customary at our mobile app company, we sent along a free app quote within two business days after receiving the app idea. All the formalities were settled by a week, and the project was handed over to our iOS development team.
		         </p>
		   </div>
		  
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	 <div class="col-lg-6"><br>
		  		<h3 class="color">What's SpeedyKey All About?</h3>
		  	    
		  	    <blockquote class="color blockspeedykey">
		  	    	Whenever I get the proposal for a new app, I try to find whether it would indeed be of value to final users. Otherwise, making it is... frankly... a waste of time. SpeedyKey is one of those apps about which I was confident from the very outset. It had simply too many top-notch features to not be liked.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		          It would be an injustice to refer to SpeedyKey as just a 'third-party iPhone keyboard app', and leave it at that. We had aimed to make typing a faster, and more fun experience, for users of this application. In essence, SpeedyKey can be described as a compact, user-friendly, and multi-featured keyboard application - just the way it had been conceived by Sp/F Meitilberg Arts. Once the final prototype was ready, we realized that there were hardly any keyboard app at the store that was as good as this one. 
		         </p>
		  	 </div>
		  	 
		  	 
		  	 <div class="col-lg-6">
		  	 	<center><img src="appstories/speedy1.png" alt="fuelup" style="width:50%"></center>
		  	 </div>   
		  	 
		  	 <div class="col-lg-12">
		  	 <blockquote class="color blockspeedykey">
		  	    	Making SpeedyKey - my dream project - stand out from the cluster of similar apps was my prime prerogative. I took my time to think up the features that the app should haveÉthe features that would actually benefit people. Credit to the Teks team here, they made sure that my ideas were incorporated in the app in the right manner.
		  	    </blockquote>
		  	    </div>
		  </div>
		</div>
		
		<div class="row">
			
			  <div class="col-lg-6">
			  	
			  		<h3 class="color">Main Features Of The SpeedyKey App</h3>
			  	    
			  	    
			         <p class="color">
			         	Apart from typing being an absolute breeze on the app, SpeedyKey has many other delightful, well-thought-out features. Right from the custom speedy replies and word suggestions, to support for foreign languages and the speedy-menu option - each feature contributes towards making typing quicker, customized and free of mistakes. The spacebar of the app, which performs several actions, deserves a special mention too.
			         </p>
			         
			         <h3 class="color">So, What Exactly Is A Speedy Reply?</h3>
			  	    
			         <p class="color">
			         	When the head of Sp/F Meitilberg Arts explained the concept of 'Speedy Reply', we immediately felt that this feature could be the main USP of the app. Users were given the option of creating and storing their very own customized replies, which they could send with a single tap. 'Speedy Reply' could be used to respond to emails as well as text messages. What's more, email ids and personalized mail signatures could also be added with the Speedy Key tab.
			         </p>
			  </div>
			  
			  <div class="col-lg-6">
		  		<center><img src="appstories/speedy6.png" alt="fuelup" style="width:47%"></center>
		     </div>
		</div>
		<p></p>
		
		<div class="row">
			<div class="col-lg-6">
		  		<center><img src="appstories/speedy3.png" alt="fuelup" style="width:75%"></center>
		    </div>
			  <div class="col-lg-6">
	                 
	                 <blockquote class="color blockspeedykey">
			  	    	When you have to type more, the chances of making mistakes go up - it's as simple as that. The central focus of SpeedyKey is about minimizing the need for typing as much as possible, particularly repetitive things like email signatures and short replies. Not surprisingly, the app brings down errors in typed replies significantly.
			  	    </blockquote>
	                 
	                 <p class="color">
	                 	The 'Speedy Reply' feature comes in particularly handy for replying to messages/mails when a user is in the midst of something important. All that (s)he has to do is tap a custom message (for instance, "I'll call you soon") to send it along - without disrupting his/her work. After due deliberation, our in-house iOS app developers also included the provision of adding reminders with these replies. If a specific time is mentioned, a small red bell appears on the top-right corner of the custom reply messages.
	                 </p>
	                 
	                 <blockquote class="color blockspeedykey">
			  	    	I am not particularly fond of third-party keyboard apps. But this SpeedyKey is different. Earlier on, I often could not respond to the client emails - particularly the ones that came in during meetings and board discussions - quickly enough. I tried out this app, and found that I can do a one-tap reply to the senders, and then follow up with more detailed replies later. SpeedyKey has made me a more efficient manager, that's for sure!
			  	    </blockquote>
			  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/speedykey1.png" alt="fuelup"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/speedykey2.png" alt="fuelup"></center></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><center><img src="appstories/speedykey3.png" alt="fuelup"></center></div>
		  
		  </div>
		</div>
		<p></p>
		<div class="row">
		  <div class="col-lg-12">
		  	
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/speedykey4.png" alt="fuelup"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/speedykey5.png" alt="fuelup"></div>
		  	<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4"><img src="appstories/speedykey6.png" alt="fuelup"></div>
		  
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-6">
		  <br>
		  		<h3 class="color">Compactness Of The App</h3>
		  		<p class="color">SpeedyKey supports three different languages - English, Faroese and Danish. There are plans to include more languages in the upgraded versions of the app in future. While creating an English keyboard was easy enough, the task was slightly tricky when it came to the foreign languages – which have extra alphabets. The SpeedyKey button (that little tab with the black smiley) maintains the compactness of the application in such circumstances. Pressing the button displays the extra letters/characters – from where users can easily choose the one(s) they wish to put in a message. The total number of buttons do not increase.</p>
		  		
		         <blockquote class="color blockspeedykey">
		  	    	One of the challenges I had to address while conceptualizing SpeedyKey was how the additional letters that many languages have would be accommodated. The last thing I wanted was extra buttons to appear, which would ruin the overall layout of the keyboard. After a couple of meetings with Hussain and his team, we locked in the idea of supporting extra characters with the SpeedyKey button.
		  	    </blockquote>
		  	    
		  	    <p class="color">
		  	    	What if a language does not have extra alphabets? For such languages, a long-press of the SpeedyKey tab pulls up the custom speedy replies and emojis. Once again, the need for including more buttons is done away with.
		         </p>
                 
		  </div>
		  
		  <div class="col-lg-6">
		  	 	<center><img src="appstories/speedy4.png" alt="fuelup" style="width:60%"></center>
		  	 </div>
		  	 
		</div>
		
		<div class="row">
		<div class="col-lg-6">
		  	 	<center><img src="appstories/speedy5.png" alt="fuelup" style="width:60%"></center>
		  	 </div>
		  <div class="col-lg-6">
		  	
		  		<h3 class="color">Ease Of Use</h3>
		  	    
		         <p class="color">
		         	The buttons in the top row of the SpeedyKey app have numbers in their bottom-right corners. 8 other buttons have similar small signs on them. To insert these numbers/signs, users have to press the appropriate button and swipe downwards. Yep, it’s really that easy!
		         </p>
                 
                 <blockquote class="color blockspeedykey">
		  	    	 A remarkable thing about SpeedyKey is how the app addresses practically all types of requirements that a person might have, while typing on an iPhone or iPad. A few swipe gestures is all it takes to move the cursor, add/delete words, and type numbers. Even the custom-created words and suggestions can be removed. SpeedyKey is indeed…speedy!
		  	    </blockquote>
		  	    
		  	     <p class="color">
		         	The app also has a built-in menu bar, for quickly activating/deactivating select functions. The suggestion bar can be minimized (a great option for those looking for more screen real estate) as well. For deleting a custom word, it has to be selected, and the ‘Forget This Word' option chosen.
		         </p>
                 
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-12">
		  	
		  		<h3 class="color">In-App Purchases</h3>
		  	    
		         <p class="color">
		         	After consultation with the chief of Sp/F Meitilberg Arts, we went ahead with a grey theme as the default for the SpeedyKey app. Four other cool themes (black, purple, green and white) were made available as in-app purchases. A single purchase enables users to get all the four themes. Playing around with the visual appearance of the app is, hence, simple.
		         </p>
                 
                 <blockquote class="color blockspeedykey">
		  	    	 I wanted to give users the option to take their pick from alternative color themes on SpeedyKey. The question was, how they could be included in the app. After a couple of rounds of discussion, it was decided that the themes will be downloadable via in-app purchase. I loved the idea, for it made things convenient for users.
		  	    </blockquote>
		  </div>
		</div>
		
		
		<div class="row">
		  <div class="col-lg-12">
		  		<div class="box">
					<p class="color">
						SpeedyKey is optimized for all iOS 8 devices (naturally, for the earlier iterations of the platform do not support third-party keyboards). Apart from iPhone 6 and iPhone 6 Plus, the app can be downloaded on upgraded iPhone 5 handsets, iPad, and iPod Touch. The memory requirement for installing SpeedyKey Keyboard is only 14.0 MB. We wanted to expand the reach of the app as much as possible – and till date, the feedback on it has been uniformly positive.
					</p>	
				</div>
		  </div>
		</div>
	
     </div>
	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>