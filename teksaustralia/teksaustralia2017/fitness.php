<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>List of all Fitness Apps made by Teksmobile</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>

    
<!-- Intro Header -->
<header class="appstories" style="height: 60%;">
    <div class="appstories-body">
        <div class="container" >
            <div class="row">
                <div class="col-md-12">
                    <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">Fitness Apps</span></h1>
	                    <p style="font-size: 25px;">Get up, close and personal with the innovative, multi-featured mobile fitness apps we have worked on. We have several apps related to healthy living and nutrition as well.</p>
                </div>
            </div>
        </div>
    </div>
</header>
    
<section id="appstory" class="dbt">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">DBT</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Lifestyle app</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">DBT is a comprehensive, multi-layered iPhone self-help application. Right from creating lists of skills and crisis/problems and updating them on a daily basis, to tracking key health parameters - the app lets users perform a wide range of tasks.</p>
				  <br><br>

				  <a href="dbt.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/dbt.png" align="center">
				</div>

		  </div>
		</div>
		<hr class="hr-style">
	</div>
</section>

<section id="appstory" class="icbf">
	<div class="container">
		<div class="row">
		  <div class="col-lg-12"><br><br>
				  <div class="col-lg-6">

				  	<span style="font-weight:bold; font-size: 25px; text-align: center;">I Can Be Anything</span><br>
				  	<span style="font-weight:bold; font-size: 16px; text-transform: uppercase;">Motivational App</span>
				  	<br><br>
				  	<p style="padding: 0px;" class="appcolor">A multi-featured stress-reliever app, with as many as 12 different suites. Conceptualized by Jacob and Ditte Strachotta and optimized for the iOS and Android platforms, the app has several free sessions and easy in-app purchase options.</p>
				  <br><br>

				  <a href="icba.php"><img src="img/view-project.png"></a><br><br>
				  </div>

				<div class="col-lg-6 storiesimg">
				  <img src="appstories/icbflogo.png" align="center">
				</div>

		  </div>
		</div>
	</div>
</section>


<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>
<script type="text/javascript">
$(document).ready(function(){
$('#stories').addClass('active');
});

</script>
</body>
</html>
