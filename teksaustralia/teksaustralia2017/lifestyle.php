<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Story Of Lifestyle</title>

	<?php include 'head.php';?>

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body data-spy="scroll" ondragstart="return false" onselectstart="return false">

<?php include 'header.php';?>
	
	<!-- Intro Header -->
    <header class="lifestyle_story" style="height: 50%;">
        <div class="lifestyle_story-body">
            <div class="container" style="margin-top: 12%">
            	<div class="row">
                    <div class="col-md-12">
                        <h1><span style="color:#fff; font-weight: 300; text-transform: uppercase;">The Story Of</span><br><span style="color:#fff; font-weight: 900;  text-transform: uppercase;">Lifestyle Travel Ballarat</span></h1>
                    </div>
                 </div>
            </div>
        </div>
    </header>
	
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
             <div class="col-lg-6">
	      <P>Mobile gaming apps are, by far, the most popular category at the Apple App Store. The figures from June 2015 reveal that while games make up close to 22% of all apps at the store, travel applications come in at 7th with a fairly measly 4.4% share. That, however, does not mean that there is any dearth of interesting and beneficial iPhone travel apps though. We had already talked about our <a href="stopover.php">Stopover</a> application earlier - and over here, the spotlight will be on My Trip, another of our real good iOS app for travelers.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/l1.png" alt="Lifestyle Travel Ballarat">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
The one thing that I had noticed about many mobile app companies was that, they either do not send app quotes on time, or are somewhat vague about their terms of service. Teknowledge, thankfully, did not pose any such problems. I received the free app quote for My Trip within 24 hours. These guys were the perfect...what shall I say...app-makers for me!”
                                <br><br>
                                <center>
				<img src="appstories/lifestyleclient.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Leonie Spencer</span> <br> 
				<span style="font-size: 25px;">(Managing Director, Lifestyle Travel Ballarat)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>



<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
              <h4>The Idea</h4>
          <div class="col-lg-6">
               <img src="appstories/l9.png" alt="Lifestyle Travel Ballarat">
             </div>
           
           <div class="col-lg-6">
               <P>My Trip is the brainchild of Leonie Spencer and her team at Lifestyle Travel Ballarat. From the moment she had outlined the app idea to us, we could see that the concept had the potential to emerge as a truly standout mobile application for travelers. It was immensely satisfying for each of our app developers to be able to do justice to what was an immensely promising project.</p>
             </div>  	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Making good mobile apps is all about being dynamic, understanding the pulse of your final users. It was smart on the part of the guys at Lifestyle Travel Ballarat to make My Trip an iOS 8-only app to start off with. After all, there was no point in extending backward support for older versions - and compromising on the usability features of the app in the process.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">The Compatibility Factor</h4>
             <div class="col-lg-6">
	      <P>Prior to the initial brainstorming and preliminary app wireframing sessions, a decision had to be taken regarding which mobile platform(s) My Trip will be compatible with. Going for an iOS app was a unanimous decision, and after detailed discussions with the senior representatives at Lifestyle Travel Ballarat, it was finalized that the app will be optimized only for the latest iOS 8 devices.</p>
             </div>
             <div class="col-lg-6">
               <img src="appstories/l10.png" alt="Lifestyle Travel Ballarat">
             </div>	
	   </div>
	</div>
     </div>	
</section>


<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
I have been a fan of traveling for well over three decades - and I will say this: there have been plenty of instances of me facing problems with travel, accommodation and general sightseeing at new places. In fact, one of our motives for making this iPhone app was to ensure that no other traveler ever runs into any such problems. Travel should always be worry-free!
                                <br><br>
                                <center>
				<img src="appstories/lifestyleclient.png" alt="Leonie Spencer" style="width:20%;"><br> 
				<span style="font-size: 30px;">Leonie Spencer</span> <br> 
				<span style="font-size: 25px;">(Managing Director, Lifestyle Travel Ballarat)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">The Features</h4>
	  		<div class="col-lg-6">
               <img src="appstories/l2.png" alt="Lifestyle Travel Ballarat">
             </div>
             <div class="col-lg-6">
	      <P>To put it in a sentence (not that it should be done, for the app has many accessory features), My Trip is an iOS application that makes tours and trips more systematic and organized for users. After going through the web check-in stage, people could purchase flight tickets directly from this user-friendly app. Following that, they could move on to chalking up daywise itineraries of the place(s) they were planning to visit. Following the itinerary is as easy as tapping on a smartphone. Literally.
	      <br><br>
	      Apart from facilitating the travel and sightseeing aspects of tours, My Trip doubles up as a more-than-handy mobile tour guide as well. Users could find out detailed, accurate, and most importantly, updated information about the place(s) they will be flying out to - all from the app. In effect, My Trip takes out the uncertainty factor from foreign tours.
	      </p>
             </div>	
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Being an enthusiastic traveler myself, I agreed with Leonie and her team that non-availability of information often ruined the fun-factor of otherwise eagerly-anticipated trips. Together, we were committed to make My Trip an absolute storehouse for information - on as many places as possible, and covering as many details as possible. Let’s just say it turned out just the way Leonie and her team, and me and my team, had envisaged.
                <br><br>
                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4 style="font-size: 35px;">Provision For Storing Travel Documents</h4>
             <div class="col-lg-6">
	      <P>This is, without a shadow of a doubt, a breakthrough feature of this mobile travel assistant app. We loved the idea floated by the personnel at Lifestyle Travel Ballarat that travel documents like passport and visa should ideally be scannable and be storable inside the application itself. Implementing this feature was not the easiest of tasks, but after a couple of slight hitches, the iPhone app developers on the job were successful.</p>
             </div>	
             <div class="col-lg-6">
               <img src="appstories/l3.png" alt="Lifestyle Travel Ballarat">
             </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
There is no feeling more hollow and helpless than realizing all on a sudden that you have misplaced your visa...or any other travel papers for that matter. I wanted My Trip to do away with the hassles of carrying the papers, and constantly being wary about them, altogether. With the app, travelers can simply scan, save, and forget about tensions.
                                <br><br>
                                <center>
				<img src="appstories/lifestyleclient.png" alt="Leonie Spencer" style="width:20%;"><br> 
				<span style="font-size: 30px;">Leonie Spencer</span> <br> 
				<span style="font-size: 25px;">(Managing Director, Lifestyle Travel Ballarat)</span>
				</center>
              </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  <h4>Real-Time Help - Around The Clock</h4>
            <div class="col-lg-7">
	      <P>Experienced and knowledgeable tour planners/guides can be directly contacted through the My Trip application. Whenever a user runs into any sort of problem, (s)he can start a chat with the app admin - available at all times - and get things sorted out in a matter of minutes. Suffice to say, My Trip is not only an information goldmine for travelers, the app offers real-time, customized help to users as well.</p>
              </div>
              <div class="col-lg-5">
                <img src="appstories/l4.png" alt="Lifestyle Travel Ballarat">
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
                No matter how well-researched a general mobile travel app might be, developers cannot possibly factor in all types of varied...at times weird...doubts that travelers might have at any time. To counter this, Leonie and her colleagues decided to include a chat option in the app - so that people could get answers, real-time.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>


<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <h4>Preserving Memories</h4>
            <div class="col-lg-6">
                <img src="appstories/l5.png" alt="Lifestyle Travel Ballarat">
              </div>
            <div class="col-lg-6">
	      <P>There is a special ‘Add Note’ feature in the My Trip iPhone/iPad app for travelers. It allows users to create and maintain records of all the places they visit during a foreign trip. What’s more, media content like images and videos can be added with each note as well. It’s basically like keeping a travel diary - and these notes are great for looking back at a later date. Fond memories and all that.</p>
              </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
Ever noticed how time tends to fly just that bit quicker when you are on a real enjoyable trip? With My Trip, I wanted to give people like me the option to create memories, make notes, preserve moments - that people would love, would be able to cherish for a long time. That’s precisely why I came up with the ‘Add Note’ feature in My Trip.
                                <br><br>
                                <center>
                <img src="appstories/lifestyleclient.png" alt="Leonie Spencer" style="width:20%;"><br>                	
				<span style="font-size: 30px;">Leonie Spencer</span> <br> 
				<span style="font-size: 25px;">(Managing Director, Lifestyle Travel Ballarat)</span>
				</center>
                               </blockquote>

		  	</div>
		</div>
	</div>	
</section>
<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
            <div class="col-lg-6">
	      <P>Voice records can also be included in the travel notes in the My Trip application. There are 5 main tabs in all in this mobile app for travel-lovers - ‘Docs’, ‘Guide’, ‘Message’, ‘Diary’ and ‘Info’. Our graphic designers have created the layout and in-app navigation of My Trip in a way that people do not face any difficulties whatsoever while using it. </p>
              </div>
            <div class="col-lg-6">
                <img src="appstories/l6.png" alt="Lifestyle Travel Ballarat">
              </div>   
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
			    <blockquote>
At Teks, we always try to make our apps as simple and user-friendly as possible - and for My Trip, this was, once again, a priority. Creating a multi-featured travelers’ app can be a waste of time, if people cannot use it quickly and with ease. If an app confuses users, they are never going to use it.
                                <br><br>
                                <center>
				<img src="appstories/hussain.png" alt="hussain fakhruddin" style="width:20%;"><br> 
				<span style="font-size: 30px;">Hussain Fakhruddin</span> <br> 
				<span style="font-size: 25px;">(CEO, Teknowledge Mobile Studio)</span>
				</center>
                            </blockquote>

		  	</div>
		</div>
	</div>	
</section>

<section>
     <div class="container">
	<div class="row">
	  <div class="col-lg-12">
	  	<h4>The Final Word</h4>
	      <div class="col-lg-5">
            <img src="appstories/l7.png" alt="Lifestyle Travel Ballarat">
	      </div>
	      <div class="col-lg-7">
            <p>Leonie and her team at Lifestyle Travel Ballarat had got in touch with us with an idea for an app for travelers that would make everything related to tours easier - right from the planning stage, to travel, sightseeing, taking care of travel documents, and even creating travel memories. We had a lovely time working on the app - and going by the ratings and reviews, My Trip is well on its way to becoming one of our most successful apps ever.</p>
	      </div>
	   </div>
	</div>
     </div>	
</section>

<section style="background:#3c90d5;">
	<div class="container">
		<div class="row">
	  		<div class="col-lg-12">
				<blockquote>
At the time of delegating my app idea to a third-party mobile app company, I had just that bit of lingering uncertainty about whether it would be able to make My Trip just the way I wanted. Credit to Hussain and his team at Teknowledge - they really did a good job. They were probably as passionate about my app as I was!”
                        <br><br>
                        <center>
			<img src="appstories/lifestyleclient.png" alt="Leonie Spencer" style="width:20%;"><br> 
			<span style="font-size: 30px;">Leonie Spencer</span> <br> 
			<span style="font-size: 25px;">(Managing Director, Lifestyle Travel Ballarat)</span>
			</center>
                            </blockquote>
		  	</div>
		</div>
	</div>	
</section>

<?php include "map.php";?>

<?php include 'footer.php';?>

<?php include 'script.php';?>

</body>
</html>