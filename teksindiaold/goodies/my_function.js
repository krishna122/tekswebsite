// JavaScript Document



/*

	id is defining the set of icons along with the device name.





*/



function loadIconImages(id){

	var imagesfor = id;

	var count = 0;

	///////////  for iphone iconset1 //////////////////////////

	if(imagesfor == "iconset1Iphone"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset1IphoneImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for iphone iconset2 //////////////////////////

	if(imagesfor == "iconset2Iphone"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset2IphoneImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for iphone iconset3 //////////////////////////

	if(imagesfor == "iconset3Iphone"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset3IphoneImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for iphone iconset4 //////////////////////////

	if(imagesfor == "iconset4Iphone"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset4IphoneImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for android iconset1 //////////////////////////

	if(imagesfor == "iconset1Android"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset1AndroidImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for android iconset2 //////////////////////////

	if(imagesfor == "iconset2Android"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset2AndroidImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

	///////////  for blackberry iconset1 //////////////////////////

	if(imagesfor == "iconset1Blackberry"){

		$('#'+imagesfor).children('div').each(function () {

			var replacewith = iconset1BlackberryImages[count];	

			 $(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

			count++;

		});

		count = 0;

	}

		

}



function loadSsImages(id){

	var imagesfor = id;

	var firstChar = imagesfor.substring( 0, 1 );

	var count = 0;

	var slide = 1;

	var device = "";

	if(firstChar == "i"){

		device = "iphone";

	}else if(firstChar == "a"){

		device ="android";	

	}else if(firstChar == "b"){

		device ="blackberry";	

	}else if(firstChar == "w"){

		device ="windows";	

	}else if(firstChar == "o"){

		device ="ourteam";	

	}else if(firstChar == "t"){

		device ="topofthestore";	

	}



	$('#'+imagesfor).children('div').each(function () {

		//var replacewith = screenshotImages[id][count];	

		var replacewith = "img/"+device+"/"+imagesfor+"/"+slide+".jpg";

		$(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;

		count++;

		slide++;

	});

}



function showAppScreenIphone(iconcontentname){

		if(iconcontentname == "iphone1"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "286px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneSt');

				$(".iphonetext").load("img/iphone/st.txt");

			}else if(iconcontentname == "iphone2"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneEskimos');

				$(".iphonetext").load("img/iphone/eskimos.txt");

			}else if(iconcontentname == "iphone3"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneUnify');

				$(".iphonetext").load("img/iphone/unify.txt");

			}else if(iconcontentname == "iphone4"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneEr');

				$(".iphonetext").load("img/iphone/er.txt");

			}else if(iconcontentname == "iphone5"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneEa');

				$(".iphonetext").load("img/iphone/pravana.txt");

			}else if(iconcontentname == "iphone6"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "286px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneBor');

				$(".iphonetext").load("img/iphone/bor.txt");

			}else if(iconcontentname == "iphone7"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneHypnosis');

				$(".iphonetext").load("img/iphone/hypnosis.txt");

			}else if(iconcontentname == "iphone8"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMfw');

				$(".iphonetext").load("img/iphone/mfw.txt");

			}else if(iconcontentname == "iphone9"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "286px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneIcw');

				$(".iphonetext").load("img/iphone/icw.txt");

			}else if(iconcontentname == "iphone10"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMybin');

				$(".iphonetext").load("img/iphone/my_bin.txt");

			}else if(iconcontentname == "iphone11"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneVc');

				$(".iphonetext").load("img/iphone/vc.txt");

			}else if(iconcontentname == "iphone12"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphonePp');

				$(".iphonetext").load("img/iphone/powpay.txt");

			}else if(iconcontentname == "iphone13"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneBio');

				$(".iphonetext").load("img/iphone/biostat.txt");

			}else if(iconcontentname == "iphone14"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneRen');

				$(".iphonetext").load("img/iphone/rentilla.txt");

			}else if(iconcontentname == "iphone15"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMc');

				$(".iphonetext").load("img/iphone/mc.txt");

			}else if(iconcontentname == "iphone16"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneBb');

				$(".iphonetext").load("img/iphone/bite_bank.txt");

			}else if(iconcontentname == "iphone17"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneSky');

				$(".iphonetext").load("img/iphone/sky_financial.txt");

			}else if(iconcontentname == "iphone18"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneTapHero');

				$(".iphonetext").load("img/iphone/tap-hero.txt");

			}else if(iconcontentname == "iphone19"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneJagota');

				$(".iphonetext").load("img/iphone/jagota.txt");

			}else if(iconcontentname == "iphone20"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMbt');

				$(".iphonetext").load("img/iphone/my_budget_tracker.txt");

			}else if(iconcontentname == "iphone21"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneAb');

				$(".iphonetext").load("img/iphone/arrive-before.txt");

			}else if(iconcontentname == "iphone24"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneCarleton');

				$(".iphonetext").load("img/iphone/carleton.txt");

			}else if(iconcontentname == "iphone31"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneCind');

				$(".iphonetext").load("img/iphone/scrumboard.txt");

			}else if(iconcontentname == "iphone27"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneSalmson');

				$(".iphonetext").load("img/iphone/salmson.txt");
				
			}else if(iconcontentname == "iphone29"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneLl');

				$(".iphonetext").load("img/iphone/logo-logo.txt");
				
			}else if(iconcontentname == "iphone34"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneTmw');

				$(".iphonetext").load("img/iphone/track-my-world.txt");
				
			}else if(iconcontentname == "iphone35"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneIml');

				$(".iphonetext").load("img/iphone/imlocal.txt");
				
			}else if(iconcontentname == "iphone57"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneDf');

				$(".iphonetext").load("img/iphone/duck-face.txt");

			}else if(iconcontentname == "iphone30"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneCind');

				$(".iphonetext").load("img/iphone/cinderella.txt");

			}else if(iconcontentname == "iphone32"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneScrum');

				$(".iphonetext").load("img/iphone/travel-genesis.txt");

			}else if(iconcontentname == "iphone26"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneRs');

				$(".iphonetext").load("img/iphone/ride-safe.txt");

			}else if(iconcontentname == "iphone42"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMr');

				$(".iphonetext").load("img/iphone/my-route.txt");

			}else if(iconcontentname == "iphone36"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneSconti');

				$(".iphonetext").load("img/iphone/sconti.txt");

			}else if(iconcontentname == "iphone43"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneTronder');

				$(".iphonetext").load("img/iphone/tronder.txt");

			}else if(iconcontentname == "iphone50"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneAlrroya');

				$(".iphonetext").load("img/iphone/alrroya.txt");

			}else if(iconcontentname == "iphone25"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneApricube');

				$(".iphonetext").load("img/iphone/apricube.txt");

			}else if(iconcontentname == "iphone33"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneBookoo');

				$(".iphonetext").load("img/iphone/bookoo.txt");

			}else if(iconcontentname == "iphone22"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneJencor');

				$(".iphonetext").load("img/iphone/jencor.txt");

			}else if(iconcontentname == "iphone38"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneiBooking');

				$(".iphonetext").load("img/iphone/ibooking.txt");

			}else if(iconcontentname == "iphone41"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "286px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneXmas');

				$(".iphonetext").load("img/iphone/xmas.txt");

			}else if(iconcontentname == "iphone44"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneNte');

				$(".iphonetext").load("img/iphone/nte.txt");

			}else if(iconcontentname == "iphone51"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneFf');

				$(".iphonetext").load("img/iphone/faceflip.txt");

			}else if(iconcontentname == "iphone54"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphonePikit');

				$(".iphonetext").load("img/iphone/pikit.txt");

			}else if(iconcontentname == "iphone56"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneMyhome');

				$(".iphonetext").load("img/iphone/myhome.txt");

			}else if(iconcontentname == "iphone23"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneImonchies');

				$(".iphonetext").load("img/iphone/imonchies.txt");

			}else if(iconcontentname == "iphone28"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneWeave');

				$(".iphonetext").load("img/iphone/weave.txt");

			}else if(iconcontentname == "iphone39"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneiShadow');

				$(".iphonetext").load("img/iphone/ishadow.txt");

			}else if(iconcontentname == "iphone47"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneRennes');

				$(".iphonetext").load("img/iphone/rennes.txt");

			}else if(iconcontentname == "iphone48"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneFolloworks');

				$(".iphonetext").load("img/iphone/followorks.txt");

			}else if(iconcontentname == "iphone49"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneHolberg');

				$(".iphonetext").load("img/iphone/holberg.txt");

			}else if(iconcontentname == "iphone52"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneFanta');

				$(".iphonetext").load("img/iphone/fanta-gazatta.txt");

			}else if(iconcontentname == "iphone53"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphoneAustin');

				$(".iphonetext").load("img/iphone/austin-tree.txt");

			}else if(iconcontentname == "iphone55"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphonePickMeUp');

				$(".iphonetext").load("img/iphone/pick-me-up.txt");

			}else if(iconcontentname == "iphone37"){

				$(".iphonetext").show();

				$(".iphonetext").css("width", "500px");

				$("#page_iphone").removeClass();

				$("#page_iphone").addClass('PopupPaneliphonejlife');

				$(".iphonetext").load("img/iphone/jlife.txt");

			}				

	}

	

	function showAppScreenAndroid(iconcontentname){

		if(iconcontentname == "android1"){

			$(".androidtext").show();

			$(".androidtext").css("width", "286px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidSt');

			$(".androidtext").load("img/android/st.txt");

		}else if(iconcontentname == "android2"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidUnify');

			$(".androidtext").load("img/android/unify.txt");

		}else if(iconcontentname == "android3"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidEr');

			$(".androidtext").load("img/android/er.txt");

		}else if(iconcontentname == "android4"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidEa');

			$(".androidtext").load("img/android/pravana.txt");

		}else if(iconcontentname == "android5"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidHypnosis');

			$(".androidtext").load("img/android/hypnosis.txt");

		}else if(iconcontentname == "android6"){

			$(".androidtext").show();

			$(".androidtext").css("width", "286px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidMfw');

			$(".androidtext").load("img/android/mfw.txt");

		}else if(iconcontentname == "android7"){

			$(".androidtext").show();

			$(".androidtext").css("width", "286px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidIcw');

			$(".androidtext").load("img/android/icw.txt");

		}else if(iconcontentname == "android8"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidMb');

			$(".androidtext").load("img/android/mb.txt");

		}else if(iconcontentname == "android9"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidJm');

			$(".androidtext").load("img/android/jm.txt");

		}else if(iconcontentname == "android10"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidNordea');

			$(".androidtext").load("img/android/nordea.txt");

		}else if(iconcontentname == "android11"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidMbt');

			$(".androidtext").load("img/android/mbt.txt");

		}else if(iconcontentname == "android12"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidSalmson');

			$(".androidtext").load("img/android/salmson.txt");

		}else if(iconcontentname == "android13"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidFlok');

			$(".androidtext").load("img/android/flok.txt");

		}else if(iconcontentname == "android16"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidTdt');

			$(".androidtext").load("img/android/tdt.txt");

		}else if(iconcontentname == "android18"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidCw');

			$(".androidtext").load("img/android/cw.txt");

		}else if(iconcontentname == "android14"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidTaxilotse');

			$(".androidtext").load("img/android/taxilotse.txt");

		}else if(iconcontentname == "android15"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidAustinTree');

			$(".androidtext").load("img/android/austin-tree.txt");

		}else if(iconcontentname == "android20"){

			$(".androidtext").show();

			$(".androidtext").css("width", "500px");

			$("#page_android").removeClass();

			$("#page_android").addClass('PopupPanelandroidLetfarm');

			$(".androidtext").load("img/android/letfarm.txt");

		}

	}

	

	function showAppScreenBlackberry(iconcontentname){

		if(iconcontentname == "blackberry1"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "286px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberrySt');

			$(".blackberrytext").load("img/blackberry/st.txt");

		}else if(iconcontentname == "blackberry2"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "286px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberryEr');

			$(".blackberrytext").load("img/blackberry/er.txt");

		}else if(iconcontentname == "blackberry4"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "286px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberryMfw');

			$(".blackberrytext").load("img/blackberry/mfw.txt");

		}else if(iconcontentname == "blackberry5"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "500px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberryMbt');

			$(".blackberrytext").load("img/blackberry/mbt.txt");

		}else if(iconcontentname == "blackberry7"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "286px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberryCg');

			$(".blackberrytext").load("img/blackberry/canada-guaranty.txt");

		}else if(iconcontentname == "blackberry8"){

			$(".blackberrytext").show();

			$(".blackberrytext").css("width", "500px");

			$("#page_blackberry").removeClass();

			$("#page_blackberry").addClass('PopupPanelblackberryMobgyn');

			$(".blackberrytext").load("img/blackberry/myobgyn.txt");

		}

	}
	
	function loadparallax(){
		var slide = 1;
		$('#target').children('div').each(function () {
			var replacewith = "goodies/par_item"+slide+".png";
			$(this).html($(this).html().replace('src="', 'src="' + replacewith + '"')) ;
			slide++;
		});
	}