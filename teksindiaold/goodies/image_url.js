// JavaScript Document



///////////////////////////////////// Image arrays //////////////////////////////////////////////////



var iconset1IphoneImages = [ 

	"img/iphone/icon/story-time.png", 

	"img/iphone/icon/eskimos.png", 

	"img/iphone/icon/unify.png", 

	"img/iphone/icon/exclusive-restaurant.png", 

	"img/iphone/icon/pravana.png",

	"img/iphone/icon/book-of-rhymes.png",

	"img/iphone/icon/hypnosis.png",

	"img/iphone/icon/my-first-word.png",

	"img/iphone/icon/i-can-write.png",

	"img/iphone/icon/my-bin.png",

	"img/iphone/icon/vampire-casino.png",
	
	"img/iphone/icon/powpay.png",

	"img/iphone/icon/biostat-calculator.png",

	"img/iphone/icon/rentilla.png",

	"img/iphone/icon/mcdonald.png"

	

];



var iconset2IphoneImages = [

	"img/iphone/icon/bite-bank.png",

	"img/iphone/icon/sky-financial.png",

	"img/iphone/icon/tap-hero.png",

	"img/iphone/icon/jagota.png",

	"img/iphone/icon/my-budget-tracker.png",
	
	"img/iphone/icon/arrivebefore.png",

	"img/iphone/icon/jencor.png",

	"img/iphone/icon/imonchies.png",

	"img/iphone/icon/carleton.png",

	"img/iphone/icon/apricube.png",

	"img/iphone/icon/ride-safe.png",

	"img/iphone/icon/salmson.png",

	"img/iphone/icon/weave.png",
	
	"img/iphone/icon/logologo.png",
	
	"img/iphone/icon/anti-duck-face.png"



];



var iconset3IphoneImages = [

	"img/iphone/icon/cinderella.png",

	"img/iphone/icon/scrumboard.png",

	"img/iphone/icon/travel-genesis.png",

	"img/iphone/icon/bookoo.png",
	
	"img/iphone/icon/track-my-world.png",
	
	"img/iphone/icon/imlocal.png",

	"img/iphone/icon/sconti.png",

	"img/iphone/icon/jlife.png",

	"img/iphone/icon/ibooking.png",

	"img/iphone/icon/ishadow.png",

	"img/iphone/icon/xmas.png",

	"img/iphone/icon/my-route.png",

	"img/iphone/icon/tronder.png"



];



var iconset4IphoneImages = [

	"img/iphone/icon/nte.png",

	"img/iphone/icon/rennes.png",

	"img/iphone/icon/followorks.png",

	"img/iphone/icon/holberg.png",

	"img/iphone/icon/alrroya.png",

	"img/iphone/icon/faceflip.png",

	"img/iphone/icon/fanta-gazatta.png",

	"img/iphone/icon/austin-tree.png",

	"img/iphone/icon/pikit.png",

	"img/iphone/icon/pick-me-up.png",

	"img/iphone/icon/myhome247.png"

];



var iconset1AndroidImages = [ 

	"img/android/icon/story-time.png",

	"img/android/icon/unify.png",

	"img/android/icon/exclusive-restaurant.png",

	"img/android/icon/pravana.png",

	"img/android/icon/hypnosis.png",

	"img/android/icon/my-first-word.png",

	"img/android/icon/i-can-write.png",

	"img/android/icon/my-bin.png",

	"img/android/icon/johan.png",

	"img/android/icon/nordea.png",

	"img/android/icon/my-budget-tracker.png"



];





var iconset2AndroidImages = [

	"img/android/icon/salmson.png",

	"img/android/icon/flok.png",

	"img/android/icon/taxilotse.png",

	"img/android/icon/austin-tree.png",

	"img/android/icon/tram-de-tour.png",

	"img/android/icon/car-wale.png",

	"img/android/icon/letfarm.png"

];



var iconset1BlackberryImages = [

	"img/blackberry/icon/story-time.png",

	"img/blackberry/icon/exclusive-restaurant.png",

	"img/blackberry/icon/my-first-word.png",

	"img/blackberry/icon/my-budget-tracker.png",

	"img/blackberry/icon/canada-guaranty.png",

	"img/blackberry/icon/myobgyn.png"

];



/*var screenshotImages = new Array();



screenshotImages ["iphone1"]  = [

	"img/iphone/st/st1.jpg",

	"img/iphone/st/st2.jpg",

	"img/iphone/st/st3.jpg",

	"img/iphone/st/st4.jpg",

	"img/iphone/st/st5.jpg"

];*/

