﻿/*
*Version: 1.0;
*Author: Behaart;
*Option:
	autoPlayState: false / trur (default:false),
	autoPlayTime: seconds (default:4),
	alignIMG: center / top / bottom / left / right / top_left / top_right / bottom_left / bottom_right (default:center),
*/
$(document).ready(function(){
	$.fn.backgroundIMG = function(o){
		var getObject = {
				autoPlayState:false,
				autoPlayTime:4,
				alignIMG:"center"
			},
			object = $(this),
			imageHolder = $("#imageHolder",object),
	 		image = $("#imageHolder > img",object),
	 		imageSRCLink = $("ul>li>a",object),
			currImg = 0,
			prevImg = 0,
			loadComplete = true,
			MSIE8 = ($.browser.msie) && ($.browser.version <= 8);
			
		$.extend(getObject, o);	
		init()
		
		function init(){
			imageHolder.css({"position":"fixed"});
			$(window).resize(resizeImageHandler).trigger('resize');
			/*-----thumbnail----*/
			if($("#previewHolder ul>li>a>img").length!=0){
				console.log("thumbnail");
				$("#inner").bind("mousemove", function(e){mouseMove(e)})
				$("#inner").bind("mouseleave", stopPreviewPosition)
				$("#previewHolder ul>li>a").each(function(){
					/*$(this).append("<div class='over'></div>")
					$(".over", this).animate({opacity:0}, 600, "easeInOutCubic")*/
				}).click(
					function(){
						if($(this).parent().index()!=currImg && loadComplete){
							/*$("#previewHolder ul>li").eq(currImg).find("a>.over").stop(true).animate({opacity:0}, 600, "easeInOutCubic")*/
							currImg=$(this).parent().index();
							autoPlayState = false;
							changeImageHandler();
						}
						return false;			
					}
				).hover(
					function(){
						if($(this).parent().index()!=currImg){
							//$(".over", this).stop(true).animate({opacity:1}, 600, "easeInOutCubic");
						}
					},
					function(){
						if($(this).parent().index()!=currImg){
						//	$(".over", this).stop().animate({opacity:0}, 600, "easeInOutCubic");
						}
					}
				);
				$("#previewHolder ul>li").eq(currImg).find("a>.over").stop().animate({opacity:1}, 0);
			}
			autoPlayHandler();
		}
		function mouseMove(e){
			var doc=$(document);
			if(e.pageX<doc.width()/2){
				changePreviewPosition(-1);
			}else{
				changePreviewPosition(1);
			};
		}
		function changePreviewPosition(number){
			var innerW = $(document).width(),
				ulW =  $("#inner>ul").innerWidth(true),
				scrollTime;			
			if(number==-1){
				scrollTime = Math.abs(parseInt($("#inner>ul").css("left"))*7000/(ulW-innerW));
				$("#inner>ul").css({"position":"relative"}).stop(true).animate({left:0}, scrollTime, "linear");	
			}else if(number==1){
				scrollTime = Math.abs((parseInt($("#inner>ul").css("left"))+(ulW-innerW))*7000/(ulW-innerW));
				$("#inner>ul").css({"position":"relative"}).stop(true).animate({left:-(ulW-innerW)}, scrollTime, "linear");
			}else if(number==0){
				if(ulW+parseInt($("#inner>ul").css("left"))<innerW){
					console.log("asd");
					$("#inner>ul").css({"position":"relative", left:-(ulW-innerW)});
				}
			}
		}
		function stopPreviewPosition(){
			$("#inner>ul").stop(true);
		}	
		function autoPlayHandler(){
			var allImg = imageSRCLink.length;
			if(getObject.autoPlayState){
				setTimeout(function(){
					prevImg = currImg;
					currImg++;
					if(currImg>=allImg){
						currImg = 0;
					}
					$("#previewHolder ul>li").eq(prevImg).find("a>.over").stop(true).animate({opacity:0}, 600, "easeInOutCubic");
					$("#previewHolder ul>li").eq(currImg).find("a>.over").stop(true).animate({opacity:1}, 600, "easeInOutCubic");
					changeImageHandler();
				}, getObject.autoPlayTime*1000);
			}
		}
		function resizeImageHandler(){
			var imageDeltaX,
				imageDeltaY,
				imageK =image.height()/image.width(),
				holderK =imageHolder.height()/imageHolder.width(),
				imagePercent = (image.height()/image.width())*100,
				doc=$(window);
				
			image = $("#imageHolder > img");
			imageK =image.height()/image.width()
			holderK =doc.height()/doc.width();
			if(holderK>imageK){
				imagePercent = (image.width()/image.height())*100;
				image.css({height:doc.height(), width:(doc.height()*imagePercent)/100});
			}else{
				imagePercent = (image.height()/image.width())*100;
				image.css({width:doc.width(), height:(doc.width()*imagePercent)/100});
			}
			switch(getObject.alignIMG){
				case "top":
					imageDeltaX=-(image.width()-doc.width())/2;
					imageDeltaY=0;
				break;
				case "bottom":
					imageDeltaX=-(image.width()-doc.width())/2;
					imageDeltaY=-(image.height()-doc.height());
				break;
				case "right":
					imageDeltaX=-(image.width()-doc.width());
					imageDeltaY=-(image.height()-doc.height())/2;
				break;
				case "left":
					imageDeltaX=0;
					imageDeltaY=-(image.height()-doc.height())/2;
				break;
				case "top_left":
					imageDeltaX=0;
					imageDeltaY=0;
				break;
				case "top_right":
					imageDeltaX=-(image.width()-doc.width());
					imageDeltaY=0;
				break;
				case "bottom_right":
					imageDeltaX=-(image.width()-doc.width());
					imageDeltaY=-(image.height()-doc.height());
				break;
				case "bottom_left":
					imageDeltaX=0;
					imageDeltaY=-(image.height()-doc.height());
				break;
				default:
					imageDeltaX=-(image.width()-doc.width())/2;
					imageDeltaY=-(image.height()-doc.height())/2;
			}
			image.css({left:imageDeltaX, top:imageDeltaY, position:"absolute"});
			changePreviewPosition(0)
		}
		function changeImageHandler(){
			var imgSRC;
			loadComplete = false;
			image.addClass("topImg");
			imgSRC = imageSRCLink.eq(currImg).attr("href");
			imageHolder.append("<div id='imgSpinner'><div></div></div><img class='bottomImg' src="+imgSRC+" alt=''>");
			$("#imgSpinner").css({opacity:0}).stop().animate({opacity:1}, 500, "easeInOutCubic");
			$(".bottomImg").bind("load", loadImageHandler)
		}
		function loadImageHandler(){
			setTimeout(function(){
				$(".bottomImg").unbind("load", loadImageHandler);
				$("#imgSpinner").stop().animate({opacity:"0"}, 1000, "easeInOutCubic")
				resizeImageHandler();
				$(".topImg").stop().animate({opacity:"0"}, 1000, "easeInOutCubic", function(){
					$("#imgSpinner").remove();
					$(".topImg").remove();
					image.removeClass("bottomImg");
					loadComplete = true;
					autoPlayHandler()
				})
			}, 1000)
		}
	}
})