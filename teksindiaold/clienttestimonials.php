<?php include_once 'mysqlconnect.php'; ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name='description' content='iPhone app development India: We have developed over 400 apps and provides all mobile application development service at low cost. Best iPhone app development, iPad, Android and other mobile app solutions at affordable rates. Custom gaming on iPhone or iPad.' />
<meta name='keywords' content='iphone app development india, iphone development india, android app development, ipad app development, ipad development, iPhone app development, android, mobile application development, mobile app development company, webservices, api development, Game Development Company India, UI Design' />


<title>iPhone App Development India | Mobile Application Development Company | Teknowledge Mobile Studio</title>

<link rel="shortcut icon" href="goodies/icon_16.png">

<link rel="stylesheet" href="goodies/reset.css">
<link rel="stylesheet" href="goodies/style.css">
<link rel="stylesheet" href="goodies/color.css">


<script src="goodies/jquery.min.js"></script>
<script src="goodies/jquery-ui.min.js"></script>
<script src="goodies/scripts.js"></script>
<script src="goodies/superfish.js" ></script>
<script src="goodies/hoverSprite.js" ></script>
<script src="goodies/backgroundIMG.js" ></script>
<script src="goodies/inputs.js"></script>
<script src="goodies/my_function.js"></script>
<script src="goodies/image_url.js"></script>

        <link rel="stylesheet" type="text/css" href="css/style1.css" />
		<script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
		<style>
body{
/*Remove below line to make bgimage NOT fixed*/
background-attachment:fixed;
background-repeat: no-repeat;
/*Use center center in place of 300 200 to center bg image*/
background-position: 300 200;
}

#everything-wrapper {
	display : none;
}
#loadingMask {
	color:#000000;
	text-align:center;
	width: 100%;
	height: 100%;
	position: absolute;
	background: #fff;
}
.message {
	width:250px;
	height:50px;
	background-color:#333333;
	color:#FFFFFF;
	border: solid #FFFFFF thin;
	z-index:2000;
	top:50%;
	left:50%;
	position:absolute;
	text-align:center;
	margin-left:-125px;
	margin-top:-25px;
	padding:6px;
}
</style>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46444858-1', 'teks.co.in');
  ga('send', 'pageview');

</script>
    </head>
 <body id="page" style="background: url(bg1.jpg);">
    
    <!--     <ul class="cb-slideshow">
            <li><span>Image 01</span></li>
            <li><span>Image 02</span></li>
            <li><span>Image 03</span></li>
            <li><span>Image 04</span></li>
            <li><span>Image 05</span></li>
            <li><span>Image 06</span></li>
        </ul> -->
        <div> <?php include 'menu.php'; ?> </div>
        

<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('8240-470-10-7173');/*]]>*/</script>
<noscript>
<a href="https://www.olark.com/site/8240-470-10-7173/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a>
</noscript>
<div id="everything-wrapper">
  
  <div id="target" class="parallax-port">
    <div class="parallax-layer item1"> <img src="goodies/par_item1.png" alt="par_item1.png"> </div>
    <div class="parallax-layer item3"> <img src="goodies/par_item2.png" alt="par_item2.png"> </div>
    <div class="parallax-layer item4"> <img src="goodies/par_item3.png" alt="par_item3.png"> </div>
    <div class="parallax-layer item5"> <img src="goodies/par_item4.png" alt="par_item4.png"> </div>
    <div class="parallax-layer item6"> <img src="goodies/par_item5.png" alt="par_item5.png"> </div>
    <div class="parallax-layer item7"> <img src="goodies/par_item6.png" alt="par_item6.png"> </div>
    <div class="parallax-layer item8"> <img src="goodies/par_item7.png" alt="par_item7.png"> </div>
  </div>


<div class="glob">
  <div class="main">
    <div class="center">
    	<article id="content">
   <?php include 'header.php'; ?>
<div style="height:40px;">
  <!--  giving space-->
</div>

<div class= "contentcss">
<div class="pageheader">TESTIMONIALS</div>
<div id="contentbg">
          <div id="youtubevideo"></div>
          <div id="videobtns"><a class="ytvbtns" n="1" id="1" href="#"><span><img src="goodies/video1.jpg"></span></a><a class="ytvbtns" n="2" id="2" href="#"><span><img src="goodies/video2.jpg"></span></a><a class="ytvbtns" n="3" id="3" href="#"><span><img src="goodies/video3.jpg"></span></a></div>
</div>

<div style="height:450px;"></div>


<div  style= "height:100%; width:100%;   "><p class="contentmarginbottom"></p><div style= "postion: absolute; float:left;"><img src="goodies/qut1.png"></div><br>
<p style= " color:#ffe0b2;  text-align: left; font-style:italic;  font-size:16px; padding: 0px 25px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"My son turned three this year, and I was looking for a gift for him that would be fun and, at the same time, have educational elements. While randomly searching for ideas on the internet, I came across the I Can Write app on this website. The features and functionality were just what I was looking for, and the price was - well, not like other run-of-the mill gifts. My kiddie loves it too!"</p>
<br><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">-- Alicia Bell</p><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">Notts County, England</p>
<br></div>

<p class="contentmarginbottom"></p>

<div  style= "height:100%; width:100%;    "><div style= "postion: absolute; float:left;"><img src="goodies/qut1.png"></div><br>
<p style= " color:#ffe0b2;  text-align: left; font-style:italic;  font-size:16px; padding: 0px 25px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Ever been frustrated by iPhone applications that promise to be the next big thing in the world of personal finance - and actually end up overcomplicating matters? I have, and that's why I was initially reluctant to download My Budget Tracker. One of my colleagues, who was already using the app, strongly recommended it though - and I decided to give it a try (I mean, what the heck, right?). The application has been nothing short of a revelation, and I now use it even for keeping track of day-to-day expenses. Two thumbs up!"</p>
<p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;"> -- Carl Starr</p><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">Los Angeles, United States of America</p>
<br></div>
<p class="contentmarginbottom"></p>

<div  style= "height:100%; width:100%;    "><div style= "postion: absolute; float:left;"><img src="goodies/qut1.png"></div><br>
<p style= " color:#ffe0b2;  text-align: left; font-style:italic;  font-size:16px; padding: 0px 25px; ">"Probably not the type of Valentine's Day gift my boyfriend was looking forward too...but he definitely liked it! <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;According to him (and I am so happy that he feels so), there can hardly anything be better than a snazzy iphone application which offered all the excitements of a gambling table, without any need to jostle among crowds at a real casino. Am pretty sure he did not say that only to please me - he does that at times, you know!"</p>
<br><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">-- Shama Ehsaan</p><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">New Delhi, India</p>
<br></div>

<p class="contentmarginbottom"></p>
<div  style= "height:100%; width:100%;    "><div style= "postion: absolute; float:left;"><img src="goodies/qut1.png"></div><br>
<p style= " color:#ffe0b2;  text-align: left; font-style:italic;  font-size:16px; padding: 0px 25px; ">"Yayy! I so love this app!!<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;My dad is in the field of mobile application development (sounds pretty boring!), and it was his idea to get me the Story Time iphone application as a New Years' present. It took me two days to get hooked to the stories on this app, and the games are also soooooo much fun!<br><br>Thank you daddy.... and thank you, Story Time! Best.Gift.Ever!!"</p>
<br><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">  -- Netra Singhania</p><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">Bengaluru, India</p>
<br></div>


<p class="contentmarginbottom"></p>
<div  style= "height:100%; width:100%;   "><div style= "postion: absolute; float:left;"><img src="goodies/qut1.png"></div><br>
<p style= " color:#ffe0b2;  text-align: left; font-style:italic;  font-size:16px; padding: 0px 25px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"I had earlier got the rather interesting Book of Rhymes iPhone application for my six-year old girl from Teknowledge Software - but I had no idea that they even had mobile apps related to gynecology! Me and Rick (that's my hubby) are expecting the arrival of our second child sometime in April - and MyObGyn has become my trusted virtual gynecologist, whom I can refer to at all times!"</p>
<br><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;"> -- Felicia Stackpole </p><p style= "margin-left:20px; margin-right:20px; text-align:right; font-size:18px;">Barmouth, Wales</p>
<br></div>
<p class="contentmarginbottom"></p>
<br><br>


</div><br><br>
<?php include 'footer2.php'; ?>
</article>

 
      </div>
    </div>  
</div>


<div class="link" id="quote"><a href="requestAQuote.php"><img id="quoteImage" src="goodies/request_quote.png" alt="request.png"></a></div>
<?php include 'footer.php'; ?>

<script>
$(window).load(function() {
	$("#everything-wrapper").fadeIn("slow");
	loadparallax();
	$(document).bind("contextmenu",function(e){
        	return false;	
  	});
});
$("#youtubevideo").load("img/video/1.txt");
$('body').css({overflow:'auto', 'min-height':'100%'});

$('#quoteImage').hover(function() {
	$(this).stop().animate({"marginLeft":"-6px"}, "slow");
}, function() {
	$(this).stop().animate({"marginLeft":"0px"}, "slow");
});


SITE.fileInputs = function() {
  var $this = $(this),
      $val = $this.val(),
      valArray = $val.split('\\'),
      newVal = valArray[valArray.length-1],
      $button = $this.siblings('.button');
  if(newVal !== '') {
    $button.text(newVal);
  }
};


	$(".ytvbtns").click(function(){
		var videoid = $(this).attr("n");
		if(videoid == 1){
			$("#youtubevideo").load("img/video/1.txt");
		}else if(videoid == 2){
			
			$("#youtubevideo").load("img/video/2.txt");
		}else if(videoid == 3){
			
			$("#youtubevideo").load("img/video/3.txt");
		}
	
	});


$("#logo").click(function(){
	window.location.href = 'index.php';
})
$("#logo").hover(function(){
	$(this).css('cursor','pointer');
})
</script>

<script type="text/javascript">
var sc_project=4361351; 
var sc_invisible=1; 
var sc_security="9083fae0"; 
</script>
<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script>
<noscript>
<div class="statcounter"><a title="site stats"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/4361351/0/9083fae0/1/"
alt="site stats"></a></div>
</noscript>

</body>
</html>
