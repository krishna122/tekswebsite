<?php include_once 'mysqlconnect.php'; ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name='description' content='iPhone app development India: We have developed over 400 apps and provides all mobile application development service at low cost. Best iPhone app development, iPad, Android and other mobile app solutions at affordable rates. Custom gaming on iPhone or iPad.' />
<meta name='keywords' content='iphone app development india, iphone development india, android app development, ipad app development, ipad development, iPhone app development, android, mobile application development, mobile app development company, webservices, api development, Game Development Company India, UI Design' />

<title>iPhone App Development India | Mobile Application Development Company | Teknowledge Mobile Studio</title>

<link rel="shortcut icon" href="goodies/icon_16.png">

<link rel="stylesheet" href="goodies/reset.css">
<link rel="stylesheet" href="goodies/style.css">
<link rel="stylesheet" href="goodies/color.css">


<script src="goodies/jquery.min.js"></script>
<script src="goodies/jquery-ui.min.js"></script>
<script src="goodies/scripts.js"></script>
<script src="goodies/superfish.js" ></script>
<script src="goodies/hoverSprite.js" ></script>
<script src="goodies/backgroundIMG.js" ></script>
<script src="goodies/inputs.js"></script>
<script src="goodies/my_function.js"></script>
<script src="goodies/image_url.js"></script>
       
        <link rel="stylesheet" type="text/css" href="css/style1.css" />
		<script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
		<style>
body{
/*Remove below line to make bgimage NOT fixed*/
background-attachment:fixed;
background-repeat: no-repeat;
/*Use center center in place of 300 200 to center bg image*/
background-position: 300 200;
}

#everything-wrapper {
	display : none;
}
#loadingMask {
	color:#000000;
	text-align:center;
	width: 100%;
	height: 100%;
	position: absolute;
	background: #fff;
}
.message {
	width:250px;
	height:50px;
	background:#3b2415; -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=85); filter: alpha(opacity=85);-moz-opacity: 0.85;-khtml-opacity: 0.85;opacity: 0.85;
	color:#FFFFFF;
	border: solid #FFFFFF thin;
	z-index:2000;
	top:50%;
	left:50%;
	position:absolute;
	text-align:center;
	margin-left:-125px;
	margin-top:-25px;
	padding:6px;
}

span{ color:red;}
</style>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46444858-1', 'teks.co.in');
  ga('send', 'pageview');

</script>
    </head>
 <body id="page" style="background: url(bg1.jpg);">
    
    <!--     <ul class="cb-slideshow">
            <li><span>Image 01</span></li>
            <li><span>Image 02</span></li>
            <li><span>Image 03</span></li>
            <li><span>Image 04</span></li>
            <li><span>Image 05</span></li>
            <li><span>Image 06</span></li>
        </ul> -->
        
        <div> <?php include 'menu.php'; ?> </div>
        
        
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('8240-470-10-7173');/*]]>*/</script>
<noscript>
<a href="https://www.olark.com/site/8240-470-10-7173/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a>
</noscript>

<div id="everything-wrapper">
  
  <div id="target" class="parallax-port">
    <div class="parallax-layer item1"> <img src="goodies/par_item1.png" alt="par_item1.png"> </div>
    <div class="parallax-layer item3"> <img src="goodies/par_item2.png" alt="par_item2.png"> </div>
    <div class="parallax-layer item4"> <img src="goodies/par_item3.png" alt="par_item3.png"> </div>
    <div class="parallax-layer item5"> <img src="goodies/par_item4.png" alt="par_item4.png"> </div>
    <div class="parallax-layer item6"> <img src="goodies/par_item5.png" alt="par_item5.png"> </div>
    <div class="parallax-layer item7"> <img src="goodies/par_item6.png" alt="par_item6.png"> </div>
    <div class="parallax-layer item8"> <img src="goodies/par_item7.png" alt="par_item7.png"> </div>
  </div>


<div class="glob">
  <div class="main">
    <div class="center">
    	<article id="content">
   <?php include 'header.php'; ?>
<div style="height:40px;">
  <!--  giving space-->
</div>

<div  class= "contentcss">

<div class="pageheader">REQUEST A QUOTE</div>
<br><br>
          <table width="85%" cellspacing="0" cellpadding="0">
            <tr>
              <td class="leftbox">Name<span>*</span>:</td>
              <td class="rightbox"><input type="text" style="background-color:#ececec; font-size:14px;" name="uname" id="name" value="Name" size="32" onFocus="javascript:if(this.value=='Name')this.value=''" onBlur="javascript:if(this.value=='')this.value='Name'"></td>
            </tr>
            <tr height="20px;"></tr>
            <tr>
              <td class="leftbox">Email<span>*</span>:</td>
              <td class="rightbox"><input type="text" style="background-color:#ececec; font-size:14px;" name="email" id="email" value="Email" size="32" onFocus="javascript:if(this.value=='Email')this.value=''" onBlur="javascript:if(this.value=='')this.value='Email'"></td>
            </tr>
            <tr height="20px;"></tr>
            <tr>
              <td class="leftbox">About app <span>*</span>:</br>
                (in one line)</td>
              <td class="rightbox"><input type="text" style="background-color:#ececec; font-size:14px;" name="des" id="des" value="App Description" size="32" onFocus="javascript:if(this.value=='App Description')this.value=''" onBlur="javascript:if(this.value=='')this.value='App Description'"></td>
            </tr>
             <tr>
              <td colspan="2" style="height:2px;">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" id="moredetails">Some more details:</td>
            </tr>
            <tr>
              <td class="leftbox"> Need a wireframe ?<span>*</span>:</td>
              <td class="rightbox2"><a href="#">
                <li class="yes" id="yes"></li>
                </a>
                <li style="font-size:14px;">Yes, I'll need you to create one.</li>
                </br></br>
                <a href="#">
                <li class="no" id="no"></li>
                </a>
                <li style="font-size:14px;">No, I will supply my own wireframe later.</li></td>
            </tr>
            <tr>
              <td colspan="2" style="height:2px;">&nbsp;</td>
            </tr>
            <tr>
              <td class="leftbox">Approximate budget<span>*</span></td>
              <td class="select" ><a href="#"><div id="selectlabel">
                  <div id="budgettext">Choose Budget</div>
                </div></a>
                <div id="selecttext">
                <a href="#"><li>$400-$500(4 Screens)</li></a>
                <a href="#"><li>$600-$900(8 Screens)</li></a>
                <a href="#"><li>$1000-$2000(Above 8 Screens)</li></a>
                </div>
               </td>
            </tr>
            <tr>
              <td colspan="2" style="height:2px;">&nbsp;</td>
            </tr>
            <tr height="20px;"></tr>
            <tr>
              <td class="leftbox">Layouts want to share</td>
              <td class="upload"><span class="file-wrapper"><input type="file" name="file" id="file" />
  				<span class="button">Upload File</span></span></td>
            </tr>
             <tr>
              <td colspan="2" style="height:2px;" align="center"><div id="send"><a href="#">Send</a></div></td>
            </tr>
          </table>
          <div class="message"  style="display:none;">Thank you for your request.</br> We will get back to you soon!</div>
   <br><br></div><br>
<?php include 'footer2.php'; ?>

        </article>
      </div>
    </div>
</div>
</div>
</div>

<div class="link" id="quote"><a href="requestAQuote.php"><img id="quoteImage" src="goodies/request_quote.png" alt="request.png"></a></div>
<?php include 'footer.php'; ?>

<script>
$(window).load(function() {
	$("#everything-wrapper").fadeIn("slow");
	loadparallax();
	$(document).bind("contextmenu",function(e){
        	return false;	
  	});
});
$('body').css({overflow:'auto', 'min-height':'100%'});


SITE.fileInputs = function() {
  var $this = $(this),
      $val = $this.val(),
      valArray = $val.split('\\'),
      newVal = valArray[valArray.length-1],
      $button = $this.siblings('.button');
  if(newVal !== '') {
    $button.text(newVal);
  }
};

var appName;
$(".iphoneIcon").click(function(){
	appName = $(this).attr("n");
	window.location.href = 'iphoneAppDetails.php?name='+appName;
})

$("#logo").click(function(){
	window.location.href = 'index.php';
})
$("#logo").hover(function(){
	$(this).css('cursor','pointer');
})

	var wireframe = "yes";
	var budget = "Choose Budget";
	$('#selecttext').hide();
	var budgetShow = 0;
	$('#yes').click(function(){
		if($(this).attr("class") == "yes"){
			$(this).removeClass('yes');
			$(this).addClass('no');
			$('#no').removeClass('no');
			$('#no').addClass('yes');
			wireframe = "no";
		}else{
			$(this).removeClass('no');
			$(this).addClass('yes');
			$('#no').removeClass('yes');
			$('#no').addClass('no');
			wireframe = "yes";
		}
	});
	$('#no').click(function(){
		if($(this).attr("class") == "no"){
			$(this).removeClass('no');
			$(this).addClass('yes');
			$('#yes').removeClass('yes');
			$('#yes').addClass('no');
			wireframe = "yes";
		}else{
			$(this).removeClass('yes');
			$(this).addClass('no');
			$('#yes').removeClass('no');
			$('#yes').addClass('yes');
			wireframe = "no";
		}
	});
	$('#selectlabel').click(function(){
		if(budgetShow == 0){
			$('#selecttext').show();
			budgetShow = 1;
		}else{
			$('#selecttext').hide();
			budgetShow = 0;
		}
	});
	$('#selecttext li').click(function(){
		$('#selecttext').hide();
		$text = $(this).text();
		$("#budgettext").html($text);
		budget = $text;
		budgetShow = 0;
	});
	$('#send').click(function(){
		
		var name = $('#name').attr('value');
		
		var email = $('#email').attr('value');
		var des = $('#des').attr('value'); 
		//var file = $("#file").val();
		var formdata = new FormData();
        	var file = $('#file')[0].files[0];
		formdata.append('name',name);
		formdata.append('email',email);
		formdata.append('des',des);
		formdata.append('wireframe',wireframe);
		formdata.append('budget',budget);
		formdata.append('file', file);
		//alert(file);
		$.ajax({
		  url:'send_mail.php',
		  type:'POST',
		  data: formdata,
		 //data: "name="+ name +"& email="+ email + "& des="+ des + "& wireframe="+ wireframe +"& budget="+ budget +"& file="+ file,
		  processData: false,
          contentType: false
		});
		  $(".message").show();
          $(".message").fadeOut(5000);
		moveout(lastScreen);
		lastScreen = "";
		iconcontentname = ""; 
		$("#scrollcontentbackground").fadeOut("slow");
	});

</script>

<script type="text/javascript">
var sc_project=4361351; 
var sc_invisible=1; 
var sc_security="9083fae0"; 
</script>
<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script>
<noscript>
<div class="statcounter"><a title="site stats"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/4361351/0/9083fae0/1/"
alt="site stats"></a></div>
</noscript>

</body>
</html>
